import React, { Component,useState,setState} from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import {config} from '../config';
import validate from 'jquery-validation'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Modal from 'react-bootstrap4-modal';
import moment from 'moment';



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Other_card_detail extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        otherid:this.props.match.params.otherid 
    };

    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/admin_othercard/api/offline/'+sessionStorage.getItem('user_id')+'/'+this.state.otherid)
        if(result.status===200){
            this.setState({data: result.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="/user_card_offline">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 h"> <span className="text-blue">CA</span>RD</h1> 
                            <p >บัตรสมาชิกต่างๆของคุณ</p>
                        </div>
                    </div>
                    <div className="my-card mt-3">
                        <div className="select-card position-relative ">
                            <div id="other-card-detail">
                                <div  className="w-100">
                                    <div className="d-flex bd-highlight pb-1 px-4 pt-4">
                                    {this.state.data.length === 0 ? <></> :
                                    this.state.data.map((row,key) =>
                                    <Form onSubmit={this.handleSubmit} id="formOtherCard" className="w-100">
                                        <Form.Group controlId="formImgFront">
                                            <Form.Label>รูปถ่ายหน้าบัตร</Form.Label>
                                            <div className="row">
                                                <div className="col-12">
                                                    <img src={row.imgfront!==null ? row.imgfront : '/assets/images/ot-card-1.png'} className="w-100 br-10" onClick={this.handleOpencam1}/>
                                                </div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group controlId="formImgBack">
                                            <Form.Label>รูปถ่ายหลังบัตร</Form.Label>
                                            <div className="row">
                                                <div className="col-12">
                                                    <img src={row.imgback!==undefined ? row.imgback : '/assets/images/ot-card-1.png'} className="w-100 br-10" onClick={this.handleOpencam2}/>                                               
                                                </div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group controlId="formCardName">
                                            <Form.Label>ชื่อบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" value={row.othername} name="cardname" readOnly/>
                                        </Form.Group>
                                        <Form.Group controlId="formBarcode">
                                            <Form.Label>หมายเลขบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" value={row.barcode} name="cardnumber" readOnly/>
                                        </Form.Group>
                                        <Form.Group controlId="formExpireDate">
                                            {
                                                row.expiredate==='0000-00-00T00:00:00' ? (
                                                    <Form.Label>ไม่มีวันหมดอายุ</Form.Label>
                                                ) : (
                                                    <>
                                                    <Form.Label>วันหมดอายุ</Form.Label>
                                                    <DatePicker
                                                    name="card_exp_date"
                                                    selected={new Date(row.expiredate)}
                                                    onChange={ this.handleChangeDate }
                                                    dateFormat="dd/MM/yyyy"
                                                    className="form-control"
                                                    disableCalendar={false}
                                                    popperPlacement="bottom-start"
                                                    id="card_exp_date"
                                                    readOnly={true}
                                                    />
                                                    </>
                                                )
                                            }
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>หมายเหตุ</Form.Label>
                                            <Form.Control as="textarea" name="remark" value={row.remark} rows="3"  readOnly/>
                                        </Form.Group>
                                    </Form>
                                    )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                {/* <!-- container --> */}
            </div>
        )
    }
}
export default Other_card_detail;
