import React, { Component,useState,setState} from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import {config} from '../config';
import validate from 'jquery-validation'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Modal from 'react-bootstrap4-modal';



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Other_card_add extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
        
    }

    onTakePhoto1 (dataUri) {
        this.setState({
            camera1: 0,
            sourceCamera1: dataUri
        })
        console.log(dataUri);
    }
    onTakePhoto2 (dataUri) {
        this.setState({
            camera2: 0,
            sourceCamera2: dataUri
        })
        console.log(dataUri);
    }
    
    onCameraError (error) {
        console.error('onCameraError', error);
    }
    
    onCameraStart (stream) {
        console.log('onCameraStart');
    }
    
    onCameraStop () {
        console.log('onCameraStop');
    }

    

    

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        card_exp_date: null,
        startDate: new Date(),
        camera1:0,
        camera2:0,
        sourceCamera1:undefined,
        sourceCamera2:undefined,
        token:getParam('token'),
        cardname : '',
        crop1: {
            unit: '%',
            width: 80,
            aspect: 16 / 10,
        },
        isOpen1: false,
        src1:null,
        crop2: {
            unit: '%',
            width: 80,
            aspect: 16 / 10,
        },
        isOpen2: false,
        src2:null,
        showDateExpire:true,
        remark:''
    };

     

    

    async componentDidMount(){
      

        
    }

   
    handleChangeNoexpire = e => {
        if(e.target.checked){
           this.setState({
               showDateExpire:false,
               startDate:'00/00/0000'
           })
          
        }else{
            this.setState({
                showDateExpire:true,
                startDate:new Date()
            })
        }
    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }

    handleChangeDate = (date) => {
        this.setState({
        startDate: date
        })
    }
    handleOpencam1 = (event) => {
        this.setState({
        camera1: 1
        })

        $('#inputSource1').click();
    }
    handleOpencam2 = (event) => {
        this.setState({
        camera2: 1
        })

        $('#inputSource2').click();
    }

    handleinputSource1 = e => { 
        e.preventDefault();
        let file = e.target.files[0];
        let reader = new FileReader();
        if(file){
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                let trueSize = getSizeFromImgDataURL(reader.result,'MB');
                if(trueSize>=100){
                  
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไฟล์ภาพต้องมีขนาดไม่เกิน 1 MB',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-black',
                        },
                    })
                    
                    
                    this.setState({
                        sourceCamera1: undefined,
                      
                    });
                }else{
                    this.setState({
                        isOpen1:true,
                        sourceCamera1: reader.result,
                        src1: reader.result
                    });
                    $('.custom-modal').css("z-index",9999999);
                    $('.modal-backdrop').css("z-index",99999);

                    console.log(reader.result)
                }

                
            };
        }else{
            this.setState({
                sourceCamera1: undefined,
            });
        }
    }

    /************ CROP **************/

    onImageLoaded1 = image => {
        this.imageRef = image;
      };
    
      onCropComplete1 = crop => {
        this.makeClientCrop1(crop);
      };
    
      onCropChange1 = (crop1, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop1 });
      };
    
      async makeClientCrop1(crop) {
        if (this.imageRef && crop.width && crop.height) {
          const sourceCamera1 = await this.getCroppedImg(
            this.imageRef,
            crop,
            'newFile.png'
          );
          //this.setState({ croppedImageUrl });
          this.setState({ sourceCamera1 });
        }
      }
    
      getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');
    
        ctx.drawImage(
          image,
          crop.x * scaleX,
          crop.y * scaleY,
          crop.width * scaleX,
          crop.height * scaleY,
          0,
          0,
          crop.width,
          crop.height
        );

        const base64Image = canvas.toDataURL('image/jpeg');
        return base64Image;
    
        /*return new Promise((resolve, reject) => {
          canvas.toBlob(blob => {
            if (!blob) {
              //reject(new Error('Canvas is empty'));
              console.error('Canvas is empty');
              return;
            }
            blob.name = fileName;
            window.URL.revokeObjectURL(this.fileUrl);
            this.fileUrl = window.URL.createObjectURL(blob);
            resolve(this.fileUrl);
          }, 'image/jpeg');
        });*/
      }
  


    handleinputSource2 = e => { 
        e.preventDefault();
        let file = e.target.files[0];
        let reader = new FileReader();
        if(file){
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                let trueSize = getSizeFromImgDataURL(reader.result,'MB');
                if(trueSize>=100){
                    /* ต้อง resize */
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไฟล์ภาพต้องมีขนาดไม่เกิน 1 MB',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-black',
                        },
                    })
                    
                    $('#slip_file').val(null)
                    this.setState({
                        sourceCamera2: undefined,
                      
                    });
                }else{
                    this.setState({

                        isOpen2:true,
                        sourceCamera2: reader.result,
                        src2: reader.result
                    });
                    $('.custom-modal').css("z-index",9999999);
                    $('.modal-backdrop').css("z-index",99999);
                }
                
            };
        }else{
            this.setState({
                sourceCamera2: undefined,
            });
        }
    }

    /************ CROP2 **************/

    onImageLoaded2 = image => {
        this.imageRef = image;
      };
    
      onCropComplete2 = crop => {
        this.makeClientCrop2(crop);
      };
    
      onCropChange2 = (crop2, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop2 });
      };
    
      async makeClientCrop2(crop) {
        if (this.imageRef && crop.width && crop.height) {
          const sourceCamera2 = await this.getCroppedImg(
            this.imageRef,
            crop,
            'newFile.png'
          );
          //this.setState({ croppedImageUrl });
          this.setState({ sourceCamera2 });
          //console.log(sourceCamera2)
     
        }
      }
    
      
    

    

    

    handleSubmit = (event) => {
        event.preventDefault();
        $.extend($.validator.messages, {
            required: "กรุณาระบุ",
        });
        $("#formOtherCard").validate({
            rules: {
                cardname: {
                    required: true,
                   
                },
                card_exp_date : {
                    required: true,
                },
                cardnumber : {
                    required: true,
                    number: true,
                   
                }
               
            },errorPlacement: function(error, element) {
                error.insertAfter(element.parent('.form-group'));
                
            },
           
           
        });
        if($("#formOtherCard").valid()){
           
            
            if(this.state.token !== undefined){
                if(this.state.sourceCamera1 && this.state.sourceCamera2){
                    let bodyFormData = new FormData();
                    bodyFormData.append('dataUriFront',this.state.sourceCamera1)
                    bodyFormData.set('token',this.state.token)
                    bodyFormData.set('expdate',this.state.startDate)
                    bodyFormData.set('cardname',this.state.cardname)
                    bodyFormData.set('cardnumber',this.state.cardnumber)
                    bodyFormData.set('remark',this.state.remark)
                    const obj = this;
                    
                    
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/admin_othercard/api/addothercard',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            if(response.status===201){ //บันทึกเสร็จมา update back ต่อ
                                let otherid = response.data.otherid
                                let bodyFormData2 = new FormData();
                                bodyFormData2.append('dataUri',obj.state.sourceCamera2)
                                bodyFormData2.set('otherid',otherid)
                                bodyFormData2.set('face','back')
                                bodyFormData2.set('token',obj.state.token)
                                
                                axios({
                                                            method: 'post',
                                                            url: config.api.base_url+'/admin_othercard/api/saveimageothercard',
                                                            data: bodyFormData2,
                                                            headers: {'Content-Type': 'multipart/form-data' }
                                                            })
                                                            .then(function (response2) {
                                                                if(response2.status===200){ 
                                                                    window.location='line://app/1654185466-nKkG6Xok?referer=user_card_offline'
                                                                }else{
                                                                    Swal.fire({
                                                                        title : 'ขออภัย',
                                                                        html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                                                        icon:'error',
                                                                        confirmButtonText:'ตกลง',
                                                                        customClass: {
                                                                            confirmButton: 'btn-darkblue',
                                                                        },
                                                                    })
                                                                }
                                                                console.log(response2);
                                                            })
                                                            .catch(function (response2) {
                                                                let errorLog = JSON.stringify(response2)
                                                                Swal.fire({
                                                                    title : 'ขออภัย',
                                                                    html: 'ไม่สามารถเชื่อมต่อได้<BR>กรุณาทำรายการใหม่อีกครั้ง(2)',
                                                                    icon:'error',
                                                                    confirmButtonText:'ตกลง',
                                                                    customClass: {
                                                                        confirmButton: 'btn-darkblue',
                                                                    },
                                                                })
                                });


                                
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                            } 
                           
                        })
                        .catch(function (response) {
                            let errorLog = JSON.stringify(response)
                            console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถเชื่อมต่อได้ กรุณาทำรายการใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                    });
                }else{
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'กรุณาถ่ายรูปหน้าบัตรและหลังบัตร',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                }
            
                
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }
        }
      }

   

    
    
    render() {
       
        return (
            <div>
               
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
                <Modal visible={this.state.isOpen1} onClickBackdrop={this.hideModal} className="custom-modal">
                    <div className="modal-header">
                        <h4>CROP รูปภาพหน้าบัตร</h4>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                           
                            <div className="col-12">
                            {this.state.src1 && (
                                <ReactCrop
                                    src={this.state.src1}
                                    crop={this.state.crop1}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded1}
                                    onComplete={this.onCropComplete1}
                                    onChange={this.onCropChange1}
                                />
                            )}
                                                               
                            </div>           
                        </div>
                        
                            
                            
                       
                        
                    </div>
                    <div className="modal-footer">
                        <div className="w-100 text-center">
                            <button className="btn btn-primary" onClick={(e) => this.setState({isOpen1:false})}>ตกลง</button>
                        </div>
                    </div>
                    
                </Modal>
                <Modal visible={this.state.isOpen2} onClickBackdrop={this.hideModal} className="custom-modal">
                    <div className="modal-header">
                        <h4>CROP รูปภาพหลังบัตร</h4>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                           
                            <div className="col-12">
                            {this.state.src2 && (
                                <ReactCrop
                                    src={this.state.src2}
                                    crop={this.state.crop2}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded2}
                                    onComplete={this.onCropComplete2}
                                    onChange={this.onCropChange2}
                                />
                            )}
                                                               
                            </div>           
                        </div>
                        
                            
                            
                       
                        
                    </div>
                    <div className="modal-footer">
                        <div className="w-100 text-center">
                            <button className="btn btn-primary" onClick={(e) => this.setState({isOpen2:false})}>ตกลง</button>
                        </div>
                    </div>
                    
                </Modal>
                
                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="line://app/1654185466-nKkG6Xok?referer=user_card_offline">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 h"> <span className="text-blue">CA</span>RD</h1> 
                            <p >บัตรสมาชิกต่างๆของคุณ</p>
                        </div>

                    </div>
                    <div className="my-card mt-3">
                        <div className="row w-100 m-0 mb-4">
                            
                            <div className="col-12 px-2">
                                <div  className="btn-card text-center shadow not-active">
                                    <h3>OFFLINE CARD</h3>
                                    <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                </div>
                            </div>
                        </div>
                        <div className="select-card position-relative ">
                           
                            <div id="other-card-detail">
                                <div  className="w-100">
                                    <div className="d-flex bd-highlight pb-4 px-4" style={{marginTop:'30px',marginBottom:'100px'}}>
                                    <Form onSubmit={this.handleSubmit} id="formOtherCard" className="w-100">
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหน้าบัตร</Form.Label>
                                            
                                            
                                            <div className="row">
                                                <div className="col-12">
                                                    <img src={this.state.sourceCamera1!==undefined ? this.state.sourceCamera1 : '/assets/images/ot-card-new.png'} className="w-100" onClick={this.handleOpencam1} id="img1"/>
                                                    <input type="file" id="inputSource1" name="inputSource1" accept="image/*" capture onChange={this.handleinputSource1} style={{display:'none'}}/>
                                                </div>
                                                {/* <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}} onClick={this.handleOpencam1}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div> */}
                                            </div>
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหลังบัตร</Form.Label>
                                            <div className="row">
                                                <div className="col-12">
                                                
                                                <img src={this.state.sourceCamera2!==undefined ? this.state.sourceCamera2 : '/assets/images/ot-card-new.png'} className="w-100" onClick={this.handleOpencam2} id="img2"/>
                                                <input type="file" id="inputSource2" name="inputSource2" accept="image/*" capture onChange={this.handleinputSource2} style={{display:'none'}}/>
                                               
                                                </div>
                                                {/* <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}} onClick={this.handleOpencam2}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div> */}
                                            </div>
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>ชื่อบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" name="cardname" onChange={this.handleChange} />
                                        
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>หมายเลขบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" name="cardnumber" onChange={this.handleChange} />
                                        
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail"> 
                                            <Form.Label>วันหมดอายุ</Form.Label>
                                            <div className="custom-control  custom-checkbox mb-1">  
                                                <input type="checkbox" className="custom-control-input" id="nodateexpire" value="1" name="nodateexpire" onChange={(e) => (this.handleChangeNoexpire(e))}/>  
                                                <label className="custom-control-label" htmlFor="nodateexpire">ไม่มีวันหมดอายุ</label>
                                            </div>
                                            {
                                                this.state.showDateExpire && (
                                                    <DatePicker
                                                    name="card_exp_date"
                                                    selected={ this.state.startDate }
                                                    onChange={ this.handleChangeDate }
                                                    dateFormat="dd/MM/yyyy"
                                                    className="form-control bg-white"
                                                    disableCalendar={false}
                                                    popperPlacement="bottom-start"
                                                    id="card_exp_date"
                                                    
                                                    
                                                    />
                                                )
                                            }
                                            
                                          
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>หมายเหตุ</Form.Label>
                                            <Form.Control as="textarea" placeholder="" name="remark" onChange={this.handleChange} rows="3"/>
                                        </Form.Group>
                                       
                                        <div className="w-100 text-center">
                                            <Button variant="primary" type="button" id="submit" onClick={this.handleSubmit}>
                                                บันทึกบัตร
                                            </Button>
                                        </div>
                                    </Form>
                                        
                                        
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                        </div>
                        <div className="app-card">
                            
                        </div>
                    </div>

                    
                   
                </div>  
                {/* <!-- container --> */}

    

                 
            </div>
        )
    }
}
export default Other_card_add;
