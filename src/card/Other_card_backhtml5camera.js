import React, { Component,useState,setState} from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import {config} from '../config';
import {getParam} from '../lib/Helper';
import validate from 'jquery-validation'



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Other_card_add extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
        
    }

    onTakePhoto1 (dataUri) {
        this.setState({
            camera1: 0,
            sourceCamera1: dataUri
        })
        console.log(dataUri);
    }
    onTakePhoto2 (dataUri) {
        this.setState({
            camera2: 0,
            sourceCamera2: dataUri
        })
        console.log(dataUri);
    }
    
    onCameraError (error) {
        console.error('onCameraError', error);
    }
    
    onCameraStart (stream) {
        console.log('onCameraStart');
    }
    
    onCameraStop () {
        console.log('onCameraStop');
    }

    

    

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        card_exp_date: null,
        startDate: new Date(),
        camera1:0,
        camera2:0,
        sourceCamera1:'',
        sourceCamera2:'',
        token:getParam('token'),
        cardname : ''
    };
    

    async componentDidMount(){
      


        
    }

   

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }

    handleChangeDate = (date) => {
        this.setState({
        startDate: date
        })
    }
    handleOpencam1 = (event) => {
        this.setState({
        camera1: 1
        })
    }
    handleOpencam2 = (event) => {
        this.setState({
        camera2: 1
        })
    }

    

    

    

    handleSubmit = (event) => {
        event.preventDefault();
        $.extend($.validator.messages, {
            required: "กรุณาระบุ",
        });
        $("#formOtherCard").validate({
            rules: {
                cardname: {
                    required: true,
                   
                },
                card_exp_date : {
                    required: true,
                },
                cardnumber : {
                    required: true,
                    number: true,
                   
                }
               
            },errorPlacement: function(error, element) {
                error.insertAfter(element.parent('.form-group'));
                
            },
           
           
        });
        if($("#formOtherCard").valid()){
            if(this.state.token !== undefined){
                if(this.state.sourceCamera1 && this.state.sourceCamera2){
                    let bodyFormData = new FormData();
                    bodyFormData.append('dataUriFront',this.state.sourceCamera1)
                    bodyFormData.set('token',this.state.token)
                    bodyFormData.set('expdate',this.state.startDate)
                    bodyFormData.set('cardname',this.state.cardname)
                    bodyFormData.set('cardnumber',this.state.cardnumber)
                    const obj = this;
                    
                    
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/admin_othercard/api/addothercard',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            if(response.status===201){ //บันทึกเสร็จมา update back ต่อ
                                let otherid = response.data.otherid
                                let bodyFormData2 = new FormData();
                                bodyFormData2.append('dataUri',obj.state.sourceCamera2)
                                bodyFormData2.set('otherid',otherid)
                                bodyFormData2.set('face','back')
                                bodyFormData2.set('token',obj.state.token)
                                
                                axios({
                                                            method: 'post',
                                                            url: config.api.base_url+'/admin_othercard/api/saveimageothercard',
                                                            data: bodyFormData2,
                                                            headers: {'Content-Type': 'multipart/form-data' }
                                                            })
                                                            .then(function (response2) {
                                                                if(response2.status===200){ 
                                                                    window.location='line://app/1654185466-nKkG6Xok?referer=user_card'
                                                                }else{
                                                                    Swal.fire({
                                                                        title : 'ขออภัย',
                                                                        html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                                                        icon:'error',
                                                                        confirmButtonText:'ตกลง',
                                                                        customClass: {
                                                                            confirmButton: 'btn-darkblue',
                                                                        },
                                                                    })
                                                                }
                                                                console.log(response2);
                                                            })
                                                            .catch(function (response2) {
                                                                let errorLog = JSON.stringify(response2)
                                                                Swal.fire({
                                                                    title : 'ขออภัย',
                                                                    html: 'ไม่สามารถเชื่อมต่อได้<BR>กรุณาทำรายการใหม่อีกครั้ง(2)',
                                                                    icon:'error',
                                                                    confirmButtonText:'ตกลง',
                                                                    customClass: {
                                                                        confirmButton: 'btn-darkblue',
                                                                    },
                                                                })
                                });


                                
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                            } 
                           
                        })
                        .catch(function (response) {
                            let errorLog = JSON.stringify(response)
                            console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถเชื่อมต่อได้ กรุณาทำรายการใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                    });
                }else{
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'กรุณาถ่ายรูปหน้าบัตรและหลังบัตร',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                }
            
                
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }
        }
      }

   

    
    
    render() {
       
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="line://app/1654185466-nKkG6Xok?referer=user_card">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 "> <span className="text-blue">CA</span>RD</h1> 
                            <p >บัตรสมาชิกต่างๆของคุณ</p>
                        </div>

                    </div>
                    <div className="my-card mt-3">
                        <div className="row w-100 m-0">
                            
                            <div className="col-12 px-2">
                                <div  className="btn-card text-center shadow not-active">
                                    <h3>OTHER CARD</h3>
                                    <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                </div>
                            </div>
                        </div>
                        <div className="select-card position-relative ">
                           
                            <div id="other-card-detail">
                                <div  className="w-100">
                                    <div className="d-flex bd-highlight py-4 px-4">
                                    <Form onSubmit={this.handleSubmit} id="formOtherCard">
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>ชื่อบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" name="cardname" onChange={this.handleChange} />
                                        
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>หมายเลขบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" name="cardnumber" onChange={this.handleChange} />
                                        
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>วันหมดอายุ</Form.Label>
                                            <DatePicker
                                            name="card_exp_date"
                                            selected={ this.state.startDate }
                                            onChange={ this.handleChangeDate }
                                            dateFormat="dd/MM/yyyy"
                                            className="form-control"
                                            
                                            
                                            />
                                          
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหน้าบัตร</Form.Label>
                                            
                                            
                                            <div className="row">
                                                <div className="col-9">
                                                    
                                            {this.state.camera1===0 ?(
                                                <img src={this.state.sourceCamera1!=='' ? this.state.sourceCamera1 : '/assets/images/ot-card-1.png'} className="w-100"/>
                                            ):(
                                                <Camera
                                                onTakePhoto = { (dataUri) => { this.onTakePhoto1(dataUri); } }
                                                onCameraError = { (error) => { this.onCameraError(error); } }
                                                idealFacingMode = {FACING_MODES.ENVIRONMENT}
                                                imageType = {IMAGE_TYPES.PNG}
                                                imageCompression = {0.97}
                                                isMaxResolution = {false}
                                                isImageMirror = {false}
                                                isSilentMode = {true}
                                                isDisplayStartCameraError = {true}
                                                isFullscreen = {false}
                                                sizeFactor = {1}
                                                onCameraStart = { (stream) => { this.onCameraStart(stream); } }
                                                onCameraStop = { () => { this.onCameraStop(); } }
                                                />
                                            )}

                                            
                                                    
                                                </div>
                                                <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}} onClick={this.handleOpencam1}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหลังบัตร</Form.Label>
                                            <div className="row">
                                                <div className="col-9">
                                                {this.state.camera2===0 ?(
                                                    <img src={this.state.sourceCamera2!=='' ? this.state.sourceCamera2 : '/assets/images/ot-card-1.png'} className="w-100"/>
                                                ):(
                                                    <Camera
                                                    onTakePhoto = { (dataUri) => { this.onTakePhoto2(dataUri); } }
                                                    onCameraError = { (error) => { this.onCameraError(error); } }
                                                    idealFacingMode = {FACING_MODES.ENVIRONMENT}
                
                                                    imageType = {IMAGE_TYPES.PNG}
                                                    imageCompression = {0.97}
                                                    isMaxResolution = {false}
                                                    isImageMirror = {false}
                                                    isSilentMode = {true}
                                                    isDisplayStartCameraError = {true}
                                                    isFullscreen = {false}
                                                    sizeFactor = {1}
                                                    onCameraStart = { (stream) => { this.onCameraStart(stream); } }
                                                    onCameraStop = { () => { this.onCameraStop(); } }
                                                    />
                                                )}
                                                </div>
                                                <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}} onClick={this.handleOpencam2}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </Form.Group>
                                       
                                        <div className="w-100 text-center">
                                            <Button variant="primary" type="button" id="submit" onClick={this.handleSubmit}>
                                                บันทึกบัตร
                                            </Button>
                                        </div>
                                    </Form>
                                        
                                        
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                        </div>
                        <div className="app-card">
                            
                        </div>
                    </div>

                    
                   
                </div>  
                {/* <!-- container --> */}

    

                 
            </div>
        )
    }
}
export default Other_card_add;
