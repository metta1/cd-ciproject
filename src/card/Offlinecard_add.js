import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import { config } from "../config";
import BarcodeReader from 'react-barcode-reader'



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();



class Offlinecard_add extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.changeThemeColor = this.changeThemeColor.bind(this);
        this.handleScan = this.handleScan.bind(this)
       
      
        
    }

    state = {
        data : [],
        offlineCardData: [],
        
        colors: [],
        backgroundColor:'',
        offlineCardURL : localStorage.getItem('offlineCardURL'),
        offlineCardName : localStorage.getItem('offlineCardName'),
        result: 'No result',
    };

    changeThemeColor = (colors) => {
        this.setState({
            backgroundColor : colors[1],
            
        })
        
    }

    handleScan(data){
        this.setState({
          result: data,
        })
    }
      handleError(err){
        console.error(err)
    }

    


    async componentDidMount(){
      
        
    }

   

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }

    scanCode = (e) => {
        window.location="/offlinecard_takescancode"
    }




  
    
    handleSubmit = event => {
    
        
    }

    
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
              


                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        
                        <div className="icon-back">
                            <a href="/user_card">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <p></p>
                            <h3 className="align-title m-0">
                            {this.state.offlineCardName}
                            </h3>     
                        </div>

                    </div>
                    <div className="container-content" >
                        
                        <div className="p-4 wrap-icon-takephoto">
                            <div className="bg-white" onClick={(e) => this.scanCode(e)}>
                                <img className="w-100 p-4" src="/assets/images/icon-upload.svg"  /> 
                            </div>
                           
                            
                        </div>

                    </div>
         
                </div>  
                {/* <!-- container --> */}

    

                 
            </div>
        )
    }
}
export default Offlinecard_add;
