import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import { config } from "../config";
import { ColorExtractor } from 'react-color-extractor'
import {Navigation} from '../template'
import {getParam, time} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();



class App_card extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleFilterOnlineCard = this.handleFilterOnlineCard.bind(this);
       
      
        
    }

    state = {
        data : [],
        offlineCardData: [],
        cat_id:this.props.match.params.cat_id,
        searchOnlineCard: "",
        colors: [],
        user_id : sessionStorage.getItem('user_id'),
    };


    async componentDidMount(){
        //click menu card
        $('#other-card-detail').hide();
        $("#app-card").click(function(){
            $("#app-card").removeClass("not-active");
            $('#other-card').addClass("not-active");

            $('#app-card-detail').show();
            $('#other-card-detail').hide();
        });
        $('#other-card').click(function(){
            $("#other-card").removeClass("not-active");
            $('#app-card').addClass("not-active");

            $('#other-card-detail').show();
            $('#app-card-detail').hide();

        });



        let user_id = sessionStorage.getItem('user_id');
        
        const result = await axios.get(config.api.base_url+'/api/card/'+user_id+'/add')
        if(result.status===200){
            this.setState({data: result.data});
        }

        const resultOffinecard = await axios.get(config.api.base_url+'/admin_othercard/api/othercard/'+user_id)
        if(resultOffinecard.status===200){
            this.setState({offlineCardData: resultOffinecard.data});
        }
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.offlineCardData)

        $('.btnAddCard').click(function(){
            Swal.fire({
                title: 'ต้องการเพิ่มบัตรใช่หรือไม่',
                showCloseButton: true,
                showCancelButton: true,
                reverseButtons :true,
                confirmButtonText:
                  'ตกลง',
                focusConfirm: false,
                cancelButtonText:
                  'ยกเลิก',
              }).then((result) => {
                if (result.value) {
                    let user_id = sessionStorage.getItem('user_id');
                    let store_id = $(this).data('store_id');
                    let user = {
                        userid : user_id,
                        storeid : store_id
                    }
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('storeid',store_id)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/api/card',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            $('.member-card[data-store_id="'+store_id+'"').hide();
                        })
                        .catch(function (response) {
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง'
                            })
                            
                            //handle error
                            //console.log(response);
                        });
                }
            })
        })
     
        
    }

    async handleFilterOnlineCard(e){
        let keyWord = this.state.searchOnlineCard;
        let user_id = sessionStorage.getItem('user_id');
        const result = await axios.get(config.api.base_url+'/store_card/api/card/'+user_id+'/add?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }

    handleSearchInput = event => {
        this.setState({searchOnlineCard: event.target.value});
    }




    clickOffLineCard = (e,offlinecardid,offlinecardName) => {
        //alert(offlinecardid)
        $('.wrap-offlinecard').data('offlinecardid');
        let linkImage =  $('.wrap-offlinecard[data-offlinecardid="'+offlinecardid+'"] img').attr('src');
        localStorage.setItem('offlineCardURL',linkImage);
        localStorage.setItem('offlineCardID',offlinecardid);
        localStorage.setItem('offlineCardName',offlinecardName);
        window.location='/offlinecard_add/'+offlinecardid;
    
    }


    otherCardAdd = e => {
        e.preventDefault();
        let URL = config.base_url+'/other_card?openExternalBrowser=1&token='+btoa(this.state.user_id+','+time())
        window.open(URL, '_blank'); 
        
    }



    
    handleSubmit = event => {
    
        
    }

    
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
              




                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <Link to="/user_card">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </Link>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 h"> <span className="text-blue">CA</span >RD</h1> 
                            <img className="hologo" src="/assets/images/icon-ho-new.png" alt=""/>
                            <p >บัตรสมาชิกต่างๆของคุณ</p>
                        </div>

                    </div>
                    <div className="my-card mt-3">
                        <div className="row w-100 m-0">
                            <div className="col-6 px-2">
                                <div id="app-card"  className="btn-card text-center shadow ">
                                    <h3>ONLINE CARD</h3>
                                    <h5>ร้านประจำของคุณ</h5>
                                </div>
                                     

                            </div> 
                            {/* <div className="col-6 px-2">
                                <div id="other-card" className="btn-card text-center shadow not-active">
                                    <h3>OFFLINE CARD</h3>
                                    <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                </div>
                            </div> */}
                        </div>
                        <div className="select-card position-relative ">
                            <div className="px-4 py-3">
                                <div className="position-relative ">
                                    <input type="text" className="search w-100 rounded form-control shadow" placeholder="Search your card" onKeyUp={this.handleSearchInput}/>
                                    <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" onClick={this.handleFilterOnlineCard}/>
                                </div>
                            </div>
                            
                            <div id="app-card-detail">

                    {


                        this.state.data.map(row => {
                                return (
                                <div id="" className="app-card-item w-100 member-card" data-store_id={row.storeid}>
                                    <div className=" bd-highlight py-2 px-4">
                                        <h3 className="name-card mb-0">{row.storename}</h3>
                                        <h4 className="card-desc mb-0">{row.descriptionshort}</h4>
                                    </div>
                                    <div className="d-flex bd-highlight py-2 px-4">
                                        <div className="pl-3 bd-highlight align-self-center w-25">
                                        
                                        </div>
                                        <div className="bd-highlight w-50">
                                            <img className="w-100" src={row.card_img==='' ? '/assets/images/card_default.png' : row.card_img} alt={row.storename}/>
                                        </div>
                                        
                                        <div className="ml-auto  bd-highlight align-self-center w-10 pb-2">
                                            <div  data-store_id={row.storeid} className="card-add shadow btnAddCard">
                                                    <img  src="/assets/images/add.svg" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                                )
                        })
                    }
                                
                            </div>
                            <div id="other-card-detail">
                                <div id="" className="app-card-item w-100 member-card" >
                                    <div className=" bd-highlight py-2 px-4">
                                        <h3 className="name-card mb-0">Add New Card</h3>
                                        <h4 className="card-desc mb-0"></h4>
                                    </div>
                                    <div className="d-flex bd-highlight py-2 px-4">
                                        <div className="pl-3 bd-highlight align-self-center w-25">
                                        
                                        </div>
                                        <div className="bd-highlight w-50">
                                            <img className="w-100" src="/assets/images/ot-card-1.png" alt=""/>
                                        </div>
                                        
                                        <div className="ml-auto  bd-highlight align-self-center w-10 pb-2">
                                            <a href={config.base_url+'/other_card?openExternalBrowser=1&token='+btoa(this.state.user_id+','+time())} target="_BLANK">
                                                <div  className="card-add shadow">
                                                    <img  src="/assets/images/add.svg" alt=""/>
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                              
                    {
                        this.state.offlineCardData === undefined ? (
                            <div/>
                        ) : (

                        
                            this.state.offlineCardData.map((row,key) => {
                                    return (
                                    <div id="" className="app-card-item w-100 member-card" >
                                        <div className=" bd-highlight py-2 px-4">
                                            <h3 className="name-card mb-0">{row.offlinecardname}</h3>
                                            <h4 className="card-desc mb-0">{row.descriptionshort}</h4>
                                        </div>
                                        <div className="d-flex bd-highlight py-2 px-4">
                                            <div className="pl-3 bd-highlight align-self-center w-25">
                                            
                                            </div>
                                            <div className="bd-highlight w-50 wrap-offlinecard" data-offlinecardid={row.offlinecardid}>

                                                <img className="w-100" src={row.offlinecardimg==='' ? '/assets/images/card_default.png' : row.offlinecardimg} alt={row.offlinecardimg}/>
                                            
                                            </div>
                                            
                                            <div className="ml-auto  bd-highlight align-self-center w-10 pb-2">
                                                <div  data-store_id={row.offlinecardid} className="card-add shadow" onClick={(e) => this.clickOffLineCard(e,row.offlinecardid,row.offlinecardname)}>
                                                        <img  src="/assets/images/add.svg" alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                    </div>
                                    )
                            })
                        )
                    }
                                
                                
                            </div>
                        </div>
                        <div className="app-card">
                            
                        </div>
                    </div>
                    <Navigation/>
                    
                </div>  
                {/* <!-- container --> */}

    
                
                 
            </div>
        )
    }
}
export default App_card;
