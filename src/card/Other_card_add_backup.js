import React, { Component,useState,setState} from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';

import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Other_card_add extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
       
      
        
    }

    onTakePhoto1 (dataUri) {
        this.setState({
            camera1: 0,
            sourceCamera1: dataUri
        })
        console.log(dataUri);
    }
    
    onCameraError (error) {
        console.error('onCameraError', error);
    }
    
    onCameraStart (stream) {
        console.log('onCameraStart');
    }
    
    onCameraStop () {
        console.log('onCameraStop');
    }

    

    

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        card_exp_date: null,
        startDate: new Date(),
        camera1:0,
        camera2:0,
        sourceCamera1:''
    };
    

    async componentDidMount(){
      


        
    }

   

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }

    handleChangeDate = (date) => {
        this.setState({
        startDate: date
        })
    }
    handleOpencam1 = (event) => {
        this.setState({
        camera1: 1
        })
    }
   
    
    handleSubmit = event => {
    
        
    }

    
    
    render() {
       
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <Link to="/app_card">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </Link>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 "> <span className="text-blue">CA</span>RD</h1> 
                            <p >บัตรสมาชิกต่างๆของคุณ</p>
                        </div>

                    </div>
                    <div className="my-card mt-3">
                        <div className="row w-100 m-0">
                            
                            <div className="col-12 px-2">
                                <div  className="btn-card text-center shadow not-active">
                                    <h3>OTHER CARD</h3>
                                    <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                </div>
                            </div>
                        </div>
                        <div className="select-card position-relative ">
                           
                            <div id="other-card-detail">
                                <div  className="w-100">
                                    <div className="d-flex bd-highlight py-4 px-4">
                                    <Form onSubmit={this.handleSubmit} id="formOtherCard">
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>ชื่อบัตร</Form.Label>
                                            <Form.Control type="text" placeholder="" name="name" onChange={this.handleChange} required/>
                                        
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>วันหมดอายุ</Form.Label>
                                            <DatePicker
                                            name="card_exp_date"
                                            selected={ this.state.startDate }
                                            onChange={ this.handleChangeDate }
                                            dateFormat="dd/MM/yyyy"
                                            className="form-control"
                                            
                                            
                                            />
                                          
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหน้าบัตร</Form.Label>
                                            
                                            
                                            <div className="row">
                                                <div className="col-9">
                                                    
                                            {this.state.camera1===0 ?(
                                                <img src={this.state.sourceCamera1!=='' ? this.state.sourceCamera1 : '/assets/images/ot-card-1.png'} className="w-100"/>
                                            ):(
                                                <Camera
                                                onTakePhoto = { (dataUri) => { this.onTakePhoto1(dataUri); } }
                                                onCameraError = { (error) => { this.onCameraError(error); } }
                                                idealFacingMode = {FACING_MODES.ENVIRONMENT}
            
                                                imageType = {IMAGE_TYPES.PNG}
                                                imageCompression = {0.97}
                                                isMaxResolution = {false}
                                                isImageMirror = {false}
                                                isSilentMode = {true}
                                                isDisplayStartCameraError = {true}
                                                isFullscreen = {false}
                                                sizeFactor = {1}
                                                onCameraStart = { (stream) => { this.onCameraStart(stream); } }
                                                onCameraStop = { () => { this.onCameraStop(); } }
                                                />
                                            )}
                                                    
                                                </div>
                                                <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}} onClick={this.handleOpencam1}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>รูปถ่ายหลังบัตร</Form.Label>
                                            <div className="row">
                                                <div className="col-9">
                                                    <img src="/assets/images/ot-card-1.png" className="w-100"/>
                                                </div>
                                                <div className="col-3">
                                                    <div  className="card-add shadow" style={{width:"50px",height:"50px",paddingTop:"0px"}}>
                                                            <img  src="/assets/images/icon-upload.svg" alt="" className="w-100"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </Form.Group>
                                       
                                        <div className="w-100 text-center">
                                            <Button variant="primary" type="submit" id="submit">
                                                บันทึกบัตร
                                            </Button>
                                        </div>
                                    </Form>
                                        
                                        
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                        </div>
                        <div className="app-card">
                            
                        </div>
                    </div>

                    
                    <div className="footer mx-auto">
                        <img className="w-100 position-relative" src="/assets/images/footer_card.svg" alt=""/>
                                <div className="menu position-absolute w-100">
                                    <div className="menu-item px-2 pt-2 ">
                                        <div className="ml-2 menu-item-active bg-white rounded-circle shadow">
                                            <div className="menu-item-active-ab">
                                                <a href="/user_card">
                                                    <img className="" src="/assets/images/icon-credit.svg" alt=""/> <br/>
                                                    <span>CARD</span>
                                                </a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                
                                        <a href="">
                                            <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                            <span>Feed</span>
                                        </a>
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a href="">
                                            <img className="" src="/assets/images/icon-home.svg" alt=""/><br/>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                                    <div className="menu-item px-2 ">
                                        <a href="/promotion_category">
                                            <img className="" src="/assets/images/icon-gift.svg" alt=""/> <br/>
                                            <span>PROMOTION</span>
                                        </a>
                    
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a href="/account">
                                            <img className="position-relative" src="/assets/images/icon-user.svg" alt=""/> <br/>
                                            <span>ACCOUNT</span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                </div>  
                {/* <!-- container --> */}

    

                 
            </div>
        )
    }
}
export default Other_card_add;
