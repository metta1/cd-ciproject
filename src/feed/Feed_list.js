import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation, Notification_num} from '../template'
import ContentLoader,{Instagram} from 'react-content-loader'
import withImageLoader from 'react-image-loader-hoc';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const Image = props => (<img alt="" {...props} />);
const ImageWithLoader = withImageLoader(Image);
const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


const feedLoader = (
    <>
 
    <div className="card feed-card w-100 shadow border-0 mb-3 container-feed">
        <div className="feed-head px-3 py-2 d-flex w-100">
            <div className="profile w-100" style={{height:'500px'}}>
                               
                <Instagram/>
                                
            </div>
                           
        </div>
                      
                    
    </div>
    
 
    </>
)


class Feed_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleFilterFeed = this.handleFilterFeed.bind(this);
    }

    state = {
        data : [],
        activeSearchBox:false
    };


    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/store_feed/api/feed').catch(function (error) {
            if (error.response) {
              //console.log(error.response.status);
                this.setState({
                    feedLoaded : true
                })
            }
        });
        if(result.status===200){
            console.log(result.data)
            this.setState({
                data: result.data,
                feedLoaded :true
            });
            //console.log(result.data)
        }else{
            this.setState({
                feedLoaded : true
            })
        }
        
      
     
        
    }

    async handleFilterFeed(e){
        this.setState({
            feedLoaded : false
        })
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store_feed/api/feed?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
        this.setState({
            feedLoaded : true
        })
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-feed').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-feed').css({"padding-top": 'unset'})

        this.componentDidMount();
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-yellow').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    
    render() {

        return (
            <div className="container-mobile m-auto">
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                    
                    <div className="modal-body">
                        <div className="form-group">
                            <label>ค้นหาข่าวสาร</label>
                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterFeed}/>
                        </div>
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                    
                </Modal>
               
               {/* <Header/> */}
               <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
        
                <div className="head bg-yellow shadow">
                {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back" style={{left: '1rem'}}>
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt="" style={{padding: '2vw'}}/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterFeed}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                        <div className="col-8 h-100 ">
                            <div className=" h-100 p-3 ml-20" style={{whiteSpace: 'nowrap'}}>
                                <h1 className="m-0 h"> 
                                    <span className="text-blue">F</span>EE<span className="text-blue">D</span>
                                    <span><img className="img-logoho pr-0" src="/assets/images/icon-ho-new.png"/></span>
                                </h1> 
                                <p >ดูข่าวสารและการแจ้งเตือนได้เลย</p>
                            </div>
                        </div>
                        <div className="col-4 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col p-2 my-auto">
                                    <div className="icon icon-top-right rounded-circle">
                                        <img className="p-3" src="/assets/images/icon-search@2x.png" alt="" onClick={this.openSearchBox}/>
                                    </div>
                                </div>
                                {/* <div className="col p-1 my-auto" style={{marginRight: '-0.4rem'}}>
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div> */}
                                <div className="col p-2 my-auto">
                                    <div className="icon icon-top-right rounded-circle">
                                        <a href="/notifications">
                                            <img className="p-3" src="/assets/images/icon-noti@2x.png" alt=""/>
                                            <Notification_num/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    )
                }
                </div>
                
                <div className="feed p-3 " style={{marginBottom:'70px'}}>
                    {
                        (!this.state.feedLoaded) &&(
                            feedLoader
                        )
                    }
            {
            this.state.data.length === 0 ? <div></div> :
          
               this.state.data.map((row,key) => (
                    <div className="card feed-card w-100 shadow border-0 mb-3 container-feed" key={key}>
                        <div className="feed-head px-3 py-2 d-flex w-100">
                            <div className="profile">
                                <div className="profile-ratio w-100 rounded-circle">
                                {
                                    <LazyLoadImage
                                        effect="blur"
                                        src={row.storelogo==='' ? '/assets/images/profile.svg' : row.storelogo}
                                        alt={'feed'}
                                        class={'w-100'}
                                        
                                    />
                                }
                                  
                                </div>
                            </div>
                            <div className="pl-2 w-100">
                                <h4 className="m-0">{row.storeid==='0' ? 'HO' : row.storename}</h4>
                                <small><ins>{row.publicdate}</ins></small>
                            </div>
                        </div>
                        <a className="" href={'/feed_detail/'+row.feedid}  style={{minHeight:'200px'}}>
                            <p className="px-3 mb-1">{row.feedname}</p>
                            {
                                <LazyLoadImage
                                    effect="blur"
                                    src={row.feed_img==='' ? '/assets/images/profile.svg' : row.feed_img}
                                    alt={'feed'}
                                    class={'w-100 feed-img'}
                                />
                            }
                            
                            {/* <img className="w-100 feed-img" src={row.feed_img}  alt=""/> */}
                        </a>
                        {/* <div className="feed-footer p-3  w-100">
                            <div className="d-flex">
                                <div className="ml-1 icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-heart.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                                <p className="mt-1 ml-2 mb-0">15</p>
                                <div className="ml-3 icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-comment.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                                <p className="mt-1 ml-2 mb-0">77 Comment</p>
                                <div className="ml-auto icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-share.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div> */}
                    </div>
                ))
            }
                    
                </div>
                    
                <Navigation active={'feed'}/>

                
               
                 
            </div>
        )
    }
}
export default Feed_list;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;