import React, { Component } from 'react'
import {Notification_num} from '../template'

class Header extends Component {
    render() {
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
        
        <div className="head bg-yellow shadow">
            <div className="row w-100 h-100">
                <div className="col-8 h-100">
                    <div className=" h-100 p-4" style={{whiteSpace: 'nowrap'}}>
                        <a href="/feed">
                            <div className="icon-back p-2" style={{width: 'auto', marginLeft: '-1.3rem'}}>
                                <div className=" icon-promo-back ">
                                    <img src="/assets/images/icons/icon-back@2x.png" alt=""/>
                                </div>
                            </div>
                        </a>
                        <h1 className="m-0 ml-4 h"> 
                            <span>FEED</span>
                            {/* <span><img className="img-logoho pr-0" src="/assets/images/icon-ho-new.png"/></span> */}
                        </h1> 
                        {/* <p >ดูข่าวสารและการแจ้งเตือนได้เลย</p> */}
                    </div>
                </div>
                <div className="col-4 h-100">
                    <div className="row float-right h-100 w-100">
                        <div className="col p-2 my-auto">
                            {/* <div className="icon icon-top-right rounded-circle">
                                <a href="#"><img className="w-100" src="/assets/images/icon-search@2x.png" alt="" onClick={this.openModal}/></a>
                            </div> */}
                        </div>
                        <div className="col p-2 my-auto">
                            <div className="icon icon-top-right rounded-circle">
                                <a href="/notifications"><img src="/assets/images/icon-noti@2x.png" alt="" className="img-noti"/></a>
                                <Notification_num/>
                            </div>
                        </div>
                    </div>
                                
                </div>
            </div>

        </div>
        </>)
    
        

    }
}
export default Header;
