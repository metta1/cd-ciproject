import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation_shop as Navigation, Notification_num} from '../../template'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Feed_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        localStorage.setItem('storageStoreID',this.props.match.params.store_id);
        this.handleFilterFeed = this.handleFilterFeed.bind(this);
    }

    state = {
        data : [],
        store_id : localStorage.getItem('storageStoreID'),
        userid:sessionStorage.getItem('user_id'),
        activeSearchBox:false,
        numNoti:0,
        colorData : [],
    };


    async componentDidMount(){
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }

        console.log(this.state.store_id)
        const result = await axios.get(config.api.base_url+'/store_feed/api/feedstore/'+this.state.store_id).catch(function (error) {
           
            if (error.response) {
              //console.log(error.response.status);
            }
        });
        if(result.status===200){
            // console.log(result.data)
            this.setState({data: result.data});
            //console.log(result.data)
        }
        
        //Noti
        const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                this.setState({
                    numNoti:numNoti
                })
            }
        } 
        const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.store_id);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })
           
        }
        
        
    }

    async handleFilterFeed(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store_feed/api/feedstore/'+this.state.store_id+'?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-feed').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-feed').css({"padding-top": 'unset'})

        this.componentDidMount();
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }
    
    render() {
        const textColor = {
            color: '#' + this.state.colorData['colorfont']
        }
        return (
            <div className="container-mobile m-auto">
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                    <input type="hidden" id="store_id" name="store_id" value={this.state.store_id}/>
                    <div className="modal-body">
                        <div className="form-group">
                            <label>ค้นหาข่าวสาร</label>
                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterFeed}/>
                        </div>
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                    
                </Modal>
               
                {/* <Header/> */}
                <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                
                <div className="head bg-black shadow">
                {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back" style={{left: '1rem'}}>
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterFeed}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                        <div className="col-8 h-100">
                            <div className=" h-100 p-3 pl-5" style={{whiteSpace: 'nowrap'}}>
                                <h1 className="m-0 h" style={textColor}>FEED</h1> 
                                <p className="" style={textColor}>ดูข่าวสารและการแจ้งเตือนได้เลย</p>
                            </div>
                        </div>
                        <div className="col-4 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col p-2 my-auto">
                                    <div className="icon rounded-circle ml-2">
                                        <img className="w-100" src="/assets/images/icon-search.svg" alt="" onClick={this.openSearchBox}/>
                                    </div>
                                </div>
                                {/* <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div> */}
                                <div className="col p-2 my-auto">
                                    <div className="icon rounded-circle">
                                        <a href="/notifications">
                                            <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                            {
                                                this.state.numNoti>0 && (
                                                    <div className="alert alert-primary noti-num-border big-alert" role="alert">
                                                        {this.state.numNoti}
                                                    </div>
                                                )
                                            }
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    )
                }
                </div>
                
                <div className="feed p-3 mb-5">
            {
                this.state.data.length === 0 ? <div></div> :
          
                this.state.data.map((row,key) => (
                    <div className="card feed-card w-100 shadow border-0 mb-3 container-feed" key={key}>
                        <div className="feed-head px-3 py-2 d-flex w-100">
                            <div className="profile">
                                <div className="profile-ratio w-100 rounded-circle">
                                   <img src={row.storelogo==='' ? '/assets/images/profile.svg' : row.storelogo} className="w-100" />
                                </div>
                            </div>
                            <div className="pl-2 w-100">
                                <h4 className="m-0">{row.storeid===null ? 'HO' : row.storename}</h4>
                                <small><ins>{row.publicdate}</ins></small>
                            </div>
                        </div>
                        <a className="" href={'/store_feed_detail/'+row.feedid}>
                            <p className="px-3 mb-1">{row.feedname}</p>
                            <img className="w-100 feed-img" src={row.feed_img}  alt=""/>
                        </a>
                        {/* <div className="feed-footer p-3  w-100">
                            <div className="d-flex">
                                <div className="icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-comment.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>

                                <p className="mt-1 ml-3 mb-0">77 Comment</p>
                                <div className="ml-auto icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-heart.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                                <div className="ml-2 icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-share.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div> */}
                    </div>
                ))
            }
                    
                </div>
                <br/><br/><br/>
                    
                <Navigation active={'feed'}/>

                
               
                 
            </div>
        )
    }
}
export default Feed_list;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;