import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import $ from 'jquery'
import {Notification_num} from '../../template'

class Header extends Component {

    state = {
        data : [],
        store_id : localStorage.getItem('storageStoreID'),
        userid:sessionStorage.getItem('user_id'),
        activeSearchBox:false,
        numNoti:0,
        colorData : [],
    };
    async componentDidMount(){
        //Noti
        const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                this.setState({
                    numNoti:numNoti
                })
            }
        } 
    
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
        if(this.state.colorData !== undefined){
            $('.noti-num-border').css('background-color','#'+this.state.colorData['colortabbar'])
            $('.noti-num-border').css("color", "#"+this.state.colorData['colorfont']);
            $('.noti-num-border').css('border-color','#'+this.state.colorData['coloricon'])
            let coloricon   = this.state.colorData['coloricon'];
        }
    }
    render() {
        const textColor = {
            color: '#' + this.state.colorData['colorfont']
        }
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
        
        <div className="head bg-black shadow">
            <div className="row w-100 h-100">
                <div className="col-8 h-100">
                    <div className=" h-100 p-4 pl-5" style={{whiteSpace: 'nowrap'}}>
                        <a href="/store_feed">
                            <div className="icon-back p-2" style={{width: 'auto', marginTop: '-0.2rem'}}>
                                <div className=" icon-promo-back ">
                                    <img src="/assets/images/icons/icon-back-v2.png" alt=""/>
                                </div>
                            </div>
                        </a>
                        <h1 className="m-0 ml-2 h" style={textColor}>FEED</h1> 
                        {/* <p className="text-red">ดูข่าวสารและการแจ้งเตือนได้เลย</p> */}
                    </div>
                </div>
                <div className="col-4 h-100">
                    <div className="row float-right h-100 w-100">
                        {/* <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                            </div>
                        </div> */}
                        <div className="col-6 p-1 my-auto ml-5">
                            <div className="icon rounded-circle">
                                <a href="/notifications">
                                    <img className="" src="/assets/images/icon-noti@2x.png" style={{padding:'0.1vw'}} alt=""/>
                                    {
                                        this.state.numNoti>0 && (
                                            <div className="alert alert-primary noti-num-border" role="alert">
                                                {this.state.numNoti}
                                            </div>
                                        )
                                    }
                                </a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </>)
    
        

    }
}
export default Header;
