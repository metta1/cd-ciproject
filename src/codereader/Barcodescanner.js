import React, { Component } from 'react'
import BarcodeReader from 'react-barcode-reader'
import {validURL} from '../lib/Helper';
import './hoqrcodescanner.css';
import $ from 'jquery'
export default class Barcodescanner extends Component {
    constructor(props){
      super(props)
  
      this.handleScan = this.handleScan.bind(this)
    }
    
    state = {
        result: 'No result',
        base_linkback : 'line://app/1654185466-nKkG6Xok'
    }

    async componentDidMount(){
      $("body" ).css( "background-color","#f9ce43" );
      $(".scanner-container>section>section>div" ).css( "box-shadow", "rgba(249, 206, 67, 0.46) 0px 0px 0px 5px inset" );
    }
     
    handleScan = data => {
        if (data) {      
          this.setState({
            result: data
          })
        }
      }
    handleError = err => {
        console.error(err)
    }
    render() {
        return (
          <>
          <div className="text-center mt-2 mb-2">
            <img className="qrcode head-logo" src="/assets/images/icon-ho-new.png" alt="" />
          </div>
          
          <div className="qrcode badge-dangerscanner-container">
            <BarcodeReader
            onError={this.handleError}
            onScan={this.handleScan}
            />
            {/* <QrReader
              delay={300}
              onError={this.handleError}
              onScan={this.handleScan}
              style={{ width: '100%' }}
              facingMode={'environment'}
              showViewFinder={true}
            /> */}
              <p>{this.state.result}</p>
          </div>
          <div className="text-center mt-3">
            <a href={this.state.base_linkback} class="qrcode btn btn-blue">กลับไปที่ HO</a>
          </div>
          
          </>
        )
    }
}
