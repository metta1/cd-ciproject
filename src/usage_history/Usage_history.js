import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios'
import {config} from '../config';
import DatePicker, { registerLocale } from 'react-datepicker';
import moment from 'moment';
import th from "date-fns/locale/th"; // the locale you want
registerLocale("th", th); // register it with the name you want

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Usage_history extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        data : [],
        storeid : localStorage.getItem('storageStoreID'),
        userid : sessionStorage.getItem('user_id'),
        startDate: new Date()
    };

    async componentDidMount(){
        
        const usageHistoryRs = await axios.get(config.api.base_url+'/users/api/storehistory/null/'+this.state.userid)
        if(usageHistoryRs.status===200){
            console.log(usageHistoryRs.data)
            this.setState({data: usageHistoryRs.data});
        }
    }

    async handleChange(date) {
        
        let get_month = date.getMonth()+1;
        let get_year = date.getFullYear();
        if (get_month < 10) {
            get_month = "0"+get_month;
        }
        this.setState({ startDate: date });
        const usageHistoryRs = await axios.get(config.api.base_url+'/users/api/storehistory/null/'+this.state.userid+'/'+get_month+'/'+get_year)
        if(usageHistoryRs.status===200){
            this.setState({data: usageHistoryRs.data});
        } else {
            this.setState({data: []});
        }
    };

    openDatepicker = () => this._calendar.setOpen(true);

    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/usage_history.css"/>
                <div className="container-mobile m-auto">
                    <div className="head bg-yellow shadow position-relative row w-100" style={{marginLeft: '0'}}>
                        <div className="col-8">
                            <div className="icon-back" style={{width: '10%'}}>
                                <a href="/account">
                                    <div className="icon rounded-circle shadow">
                                        <img src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className=" h-100 p-3 ml-3">
                                <h3 className="align-title m-0">
                                    ประวัติการใช้งาน
                                </h3> 
                            </div>
                        </div>
                        <div className="h-100 px-3 pb-3 pt-2 col-4 mt-3 ">
                            <div className="ml-4 shadow rounded-circle calendar-icon">
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    dateFormat="MM/yyyy"
                                    locale="th"
                                    showMonthYearPicker
                                    showFullMonthYearPicker
                                    className={'shop_history_DatePicker'}
                                    ref={(c) => this._calendar = c}
                                />
                                {/* <img src={'/assets/images/calendar.svg'}  className="calendar-history" onClick={this.openDatepicker} /> */}
                                <svg style={{width:'100%',height:'auto'}}
                                    version="1.1" 
                                    viewBox="0 0 60 50"
                                    preserveAspectRatio="xMinYMin slice"
                                    id="Capa_1" xmlns="http://www.w3.org/2000/svg" className="calendar-history" onClick={this.openDatepicker}>
                                    <g>
                                        <g>
                                            <path d="M30.224,3.948h-1.098V2.75c0-1.517-1.197-2.75-2.67-2.75c-1.474,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75
                                                c0-1.517-1.197-2.75-2.67-2.75c-1.473,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75c0-1.517-1.197-2.75-2.67-2.75
                                                c-1.473,0-2.67,1.233-2.67,2.75v1.197H6.224c-2.343,0-4.25,1.907-4.25,4.25v24c0,2.343,1.907,4.25,4.25,4.25h24
                                                c2.344,0,4.25-1.907,4.25-4.25v-24C34.474,5.855,32.567,3.948,30.224,3.948z M25.286,2.75c0-0.689,0.525-1.25,1.17-1.25
                                                c0.646,0,1.17,0.561,1.17,1.25v4.896c0,0.689-0.524,1.25-1.17,1.25c-0.645,0-1.17-0.561-1.17-1.25V2.75z M17.206,2.75
                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z M9.125,2.75
                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z
                                                M31.974,32.198c0,0.965-0.785,1.75-1.75,1.75h-24c-0.965,0-1.75-0.785-1.75-1.75v-22h27.5V32.198z" fill="#000" className="icon-calendar"/>
                                            <rect x="6.724" y="14.626" width="4.595" height="4.089" fill="#000" className="icon-calendar"/>
                                            <rect x="12.857" y="14.626" width="4.596" height="4.089" fill="#000" className="icon-calendar"/>
                                            <rect x="18.995" y="14.626" width="4.595" height="4.089" fill="#000" className="icon-calendar"/>
                                            <rect x="25.128" y="14.626" width="4.596" height="4.089" fill="#000" className="icon-calendar"/>
                                            <rect x="6.724" y="20.084" width="4.595" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="12.857" y="20.084" width="4.596" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="18.995" y="20.084" width="4.595" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="25.128" y="20.084" width="4.596" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="6.724" y="25.54" width="4.595" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="12.857" y="25.54" width="4.596" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="18.995" y="25.54" width="4.595" height="4.086" fill="#000" className="icon-calendar"/>
                                            <rect x="25.128" y="25.54" width="4.596" height="4.086" fill="#000" className="icon-calendar"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div  style={{marginTop: "5%"}}>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="th monthtab-header" colSpan="2">
                                        <p>{moment(this.state.startDate).format('MMMM Y')}</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.length === 0 ? (
                                        <></>
                                    ) : (
                                    this.state.data.map((row,key) => 
                                        <tr key={key}>
                                            <td className="td-1">
                                                <p>{moment(row.datetime).format('dddd')}</p>
                                                <p>{moment(row.datetime).format('D-MM-Y')}</p>
                                                <p>{moment(row.datetime).format('HH:MM น.')}</p>
                                            </td>
                                            <td className="td-2">
                                                <span> 
                                                    <p style={{fontSize:"25px"}} >{row.point} คะแนน</p>
                                                </span>
                                                <span>
                                                    <p>{row.detail}</p>
                                                </span>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                    
                    {/* <div  style={{marginTop: "5%"}}>

                    {
                        this.state.data.length==0 ? (
                            <div className="text-center" style={{marginTop: "15%"}}>
                                <h4 className="" style={{fontWeight: "600"}}>
                                    ไม่พบข้อมูล
                                </h4>
                            </div>
                    ) : (

                        <>
                        {
                           
                           this.state.data.map(row => (

                            <table className="table">
                                <thead>
                                    <tr>
                                        <th className="th" colSpan="2">
                                            <p>{moment(row.pointdate).format('MMMM Y')}</p>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="td-1">
                                            <p>{moment(row.pointdate).format('dddd')}</p>
                                            <p>{moment(row.pointdate).format('D-MM-Y')}</p>
                                            <p>{moment(row.pointdate).format('hh:mm น.')}</p>
                                        </td>
                                        <td className="td-2">
                                            <span>
                                            { row.point > 0 ? ( 
                                                    <p style={{color: "#F9CE43" ,fontSize:"25px"}} >{row.point} คะแนน</p>
                                                ) : (
                                                    <p style={{color: "#000000" ,fontSize:"25px"}} >{row.point} คะแนน</p>
                                                )
                                            } 
                                            </span>
                                            <span>
                                                <p></p>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            ))

                        }
                       </> 
                )} */}
                        {/* <table className="table">
                            <thead>
                                <tr>
                                    <th className="th" colSpan="2">
                                        <p>ธันวาคม 2562</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="td-1">
                                        <p>ศุกร์ 27-12-2562 19:14 น.</p>
                                    </td>
                                    <td className="td-2">
                                        <span> 
                                            <p style={{color: "#F9CE43" ,fontSize:"25px"}} >รับ 1แสตมป์</p>
                                        </span>
                                        <span>
                                            <p>ผ่านร้านค้า Vara's Nails Spa</p>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="td-1">
                                        <p>เสาร์ 02-12-2562 20:45 น.</p>
                                    </td>
                                    <td className="td-2">
                                        <span class=""> 
                                            <p style={{color: "#000000" ,fontSize:"25px"}} >ใช้แสตมป์</p>
                                        </span>
                                        <span class="">
                                            <p>แลกสิทธิพิเศษผ่านร้าน Vara's Nails Spa</p>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table> */}
                    {/* </div> */}

                </div>
            </div>
        )
    }
}
export default Usage_history;
