import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
        
        <div className="head bg-yellow shadow">
            <div className="row w-100 h-100">
                <div className="col-8 h-100">
                    <div className=" h-100 p-3 pl-5">
                        <h1 className="m-0 "> 
                            <span className="text-blue">E</span>VEN<span className="text-blue">T</span>
                        </h1> 
                        <p >สิทธิพิเศษที่โหจัดมาให้คุณ</p>
                    </div>
                </div>
                <div className="col-4 h-100">
                    <div className="row float-right h-100 w-100">
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
        </>)
    
        

    }
}
export default Header;
