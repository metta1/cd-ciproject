import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Event_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
    };

    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/admin_event/api/event').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
            console.log(result.data)
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/event.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-3 pl-4">
                        <div className="row">
                            <div className="col-8 head-text">
                                <Link to="/home">
                                    <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </Link>
                                <h3 className="m-0 pl-2">สนุกกับกิจกรรมกันเลย</h3> 
                            </div>
                            <div className="col-4">
                                <div className="float-right">
                                    <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                        <a href="#">
                                            <img className="h-50" src="/assets/images/icon-search.svg" alt="..." />
                                        </a>
                                    </div>
                                    <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                        <a href="#">
                                            <img className="h-50" src="/assets/images/icon-noti.svg" alt="..." />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {
                        this.state.data === undefined ? (
                            <></>
                        ) : (
                        this.state.data.map((row,key) => (
                            <div classNme="banner m-2 " key={key}>
                                <img class="w-100 mr-event" src={row.eventimg==='' ? '/assets/images/banner.png' : row.eventimg} alt=""/>
                            </div>
                        )))
                    }
                </div>
            </div>
        )
    }
}
export default Event_list;
