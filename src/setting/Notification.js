import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Notification extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        userAddressData : [],
        userid: sessionStorage.getItem('user_id')
    };


    async componentDidMount(){

        const rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rAddres.status===200){
            this.setState({userAddressData: rAddres.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    render() {
        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=2"/>
                <div className="head head-setting bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <Link to="/setting">
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </Link>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="align-title m-0" style={{fontSize:'1.75rem'}}> การแจ้งเตือน</h3>
                    </div>
                </div>
                
                <div className="container-mobile m-auto ">
                <div className="feed p-3">
                    <a href="/notify">
                        <div className="account-menu w-100 shadow  rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">การแจ้งเตือน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="/assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href="/notify_email">
                        <div className="account-menu w-100 shadow  mt-2 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">การแจ้งเตือนทาง E-mail</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="/assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                </div>   
                </div>
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Notification;
