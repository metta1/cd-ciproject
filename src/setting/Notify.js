import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import Switch from "react-switch";

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Notify extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.isNotifyChange = this.isNotifyChange.bind(this);
    }

    state = {
        userid: sessionStorage.getItem('user_id'),
        checked: false,
    };


    async componentDidMount(){

        const rNotify = await axios.get(config.api.base_url+'/users/api/usernotify/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rNotify.status===200){
            console.log(rNotify.data[0].usernotify)
            this.setState({checked: rNotify.data[0].usernotify === '1' || rNotify.data[0].usernotify === '0' ? true : false });
        }
        console.log(this.state.checked)
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    isNotifyChange(event) {
        this.setState({ checked: event });

        const data = new FormData();
        data.set('userid', this.state.userid);
        data.set('usernotify', event ? 1 : 3);
        data.set('usernotify_from', "1");
        axios({
            method: 'post',
            url: config.api.base_url+'/users/api/usernotify',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data);
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
        });
    }

    render() {
        const inputField = {
            right: '0',
            textAlign:'right',
            marginTop: '-2rem'
        };
        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=2"/>
                <div className="head head-setting bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <Link to="/notification">
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </Link>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="align-title m-0"> การแจ้งเตือน</h3>
                    </div>
                </div>
                
                <div className="container-mobile m-auto ">
                    <div className="px-4 py-2 border-bottom" >
                        <h5>การแจ้งเตือน </h5>
                        <div className="position-absolute mr-2" style={inputField}>
                            <Switch name="usernotify" onChange={this.isNotifyChange} checked={this.state.checked}/>
                        </div>
                    </div>
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Notify;
