import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import Switch from "react-switch";

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Notify_email extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.isEmailNotifyChange = this.isEmailNotifyChange.bind(this);
        this.isOrderNotifyChange = this.isOrderNotifyChange.bind(this);
        this.isNewsNotifyChange = this.isNewsNotifyChange.bind(this);
    }

    state = {
        userid:sessionStorage.getItem('user_id'),
        emailchecked: false,
        orderchecked: false,
        newschecked: false
    };


    async componentDidMount(){

        const rNotify = await axios.get(config.api.base_url+'/users/api/usernotify/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rNotify.status===200){
            console.log(rNotify.data[0].usernotify)
            this.setState({emailchecked: rNotify.data[0].usernotify === '2' || rNotify.data[0].usernotify === '0' ? true : false });
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    isEmailNotifyChange(event) {
        this.setState({ emailchecked: event });

        const data = new FormData();
        data.set('userid', this.state.userid);
        data.set('usernotify', event ? 2 : 3);
        data.set('usernotify_from', "2");
        axios({
            method: 'post',
            url: config.api.base_url+'/users/api/usernotify',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data);
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
        });
    }

    isOrderNotifyChange(event) {
        this.setState({ orderchecked: event });

        const data = new FormData();
        data.set('userid', this.state.userid);
        data.set('usernotify', event ? 1 : 3);
        // axios({
        //     method: 'post',
        //     url: config.api.base_url+'/users/api/usernotify',
        //     data: data,
        //     headers: {'Content-Type': 'multipart/form-data' }
        //     })
        //     .then(function (response) {
        //         console.log(response.data);
        //     })
        //     .catch(function (response) {
        //         //handle error
        //         //console.log(response);
        // });
    }

    isNewsNotifyChange(event) {
        this.setState({ newschecked: event });

        const data = new FormData();
        data.set('userid', this.state.userid);
        data.set('usernotify', event ? 1 : 3);
        // axios({
        //     method: 'post',
        //     url: config.api.base_url+'/users/api/usernotify',
        //     data: data,
        //     headers: {'Content-Type': 'multipart/form-data' }
        //     })
        //     .then(function (response) {
        //         console.log(response.data);
        //     })
        //     .catch(function (response) {
        //         //handle error
        //         //console.log(response);
        // });
    }

    render() {
        const inputField = {
            right: '0',
            textAlign:'right',
            marginTop: '-2rem'
        };
        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=2"/>
                <div className="head head-setting bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <Link to={"/notification/"+this.state.userid+"?v="+ new Date().getTime()}>
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </Link>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="align-title m-0"> การแจ้งเตือนทาง E-mail</h3>
                    </div>
                </div>
                <div className="container-mobile m-auto ">
                    <div className="px-4 py-2 mt-2 border-bottom" >
                        <h5>การแจ้งเตือนทาง E-mail </h5>
                        <div className="position-absolute mr-2" style={inputField}>
                            <Switch name="emailnotify" onChange={this.isEmailNotifyChange} checked={this.state.emailchecked}/>
                        </div>
                    </div>
                    {/* <div className="px-4 py-2 mt-2 border-bottom" >
                        <h5>อัพเดทคำสั่งซื้อ </h5>
                        <div className="position-absolute mr-2" style={inputField}>
                            <Switch name="ordernotify" onChange={this.isOrderNotifyChange} checked={this.state.orderchecked}/>
                        </div>
                    </div>
                    <div className="px-4 py-2" style={{fontSize: '13px', color: 'gray'}} >แจ้งเตือนเมื่อมีอัพเดทเกี่ยวกับคำสั่งซื้อและการชำระเงิน</div>
                    <div className="px-4 py-2 mt-2 border-bottom" >
                        <h5>จดหมายข่าว </h5>
                        <div className="position-absolute mr-2" style={inputField}>
                            <Switch name="newsnotify" onChange={this.isNewsNotifyChange} checked={this.state.newschecked}/>
                        </div>
                    </div>
                    <div className="px-4 py-2" style={{fontSize: '13px', color: 'gray'}} >แจ้งเตือนอัพเดทล่าสุด เทรนด์มาแรงและส่วนลดต่างๆให้ฉัน</div> */}
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Notify_email;
