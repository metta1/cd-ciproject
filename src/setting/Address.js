import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import Swal from 'sweetalert2'
import $ from 'jquery'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Address extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        userAddressData : [],
        userid:sessionStorage.getItem('user_id'),
        address_active: ''
    };


    async componentDidMount(){
        const rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rAddres.status===200){
            this.setState({userAddressData: rAddres.data});
            let activeAddress = '';
            for (let i=0;i<rAddres.data.length;i++) {
                if (rAddres.data[i].active === '1') {
                    activeAddress = rAddres.data[i].addressid;
                }
            }
            this.setState({address_active: activeAddress});
        }
        console.log(this.state.address_active)
        console.log(this.state.userAddressData);
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {
        event.preventDefault();
        const data = new FormData(event.target);
        data.set('user_id', this.state.userid);
        // console.log(data.get('user_id'))
        axios({
            method: 'post',
            url: config.api.base_url+'/users/api/updatedefaultaddress',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                window.location.reload()
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
        });

    }

    deleteAddress(e,addressid) {
        e.preventDefault();
        Swal.fire({
            title: 'ต้องการลบที่อยู่ใช่ไหม',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ตกลง',
            focusConfirm: false,
            cancelButtonText:
              'ยกเลิก',
          }).then((result) => {
            if (result.value) {
                let bodyFormData = new FormData();
                bodyFormData.set('addressid',addressid)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/users/api/removeuseraddress',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        window.location = '/address';
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถลบข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                        
                        //handle error
                        //console.log(response);
                    });
            }
        })
      }

    render() {
        const imgPlus = {
            width: '50%'
        }
        const submitButton = {
            background: 'none',
            outline: 'none',
            color: 'white',
            border: 0,
            marginTop: '0.5rem'
        };
        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/address.css?v=3"/>
                <div className="head bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <Link to="/setting">
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </Link>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="align-title m-0"> ที่อยู่ของฉัน</h3>
                    </div>
                </div>
                
                <div className="container-mobile m-auto ">
                    {/* <div className="px-4 py-2 border-bottom w-100">
                        <a href="/add_address" className="w-100">
                            <div className="account-menu w-100 d-flex flex-row h-50">
                                <div className="w-100">
                                    <h5 className="mt-3">เพิ่มที่อยู่ใหม่</h5>
                                </div>
                                <div className="account-menu-detail ml-auto">
                                    <img className="mt-3 mr-2" width="" src="/assets/images/add-plus-button.svg" alt="" style={imgPlus}/>
                                </div>
                            </div>
                        </a>
                    </div> */}
                    <div className="address-detail mb-5 ">
                        <form onSubmit={this.handleSubmit} id="myAddressForm">
                        <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                            <div className="col-12 p-0">
                                <div className="">
                                    <ul className="address-list">
                                    {
                                        this.state.userAddressData === undefined ? (
                                            <div/>
                                        ) : (                       
                                            this.state.userAddressData.map((row,key) => (
                                                    <>
                                                        <li className="address-item-detail shadow p-2" key={key}>
                                                            {/* <input type="hidden" name="addressid" id="addressid" value={this.s}/> */}
                                                            <div className="row w-100">
                                                                    <div className="col-1 pr-0">
                                                                    <input type="radio" id={'radio'+row.addressid} name="address_active" value={row.addressid} onClick={this.handleChange} checked={this.state.address_active===row.addressid ? 'checked' : ''}/>
                                                                </div>
                                                                <div className="col-11 pl-2">
                                                                    <h4 className="m-0"><label for={'radio'+row.addressid}>ที่อยู่การจัดส่ง</label> {row.active === "1" ? <span style={{color: 'green'}}>(ที่อยู่ตั้งต้น)</span> : ""}</h4>
                                                                </div>
                                                            </div>
                                                            <div className="row w-100">
                                                                <div className="col-1">
                                                                </div>
                                                                <div className="col-11 pl-4">
                                                                    <label for={'radio'+row.addressid}>
                                                                        <a href={"/edit_address/"+row.addressid+"?v="+ new Date().getTime()} key={key++}>
                                                                        <h5 className="m-0  text-gray">
                                                                            {row.name}
                                                                            <br/>
                                                                            {row.tel+' '}
                                                                            {row.detail+', '}
                                                                            {row.subdistrictname+', '}
                                                                            {row.districtname+', '}
                                                                            {row.provincename+' '}
                                                                            {row.zipcode}
                                                                        </h5>
                                                                        </a>
                                                                    </label>
                                                                </div>

                                                                {/* <div className="col-12"> */}
                                                                    {/* <label for={'radio'+row.addressid}> */}
                                                                        {/* <h5 className="m-0  text-gray">
                                                                            {row.name} {row.active === "1" ? <span style={{color: 'green'}}>(ที่อยู่ตั้งต้น)</span> : ""}
                                                                            <br/>
                                                                            {row.tel+' '}
                                                                            {row.detail+', '}
                                                                            {row.subdistrictname+', '}
                                                                            {row.districtname+', '}
                                                                            {row.provincename+' '}
                                                                            {row.zipcode}
                                                                        </h5> */}
                                                                    {/* </label> */}
                                                                {/* </div> */}
                                                                {/* <div className="col-2">
                                                                    <a href={"/edit_address/"+row.addressid+"?v="+ new Date().getTime()} key={key++}>
                                                                        <img className=" ml-2" width="" src="/assets/images/edit-button.svg" alt="" style={imgPlus}/>
                                                                    </a>
                                                                    <img className=" ml-2" width="" src="/assets/images/trash-button.svg" alt="" style={imgPlus}
                                                                        onClick={(e) => {this.deleteAddress(e, row.addressid)}}/>
                                                                </div> */}
                                                            </div>  
                                                        </li>
                                                    </>
                                                )
                                            )
                                        )
                                    }
                                        <li className="address-item-detail shadow p-2">
                                        <a href="/add_address" className="w-100">
                                            <div className="row w-100">
                                                <div className="col-10">
                                                    <h4 className="m-0">เพิ่มที่อยู่ใหม่</h4>
                                                </div>
                                                <div className="col-2 pr-0">
                                                    <h4 className="m-0 text-right text-gray"><i class="fas fa-plus"></i></h4>
                                                </div>

                                            </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <div className="foot bg-yellow shadow fixed-bottom">
                            <button className="text-center h2 h-100 w-100 " style={submitButton} onclick>ยืนยัน</button>
                        </div>
                        </form>
                    </div>
                    {/* {
                        this.state.userAddressData === undefined ? (
                            <div/>
                        ) : (
                        this.state.userAddressData.map((row,key) => (
                            <div className="border-bottom">
                            <a href={"/edit_address/"+row.addressid+"?v="+ new Date().getTime()} key={key++}>
                                <div className="px-4 py-2 d-flex flex-row h-50">
                                    <div className="w-100">{key}. {row.name} {row.detail}</div>
                                    <div className="account-menu-detail ml-auto">
                                        <img className=" mr-2" width="" src="/assets/images/edit-button.svg" alt="" style={imgPlus}/>
                                    </div>
                                </div>
                            </a>
                            </div>
                        )))
                    } */}
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Address;
