import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import $ from 'jquery'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Setting extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {

    };


    async componentDidMount(){

    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    render() {

        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=2"/>
                
                <div className="container-mobile m-auto ">
                    <div className="head head-setting bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="/account">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h3 className="align-title m-0" style={{fontSize:'1.75rem'}}> 
                                ตั้งค่าบัญชี
                            </h3> 
                        </div>
                    </div>
                <div className="feed p-3 setting-account">
                    <a href="/address">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">ที่อยู่ของฉัน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="/assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href="/notification">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">การแจ้งเตือน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="/assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    {/* <a href="#">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">ภาษา</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="/assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a> */}
                   
                </div>
                    
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Setting;
