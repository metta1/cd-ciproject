import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Faq extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        faqid:this.props.match.params.faqid
    };


    async componentDidMount(){
        
        const result = await axios.get(config.api.base_url+'/admin_faq/api/faq/'+this.state.faqid).catch(function (error) {

           
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
            console.log(result.data)
        }
        
      
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            
            <>
            <link rel="stylesheet" type="text/css" href="assets/css/faq.css?v=2"/>
            <div className="container-mobile m-auto">
                
                <div className="head bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <a href="/help">
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="align-title m-0">
                            คำถามที่พบบ่อย
                        </h3> 
                    </div>
                </div>

                <div className="feed p-3" style={{marginBottom: '4em'}}>
                
                           
                        
                {
                    this.state.data.map((row,key) => (
                        
                        // <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                        //     <div className="box rounded-left "></div>
                        //     <div className="account-menu-detail pl-3 h-100 w-100 ">
                        //         <span className="pl-auto">{row.question}</span>
                        //     </div>
                        //     <div className="account-menu-detail ml-auto">
                        //         <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                        //     </div>
                        // </div>
                        <Accordion>
                            <Card className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                                <Accordion.Toggle as={Button} variant="" eventKey="0">
                                    <Card.Header className="card-header">
                                        <span className="pl-auto">{row.question}</span>
                                    </Card.Header>
                                </Accordion.Toggle>
                            </Card>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <span className="pl-auto" style={{ fontSize: "18px", whiteSpace: 'pre-line'}}>{row.answer}</span>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Accordion>
                        
                        
                    ))
                }
                </div>
               
                
                            
            </div>
            </>
        )
    }
}
export default Faq;
