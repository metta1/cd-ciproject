import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
//import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import {config} from '../config';
import $ from 'jquery'
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Modal from 'react-bootstrap4-modal';
import {Navigation,Notification_num} from '../template'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();


class Promotion_category extends Component {
    constructor(){//เริ่มต้น run component
        super();
        this.handleFilterCoupon = this.handleFilterCoupon.bind(this);
    }

    state = {
        cateData : [],
        couponData : [],
        rewardData: [],
        activeSearchBox:false
    };


    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/admin_shop_category/api/category').catch(function (error) {
        //const result = await axios.get('http://dev.backend.ho.com/admin_api').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            console.log(result.data);
            this.setState({cateData: result.data});
        }

        const couponRs = await axios.get(config.api.base_url+'/store_coupon/api/allusercoupon/'+sessionStorage.getItem('user_id'))
        if(couponRs.status===200){
                this.setState({couponData: couponRs.data});
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id'))
        if(rewardRs.status===200){
            this.setState({rewardData: rewardRs.data});
        }
        console.log(this.state.rewardData);
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        $('.btn-getcoupon, .btn-usecoupon').click(function(){
            let key = $(this).data('primary');
            let storeid = $(this).data('storeid');
            let couponid = $(this).data('couponid');
            let coupontype = $(this).data('coupontype');

            if (coupontype === 1) {
                $(this).css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
                $('body').addClass('modal-open')
                $('<div class="modal-backdrop fade show"></div>').appendTo('body');
                $('#couponModal-'+key).css({'display':'block'})
            } else {
                localStorage.setItem('storageStoreID',storeid);
                window.location = "/coupon_detail/"+couponid;
            }
        })

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })
        


    }

    async handleFilterCoupon(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store_coupon/api/allusercoupon/'+sessionStorage.getItem('user_id')+'?keyword='+keyWord)
        if(result.status===200){
            this.setState({couponData: result.data});
        } else {
            this.setState({couponData: []});
        }

        const cateresult = await axios.get(config.api.base_url+'/admin_shop_category/api/category?keyword='+keyWord)
        if(cateresult.status===200){
            this.setState({cateData: cateresult.data});
        } else {
            this.setState({cateData: []});
        }
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-coupon').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-coupon').css({"padding-top": 'unset'})

        this.componentDidMount();
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    getCoupon(event,e) {
        const couponid = e.couponid;
        const storeid = e.storeid;
        const userid = sessionStorage.getItem('user_id');

        if (e.coupontype==="1") {
            const data = new FormData();
            data.set('couponid', couponid);
            data.set('storeid', storeid);
            data.set('userid', userid);
            axios({
                method: 'post',
                url: config.api.base_url+'/store_coupon/api/coupon',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data)
            })
            .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        }
    }

    useCoupon(e,data) {
        e.preventDefault();
        localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    handleChangeReward = (e,itemid,storeid) => {
        e.preventDefault();
        localStorage.setItem('storageStoreID',storeid);
        window.location='/reward_detail/'+itemid;
    }

    async couponDetail(e,data) {
        e.preventDefault();
        
        if (data.coupontype==="0" && data.couponused==="1") {
            console.log(data);
            localStorage.setItem('storageStoreID',data.storeid);
            window.location = "/coupon_detail/"+data.couponid;
        }
    }
    
    render() {
        const useCouponButton = {
            backgroundColor: '#12a508'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0'
        };

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/promotion_category.css?v=4.1"/>
                <div className="container-mobile m-auto">
                    <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                        <div className="modal-body">
                            <div className="form-group">
                                <label>ค้นหาคูปอง</label>
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCoupon}/>
                            </div>
                            <div className="w-100 text-right">
                                <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                            </div>
                        </div>
                    </Modal>
                    <div className="head bg-yellow shadow">
                    {
                        this.state.activeSearchBox ? (
                            <div className="row w-100 h-100">
                                    
                                <div className=" h-100 p-3 pt-4 pl-5 w-100">
                                    <div className="icon-back" style={{left: '1rem'}}>
                                        <a onClick={this.handleBackPage}>
                                                <div className="icon rounded-circle shadow">
                                                    <img src="/assets/images/back.svg" alt=""/>
                                                </div>
                                        </a>
                                    </div>
                                    <div className=" h-100 pl-3">
                                    <div className="input-group search-group mb-3">
                                        <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCoupon}/>
                                        <div className="input-group-append">
                                            <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                        
                                    </div>
                                        
                                </div>
                            </div>
                        ) : (
                        <div className="row w-100 h-100">
                            <div className="col-8 h-100">
                                <div className=" h-100 pt-3 pl-0 ml-20" style={{whiteSpace: 'nowrap'}}>
                                    <h1 className="m-0 mt-1 coupon-text"> 
                                        REWARD&#38;COUPON
                                    </h1> 
                                    <p >รวมสิทธิพิเศษร้านประจำของคุณ</p>
                                </div>
                            </div>
                            <div className="col-4 h-100">
                                <div className="row float-right h-100 w-100">
                                    <div className="col p-2 my-auto">
                                        <div className="icon icon-top-right rounded-circle">
                                            <a href="#"><img className="w-100" src="/assets/images/icon-search@2x.png" alt="" onClick={this.openSearchBox}/></a>
                                        </div>
                                    </div>
                                    <div className="col p-2 my-auto">
                                        <div className="icon icon-top-right rounded-circle">
                                            <a href="/notifications">
                                                <img src="/assets/images/icon-noti@2x.png" alt="" className="img-noti"/>
                                                <Notification_num/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                            
                            </div>
                        </div>
       
                        )
                    }
                    </div>
                    {
                        this.state.cateData.length === 0 ? (
                            <></>
                        ) : (
                            <div className="score shadow pl-3 py-2">
                                <div className="d-flex bd-highlight ">
                                    <div className="px-2 py-0 text-head bd-highlight">
                                        <h5 className="text-black">หมวดหมู่</h5>
                                        <p className="text-grey">เลือกหมวดหมู่ดีลสำหรับ</p>
                                    </div>
                                    <div className="ml-auto px-2 pr-3 bd-highlight ">
                                        <a className="text-blue" href="/all_promotion_category">
                                            <u className="more"> เพิ่มเติม </u>
                                        </a>
                                        
                                    </div>
                                </div>
                                <div className="row pb-2 w-100 mt-7 ml-0">
                                    { this.state.cateData.map((row, key) => (
                                        (row.parentcateid === "0" ?
                                            <div className="col-5 p-2" key={key}>
                                                <a className="" href={parseInt(row.promo_count)>0 || parseInt(row.reward_count)>0 ? '/promotion/'+row.cateid : '#'}>
                                                    <div className={parseInt(row.promo_count)>0 || parseInt(row.reward_count)>0 ? 'promotion-box shadow rounded': 'promotion-box shadow rounded unactive'}>
                                                        <div className="promotion-item">
                                                            <img className="mt-4" src={row.cate_img} alt={row.catename}/> <br/>
                                                            <p className="mt-4 u">{row.catename}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        :
                                            <></>
                                        )
                                    ))}
                                </div>
                            </div>
                        )
                    }
                    <div className="row pl-3 py-2 pb-2 w-100">
                        <div className="text-head px-3 mt-2">
                            <h5 className="m-0 text-black">ดีลร้านค้า</h5>
                            <p className="text-grey mb-0">ดีลร้านค้าที่ HO จัดมาให้สำหรับคุณ</p>
                        </div>
                    </div>
                    <div className="coupon">
                        { 
                            this.state.couponData.length === 0 ? (
                                <></>
                            ) : (
                            this.state.couponData.map((row,key) => (
                                ((new Date(row.enddate).getTime() > nowDate) && (new Date(row.startdate).getTime() < nowDate) && row.couponstatus == "1") ?
                                    <div className="col-12 p-0 pl-2 container-coupon" key={key}>
                                        <div className="position-relative">
                                            <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                            <div className="coupon-detail row ml-0 h-100">
                                                <div className="col-4 align-coupon">
                                                    <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                    <div className="w-100 text-center mt-2">
                                                        <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                            <img className="" src={row.storelogo} alt=""/>
                                                        </div>
                                                        <p className="mt-2 store-name">{row.storename}</p>
                                                    </div>
                                                </div>
                                                <div className="col-4 px-0 text-center h-100" style={{marginTop:'1.1rem'}}>
                                                    <p className="mt-2 mb-0">{row.couponname}</p>
                                                    <h2>ลดราคา</h2>
                                                    {row.branchName!== null ? (
                                                        <small className="d-block">สาขา : {row.branchName}</small>
                                                    ):(
                                                        <></>
                                                    )}
                                                
                                                    <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                    {(row.statususer === "1" && row.couponused === "1") || (row.statususer === "1" && row.offline_coupon_status === "1") ? ( 
                                                        <button className="btn btn-orange p-0 mt-2" style={usedCouponButton} onClick={(e) => this.couponDetail(e,row)} data-primary={key}> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                    ) : row.statususer === "1" ? (
                                                        <button className="btn btn-orange p-0 mt-2 btn-usecoupon" style={useCouponButton} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}>
                                                            <h5 className="m-0">ใช้คูปอง</h5>
                                                        </button>
                                                    ) : (
                                                        <div key={key}>
                                                            <button className="btn btn-orange p-0 mt-2 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}> <h5 className="m-0">รับคูปอง</h5></button>
                                                        </div>
                                                    )}
                                                </div>
                                                {/* MODAL COUPON */}
                                                <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                    <div className="modal-dialog modal-dialog-centered" role="document">
                                                    <div className="modal-content w-75 mx-auto border-0">
                                                        <div className="modal-header bg-yellow">
                                                        <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                            <div className="position-relative">
                                                                <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                <div className="coupon-detail row ml-1 h-100">
                                                                    <div className="col-4">
                                                                        <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                        <div className="w-100 text-center mt-2">
                                                                            <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                <img className="" src={row.storelogo} alt=""/>
                                                                            </div>
                                                                            <p className="mt-2">{row.storename}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-4 px-0 text-center mt-2 h-100">
                                                                        <p className="mt-2 mb-0">{row.couponname}</p>
                                                                        <h2>ลดราคา</h2>
                                                                        {row.branchName!== null ? (
                                                                            <small className="d-block">สาขา : {row.branchName}</small>
                                                                        ):(
                                                                            <></>
                                                                        )}
                                                                        <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                        <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                    </div>
                                                                    <div className="col-4 p-0 text-center my-auto">
                                                                        <div className="coupon-detail-side ">
                                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                            <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div></h5>
                                                        <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                            <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                        </button>
                                                        </div>
                                                        <div className="modal-body text-center">
                                                            <h4>คุณได้รับคูปองเรียบร้อย!</h4>
                                                            <button className="btn bg-yellow w-75">
                                                                <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div className="col-4 p-0 text-center ml-2" style={{marginTop:'2.9rem'}}>
                                                    <div className="coupon-detail-side mt-2" style={{whiteSpace: "nowrap"}}>
                                                    <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                        <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                : 
                                <></>
                            )))
                        }
                        {
                            this.state.rewardData.length === 0 ? (
                                    <div/>
                            ) : (
                                this.state.rewardData.map(row => (
                                    <div className="col-12 p-0 pl-2 pr-2">
                                        <div className="position-relative">
                                            <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                            <div className="coupon-detail row ml-1 h-100">
                                                <div className="col-5">
                                                    <div className="w-100 text-center mt-2">
                                                        <div className="row w-100">
                                                            <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                            <p className="mt-3 ml-2 mb-0 store-name">{row.storename}</p>
                                                        </div>
                                                        <p className="mb-0 fs-40 " style={{marginTop: '-0.5rem'}}><h2 className="mb-2">{row.itempoint}<br/>คะแนน</h2></p>
                                                    {
                                                        (parseFloat(row.itempoint) <= row.user_point) ? (
                                                            (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                                // <button className="btn coupon-green-btn btn-secondary w-100 p-10 mt-0" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf">แลกแล้ว</p></button>
                                                                <button className="btn btn-green coupon-green-btn p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                { row.redeemtime_status === "1" ? 
                                                                <p className="m-0 font-tf">แลกแล้ว</p>
                                                                :
                                                                <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                }
                                                                </button>
                                                            ):(
                                                                <button className="btn coupon-green-btn btn-green p-10" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                            )
                                                        ) : (
                                                            <button className="btn coupon-btn btn-orange p-10 mt-0" > <p className="m-0 font-tf ">สะสมคะแนนเพิ่ม</p></button>
                                                        )
                                                    }
                                                    </div>
                                                </div>
                                                <div className="col-7 p-0 text-center mt-2">
                                                    <p className="m-0 align-text">{row.catename}</p>
                                                        <img className="coupon-pic" src={row.itemimg} width="38%" alt=""/>
                                                    <p className="m-0 px-3 pt-2 text-center">{row.itemname}</p>
                                                    {/* <p class="m-0 pl-3 pr-3 f-value"><h3>มูลค่า 1,750 บาท</h3></p> */}
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                ))
                            )
                        }
                    </div>
                    <br/><br/><br/><br/>
                    <Navigation active={'deal'}/>
                </div>
                 
            </div>
        )
    }
}
export default Promotion_category;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;