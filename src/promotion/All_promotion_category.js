import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
//import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import {config} from '../config';
import Footer from './Footer'
import {Route, Link, useParams} from 'react-router-dom'
import {Navigation, Notification_num} from '../template'
import $ from 'jquery'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Promotion_category extends Component {
    constructor(){//เริ่มต้น run component
        super();
        this.handleFilterCate = this.handleFilterCate.bind(this);
    }

    state = {
        data : [],
        activeSearchBox:false
    };


    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/admin_shop_category/api/category').catch(function (error) {
        //const result = await axios.get('http://dev.backend.ho.com/admin_api').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
        }
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    async handleFilterCate(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/admin_shop_category/api/category?keyword='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    handleBackPage = e => {
        e.preventDefault()
        window.location='/promotion_category';
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-yellow').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/all_promotion_category.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/feed.css?v=3"/>
                
                    {/* <div className="row w-100 h-100">
                        <div className="col-10 h-100" style={{whiteSpace: 'nowrap'}}>
                            <div className=" h-100 p-3 pl-5">
                                <h1 className="m-0 text-white">DEAL</h1> 
                                <p className="text-red">รวมสิทธิพิเศษ คะแนน คูปอง ส่วนลด ร้านค้าประจำของคุณ</p>
                            </div>
                        </div>
                        <div className="col-2 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div> */}
                    {/* <div className="head bg-yellow shadow">
                         <div className="row w-100 h-100">
-                            <div className="col-8 h-100" style={{whiteSpace: 'nowrap'}}>
                                 <div className=" h-100 p-3 pl-4">
-                                    <h1 className="m-0 mb-3"> 
-                                        <span className="text-blue">DE</span>AL
                                     </h1> 
-                                    <p >รวมสิทธิพิเศษ คะแนน คูปอง ส่วนลด ร้านค้าประจำของคุณ</p>
                                 </div>
                             </div>
                             <div className="col-4 h-100">
-                                <div className="row float-right h-100 w-100">
-                                    <div className="col p-1 my-auto">
-                                        <div className="icon rounded-circle">
-                                            <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
-                                        </div>
-                                    </div>
                                     <div className="col p-1 my-auto">
                                         <div className="icon rounded-circle">
-                                            <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                         </div>
                                     </div>
                                     <div className="col p-1 my-auto">
                                         <div className="icon rounded-circle">
-                                            <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                         </div>
                                     </div>
                                 </div>
                                 
                             </div>
                         </div>
-
                     </div> */}
                     <div className="container-mobile m-auto">
                     <div className="head bg-yellow shadow">
                     {
                        this.state.activeSearchBox ? (
                            <div className="row w-100 h-100">
                                    
                                <div className=" h-100 p-3 pt-4 pl-5 w-100">
                                    <div className="icon-back">
                                        <a onClick={this.handleBackPage}>
                                                <div className="icon icon-promo-search-back rounded-circle shadow">
                                                    <img src="/assets/images/back.svg" alt=""/>
                                                </div>
                                        </a>
                                    </div>
                                    <div className=" h-100 pl-3">
                                        <div className="input-group search-group mb-3">
                                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCard}/>
                                            <div className="input-group-append">
                                                <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : (
                        <div className="row w-100 h-100">
                                
                            <div className="col-8 h-100">
                            <div className="icon-back">
                                <a onClick={this.handleBackPage}>
                                    <div className="icons icon-promo-back rounded-circle shadow">
                                        <img src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                                <div className=" h-100 p-3 pl-5">
                                    <h3 className="align-title m-0">
                                        หมวดหมู่
                                    </h3> 
                                </div>
                            </div>
                            {/*<div className="icon-back">
                                <a href="/account">
                                    <div className="icon rounded-circle shadow">
                                        <img src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className=" h-100 p-3 pl-5">
                                <p></p>
                                <h3 className="align-title m-0">
                                หมวดหมู่
                                </h3>     
                            </div>*/}
                        
                            {/*<div className="col-8 h-100">
                                    <Link to="/promotion_category">
                                        <img  src="/assets/images/icons/icon-back@2x.png" alt=""/>
                                    </Link>
                                 <div className=" h-100 p-3 pl-2">
                                    <h3 className="ml-4">หมวดหมู่</h3> 
                                </div> 
                                <h3 className="ml-5" >หมวดหมู่</h3> 
                            </div>*/}
                            <div className="col-4 h-100">
                                <div className="row float-right h-100 w-100">
                                    <div className="col p-2 my-auto">
                                        <div className="icon icon-top-right rounded-circle">
                                            <img className="p-3" src="/assets/images/icon-search@2x.png" alt="" onClick={this.openSearchBox}/>
                                        </div>
                                    </div>
                                    <div className="col p-2 my-auto">
                                        <div className="icon icon-top-right rounded-circle">
                                            <a href="/notifications">
                                                <img className="p-3" src="/assets/images/icon-noti@2x.png" alt=""/>
                                                <Notification_num/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        )
                    }
                    </div>
                    
                    <div className="promotion p-2" style={{marginTop: "28%"}}>

                        {/* <h3 style={{marginBottom: '-0.5rem'}}><span className="p-2 m-0">ดีลร้านค้า</span></h3>
                        <span className="text-grey p-2">เลือกดีลที่ใช่ Ho คัดให้สำหรับคุณ</span> */}
                        <div className="row m-0">


                    {


                        this.state.data.map(row => (
                            
                            <div className="col-4 p-2">
                                <a className="" href={parseInt(row.promo_count)>0 || parseInt(row.reward_count)>0 ? '/promotion/'+row.cateid : '#'}>
                                {/* {
                                    parseInt(row.promo_count)>0 ?  <div className="promotion-box shadow rounded"> : <div className="promotion-box shadow rounded" style={{backgroundColor: 'gray'}}>
                                } */}
                                {/* <div style={{backgroundColor: 'gray'}}> */}
                                    <div className="promotion-box shadow rounded">
                                        <div className={parseInt(row.promo_count)>0 || parseInt(row.reward_count)>0 ? 'promotion-item' : 'promotion-item unactive'}>
                                                <img className="mt-4" src={row.cate_img} alt={row.catename}/> <br/>
                                                <p className="mt-2">{row.catename}</p>
                                        </div>
                                    </div>
                                {/* </div> */}
                                </a>
                            </div>
                        ))


                    }
                            

                    </div> 
                        {/* <!-- row --> */}
                    </div>
                    {/* <!-- promotion --> */}

                    {/* <div className="footer mx-auto">
                            <img className="w-100 position-relative" src="assets/images/footer_promotion.svg" alt=""/>
                            <div className="menu position-absolute w-100">
                                <div className="menu-item px-2 pt-2 ">
                                    <a href="/user_card">
                                        <img className="position-relative" src="assets/images/icon-credit.png" alt=""/> <br/>
                                        <span>CARD</span>
                                    </a>
                                    
                                </div>
                                <div className="menu-item px-2 pt-2">
                
                                    <a href="/feed">
                                        <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                        <span>Feed</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="/home">
                                        <img className="" src="assets/images/icon-home.png" alt=""/><br/>
                                        <span>HOME</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 ">
                                    <div className="menu-item-active bg-white rounded-circle shadow">
                                        <div className="menu-item-active-ab">
                                            <a href="/promotion_category">
                                                <img className="" src="assets/images/icon-gift.png" alt=""/> <br/>
                                                <span>DEAL</span>
                                            </a>
                                        </div>
                                    </div>
                
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="/account">
                                        <img className="" src="assets/images/icon-user.png" alt=""/> <br/>
                                        <span>ACCOUNT</span>
                                    </a>
                                </div>
                            </div>

                    </div> */}
                    <Navigation/>
                </div>
                 
            </div>
        )
    }
}
export default Promotion_category;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;