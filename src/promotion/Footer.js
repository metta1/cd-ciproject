import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <>
                
                <div className="footer mx-auto">
                    <img className="w-100 position-relative" src="/assets/images/footer_promotion.svg" alt=""/>
                    <div className="menu position-absolute w-100">
                        <div className="menu-item px-2 pt-2 ">
                            <a href="/user_card">
                                <img className="position-relative" src="/assets/images/icon-credit.png" alt=""/> <br/>
                                <span>CARD</span>
                            </a>
                            
                        </div>
                        <div className="menu-item px-2 pt-2">
        
                            <a href="/feed">
                                <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                <span>Feed</span>
                            </a>
                        </div>
                        <div className="menu-item px-2 pt-2">
                            <a href="/home">
                                <img className="" src="/assets/images/icon-home.png" alt=""/><br/>
                                <span>HOME</span>
                            </a>
                        </div>
                        <div className="menu-item px-2 ">
                            <div className="menu-item-active bg-white rounded-circle shadow" style={{marginLeft:0+'%'}}>
                                <div className="menu-item-active-ab">
                                    <a href="/promotion_category">
                                        <img className="" src="/assets/images/icon-gift.png" alt=""/> <br/>
                                        <span>DEAL</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="menu-item px-2 pt-2">
                            <a href="/account">
                                <img className="" src="/assets/images/icon-user.png" alt=""/> <br/>
                                <span>ACCOUNT</span>
                            </a>
                        </div>
                    </div>
                </div>
        
            </>
        )
    
        

    }
}
export default Footer;
