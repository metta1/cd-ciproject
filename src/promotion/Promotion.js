import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2'
import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import {Navigation} from '../template'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();


class Promotion extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        // this.getCoupon = this.getCoupon.bind(this);
    }

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        cate_name: "",
        rewardData: []
    };


    async componentDidMount(){

        const result = await axios.get(config.api.base_url+'/store_coupon/api/catecoupon/'+this.state.cat_id+'/'+sessionStorage.getItem('user_id')).catch(function (error) {
            if (error.response) {
              //console.log(error.response.status);
            }
        });
        if(result.status===200){
            console.log(result.data)
            this.setState({cate_name: result.data[0].catename});
            this.setState({data: result.data});
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id')+'&cate_id='+this.state.cat_id)
        if(rewardRs.status===200){
            this.setState({cate_name: rewardRs.data[0]['catename']});
            this.setState({rewardData: rewardRs.data});
        }
        console.log(this.state.rewardData);

        $('.btn-getcoupon, .btn-usecoupon').click(function(){
            let key = $(this).data('primary');
            let storeid = $(this).data('storeid');
            let couponid = $(this).data('couponid');
            let coupontype = $(this).data('coupontype');

            if (coupontype === 1) {
                $(this).css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
                $('body').addClass('modal-open')
                $('<div class="modal-backdrop fade show"></div>').appendTo('body');
                $('#couponModal-'+key).css({'display':'block'})
            } else {
                localStorage.setItem('storageStoreID',storeid);
                window.location = "/coupon_detail/"+couponid;
            }
        })

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    coudownTime(enddate){
        var countDownDate = new Date(enddate).getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            let timer = days + " วัน " + hours + " : " + minutes + " : " + seconds + " นาที";
            $('.popupExpireDate').html(timer)
            

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $('.popupExpireDate').html('หมดอายุ')
                //document.getElementById("demo").innerHTML = "EXPIRED";
            }
            }, 1000);
    }

    getCoupon(event,e) {
        event.preventDefault();
        const couponid = e.couponid;
        const storeid = e.storeid;
        const userid = sessionStorage.getItem('user_id');

        console.log(e.couponid)
        if (e.coupontype==="1") {
            const data = new FormData();
            data.set('couponid', couponid);
            data.set('storeid', storeid);
            data.set('userid', userid);
            axios({
                method: 'post',
                url: config.api.base_url+'/store_coupon/api/coupon',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data)
            })
            .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        }
        /* let minpurchase = (e.minpurchase).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.coudownTime(e.enddate)
        Swal.fire({
            html: `
            <div class="container-mobile m-auto">
            <div class="my-card" style="position: relative">
                <div class=" position-relative ">
                    <div id="user_coupon_detail">
                        <div class="coupon">
                            <div class="row w-100 m-0 mb-4" style="flex-wrap: wrap">
                                <div  class="col-12 p-0 pl-2 pr-2">
                                    <div class="position-relative">
                                        <img class="w-100" src="/assets/images/icons/icon-back@2x.png" alt=""/>
                                        <div class="coupon-detail row ml-1 h-100">
                                            <div class="col-4">
                                                <p class="mt-3 ml-2 mb-0" style="font-weight: normal;font-size: 1rem">${e.catename}</p>
                                                <div class="w-100 text-center mt-2">
                                                    <div class="coupon-detail-img rounded-circle mx-auto shadow">
                                                        <img class="" src=${e.storelogo} alt=""/>
                                                    </div>
                                                    <p class="mt-2" style="font-weight: normal;font-size: 1rem">${e.storename}</p>
                                                </div>
                                            </div>
                                            <div class="col-4 px-0 text-center mt-2 h-100">
                                                <p class="mt-2 mb-0">${e.couponname}</p>
                                                <h2>ลดราคา</h2>
                                                ${e.storebranch!== null ? (
                                                    `<small class="d-block" >สาขา : ${e.storebranch}</small>`
                                                ):(
                                                    <></>
                                                )}
                                                <small class="popupExpireDate" style="font-size: 0.9rem"></small>
                                            </div>
                                            <div class="col-4 p-0 text-center my-auto">
                                                <div class="coupon-detail-side ">
                                                <h1 class="m-0">${e.coupondiscount}${e.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                <h5>ขั้นต่ำ ${minpurchase} บาท</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            คุณได้รับดีลเรียบร้อย!
            `,
            confirmButtonText: '<a href="javascript: window.location=\'/shop/'+storeid+'\'" style="color:white;">เริ่มช็อปกันเลย!</a>',
            allowOutsideClick: () => { 
                // add your logic here and return boolean
                window.location='/promotion/'+this.state.cat_id;
            }
        }) */
    }

    useCoupon(e,data) {
        e.preventDefault();
        localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
    }

    handleChangeReward = (e,itemid,storeid) => {
        e.preventDefault();
        localStorage.setItem('storageStoreID',storeid);
        window.location='/reward_detail/'+itemid;
    }

    async couponDetail(e,data) {
        e.preventDefault();
        
        if (data.coupontype==="0" && data.couponused==="1") {
            console.log(data);
            localStorage.setItem('storageStoreID',data.storeid);
            window.location = "/coupon_detail/"+data.couponid;
        }
    }


    
    render() {
        const couponlist = {
            flexWrap: 'wrap'
        };
        const useCouponButton = {
            backgroundColor: '#12a508',
            height: '2.1rem'
        };
        const getCouponButton = {
            height: '2.1rem'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0',
            height: '2.1rem'
        };
        const coupon = {
            marginTop: '0.5rem',
            marginBottom: '5rem'
        };
        return (
            <div className="container-mobile m-auto">
                <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/promotion_category.css?v=4.1"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
                <div className="head bg-yellow shadow">
                    
                    <div className="row w-100 h-100 pl-3">
                        <div className="col-8 h-100" style={{whiteSpace: 'nowrap'}}>
                            <div className="icon-back">
                                <Link to="/all_promotion_category">
                                    <div className=" icon-promo-back">
                                        <img src="/assets/images/icons/icon-back@2x.png" alt=""/>
                                    </div>
                                </Link>
                            </div>
                            <div className=" h-100 p-3 pl-2">
                                <h3 className="align-title-promo">{this.state.cate_name}</h3> 
                            </div>
                        </div>
                        {/* <div className="col-2 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            
                        </div> */}
                    </div>

                </div>
                <div className="pt-5 mt-5" style={{position: 'initial'}}>
                    <div className="pt-3 position-relative ">
                        <div id="user_coupon_detail">
                        {
                            <div className="coupon" style={coupon}>
                                <div className="row w-100 m-0 mb-4" style={couponlist}>
                                {
                                    this.state.data === undefined ? (
                                        <div/>
                                    ) : (
                                    this.state.data.map((row,key) => (
                                        ((new Date(row.enddate).getTime() > nowDate) && (new Date(row.startdate).getTime() < nowDate) && row.couponstatus == "1") ? 
                                            <div  className="col-12 p-0 pl-2 pr-2 container-coupon">
                                                <div className="position-relative">
                                                    <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-4">
                                                            <p className="mt-3 ml-2 mb-0 text-overflow" style={{fontWeight: 'normal',fontSize: '1rem',whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden'}}>{row.catename}</p>
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                    <img className="" src={row.storelogo} alt=""/>
                                                                </div>
                                                                <p className="mt-2 text-overflow" style={{fontWeight: 'normal',fontSize: '1rem',whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden'}}>{row.storename}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 px-0 text-center mt-2 h-100">
                                                            <p className="mt-2 mb-0 text-overflow">{row.couponname}</p>
                                                            <h2>ลดราคา</h2>
                                                            {row.branchName!== null ? (
                                                                <small className="d-block" >สาขา : {row.branchName}</small>
                                                            ):(
                                                                <></>
                                                            )}
                                                            <small className="time-text-overflow" style={{fontSize: '0.9rem'}}><DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                            {row.statususer === "1" && row.couponused === "1" ? ( 
                                                                // <button className="btn btn-orange p-0 mt-1" style={usedCouponButton} disabled data-primary="cp-1"> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                                <button className="btn btn-orange p-0 mt-2" style={usedCouponButton} onClick={(e) => this.couponDetail(e,row)} data-primary={key}> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                            ) : row.statususer === "1" ? (
                                                                <button className="btn btn-orange p-0 mt-1 btn-usecoupon" style={useCouponButton} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}>
                                                                    <h5 className="m-0">ใช้คูปอง</h5>
                                                                </button>
                                                            ) : (
                                                                <div key={key}>
                                                                    <button className="btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}> <h5 className="m-0">รับคูปอง</h5></button>
                                                                </div>
                                                            )}
                                                        </div>
                                                        {/* MODAL COUPON */}
                                                        <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                            <div className="modal-dialog modal-dialog-centered" role="document">
                                                            <div className="modal-content w-75 mx-auto border-0">
                                                                <div className="modal-header bg-yellow">
                                                                <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                                    <div className="position-relative">
                                                                        <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                        <div className="coupon-detail row ml-1 h-100">
                                                                            <div className="col-4">
                                                                                <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                                <div className="w-100 text-center mt-2">
                                                                                    <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                                    </div>
                                                                                    <p className="mt-2">{row.storename}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-4 px-0 text-center mt-2 h-100">
                                                                                <p className="mt-2 mb-0">{row.couponname}</p>
                                                                                <h2>ลดราคา</h2>
                                                                                {row.branchName!== null ? (
                                                                                    <small className="d-block">สาขา : {row.branchName}</small>
                                                                                ):(
                                                                                    <></>
                                                                                )}
                                                                                <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                                <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                            </div>
                                                                            <div className="col-4 p-0 text-center my-auto">
                                                                                <div className="coupon-detail-side ">
                                                                                    <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                                    <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div></h5>
                                                                <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                                    <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                                </button>
                                                                </div>
                                                                <div className="modal-body text-center">
                                                                    <h4>คุณได้รับคูปองเรียบร้อย!</h4>
                                                                    <button className="btn bg-yellow w-75">
                                                                        <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 p-0 text-center my-auto">
                                                            <div className="coupon-detail-side ">
                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                                <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        : 
                                        <></>
                                    ))
                                    )
                                }
{
                            this.state.rewardData.length === 0 ? (
                                    <div/>
                            ) : (
                                this.state.rewardData.map(row => (
                                    <div className="col-12 p-0 pl-2 pr-2">
                                        <div className="position-relative">
                                            <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                            <div className="coupon-detail row ml-1 h-100">
                                                <div className="col-5">
                                                    <div className="w-100 text-center mt-2">
                                                        <div className="row w-100">
                                                            <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                            <p className="mt-3 ml-2 mb-0">{row.storename}</p>
                                                        </div>
                                                        <p className="mb-0 fs-40" style={{marginTop: '-0.5rem'}}><h2 className="mb-2">{row.itempoint}<br/>คะแนน</h2></p>
                                                    {
                                                        (parseFloat(row.itempoint) <= row.user_point) ? (
                                                            // <button className="btn coupon-btn btn-green p-10 mt-2" style = {{position:'relative', bottom: 9}} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                            (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                                // <button className="btn coupon-green-btn btn-secondary w-100 p-10 mt-0" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf">แลกแล้ว</p></button>
                                                                <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                { row.redeemtime_status === "1" ? 
                                                                <p className="m-0 font-tf">แลกแล้ว</p>
                                                                :
                                                                <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                }
                                                                </button>
                                                            ):(
                                                                <button className="btn coupon-green-btn btn-green p-10" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                            )
                                                        ) : (
                                                            <button className="btn coupon-btn btn-orange p-10" style = {{position:'relative', bottom: 10}}> <p className="m-0 font-tf">สะสมคะแนนเพิ่ม</p></button>
                                                        )
                                                    }
                                                    </div>
                                                </div>
                                                <div className="col-7 p-0 text-center mt-2">
                                                    <p className="mr-3 mb-0 text-right">{row.catename}</p>
                                                    <img className="coupon-pic" src={row.itemimg}  alt=""/>
                                                    <p className="m-0 px-3 pt-2 text-center">{row.itemname}</p>
                                                    {/* <p class="m-0 pl-3 pr-3 f-value"><h3>มูลค่า 1,750 บาท</h3></p> */}
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                ))
                            )
                        }
                                </div>
                            </div>
                        }
                        
                        </div>
                    </div>
                </div>
                
                    
                <Navigation/>

                
               
                 
            </div>
        )
    }
}
export default Promotion;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;