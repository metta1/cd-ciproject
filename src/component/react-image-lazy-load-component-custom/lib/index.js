"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./style.css");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var IMAGE_FADE_IN_CLASS = "iron-image--fade-in ";

var IronImage =
/*#__PURE__*/
function (_Component) {
  _inherits(IronImage, _Component);

  function IronImage(props) {
    var _this;

    _classCallCheck(this, IronImage);

    _this = _possibleConstructorReturn(this, (IronImage.__proto__ || Object.getPrototypeOf(IronImage)).call(this, props));
    _this.state = {
      imageLoadFinishedClass: "",
      placeholderStyle: {
        backgroundImage: "url(".concat(props.placeholder, ")"),
        backgroundSize: 'contain'
      }
    };
    _this.imageLoadHandler = _this.imageLoadHandler.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(IronImage, [{
    key: "imageLoadHandler",
    value: function imageLoadHandler() {
      this.setState({
        imageLoadFinishedClass: IMAGE_FADE_IN_CLASS
      });
    }
  }, {
    key: "render",
    value: function render() {
      let classContainer = this.props.classContainer !== undefined ? this.props.classContainer : '';
      return _react.default.createElement("div", {
        className: "iron-image__container "+classContainer,
        style: this.state.placeholderStyle
      }, _react.default.createElement("img", {
        className: "iron-image ".concat(this.state.imageLoadFinishedClass)+" "+this.props.classImage,
        alt: this.props.alt,
        src: this.props.src,
        style: this.state.imageStyle,
        onLoad: this.imageLoadHandler
      }));
    }
  }]);

  return IronImage;
}(_react.Component);

var _default = IronImage;
exports.default = _default;