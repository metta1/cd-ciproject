import React, { Component } from 'react'
import axios from 'axios'

class Post extends Component {
    constructor(){
        super();
        console.log('construct')
    }

    state = {
        data : []
    };


    async componentDidMount(){
        //console.log('componentDidMount')
        const result = await axios.get('https://jsonplaceholder.typicode.com/posts');
        
        this.setState({data: result.data});
        
    }

    onChange = event => {
        //console.log(event.target.value)
        this.props.onNameChange(event.target.value)//ส่งค่าไปเปลี่ยน state
    }



    render() {
        console.log('render');
        const name = this.props.name;//รับ props ที่ส่งมาจาก app
        return (
            <div>
                
              
                <table>
                    <tr>
                        <th>userId</th>
                        <th>Id</th>
                        <th>title</th>
                        <th>body</th>
                    </tr>
                {this.state.data.map(user => (
                    <tr>
                        <td>{user.userId}</td>
                        <td>{user.id}</td>
                        <td>{user.title}</td>
                        <td>{user.body}</td>
                    </tr>
                  
                ))}
                </table>
                
            </div>
        )
    }
}

export default Post;