import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Link} from 'react-router-dom'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class News extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        newsid : this.props.match.params.newsid
    };


    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/admin_news/api/news/'+this.state.newsid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    render() {

        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=2"/>
                <div className="head bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <Link to={"/home?v="+new Date().getTime()}>
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </Link>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <p></p>
                        <h3 className="m-0 "> ข่าวสารและโปรโมชั่น</h3> 
                    </div>
                </div>
                <div className="container-mobile m-auto ">
                    <div className="feed" style={{marginBottom: '4em'}}>
                        { this.state.data.map((row,key) => (
                        <div className="card w-100 border-0" key={key} style={{background: '#f8f8f8'}}>
                            <img className="w-100" src={row.newsimg}  alt=""/>
                            <p className="px-3 mb-1 mt-3 pl-5 pr-5"><h2>{row.newsname}</h2></p>
                            <p className="px-3 mt-3 pl-5 pr-5">{row.newsdesc}</p>
                        </div>
                        ))}
                    </div>
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default News;
