/* IMPORT node module */
import React, { Component } from 'react';
import {Route, Link, useParams, Redirect, Switch} from 'react-router-dom'
import {DatePicker} from 'react-datepicker'
/* import logo from './logo.svg'; */

/* IMPORT PAGE*/
import {Signin, Auth,Login,Intro, AuthOTP,Register, register_store, register_userstore, Interest, AuthLiff} from './auth'
import User from "./user/User";
import Post from "./post/Post";
import Request from "./lib/Request"
import Chatroom from './chat/Chatroom';
import Account from './user/Account'
import {User_card, User_card_touch, Edit_account, TakePhotoProfile, User_card_offline} from './user'
import UserForm from './user/UserForm'
// import User_card_offline from './user/User_card_offline'
import Usage_history from './usage_history/Usage_history'
import User_interest from './user_interest/User_interest'
import App_card from './card/App_card'
import Other_card_add from './card/Other_card_add'
import {Offlinecard_scan,Offlinecard_takescan, App_card_force, Offlinecard_add, Other_card_detail} from './card'
// import Promotion_category from './promotion/Promotion_category'
// import Promotion_list from './promotion/Promotion'
// import Promotion_detail from './promotion/Promotion_detail'
import {Promotion_category,Promotion_list,Promotion_detail,All_promotion_category} from './promotion'
import Feed_list from './feed/Feed_list'
import Feed_detail from './feed/Feed_detail'
import Store_feed_list from './feed/store/Feed_list'
import Store_feed_detail from './feed/store/Feed_detail'
import Shop from './shop/Shop_home'
import Shop_history from './shop/Shop_history'
import Product_detail from './shop/Product_detail'
import Cart from './shop/Cart'
import Cart_list from './shop/Cart_list'
import {PayType,Coupon,OrderHistory,Account_shopv1,Account_shop,Shop_about,Shop_search,OrderDetail,OnlineRewardDetail} from './shop'
import GetProductType from './shop/GetProductType';
import OrderShipingtype from './shop/Shiping_type';
import Order_address from './shop/Order_address';
import Order_formaddress from './shop/Order_formaddress';
import Product_category from './shop/Product_category'
import Product_list from './shop/Product_list'
import Product_list_recommend from './shop/Product_list_recommend'
import Reward_stamp from './shop/Reward_stamp'
import {Home, Home2} from './home'
import {Event_detail,Event_list} from './event'
import Coupon_list from './coupon/Coupon_list'
import Deal from './coupon/store/Coupon_list'
import Reward_detail from './coupon/store/Reward_detail'
import Obtain_reward from './coupon/store/Obtain_reward'
import Obtain_stamp_reward from './coupon/store/Obtain_stamp_reward'
import Coupon_offline from './coupon/store/Coupon_offline'
import Obtain_coupon from './coupon/store/Obtain_coupon'
import Help from './help/Help'
import Help_privacy_policy from './help/Help_privacy_policy'
import Help_privacy_condition from './help/Help_privacy_condition'
import Help_rules_of_use from './help/Help_rules_of_use'
import Faq from './faq/Faq'
import Ho_membership_card from './user/Ho_membership_card'
// import Setting from './setting/Setting'
// import Address from './setting/Address'
// import Add_address from './setting/Add_address'
// import Notification from './setting/Notification'
// import Notify from './setting/Notify'
import {Setting,Address,Add_address,Notification,Notify,Notify_email} from './setting'
import {News, News_list} from './news/'
import {Point} from './point'
import {Share} from './share'
import {Recommend_store} from './recommendstore'
import {Product_promotion} from './product'
import {Maps,Store_maps} from './map'
import {Notifications,Ho_notifications,Store_notifications} from './notifications'
import Page404 from './home/Page404'
import $ from 'jquery'
//import Qrcodescanner from './home/Qrcodescanner'
import {Qrcodescanner, Barcodescanner} from './codereader'
import axios from 'axios'
import {config} from './config';
import { inArray } from './lib/Helper';



class App extends Component {
  state = {
    name : 'app-ho.dreamnolimit.com',
    user_id : sessionStorage.getItem('user_id')
  }

  async componentDidMount(){
        
    $('#root').fadeIn(1000);

    // อัพเดทหน้าที่ user เข้าใช้ล่าสุด
    let _pageNotCallVisit = [
      '/signin',
      '/auth',
      '/logout',
      '/credit_transaction',
      '/credit_topup',
      '/credit_topup_bankdetail',
      '/credit_topup_upload',
      '/'
    ]
    let page_url = window.location.pathname;
    if ((this.state.user_id !== undefined && this.state.user_id !== null) && !inArray(page_url,_pageNotCallVisit)) {

      let bodyFormData = new FormData();
      bodyFormData.set('user_id',this.state.user_id)
      bodyFormData.set('visit',page_url+window.location.search)
      bodyFormData.set('store_id',localStorage.getItem('storageStoreID'))
      axios({
          method: 'post',
          url: config.api.base_url+'/users/api/uservisit',
          data: bodyFormData,
          headers: {'Content-Type': 'multipart/form-data' }
      })
      .then(function (response) {

      }).catch(function (response) {

      });
    }
 
    
  }

  onNameChange = name => {
    this.setState({ name : name })
  }
  render() {
  
    return (
      <div >
        
        <Switch>        
          <Route path="/signin" component={Signin} ></Route>        
          <Route path="/auth" component={Auth}></Route>{/* line */}
          <Route path="/authliff" component={AuthLiff}></Route>
          <Route path="/intro" component={Intro}></Route>
          <Route path="/authotp" component={AuthOTP}></Route>
          <Route path="/Home" component={Home}></Route>
          <Route path="/Home2" component={Home2}></Route>
          <Route path="/qrcodescanner" component={Qrcodescanner}></Route>
          <Route path="/barcodescanner" component={Barcodescanner}></Route>
          <Route path="/register" component={Register}></Route>
          <Route path="/register_store" component={register_store}></Route>
          <Route path="/register_userstore" component={register_userstore}></Route>
          <Route path="/interest" component={Interest}></Route>
          <Route path="/promotion_category" component={Promotion_category}></Route>
          <Route path="/all_promotion_category" component={All_promotion_category}></Route>
          <Route path="/promotion/:cat_id"  component={Promotion_list}></Route>
          <Route path="/promotion_detail/:cat_id/:promotion_id/"  component={Promotion_detail}></Route>
          <Route path="/account" component={Account}></Route>
          <Route path="/edit_account" component={Edit_account}></Route>
          <Route path="/TakePhotoProfile" component={TakePhotoProfile}></Route>
          <Route path="/user_card" component={User_card}></Route>
          <Route path="/user_card_touch" component={User_card_touch}></Route>
          <Route path="/user_card_offline" component={User_card_offline}></Route>
          <Route path="/usage_history" component={Usage_history}></Route>
          <Route path="/user_interest" component={User_interest}></Route>
          <Route path="/app_card" component={App_card}></Route>
          <Route path="/app_card_force/:storeid" component={App_card_force}></Route>{/* บังคับเพิ่มบัตรก่อนเข้าร้าน */}
          <Route path="/offlinecard_add/:offlinecardid" component={Offlinecard_add}></Route>
          <Route path="/offlinecard_scan/:offlinecardid" component={Offlinecard_scan}></Route>
          <Route path="/offlinecard_takescan/:offlinecardid" component={Offlinecard_takescan}></Route>
          <Route path="/shop/:store_id" component={Shop}></Route>
          <Route path="/shop_history/:store_id" component={Shop_history}></Route>
          <Route path="/product/category" component={Product_category}></Route>
          <Route path="/products_incategory/:store_id/:cat_id" component={Product_list}></Route>
          <Route path="/products/:store_id" component={Product_list}></Route>
          <Route path="/products_recommend/:store_id" component={Product_list_recommend}></Route>
          <Route path="/product/:store_id/:product_id" component={Product_detail}></Route>
          <Route path="/cart" component={Cart}></Route>
          <Route path="/cart_list" component={Cart_list}></Route>
          <Route path="/getProductType" component={GetProductType}></Route>
          <Route path="/order_history" component={OrderHistory}></Route>
          <Route path="/order_detail/:orderid" component={OrderDetail}></Route>
          <Route path="/OrderShipingtype" component={OrderShipingtype}></Route>
          <Route path="/OrderPayType" component={PayType}></Route>
          <Route path="/OrderCoupon" component={Coupon}></Route>
          <Route path="/order_address" component={Order_address}></Route>
          <Route path="/order_addaddress"  component={Order_formaddress}></Route>
          <Route path="/order_editaddress/:addressid"  component={Order_formaddress}></Route>
          <Route path="/Account_shop/"  component={Account_shop}></Route>
          <Route path="/Shop_about"  component={Shop_about}></Route>
          <Route path="/Shop_search"  component={Shop_search}></Route>
          <Route path="/other_card" component={Other_card_add}></Route>
          <Route path="/feed" component={Feed_list}></Route>
          <Route path="/feed_detail/:feed_id"  component={Feed_detail}></Route>
          <Route path="/store_feed/:store_id" component={Store_feed_list}></Route>
          <Route path="/store_feed_detail/:feed_id"  component={Store_feed_detail}></Route>
          <Route path="/posts" component={Post}></Route>
          <Route path="/chat" component={Chatroom}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/event_list"  component={Event_list}></Route>
          <Route path="/event_detail/:eventid"  component={Event_detail}></Route>
          <Route path="/coupon_list"  component={Coupon_list}></Route>
          <Route path="/deal"  component={Deal}></Route>
          <Route path="/reward_detail/:itemid"  component={Reward_detail}></Route>
          <Route path="/coupon_detail/:couponid"  component={Coupon_offline}></Route>
          <Route path="/help/"  component={Help}></Route>
          <Route path="/privacy_condition"  component={Help_privacy_condition}></Route>
          <Route path="/privacy_policy"  component={Help_privacy_policy}></Route>
          <Route path="/rules_of_use"  component={Help_rules_of_use}></Route>
          <Route path="/faq/"  component={Faq}></Route>
          <Route path="/ho_membership_card/"  component={Ho_membership_card}></Route>
          <Route path="/setting"  component={Setting}></Route>
          <Route path="/address"  component={Address}></Route>
          <Route path="/add_address"  component={Add_address}></Route>
          <Route path="/edit_address/:addressid"  component={Add_address}></Route>
          <Route path="/notification"  component={Notification}></Route>
          <Route path="/notify"  component={Notify}></Route>
          <Route path="/notify_email"  component={Notify_email}></Route>
          <Route path="/news_list"  component={News_list}></Route>
          <Route path="/news/:newsid"  component={News}></Route>
          <Route path="/point" component={Point}></Route>
          <Route path="/share" component={Share}></Route>
          <Route path="/recommendstore" component={Recommend_store}></Route>
          <Route path="/productpromotion" component={Product_promotion}></Route>
          <Route path="/maps" component={Maps}></Route>
          <Route path="/store_maps" component={Store_maps}></Route>
          <Route path="/notifications" component={Notifications}></Route>
          <Route path="/ho_notifications" component={Ho_notifications}></Route>
          <Route path="/store_notifications" component={Store_notifications}></Route>
          <Route path="/obtain_reward/:itemid" component={Obtain_reward}></Route>
          <Route path="/obtain_stamp_reward/:rewardid" component={Obtain_stamp_reward}></Route>
          <Route path="/obtain_coupon/:couponid" component={Obtain_coupon}></Route>
          <Route path="/other_card_detail/:otherid" component={Other_card_detail}></Route>
          <Route path="/Reward_stamp/:rewardid" component={Reward_stamp}></Route> 
          <Route path="/online_reward_detail/:pointid" component={OnlineRewardDetail}></Route>
          <Route path="/" component={Signin} ></Route>
        </Switch>
        
        

        {/* <Route path="/users" component={User}></Route>
        <Route path="/posts" component={Post}></Route> */}
        
       
      </div>

      
    );
  }
}

function Child() {
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  let { id } = useParams();

  return (
    <div>
      <h3>ID: {id}</h3>
    </div>
  );
}


export default App;
