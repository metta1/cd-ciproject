import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();

class Share extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        value : config.recommend.link,
        //copied: false,
        //user_id : sessionStorage.getItem('user_id'),
        usertel : getParam('re'),
        storeid : getParam('s')
    };

    handleBackPage = event => {
        if(getParam('s') !== undefined){//กลับไปหน้า account shop
            window.location=config.liff_auth_url+'?referer=account_shop/'+this.state.storeid+'&store='+this.state.storeid;
        }else{
            window.location=config.liff_auth_url+'?referer=account';
        }
    }

    /*
    async componentDidMount(){
       
        const userData =  await axios.get(config.api.base_url+'/api/userData/userid/'+this.state.user_id)
        if(userData.status===200){
            let rowData = userData.data;
            console.log(rowData)
            this.setState({
                usertel: rowData.usertel,               
            });
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    onCopy = () => {
        this.setState({copied: true});
    };
    */

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/share.css?v=3"/>
                <div class="container-mobile m-auto">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                                <a onClick={this.handleBackPage}>
                                    <div className="icon rounded-circle shadow">
                                        <img src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                        </div>

                        <div className=" h-100 p-3 pl-5">
                            <h3 className="share-title m-0">แชร์ให้เพื่อนของคุณเลย</h3>    
                        </div>     
                         
                    </div>

                    <div class="banner ban-share">
                        <img class="w-100 " src="/assets/images/banner.png" alt=""/>
                    </div>
                    <h3 >แชร์ให้เพื่อนวันนี้</h3>
                    <h3>คุณรับ 50 HO Point</h3>
                    <h3>เพื่อนคุณรับอีก 50 HO Point</h3>
                    <h2>WIN WIN!!!</h2>
                    <div class="group">
                        {/*
                        <input type="text" class="forms"  placeholder={this.state.value+'?re='+this.state.usertel} readOnly/>
                        <CopyToClipboard onCopy={this.onCopy} class="btn" text={this.state.value+'?re='+this.state.usertel}>
                            <button>คัดลอก</button>
                        </CopyToClipboard>
                        */}
                        <div class="line-it-button"  data-lang="th" data-type="share-a" data-ver="3" data-url={'ลอง Add Ho เป็นเพื่อนสิ แล้วจะเจอร้านดีๆ ดีลเด็ดๆ เพียบเลย กดที่ link เลย '+this.state.value+'?re='+this.state.usertel} data-color="default" data-size="large" data-count="false"></div>
                        <div class="fb-share-button" data-href={this.state.value+'?re='+this.state.usertel} data-layout="button" data-size="large"><a target="_blank" href={'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fliff.line.me%2F1654185466-nKkG6Xok%3Fre%3D'+this.state.usertel+'&amp;src=sdkpreparse'} class="fb-xfbml-parse-ignore">แชร์</a></div>
                    </div>
                    <section className="section text-center">
                        {this.state.copied ? <span style={{color: 'red'}}>Copied.</span> : null}
                    </section>

                    {/*<button type="button" class="btnShare">แชร์ให้เพื่อน</button>*/}
                </div>
            </div>    
        )
    }
}
export default Share;