import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import NumberFormat from 'react-number-format';
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import moment from 'moment';
import 'moment/locale/th';
import DateCountdown from 'react-date-countdown-timer';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import queryString from 'query-string'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Swal from 'sweetalert2'
import * as liff2 from '@line/liff';


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();





class Auth extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        sessionStorage.removeItem('registerStore')


        let referer        = getParam('referer');
        let registerStore  = getParam('registerStore');//จาก QRCode dashboard ร้าน
        let showHome       = getParam('showHome');// 0 = ซ่อนเมนู home
        let refer_url      = '';
        let store          = getParam('store')
        

        /********* ซ่อนเมนู Home *********/
        if(parseInt(registerStore) && showHome==='0'){
            sessionStorage.setItem('showHome',showHome)
            localStorage.setItem('storageStoreID',parseInt(registerStore));
        }else{
            sessionStorage.removeItem('showHome')
        }

        
        if(referer !== undefined && referer !== ''){
            refer_url = '/'+referer;
        }


        this.state = {
            displayName : '',
            userId : '',
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
            refer_url : refer_url,
            registerStore : registerStore == parseInt(registerStore) ? registerStore : undefined,
            store_id : store == parseInt(store) ? store : undefined
        };
        

        let site = getParam('site')
        if(site==='demo'){
            sessionStorage.setItem('user_id',146)  
        } 
        let user_id = sessionStorage.getItem('user_id');
        if(user_id==null){
            
        }else{
            /************ from qrcode store dashboard ***********/
            let obj = this
            if(obj.state.registerStore !== undefined){
               
                let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('storeid',obj.state.registerStore)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/api/card',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                    if(response.status===200 || response.status===201){
                        if(this.state.refer_url){
                            window.location=obj.state.refer_url;
                        }else{
                            window.location='/shop/'+obj.state.registerStore
                        }
                        
                    }else{
                        window.location=obj.state.refer_url;
                    }
                }).catch(function (response) {
                    console.log(response)
                    //window.location=obj.state.refer_url;
                });
                
            }else{
                //window.location=obj.state.refer_url;
                // this.handleVisitPage()
            
                if(this.state.refer_url!=='' && this.state.store_id!==0){
                    
                    /*** check member */
                    const userStoreData = axios.get(config.api.base_url+"/store_member/api/userStoreData/id/" +sessionStorage.getItem('user_id')+'/'+obj.state.store_id);
                    Promise.all([userStoreData]).then((res) => {
                        /*** set session */
                        localStorage.setItem('storageStoreID',obj.state.store_id);
                      
                        
                        if(res[0].status===200 && res[0].data.userid==sessionStorage.getItem('user_id')){//เป็นสมาชิกอยู่แล้ว
                            /**** ไปหน้าตาม URL  */
                            window.location=obj.state.refer_url
                        }else{
                            /********* ให้ไปหน้าสมัครสมาชิกร้านก่อน แต่ปิดไว้ให้สมัครออโต้ก่อน*/
                            //window.location='/register_userstore'
                            let bodyFormData = new FormData();
                            bodyFormData.set('userid',user_id)
                            bodyFormData.set('storeid',obj.state.store_id)
                            axios({
                                method: 'post',
                                url: config.api.base_url+'/api/card',
                                data: bodyFormData,
                                headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                if(response.status===200 || response.status===201){
                                    if(this.state.refer_url){
                                        window.location=obj.state.refer_url;
                                    }else{
                                        window.location='/shop/'+obj.state.registerStore
                                    }
                                    
                                }else{
                                    window.location=obj.state.refer_url;
                                }
                            }).catch(function (response) {
                                console.log(response)
                                //window.location=obj.state.refer_url;
                            });
                        }
                    });
                
              
                }else{

                    const uservisit = axios.get(config.api.base_url+'/users/api/uservisit/'+sessionStorage.getItem('user_id'));
                    Promise.all([uservisit]).then((values) => {
                        let responsedata = values[0];
                        if (responsedata.status === 200) {
                            if (this.state.refer_url !== "") {
                                window.location = this.state.refer_url;
                            } else {
                                localStorage.setItem('storageStoreID',responsedata.data[0]['store_id']);
                                localStorage.setItem('isFromLogin', "Y");
                                window.location = responsedata.data[0]['visit'];
                            }
                        } else {
                            window.location = this.state.refer_url;
                        }
                    });
                }
            }
            
            
        }

        this.initialize = this.initialize.bind(this);       
    }

    state = {
        couponData : [],
        specialpointData : [],
        feedData : []
    };
    
    async handleVisitPage(){
        const uservisit = await axios.get(config.api.base_url+'/users/api/uservisit/'+sessionStorage.getItem('user_id'));
        if(uservisit.status===200){
            if (this.state.refer_url !== "") {
                        window.location = this.state.refer_url;
            } else {
                        localStorage.setItem('storageStoreID',uservisit.data[0]['store_id']);
                        localStorage.setItem('isFromLogin', "Y");
                        window.location = uservisit.data[0]['visit'];
            }
        } else {
                    window.location="/home";
        }
    }

    async componentDidMount(){
        if(getParam('re') !== undefined){
            sessionStorage.setItem('registerRecommend',getParam('re'));
        }
        
        window.addEventListener('load', this.initialize);
      
         
    }

    initialize(){
        var error;
        liff2.init({ liffId: config.liff.auth.id }, () => {
            
            if (liff2.isLoggedIn()) {
              this.runApp()
            } else {
              liff2.login();
            }
        }, err => {
            Swal.fire({
                title : 'ขออภัย',
                html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาลองใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            }).then((result) => {
                if (result.value) {
                    window.location.reload();
                }
            })

            //console.error(err.code, err.message)
        }
        
            
        );


    }

    runApp(){
        let obj = this
        liff2.getProfile().then(profile => {
            const userProfile = profile.userId;
            const displayName = profile.displayName;
            const statusMessage = profile.statusMessage;
            const pictureUrl = profile.pictureUrl;
            const email = liff2.getDecodedIDToken().email;
            //alert(JSON.stringify(profile))
            //เก็บข้อมูลใน state
            this.setState({
                displayName : profile.displayName,
                userId : profile.userId,
                pictureUrl : profile.pictureUrl,
                statusMessage : profile.statusMessage
            });

            axios({
                method: 'get',
                url: config.api.base_url+'/api/userData/lineid/'+obj.state.userId,
                })
                .then(async function (result) {
                    sessionStorage.setItem('line_id',obj.state.userId)
                    if(result.status===200){
                        let user_id = result.data.userid;
                        sessionStorage.setItem('user_id',user_id)
                        
                        
                        /*******************/
                        if(obj.state.registerStore !== undefined){//register store
                            sessionStorage.setItem('registerStore',obj.state.registerStore);
                            //window.location='/register_userstore'
                            
                            let bodyFormData = new FormData();
                            bodyFormData.set('userid',user_id)
                            bodyFormData.set('storeid',obj.state.registerStore)
                            axios({
                                method: 'post',
                                url: config.api.base_url+'/api/card',
                                data: bodyFormData,
                                headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                if(response.status===200 || response.status===201){
                                    window.location='/shop/'+obj.state.registerStore
                                }else{
                                    window.location=obj.state.refer_url;
                                }
                            }).catch(function (response) {
                                window.location=obj.state.refer_url;
                            });
                            
                        }else{
                            
                            // window.location=obj.state.refer_url;
                           
                            if(obj.state.refer_url!=='' && obj.state.store_id!==0){
                               
                                /*** check member */
                                    const userStoreData = await axios.get(config.api.base_url+"/store_member/api/userStoreData/id/" +sessionStorage.getItem('user_id')+'/'+obj.state.store_id);
                               
                                    /*** set session */
                                    localStorage.setItem('storageStoreID',obj.state.store_id);
                                  
                                    
                                    if(userStoreData.status===200 && userStoreData.data.userid==sessionStorage.getItem('user_id')){//เป็นสมาชิกอยู่แล้ว
                                        /**** ไปหน้าตาม URL  */
                                        window.location=obj.state.refer_url
                                    }else{
                                        /********* ให้ไปหน้าสมัครสมาชิกร้านก่อน แต่ปิดไว้ให้สมัครออโต้ก่อน*/
                                        //window.location='/register_userstore'
                                        let bodyFormData = new FormData();
                                        bodyFormData.set('userid',user_id)
                                        bodyFormData.set('storeid',obj.state.store_id)
                                        axios({
                                            method: 'post',
                                            url: config.api.base_url+'/api/card',
                                            data: bodyFormData,
                                            headers: {'Content-Type': 'multipart/form-data' }
                                        })
                                        .then(function (response) {
                                            if(response.status===200 || response.status===201){
                                                if(obj.state.refer_url){
                                                    window.location=obj.state.refer_url;
                                                }else{
                                                    window.location='/shop/'+obj.state.store_id
                                                }
                                                
                                            }else{
                                                window.location=obj.state.refer_url;
                                            }
                                        }).catch(function (response) {
                                            console.log(response)
                                            //window.location=obj.state.refer_url;
                                        });
                                    }
                                
                            
                          
                            }else{
                                const uservisit = await axios.get(config.api.base_url+'/users/api/uservisit/'+sessionStorage.getItem('user_id'));
                                if(uservisit.status===200){
                                    if (obj.state.refer_url !== "") {
                                        window.location = obj.state.refer_url;
                                    } else {
                                        localStorage.setItem('storageStoreID',uservisit.data[0]['store_id']);
                                        window.location = uservisit.data[0]['visit'];
                                    }
                                } else {
                                    window.location = "/home";
                                }
                            }
                            
                            
                        }
                        /************************* */
                        
                    }else{
                        if(getParam('re') !== undefined){
                            sessionStorage.setItem('registerRecommend',getParam('re'));
                        }
        
                        /************ from qrcode store dashboard ***********/
                        if(obj.state.registerStore !== undefined){
                            sessionStorage.setItem('registerStore',obj.state.registerStore);
                            localStorage.setItem('storageStoreID',obj.state.registerStore);
                            window.location='/login';
                            //window.location='/register_userstore'
                        }else if(obj.state.store_id != undefined && obj.state.store_id != 0){
                            sessionStorage.setItem('registerStore',obj.state.store_id);
                            localStorage.setItem('storageStoreID',obj.state.store_id);
                            window.location='/login';
                        }else{
                            window.location='/intro';
                        }
        
                        
                        
        
                    }
                }).catch(function (response) {    
                    window.location='/intro';
            });


        }).catch(
              
            //err => alert(JSON.stringify(err))
            
        );
    }

            





   



    
    handleSubmit = event => {
    
        
    }
    
    render(props) {
       
       
    
        return (
          <>
            <link
              rel="stylesheet"
              type="text/css"
              href="/assets/css/auth.css?v=3"
            />

            <div className="container-mobile m-auto center">
              <div className="row w-100 m-0">
                {/* <div className="col-12 mt-150 w3-animate-right">
                        <img src="/assets/images/logohofirst.png"/>
                    </div>
                    <div className="col-12 mt-30 w3-animate-left">
                       <img className="" src="/assets/images/imgtextfirst.png"/>
                    </div> */}
                {/* <ul class="c">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul> */}
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          </>
        );
    }
}
export default Auth;
