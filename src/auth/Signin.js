import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import {Route, Link, Redirect} from 'react-router-dom'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import Cookies from 'universal-cookie';
import InputMask from 'react-input-mask';
import Swal from 'sweetalert2'
import {config} from '../config';
import { Store_color } from '../template';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { GoogleLogin } from 'react-google-login';
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const cookies = new Cookies();
const registerStore   = (getParam('store') !== '' && !isNaN(parseInt(getParam('store')))) ? parseInt(getParam('store')) : null


class Login extends Component {
    constructor(){
        super();
        
        if(parseInt(registerStore)){
            localStorage.setItem('storageStoreID',parseInt(registerStore));
        }else{
            localStorage.removeItem('storageStoreID');
        }

    }

    state = {
        name: '',
        username:'',
        password:'',
        usertel:'',
        agree:false,
        store_id: (registerStore),
        storeData:[],
        storeLogo:'',
        isVerifyOTP:true,
        currentPage:'username',
        disableSubmit : false
    }

    async componentDidMount() {
        //console.log(this.state.store_id)
        //localStorage.setItem('storageStoreID',122)
        const obj = this;
        if (this.state.store_id) {

          await axios({
            method: "get",
            url:
              config.api.base_url +
              "/store/api/store/" +
              this.state.store_id,
          })
            .then(function (response) {
                if (response.status == 200) {
                    //console.log(response)
                    obj.setState({
                        pdpa_condition: response.data.pdpa_condition,
                        storeLogo: response.data[0] ? response.data[0].storelogo : ''
                    });
                }else{
                    
                }
            })
            .catch(function (response) {
                
            });
        }else{
             
        }
    }
    
    handleChange = event => {
       if(event.target.name=='agree'){
        this.setState({agree: event.target.checked});
       }else if(event.target.name=='usertel'){
            let tel = (event.target.value).replace(/[^\d]/g, '');
            this.setState({[event.target.name]: tel});
       }else{
            this.setState({[event.target.name]: event.target.value});
       }
        
        //console.log(this.state)
    }

    responseFacebook = (response) => {
        const obj = this;
        if(response){
            let res = response;
            let facebookId = res.id;
            let imageUrl = res.picture.data.url;
            let email = res.email;
            let name  = res.name;
            let lname = '';
            obj.setState({
                disableSubmit:true
            })
            let bodyFormData = new FormData();
            bodyFormData.set('facebookId',facebookId)
            bodyFormData.set('email',email)
            bodyFormData.set('name',name)
            bodyFormData.set('lname',lname)
          
            axios({
                method: 'post',
                url: config.api.base_url+'/auth/api/facebooksignin',
                data: bodyFormData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function (response) {
                    //console.log(response)
                    if(response.status===200){
                        if(response.data.isOk){
                            let user_id = response.data.userid;
                            if(obj.state.store_id){
                                    let store_id2 = obj.state.store_id;
                                    let bodyFormDataCard2 = new FormData();
                                    bodyFormDataCard2.set('userid',user_id)
                                    bodyFormDataCard2.set('storeid',store_id2)
                                    axios({
                                        method: 'post',
                                        url: config.api.base_url+'/api/card',
                                        data: bodyFormDataCard2,
                                        headers: {'Content-Type': 'multipart/form-data' }
                                        })
                                    .then(function (response) {
                                            window.location='/account_shop/'+store_id2
                                })
                                .catch(function (response) {    
                                        window.location='/account_shop/'+store_id2
                                });
                            }else{
                                sessionStorage.setItem('user_id',response.data.userid)
                                localStorage.setItem('user_id',response.data.userid)  
                                window.location='/shop/4';
                            }
                            
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ระบบอยู่ระหว่างพัฒนา',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }
                        
                    }else if(response.status===201){//insert user
                        let user_id = response.data.userid;
                        /***** เพิ่ม HO Card */
                        let store_id = 4;
                        let bodyFormDataCard = new FormData();
                        bodyFormDataCard.set('userid',user_id)
                        bodyFormDataCard.set('storeid',store_id)
                        axios({
                            method: 'post',
                            url: config.api.base_url+'/api/card',
                            data: bodyFormDataCard,
                            headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                /***** เพิ่มบัตรร้าน */
                                if(obj.state.store_id){
                                    let store_id2 = obj.state.store_id;
                                    let bodyFormDataCard2 = new FormData();
                                    bodyFormDataCard2.set('userid',user_id)
                                    bodyFormDataCard2.set('storeid',store_id2)
                                    axios({
                                        method: 'post',
                                        url: config.api.base_url+'/api/card',
                                        data: bodyFormDataCard2,
                                        headers: {'Content-Type': 'multipart/form-data' }
                                        })
                                        .then(function (response) {
                                            window.location='/account_shop/'+store_id2
                                    })
                                    .catch(function (response) {    
                                        window.location='/account_shop/'+store_id2
                                    });
                                }else{
                                    window.location='/shop/4'
                                }
                        })
                        .catch(function (response) {    
                            
                        });

                    }else{
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ระบบอยู่ระหว่างพัฒนา',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-darkblue',
                            },
                        })
                    }
                    obj.setState({
                        disableSubmit:false
                    })
                })
            .catch(function (response) {
                    
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถเชื่อมต่อได้ กรุณาตรวจสอบอีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                    obj.setState({
                        disableSubmit:false
                    })
            });
        }
    }

    
    responseGoogle = (response) => {
        const obj = this;
        if(response.profileObj){
            let res = response.profileObj;
            let googleId = res.googleId;
            let imageUrl = res.imageUrl;
            let email = res.email;
            let name  = res.givenName;
            let lname = res.familyName;
            obj.setState({
                disableSubmit:true
            })
            let bodyFormData = new FormData();
            bodyFormData.set('googleId',googleId)
            bodyFormData.set('email',email)
            bodyFormData.set('name',name)
            bodyFormData.set('lname',lname)
          
            axios({
                method: 'post',
                url: config.api.base_url+'/auth/api/googlesignin',
                data: bodyFormData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function (response) {
                    //console.log(response)
                    if(response.status===200){
                        if(response.data.isOk){
                            let user_id = response.data.userid;
                            if(obj.state.store_id){
                                    let store_id2 = obj.state.store_id;
                                    let bodyFormDataCard2 = new FormData();
                                    bodyFormDataCard2.set('userid',user_id)
                                    bodyFormDataCard2.set('storeid',store_id2)
                                    axios({
                                        method: 'post',
                                        url: config.api.base_url+'/api/card',
                                        data: bodyFormDataCard2,
                                        headers: {'Content-Type': 'multipart/form-data' }
                                        })
                                    .then(function (response) {
                                            window.location='/account_shop/'+store_id2
                                })
                                .catch(function (response) {    
                                        window.location='/account_shop/'+store_id2
                                });
                            }else{
                                sessionStorage.setItem('user_id',response.data.userid)
                                localStorage.setItem('user_id',response.data.userid)  
                                window.location='/shop/4';
                            }
                            
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ระบบอยู่ระหว่างพัฒนา',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }
                        
                    }else if(response.status===201){//insert user
                        let user_id = response.data.userid;
                        /***** เพิ่ม HO Card */
                        let store_id = 4;
                        let bodyFormDataCard = new FormData();
                        bodyFormDataCard.set('userid',user_id)
                        bodyFormDataCard.set('storeid',store_id)
                        axios({
                            method: 'post',
                            url: config.api.base_url+'/api/card',
                            data: bodyFormDataCard,
                            headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                /***** เพิ่มบัตรร้าน */
                                if(obj.state.store_id){
                                    let store_id2 = obj.state.store_id;
                                    let bodyFormDataCard2 = new FormData();
                                    bodyFormDataCard2.set('userid',user_id)
                                    bodyFormDataCard2.set('storeid',store_id2)
                                    axios({
                                        method: 'post',
                                        url: config.api.base_url+'/api/card',
                                        data: bodyFormDataCard2,
                                        headers: {'Content-Type': 'multipart/form-data' }
                                        })
                                        .then(function (response) {
                                            window.location='/account_shop/'+store_id2
                                    })
                                    .catch(function (response) {    
                                        window.location='/account_shop/'+store_id2
                                    });
                                }else{
                                    window.location='/shop/4'
                                }
                        })
                        .catch(function (response) {    
                            
                        });

                    }else{
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ระบบอยู่ระหว่างพัฒนา',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-darkblue',
                            },
                        })
                    }
                    obj.setState({
                        disableSubmit:false
                    })
                })
            .catch(function (response) {
                    
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถเชื่อมต่อได้ กรุณาตรวจสอบอีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                    obj.setState({
                        disableSubmit:false
                    })
            });
        }else{
            /*Swal.fire({
                title : 'ขออภัย',
                html: 'พบข้อผิดพลาด กรุณาลองใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })*/
        }
    }


    
  
  
    handleSubmit = event => {
        event.preventDefault();
        let currentPage = this.state.currentPage;
        const obj = this;
        if(currentPage==='tel'){
            if(this.state.usertel.length<9){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }else if(!this.state.agree){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณายอมรับข้อกำหนดและเงื่อนไข',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }else{
                
                if(this.state.usertel){
                    sessionStorage.setItem('registerTel',this.state.usertel);
                    //window.location='/authotp';
                    if(this.state.isVerifyOTP){
                        let bodyFormData = new FormData();
                        bodyFormData.set('usertel',this.state.usertel)
                        axios({
                            method: 'post',
                            url: config.api.base_url+'/api/sendotp',
                            data: bodyFormData,
                            headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                if(response.status===200){
                                    let otpid = response.data.otpid
                                    window.location='/authotp?token='+otpid;
                                }else{
                                    Swal.fire({
                                        title : 'ขออภัย',
                                        html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                                        icon:'error',
                                        confirmButtonText:'ตกลง',
                                        customClass: {
                                            confirmButton: 'btn-darkblue',
                                        },
                                    })
                                }
                                //console.log(response);
                            })
                            .catch(function (response) {
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                        });
                    }else{
                        window.location='/register';
                    }
                }else{
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'กรุณากรอกข้อมูลให้ครบถ้วน',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                }
                
                
            } 
        }else{
           
            if(!this.state.username){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณากรอกอีเมล',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }else if(!this.state.password){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณากรอกรหัสผ่าน',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }else if(!this.state.agree){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณายอมรับข้อกำหนดและเงื่อนไข',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }else{
                obj.setState({
                    disableSubmit:true
                })
                let bodyFormData = new FormData();
                bodyFormData.set('username',this.state.username)
                bodyFormData.set('password',this.state.password)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/auth/api/signin',
                    data: bodyFormData,
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function (response) {
                        //console.log(response)
                        if(response.status===200){
                            if(response.data.isOk){
                                sessionStorage.setItem('user_id',response.data.userid)
                                localStorage.setItem('user_id',response.data.userid)  
                                window.location='/shop/4';
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'email หรือ password ไม่ถูกต้อง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                            }
                            
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'email หรือ password ไม่ถูกต้อง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }
                        obj.setState({
                            disableSubmit:false
                        })
                    })
                    .catch(function (response) {
                        
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-darkblue',
                            },
                        })
                        obj.setState({
                            disableSubmit:false
                        })
                });
            }
        }
       
      
        
    }
    

    render() {

        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/login.css?v=3"/>
            <link rel="stylesheet" type="text/css" href="/assets/css/signin.css?v=3"/>
            {
                this.state.store_id &&(
                    <Store_color/>
                )
            }
            
            <div className="container-mobile m-auto bg-yellow">
                <div className="shadow header text-center" >
                    {
                        this.state.storeLogo !== '' ? (
                            <>
                            <img src={this.state.storeLogo} alt="logo" className="logo"/>
                            </>
                        ):(
                            <>
                            <img src="/assets/images/logo.png?v=2" alt="logo" className="logo"/>
                            <h2 className="title-header">HO CRM</h2>
                            </>
                        )
                    }
                </div>
                <div className="account p-4">
                <div className="shadow rounded-lg bg-white pb-3">
                    <div className="container px-4 py-2 ">
                        {
                            this.state.currentPage=='tel' ? (
                        
                                <div className="text-center ">
                                    <h3 className="text-center head text-grey " >เข้าสู่ระบบด้วยโทรศัพท์</h3>
                                    <form method="POST" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                        <InputMask mask="999-999-9999" maskChar={null}  
                                        name="usertel"
                                        className="form text-grey"
                                        placeholder="เบอร์โทรศัพท์"
                                        onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" id="exampleCheck1" name="agree"  onChange={this.handleChange} value="1"/>
                                            <label 
                                            className="form-check-label text-grey link" 
                                            htmlFor="exampleCheck1" >
                                            ยอมรับ
                                        </label> 
                                            <Link className="link"
                                                to={{
                                                    pathname: "/privacy_policy",
                                                    state: { prevPath: this.props.location.pathname }
                                                }}
                                                >ข้อกำหนดและเงื่อนไข
                                            </Link>
                                                
                                        </div>
                                        {
                                            this.state.disableSubmit ? (
                                                <button type="button" className="btn btn-signin" disabled>กรุณารอสักครู่</button>
                                            ) : (
                                                <button type="button" className="btn btn-signin" onClick={this.handleSubmit}>เข้าสู่ระบบ</button>
                                            )
                                        }
                                    </form>
                                    <div className="text-center mt-2 signin-choice-container">
                                        <label>หรือเข้าสู่ระบบด้วย</label>
                                        <ul className="icon-signin-choice">
                                            <FacebookLogin
                                                appId={config.fbLogin.appId}
                                                reauthenticate={true}
                                                version={'12.0'}
                                                callback={this.responseFacebook}
                                                fields="name,email,picture"
                                                render={renderProps => (
                                                    <li onClick={renderProps.onClick}>
                                                        <img src="/assets/images/icon-fb.svg"/>
                                                        <span>FACEBOOK</span>
                                                    </li>
                                            )}/>
                                            <GoogleLogin
                                                clientId={config.googleLogin.clientId}
                                                render={renderProps => (
                                                    <li onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                                        <img src="/assets/images/icon-gmail.svg"/>
                                                        <span>GMAIL</span>
                                                    </li>
                                                )}
                                                buttonText="Login"
                                                onSuccess={this.responseGoogle}
                                                onFailure={this.responseGoogle}
                                                cookiePolicy={'single_host_origin'}
                                            />
                                            
                                            <li onClick={()=> {
                                                this.setState({
                                                    currentPage : 'username'
                                                })
                                            }}>
                                                <img src="/assets/images/icon-email.svg"/>
                                                <span>EMAIL</span>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div> 
                        
                            ) : (
                                <div className="text-center ">
                                    <h3 className="text-center head text-grey " >เข้าสู่ระบบ</h3>
                                    <form method="POST" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <input type="text" className="form text-grey" name="username" placeholder="กรุณากรอกอีเมล" onChange={this.handleChange} value={this.state.username}/>
                                        </div>
                                        <div className="form-group">
                                            <input type={this.state.password !== '' ? 'password' : 'text'} className="form text-grey" name="password" placeholder="รหัสผ่าน" onChange={this.handleChange} value={this.state.password} autoComplete="off"/>
                                        </div>
                                        
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" id="exampleCheck1" name="agree"  onChange={this.handleChange} value="1"/>
                                            <label 
                                            className="form-check-label text-grey link" 
                                            htmlFor="exampleCheck1" >
                                            ยอมรับ
                                        </label> 
                                            <Link className="link"
                                                to={{
                                                    pathname: "/privacy_policy",
                                                    state: { prevPath: this.props.location.pathname }
                                                }}
                                                >ข้อกำหนดและเงื่อนไข
                                            </Link>
                                                
                                        </div>
                                        {
                                            this.state.disableSubmit ? (
                                                <button type="button" className="btn btn-signin" disabled>กรุณารอสักครู่</button>
                                            ) : (
                                                <button type="button" className="btn btn-signin" onClick={this.handleSubmit}>เข้าสู่ระบบ</button>
                                            )
                                        }
                                        
                                    </form>
                                    <div className="text-center mt-2 signin-choice-container">
                                        <label>หรือเข้าสู่ระบบด้วย</label>
                                        <ul className="icon-signin-choice">
                                            <FacebookLogin
                                                appId={config.fbLogin.appId}
                                                reauthenticate={true}
                                                version={'12.0'}
                                                callback={this.responseFacebook}
                                                fields="name,email,picture"
                                                render={renderProps => (
                                                    <li onClick={renderProps.onClick}>
                                                        <img src="/assets/images/icon-fb.svg"/>
                                                        <span>FACEBOOK</span>
                                                    </li>
                                            )}/>
                                            <GoogleLogin
                                                clientId={config.googleLogin.clientId}
                                                render={renderProps => (
                                                    <li onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                                        <img src="/assets/images/icon-gmail.svg"/>
                                                        <span>GMAIL</span>
                                                    </li>
                                                )}
                                                buttonText="Login"
                                                onSuccess={this.responseGoogle}
                                                onFailure={this.responseGoogle}
                                                cookiePolicy={'single_host_origin'}
                                            />
                                            <li onClick={()=> {
                                                this.setState({
                                                    currentPage : 'tel'
                                                })
                                            }}>
                                                <img src="/assets/images/icon-otp.svg"/>
                                                <span>EMAIL</span>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div> 
                            )
                        }
                    </div>
                </div>
            </div>
        
            </div>
            </>
        )
    }
}
export default Login;
