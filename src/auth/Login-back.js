import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import {Route, Link, Redirect} from 'react-router-dom'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import SweetAlert from "react-bootstrap-sweetalert";
import Cookies from 'universal-cookie';

const required = value => value ? undefined : 'Required'
const cookies = new Cookies();



class Register extends Component {
    constructor(){
        super();

    }

    state = {
        name: '',
        password:'',
        password2:''
    }
    
    handleChange = event => {
        console.log(event.target.name)
        //this.setState({ name: event.target.value });
        this.setState({[event.target.name]: event.target.value});
    }
    
    handleSubmit = event => {
        
        //console.log($('input[name="name"').val()+'fff')
        $('#submit').prop('disabled',true);
        
        event.preventDefault();
       
      
            const user = {
                username: this.state.email,
                password: this.state.password
              };
      
          
              axios.post(`https://jsonplaceholder.typicode.com/users`, { user })
                .then(res => {
                  //console.log(res);
                  //console.log(res.data);
                  cookies.set('access_token', 'userID='+user.username, { path: '/' });
                  console.log(cookies.get('access_token')); 
                  return window.location='/account';
                  $('#submit').prop('disabled',false);
                
            })
        
        
    }
    

    render() {

        return (
            <div>
                <script src="https://unpkg.com/react/umd/react.production.min.js" ></script>
                <script
                src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
                ></script>

                <script
                src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
                ></script>
                <div class="container">
                    <div class="row text-left">
                        <div className="col-12 mt-5">
                        <dic className="card">
                            <div className="card-header text-center">
                                <h5 className="m-0">เข้าสู่ระบบด้วย Email</h5>
                            </div>
                            <div className="card-body text-center">
                                <Form onSubmit={this.handleSubmit}>
                                    <Form.Group controlId="formBasicEmail"> 
                                        <Form.Control type="email" placeholder="Email" required name="email" onChange={this.handleChange}/>
                                    </Form.Group>
                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Control type="password" placeholder="รหัสผ่าน" required name="password" onChange={this.handleChange} />
                                    </Form.Group>

                                    <Button variant="primary" type="submit" id="submit">
                                        เข้าสู่ระบบ
                                    </Button>
                                </Form>
                                    <div className="row">
                                        <div className="col-6 text-right">
                                            ลืมรหัสผ่าน
                                        </div>
                                        <div className="col-6 text-left">
                                            <Link to="/register">ลงทะเบียนผู้ใช้งานใหม่</Link>
                                        </div>
                                    </div>
                                
                            </div>
                        </dic>
               
                        

                        </div>
                    </div>
                </div>
                 
            </div>
        )
    }
}
export default Register;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;