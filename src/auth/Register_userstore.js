import React, { Component } from "react";
import axios from "axios";
import withRequest from "../lib/withRequest";
import { Route, Link, Redirect } from "react-router-dom";
import { Form, Well, Button, FormGroup, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Field, reduxForm } from "redux-form";
import $ from "jquery";
import SweetAlert from "react-bootstrap-sweetalert";
import Cookies from "universal-cookie";
import { config } from "../config";
import queryString from "query-string";
import Swal from "sweetalert2";
import moment from "moment";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import th from "date-fns/locale/th"; // the locale you want
import validate from "jquery-validation";
import InputMask from "react-input-mask";
import {
  getParam,
  inArray,
  readURL,
  getSizeFromImgDataURL,
  validateEmail,
} from "../lib/Helper";

registerLocale("th", th); // register it with the name you want

const required = (value) => (value ? undefined : "Required");
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[
  currentURLArray.length - 1
].toLowerCase();
const registerRecommend = sessionStorage.getItem("registerRecommend");

class Login extends Component {
  constructor(props) {
    //เริ่มต้น run component
    super(props);
    this.handleRequestOTP = this.handleRequestOTP.bind(this);

    let locationSearch = this.props.location.search;
    let getParam = queryString.parse(locationSearch);
    let refer_url = "/home";
    if (getParam.referer !== undefined && getParam.referer !== "") {
      refer_url = "/" + getParam.referer;
    }
    let registerTel = sessionStorage.getItem("registerTel");
    //sessionStorage.setItem("registerStore", 83);
    this.state = {
      registerStore: sessionStorage.getItem("registerStore"),
      storeData: [],
      displayName: "",
      lineid: "",
      pictureUrl: "assets/images/profile.svg",
      statusMessage: "",
      refer_url: refer_url,
      userid: null,
      fname: "",
      lname: "",
      email: "",
      password: "",
      password2: "",
      usertel: null,
      usertelold: null, //เก็บเบอร์เผื่อ user แก้ไข
      recommend:
        registerRecommend !== undefined && registerRecommend !== null
          ? registerRecommend
          : undefined,
      waitingData: "Y",
      birthday: "",
      gender: "",
      currentPage: "",
      timeRequestOTP: 60,
      validateOtpSection: 1,
      otp1: "",
      otp2: "",
      otp3: "",
      otp4: "",
      otp5: "",
      otp6: "",
      envDev: false,
      wantValidateOtp: true,
      storeColorData: {
        colortabbar: "",
        coloricon: "",
        colorfont: "",
      },
      disableForm:true
    };

    this.initialize = this.initialize.bind(this);
    this.getUserByTel = this.getUserByTel.bind(this);
  }

  countDownTimer() {
    const _this = this;
    var refreshId = setInterval(function () {
      _this.setState({
        timeRequestOTP: _this.state.timeRequestOTP - 1,
      });
      if (_this.state.timeRequestOTP < 1) {
        clearInterval(refreshId);
      }
    }, 1000);
  }

  setStatePage = (page) => {
    this.setState({
      currentPage: page,
    });
  };

  handleChangeColor = () => {
    if (this.state.storeColorData) {
      let colortabbar = this.state.storeColorData["colortabbar"];
      let coloricon = this.state.storeColorData["coloricon"];
      let colorfont = this.state.storeColorData["colorfont"];
      if (colortabbar && coloricon && colorfont) {
        console.log("d");
        $(
          ".register-header, .register-header.logo, .btn-register, .btn-register, .button-submit, .btn-yellow "
        ).css("background-color", "#" + colortabbar);
        $(".border-top-store").css("border-color", "#" + colortabbar);
        $(".text-store-color, .btn-register").css("color", "#" + colorfont);
      }
    }
  };

  handleResetPage = () => {
    this.setState({
      currentPage: null,
    });
    this.handleChangeColor();
  };

  handleChangePage = (page) => {
    this.handleChangeColor();
    //console.log(page);]
    $(".digit-group")
      .find("input")
      .each(function () {
        //$(this).attr('maxlength', 1);
        $(this).on("keyup", function (e) {
          var parent = $($(this).parent());
          if (e.keyCode) {
            $("#log").val(e.keyCode);
            var next = parent.find("input#" + $(this).data("next"));
            if (next.length) {
              $(next).select();
            }
          }
        });
      });

    switch (page) {
      case "formpassword":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12 bg-white">
                <h2 className="text-center mt-4">ตั้ง PIN 6 หลัก</h2>
                <form id="formPassword">
                  <div className="row">
                    <div className="inputDisplayBox">
                      <input
                        type="text"
                        name="displayPass"
                        id="telNumber"
                        className="form-control displayInput3"
                        readOnly="readonly"
                        value={this.state.password}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="num-pad num-pad1">
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 1)}
                        >
                          <div className="txt">1</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 2)}
                        >
                          <div className="txt">2</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 3)}
                        >
                          <div className="txt">3</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 4)}
                        >
                          <div className="txt">4</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 5)}
                        >
                          <div className="txt">5</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 6)}
                        >
                          <div className="txt">6</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 7)}
                        >
                          <div className="txt">7</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 8)}
                        >
                          <div className="txt">8</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 9)}
                        >
                          <div className="txt">9</div>
                        </div>
                      </div>
                      <div className="col-xs-4"></div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) => this.handleClickNumPad("password", 0)}
                        >
                          <div className="txt">0</div>
                        </div>
                      </div>
                      <div className="col-xs-4">
                        <div
                          className="num"
                          onClick={(e) =>
                            this.handleClickNumPad("password", "del")
                          }
                        >
                          <div className="txt del">
                            <i
                              className="fa fa-chevron-left"
                              aria-hidden="true"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="clearfix"></div>
                    <br />
                  </div>
                  <div className="row">
                    <div
                      className="w-100 footer mx-auto shadow button-submit text-store-color"
                      style={{
                        color: "#" + this.state.storeColorData["colorfont"],
                      }}
                    >
                      <div
                        className="btn-group w-100 border-top-store"
                        style={{
                          borderColor:
                            "#" + this.state.storeColorData["colortabbar"],
                        }}
                      >
                        <button
                          className="btn btn-footer btn-light"
                          type="button"
                          onClick={() => {
                            this.handleResetPage();
                          }}
                        >
                          กลับ
                        </button>
                        <button
                          type="button"
                          className="btn btn-footer text-store-color"
                          style={{
                            color: "#" + this.state.storeColorData["colorfont"],
                            backgroundColor:
                              "#" + this.state.storeColorData["colortabbar"],
                          }}
                          onClick={(e) => {
                            if (this.state.password.length < 6) {
                              Swal.fire({
                                title: "ขออภัย",
                                html: "กรุณาตั้ง PIN อย่างน้อย 6 หลัก",
                                icon: "info",
                                confirmButtonText: "ตกลง",
                                customClass: {
                                  confirmButton: "btn-darkblue",
                                },
                              });
                            } else {
                              this.setState({
                                currentPage: "formpassword2",
                              });
                            }
                          }}
                        >
                          ถัดไป
                        </button>
                      </div>
                    </div>
                  </div>

                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        );
        break;
      case "formpassword2":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12 bg-white">
                <h2 className="text-center mt-4">กรอก PIN อีกครั้ง</h2>

                <div className="row">
                  <div className="inputDisplayBox">
                    <input
                      type="text"
                      name="displayPass"
                      id=""
                      className="form-control displayInput3"
                      readOnly="readonly"
                      value={this.state.password2}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="num-pad num-pad1">
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 1)}
                      >
                        <div className="txt">1</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 2)}
                      >
                        <div className="txt">2</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 3)}
                      >
                        <div className="txt">3</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 4)}
                      >
                        <div className="txt">4</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 5)}
                      >
                        <div className="txt">5</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 6)}
                      >
                        <div className="txt">6</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 7)}
                      >
                        <div className="txt">7</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 8)}
                      >
                        <div className="txt">8</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 9)}
                      >
                        <div className="txt">9</div>
                      </div>
                    </div>
                    <div className="col-xs-4"></div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) => this.handleClickNumPad("password2", 0)}
                      >
                        <div className="txt">0</div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div
                        className="num"
                        onClick={(e) =>
                          this.handleClickNumPad("password2", "del")
                        }
                      >
                        <div className="txt del">
                          <i
                            className="fa fa-chevron-left"
                            aria-hidden="true"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="clearfix"></div>
                  <br />
                </div>
                <div className="row">
                  <div
                    className="w-100 footer mx-auto shadow button-submit text-store-color"
                    style={{
                      color: "#" + this.state.storeColorData["colorfont"],
                    }}
                  >
                    <div
                      className="btn-group w-100 border-top-store"
                      style={{
                        borderColor:
                          "#" + this.state.storeColorData["colortabbar"],
                      }}
                    >
                      <button
                        className="btn btn-footer btn-light"
                        type="button"
                        onClick={() => {
                          this.handleChangePage("formpassword");
                        }}
                      >
                        กลับ
                      </button>
                      <button
                        type="button"
                        className="btn btn-footer text-store-color"
                        style={{
                          color: "#" + this.state.storeColorData["colorfont"],
                          backgroundColor:
                            "#" + this.state.storeColorData["colortabbar"],
                        }}
                        onClick={(e) => {
                          if (this.state.password !== this.state.password2) {
                            Swal.fire({
                              title: "ขออภัย",
                              html: "กรุณาตั้ง PIN ให้ตรงกัน",
                              icon: "info",
                              confirmButtonText: "ตกลง",
                              customClass: {
                                confirmButton: "btn-darkblue",
                              },
                            });
                          } else {
                            this.setState({
                              timeRequestOTP: 60,
                            });
                            this.handleSendOTP(this.state.usertel);
                          }
                        }}
                      >
                        รับรหัส OTP
                      </button>
                    </div>
                  </div>
                </div>

                <div className="clearfix"></div>
              </div>
            </div>
          </div>
        );
        break;
      case "otp":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12 bg-white">
                <h2 className="text-center mt-4">ใส่รหัสยืนยัน</h2>
                <div className="rows digit-group text-center">
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp1"
                    name="otp1"
                    data-next="otp2"
                    value={this.state.otp1}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp2"
                    name="otp2"
                    data-next="otp3"
                    data-previous="otp1"
                    value={this.state.otp2}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp3"
                    name="otp3"
                    data-next="otp4"
                    data-previous="otp2"
                    value={this.state.otp3}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp4"
                    name="otp4"
                    data-next="otp5"
                    data-previous="otp3"
                    value={this.state.otp4}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp5"
                    name="otp5"
                    data-next="otp6"
                    data-previous="otp4"
                    value={this.state.otp5}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp6"
                    name="otp6"
                    data-previous="otp5"
                    value={this.state.otp6}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                </div>
                <div className="w-100 text-center mt-4">
                  <button
                    type="button"
                    className="btn btn-success"
                    style={{ padding: "7px 30px" }}
                    onClick={(e) => {
                      let state = this.state;
                      let otpcode = "";
                      otpcode =
                        state.otp1 +
                        state.otp2 +
                        state.otp3 +
                        state.otp4 +
                        state.otp5 +
                        state.otp6;
                      if (otpcode < 6) {
                        Swal.fire({
                          title: "ขออภัย",
                          html: "กรุณากรอก OTP อย่างน้อย 6 หลัก",
                          icon: "info",
                          confirmButtonText: "ตกลง",
                          customClass: {
                            confirmButton: "btn-darkblue",
                          },
                        });
                      } else {
                        this.handleSubmitOtp(e);
                      }
                    }}
                  >
                    ตกลง
                  </button>
                </div>
                <div className="mt-1">
                  {this.state.timeRequestOTP < 1 ? (
                    <>
                      <p
                        className="text-center text-primary link"
                        onClick={this.handleRequestOTP}
                      >
                        รับ OTP ใหม่อีกครั้ง
                      </p>
                      {this.state.validateOtpSection === 2 && (
                        <>
                          <p className="text-center">หรือ</p>
                          <p
                            className="text-center text-primary link"
                            onClick={() => {
                              this.setState({
                                currentPage: "formemail",
                              });
                            }}
                          >
                            ยืนยันตัวตนด้วยอีเมล
                          </p>
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <p className="text-center text-secondary">
                        รับ OTP ใหม่อีกครั้ง ({this.state.timeRequestOTP} วิ)
                      </p>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        );
        break;
      case "otpEmail":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12 bg-white">
                <h2 className="text-center mt-4">ยืนยันตัวตนด้วยอีเมล</h2>
                <p className="text-center text-secondary">
                  <>
                    ระบบได้ส่งอีเมลยืนยันตัวตนให้คุณแล้ว
                    <br />
                    กรุณาตรวจสอบอีเมลของคุณ
                    <br />
                  </>
                </p>
                <p className="text-center">{this.state.email}</p>

                <div className="rows digit-group text-center">
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp1"
                    name="otp1"
                    data-next="otp2"
                    value={this.state.otp1}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp2"
                    name="otp2"
                    data-next="otp3"
                    data-previous="otp1"
                    value={this.state.otp2}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp3"
                    name="otp3"
                    data-next="otp4"
                    data-previous="otp2"
                    value={this.state.otp3}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp4"
                    name="otp4"
                    data-next="otp5"
                    data-previous="otp3"
                    value={this.state.otp4}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp5"
                    name="otp5"
                    data-next="otp6"
                    data-previous="otp4"
                    value={this.state.otp5}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp6"
                    name="otp6"
                    data-previous="otp5"
                    value={this.state.otp6}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                </div>
                <div className="w-100 text-center mt-4">
                  <button
                    type="button"
                    className="btn btn-success"
                    style={{ padding: "7px 30px" }}
                    onClick={(e) => {
                      let state = this.state;
                      let otpcode = "";
                      otpcode =
                        state.otp1 +
                        state.otp2 +
                        state.otp3 +
                        state.otp4 +
                        state.otp5 +
                        state.otp6;
                      if (otpcode < 6) {
                        Swal.fire({
                          title: "ขออภัย",
                          html: "กรุณากรอก OTP อย่างน้อย 6 หลัก",
                          icon: "info",
                          confirmButtonText: "ตกลง",
                          customClass: {
                            confirmButton: "btn-darkblue",
                          },
                        });
                      } else {
                        this.handleSubmitOtp(e);
                      }
                    }}
                  >
                    ตกลง
                  </button>
                </div>
                <div className="mt-1">
                  {this.state.timeRequestOTP < 1 ? (
                    <>
                      <p
                        className="text-center text-primary link"
                        onClick={() => {
                          this.setState({
                            currentPage: "formemail",
                          });
                        }}
                      >
                        รับรหัสใหม่อีกครั้ง
                      </p>

                      <p className="text-center">หรือ</p>
                      <p
                        className="text-center text-primary link"
                        onClick={() => {
                          this.setState({
                            currentPage: "",
                          });
                        }}
                      >
                        ยืนยันตัวตนด้วยเบอร์โทรศัพท์
                      </p>
                    </>
                  ) : (
                    <>
                      <p className="text-center text-secondary">
                        รับรหัสใหม่อีกครั้ง ({this.state.timeRequestOTP} วิ)
                      </p>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        );
        break;
      case "formemail":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12 bg-white">
                <h2 className="text-center mt-4">ยืนยันตัวตนด้วยอีเมล</h2>
                <p className="text-center text-secondary">
                  กรุณาใส่อีเมลที่คุณต้องการยืนยันตัวตน
                </p>
                <form id="formemail">
                  <div className="form-group">
                    <input
                      type="text"
                      name="email"
                      className="form-control"
                      placeholder="อีเมล"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                  </div>
                </form>
                <div className="text-center">
                  <button
                    className="btn btn-yellow w-25 mb-4 text-store-color"
                    style={{
                      color: "#" + this.state.storeColorData["colorfont"],
                    }}
                    onClick={(e) => this.handleSendConfirmViaEmail(e)}
                  >
                    ตกลง
                  </button>
                </div>

                <p
                  className="text-center text-primary link"
                  onClick={() => {
                    this.setState({
                      currentPage: "",
                    });
                  }}
                >
                  ยืนยันตัวตนด้วยเบอร์โทรศัพท์
                </p>
              </div>
            </div>
          </div>
        );
        break;

      default:
        return;
        break;
    }
  };

  async componentDidMount() {
    this.initialize();

    /***** สี */
    const storeColorData = await axios.get(
      config.api.base_url + "/store/api/storeColor/" + this.state.registerStore
    );
    if (storeColorData.status === 200) {
      this.setState({ storeColorData: storeColorData.data });
      let colortabbar = this.state.storeColorData["colortabbar"];
      let coloricon = this.state.storeColorData["coloricon"];
      let colorfont = this.state.storeColorData["colorfont"];
      if (colortabbar && coloricon && colorfont) {
        $(
          ".register-header, .register-header.logo, .btn-register, .btn-register, .button-submit, .btn-yellow "
        ).css("background-color", "#" + colortabbar);
        $(".border-top-store").css("border-color", "#" + colortabbar);
        $(".text-store-color, .btn-register").css("color", "#" + colorfont);
      }
    }

    const storeData = await axios.get(
      config.api.base_url + "/store/api/store/" + this.state.registerStore
    );

    if (storeData.status == 200) {
      this.setState({
        storeData: storeData.data[0],
      });
    }

    console.log(storeData);

    //this.getUserByLineID(this.state.lineid);

    //let registerTel = sessionStorage.getItem("registerTel");
    this.countDownTimer();

    $(".digit-group")
      .find("input")
      .each(function () {
        //alert();
        //$(this).attr('maxlength', 1);
        $(this).on("keyup", function (e) {
          var parent = $($(this).parent());
          if (e.keyCode) {
            $("#log").val(e.keyCode);
            var next = parent.find("input#" + $(this).data("next"));
            if (next.length) {
              $(next).select();
            }
          }
        });
      });

    ///this.getUserByTel(registerTel);

    if (this.state.lineid === "") {
      /*Swal.fire({
        position: "middle",
        title: "กรุณารอสักครู่",
        showConfirmButton: false,
        allowOutsideClick: false,
        customClass: "swal-wide",
      });*/
    }
  }

  handleClickNumPad = (key, num) => {
    if (num === "del") {
      this.setState({
        [key]: this.state[key].slice(0, -1),
      });
    } else {
      if (this.state[key].length < 6) {
        this.setState({
          [key]: this.state[key] + num.toString(),
        });
      }
    }
  };

  async getUserByTel(registerTel) {
    const userData = await axios.get(
      config.api.base_url + "/api/userData/usertel/" + registerTel
    );
    console.log(userData);
    if (userData.status === 200) {
      let rowData = userData.data;
      this.setState({
        userid: rowData.userid,
        fname: rowData.fname,
        lname: rowData.lname,
        usertel: rowData.usertel,
        email: rowData.email,
        birthday:
          rowData.birthday !== "" ? moment(rowData.birthday).toDate() : "",
        gender: rowData.gender,
      });
    }
  }

  async getUserByLineID(lineid) {
    const _this = this
    const userData = await axios.get(
      config.api.base_url + "/api/userData/lineid/" + lineid
    );
    
    console.log(userData);
    if (userData.status === 200) {
      let rowData = userData.data;
      const userStoreData = await axios.get(
        config.api.base_url + "/store_member/api/userStoreData/id/" + rowData.userid+'/'+_this.state.registerStore
      );

      //console.log(parseInt(rowData.userid))
      
      if(userStoreData.status===200 && userStoreData.data.userid==rowData.userid){//เป็นสมาชิกอยู่แล้ว
        window.location='/shop/'+_this.state.registerStore

      }else{

        if (rowData.usertel) {
          this.setState({
            wantValidateOtp: false,
          });
        }

        this.setState({
          userid: rowData.userid,
          fname: rowData.fname,
          lname: rowData.lname,
          usertel: rowData.usertel,
          usertelold: rowData.usertel,
          email: rowData.email,
          birthday:
            rowData.birthday !== "" ? moment(rowData.birthday).toDate() : "",
          gender: rowData.gender,
          disableForm:false
        });
      }
    }else{
      _this.setState({
        disableForm:false
      })
    }
  }

  initialize() {
    liff.init(async (data) => {
      let profile = await liff.getProfile();
      this.setState({
        displayName: profile.displayName,
        lineid: profile.userId,
        pictureUrl: profile.pictureUrl,
        statusMessage: profile.statusMessage,
      });
      this.getUserByLineID(profile.userId);
    });
  }

  handleChange = (event) => {
    if (event.target.name == "agree") {
      this.setState({ agree: event.target.checked });
    } else if (event.target.name == "usertel") {
      let tel = event.target.value.replace(/[^\d]/g, "");
      this.setState({ [event.target.name]: tel });

      if (tel == this.state.usertelold && this.state.userid) {
        this.setState({
          wantValidateOtp: false,
        });
      } else {
        this.setState({
          wantValidateOtp: true,
        });
      }
    } else {
      this.setState({ [event.target.name]: event.target.value });
    }

    //console.log(this.state);
  };

  async handleChangeDate(date) {
    if (date !== null) {
      this.setState({ birthday: moment(date).toDate() });
    } else {
      this.setState({ birthday: "" });
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const _this = this;

    $("#myForm").validate({
      rules: {
        store_name: "required",
        fname: {
          required: true,
        },
        usertel: {
          required: true,
        },
        password: {
          required: true,
          minlength: 5,
        },
        repassword: {
          minlength: 5,
          equalTo: "#password",
        },
      },
      messages: {
        store_name: "กรุณากรอกชื่อร้านค้า",
        fname: {
          required: "กรุณากรอกชื่อผู้ติดต่อ",
        },
        usertel: {
          required: "กรุณากรอกเบอร์โทรศัพท์",
        },
        password: {
          required: "กรุณาตั้งรหัสผ่าน",
          minlength: "กรุณาตั้งรหัสผ่านอย่างน้อย 5 หลัก",
        },
        repassword: {
          equalTo: "กรุณาตั้งรหัสผ่านให้เหมือนกัน",
        },
      },

      errorPlacement: function (error, element) {
        $(element).parents(".form-group").append(error);
      },
    });

    if ($("#myForm").valid()) {
      if (!this.state.agree) {
        Swal.fire({
          title: "ขออภัย",
          html: "กรุณายอมรับนโยบายข้อมูลส่วนบุคคล ของบริษัท",
          icon: "info",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      } else if (this.state.usertel) {
        /* _this.setState({
          timeRequestOTP: 60,
        });
        _this.handleSendOTP(this.state.usertel); */

        _this.setState({
          currentPage: "formpassword",
        });
      } else {
        console.log(_this.state);
        Swal.fire({
          title: "ขออภัย",
          html: "กรุณากรอกข้อมูลให้ครบถ้วน",
          icon: "info",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      }
    } else {
    }
  };

  handleSendOTP = (usertel) => {
    //alert()
    const _this = this;
    sessionStorage.setItem("registerTel", usertel);
    let bodyFormData = new FormData();
    bodyFormData.set("usertel", usertel);

    axios({
      method: "post",
      url: config.api.base_url + "/api/sendotp",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        if (response.status === 200) {
          let otpid = response.data.otpid;
          console.log(otpid);

          _this.setState({
            otpid: otpid,
            currentPage: "otp",
          });
        } else {
          Swal.fire({
            title: "ขออภัย",
            html: "ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง",
            icon: "error",
            confirmButtonText: "ตกลง",
            customClass: {
              confirmButton: "btn-darkblue",
            },
          });
        }
        //console.log(response);
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  };

  handleSendConfirmViaEmail = (event) => {
    event.preventDefault();
    const _this = this;
    $.validator.addMethod(
      "validateEmail",
      function (value, element) {
        return validateEmail(value);
      },
      "Please enter a valid email address."
    );

    $("#formemail").validate({
      rules: {
        email: {
          required: true,
          validateEmail: true,
        },
      },
      messages: {
        email: {
          required: "กรุณาระบุอีเมลที่ต้องการยืนยันตัวตน",
          validateEmail: "กรุณาระบุอีเมลให้ถูกต้อง",
        },
      },

      errorPlacement: function (error, element) {
        $(element).parents(".form-group").append(error);
      },
    });

    if ($("#formemail").valid()) {
      let bodyFormData = new FormData();
      bodyFormData.set("email", this.state.email);
      axios({
        method: "post",
        url: config.api.base_url + "/shop_register/api/sendValidateEmail",
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(function (response) {
          if (response.status === 200) {
            _this.setState({
              timeRequestOTP: 90,
              otpemailid: response.data.otpid,
              currentPage: "otpEmail",
            });
            //_this.countDownTimer();
          } else {
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถส่ง OTP ได้ กรุณาทำรายการใหม่อีกครั้ง",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-darkblue",
              },
            });
          }

          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  handleSubmitData = (event) => {
    //insert ลง DB
    event.preventDefault();
    const _this = this;
    //console.log(this.state)
    // if(this.state.lineid !==''){
    // check email format
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $("#myForm").validate({
      rules: {
        store_name: "required",
        fname: {
          required: true,
        },
        usertel: {
          required: true,
        },
        password: {
          required: true,
          minlength: 5,
        },
        repassword: {
          minlength: 5,
          equalTo: "#password",
        },
      },
      messages: {
        store_name: "กรุณากรอกชื่อร้านค้า",
        fname: {
          required: "กรุณากรอกชื่อผู้ติดต่อ",
        },
        usertel: {
          required: "กรุณากรอกเบอร์โทรศัพท์",
        },
        password: {
          required: "กรุณาตั้งรหัสผ่าน",
          minlength: "กรุณาตั้งรหัสผ่านอย่างน้อย 5 หลัก",
        },
        repassword: {
          equalTo: "กรุณาตั้งรหัสผ่านให้เหมือนกัน",
        },
      },

      errorPlacement: function (error, element) {
        $(element).parents(".form-group").append(error);
      },
    });

    if ($("#myForm").valid()) {
      if (!this.state.agree) {
        Swal.fire({
          title: "ขออภัย",
          html: "กรุณายอมรับนโยบายข้อมูลส่วนบุคคล ของบริษัท",
          icon: "info",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      }else{
        if (this.state.fname && this.state.lineid && this.state.usertel) {
          if (this.state.userid) {
            let bodyFormData = new FormData();
            bodyFormData.set("userid", this.state.userid);
            bodyFormData.set("storeid", _this.state.registerStore);
            axios({
              method: "post",
              url: config.api.base_url + "/api/card",
              data: bodyFormData,
              headers: { "Content-Type": "multipart/form-data" },
            })
              .then(function (response) {
                if (response.status === 200 || response.status === 201) {
                  window.location = "/shop/" + _this.state.registerStore;
                } else {
                  window.location = _this.state.refer_url;
                }
              })
              .catch(function (response) {
                window.location = _this.state.refer_url;
              });
          } else {
            let bodyFormData = new FormData();
            bodyFormData.set("fname", this.state.fname);
            bodyFormData.set("lname", this.state.lname);
            bodyFormData.set("lineID", this.state.lineid);
            bodyFormData.set("usertel", this.state.usertel);
            bodyFormData.set("password", this.state.password);
            axios({
              method: "post",
              url: config.api.base_url + "/users/api/register",
              data: bodyFormData,
              headers: { "Content-Type": "multipart/form-data" },
            })
              .then(function (response) {
                if (response.status === 200 || response.status === 201) {
                  console.log(response);
                  sessionStorage.removeItem("registerTel");
                  let user_id = response.data.userid;
                  sessionStorage.setItem("user_id", user_id);

                  /***** เพิ่ม HO Card */
                  let store_id = 4;
                  let bodyFormDataCard = new FormData();
                  bodyFormDataCard.set("userid", user_id);
                  bodyFormDataCard.set("storeid", store_id);
                  axios({
                    method: "post",
                    url: config.api.base_url + "/api/card",
                    data: bodyFormDataCard,
                    headers: { "Content-Type": "multipart/form-data" },
                  })
                    .then(function (response) {
                      let bodyFormData = new FormData();
                      bodyFormData.set("userid", _this.state.userid);
                      bodyFormData.set("storeid", _this.state.registerStore);
                      axios({
                        method: "post",
                        url: config.api.base_url + "/api/card",
                        data: bodyFormData,
                        headers: { "Content-Type": "multipart/form-data" },
                      })
                        .then(function (response) {
                          if (response.status === 200 || response.status === 201) {
                            window.location = "/shop/" + _this.state.registerStore;
                          } else {
                            window.location = _this.state.refer_url;
                          }
                        })
                        .catch(function (response) {
                          window.location = _this.state.refer_url;
                        });
                    })
                    .catch(function (response) {
                      console.log(response);
                      //window.location = "/interest";
                    });
                } else {
                  Swal.fire({
                    title: "ขออภัย",
                    html: "บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง",
                    icon: "error",
                    confirmButtonText: "ตกลง",
                    customClass: {
                      confirmButton: "btn-darkblue",
                    },
                  });
                }
                //console.log(response);
              })
              .catch(function (response) {
                //handle error
                //console.log(response);
              });
          }
        } else {
          Swal.fire({
            title: "ขออภัย",
            html: "กรุณากรอกข้อมูลให้ครบถ้วน",
            icon: "info",
            confirmButtonText: "ตกลง",
            customClass: {
              confirmButton: "btn-darkblue",
            },
          });
        }
      }
    }
  };
  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve);
    });
  }

  handleChangeOTP = async (event) => {
    event.preventDefault();
    const { value, maxLength, name } = event.target;
    const inputValue = value.slice(0, maxLength);
    await this.setStateAsync({ [event.target.name]: inputValue });

    /*if (name == "otp6") {
      this.handleSubmitOtp(event);
    }*/
  };

  handleSubmitOtp = (event) => {
    //validate OTP
    event.preventDefault();
    const _this = this;
    let state = this.state;
    let otpcode = "";
    otpcode =
      state.otp1 +
      state.otp2 +
      state.otp3 +
      state.otp4 +
      state.otp5 +
      state.otp6;
    //alert(otpcode);
    if (
      state.otp1 &&
      state.otp2 &&
      state.otp3 &&
      state.otp4 &&
      state.otp5 &&
      state.otp6
    ) {
      otpcode =
        state.otp1 +
        state.otp2 +
        state.otp3 +
        state.otp4 +
        state.otp5 +
        state.otp6;

      /* if(this.state.fakeOTP==otpcode){
            window.location='/register';
        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
        } */
      console.log(this.state.otpemailid);
      if (otpcode && (this.state.otpid || this.state.otpemailid)) {
        let bodyFormData = new FormData();
        let url_validate_otp = "";
        if (state.currentPage == "otpEmail") {
          bodyFormData.set("otpcode", otpcode);
          bodyFormData.set("otpid", this.state.otpemailid);
          bodyFormData.set("email", this.state.email);
          url_validate_otp =
            config.api.base_url + "/shop_register/api/sendValidateEmail";
        } else {
          bodyFormData.set("otpcode", otpcode);
          bodyFormData.set("otpid", this.state.otpid);
          bodyFormData.set("usertel", this.state.usertel);
          url_validate_otp = config.api.base_url + "/api/validateotp";
        }
        axios({
          method: "post",
          url: url_validate_otp,
          data: bodyFormData,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              console.log(response);
              _this.handleSubmitData(event);
            } else {
              Swal.fire({
                title: "ขออภัย",
                html: "OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง",
                icon: "error",
                confirmButtonText: "ตกลง",
                customClass: {
                  confirmButton: "btn-darkblue",
                },
              });
            }
            //console.log(response);
          })
          .catch(function (response) {
            console.log(response);
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถเชื่อมต่อ กรุณาตรวจสอบอีกครั้ง (OTP)",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-darkblue",
              },
            });
          });
      } else {
        Swal.fire({
          title: "ขออภัย",
          html: "พบข้อผิดพลาดในการตรวจสอบ<br>กรุณาทำรายการใหม่อีกครั้ง",
          icon: "error",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      }
    } else {
      Swal.fire({
        title: "ขออภัย",
        html: "กรุณากรอกรหัส OTP ให้ครบ 6 หลัก",
        icon: "info",
        confirmButtonText: "ตกลง",
        customClass: {
          confirmButton: "btn-darkblue",
        },
      });
    }

    //console.log(otpcode);

    //window.location='/register';
  };

  handleRequestOTP = (event) => {
    event.preventDefault();
    let obj = this;

    let bodyFormData = new FormData();
    bodyFormData.set("usertel", this.state.usertel);
    axios({
      method: "post",
      url: config.api.base_url + "/api/resendotp",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          let responseotpid = response.data.otpid;
          obj.setState({
            otpid: responseotpid,
            validateOtpSection: 2,
          });
          obj.setState({
            timeRequestOTP: 60,
          });
          obj.countDownTimer();
        } else {
          Swal.fire({
            title: "ขออภัย",
            html: "ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง",
            icon: "error",
            confirmButtonText: "ตกลง",
            customClass: {
              confirmButton: "btn-darkblue",
            },
          });
        }
      })
      .catch(function (response) {
        console.log(response);
        Swal.fire({
          title: "ขออภัย",
          html: "ไม่สามารถเชื่อมต่อได้ กรุณาตรวจสอบอีกครั้ง",
          icon: "error",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      });
  };

  render() {
    return (
      <>
        <link
          rel="stylesheet"
          type="text/css"
          href="/assets/css/register.css?v=4"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="/assets/css/register_userstore.css?v=4"
        />
        {!this.state.lineid ? (
          ""
        ) : !this.state.currentPage ? (
          <div className={this.state.disableForm ? 'd-none' : 'container-mobile m-auto'}>
            <div className="row w-100 m-0">
              <div
                className="col-12 register-header"
                style={{
                  color: "#" + this.state.storeColorData["colorfont"],
                  backgroundColor:
                    "#" + this.state.storeColorData["colortabbar"],
                }}
              >
                <div className="img-logo-wrap">
                  <img
                    className="logo"
                    src={
                      this.state.storeData.storelogo !== ""
                        ? this.state.storeData.storelogo
                        : "/assets/images/logo-ho-white.png"
                    }
                    width="60"
                  />
                  <img className="logo-ho" src={"/assets/images/logo-ho.png"} />
                </div>
                <h2
                  className="mt-3 text-store-color"
                  style={{
                    color: "#" + this.state.storeColorData["colorfont"],
                  }}
                >
                  สมัครสมาชิกร้าน {this.state.storeData.storename}
                </h2>
              </div>
            </div>
            <form
              method="post"
              onSubmit={this.handleSubmit}
              id="myForm"
              style={{ marginBottom: "250px" }}
            >
              <div className="container">
                <div className="register-container">
                  <div className="col-md-12">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="fname"
                        placeholder="ดึงชื่อจาก Line ID"
                        value={this.state.fname}
                        readOnly
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <InputMask
                        mask="999-999-9999"
                        maskChar={null}
                        value={this.state.usertel}
                        type="text"
                        name="usertel"
                        className="form-control"
                        placeholder="เบอร์โทรศัพท์เพื่อรับ OTP"
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>

                <div className="form-check text-center pt-3">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="exampleCheck1"
                    name="agree"
                    onChange={this.handleChange}
                    value="1"
                  />
                  <label
                    className="form-check-label text-grey link"
                    htmlFor="exampleCheck1"
                  >
                    ยอมรับเงื่อนไข&nbsp;
                  </label>
                  <Link
                    className="link"
                    to={{
                      pathname: "/privacy_policy",
                      state: { prevPath: this.props.location.pathname },
                    }}
                  >
                    นโยบายข้อมูลส่วนบุคคล&nbsp;
                  </Link>
                  ของบริษัท
                </div>
              </div>
              <div
                className="col-12 footer mx-auto shadow button-submit text-store-color"
                style={{
                  backgroundColor:
                    "#" + this.state.storeColorData["colortabbar"],
                }}
              >
                {this.state.wantValidateOtp ? (
                  <>
                    {/* <input
                      className="w-100 position-relative btn-register text-store-color"
                      type="submit"
                      value="รับรหัส OTP"
                      onClick={this.handleSubmit}
                    /> */}
                    <input
                      className="w-100 position-relative btn-register text-store-color"
                      type="submit"
                      value="ถัดไป"
                      style={{
                        backgroundColor:
                          "#" + this.state.storeColorData["colortabbar"],
                        color: "#" + this.state.storeColorData["colorfont"],
                      }}
                      onClick={this.handleSubmit}
                    />
                  </>
                ) : (
                  <input
                    className="w-100 position-relative btn-register text-store-color"
                    type="submit"
                    value="ตกลง"
                    style={{
                      backgroundColor:
                        "#" + this.state.storeColorData["colortabbar"],
                      color: "#" + this.state.storeColorData["colorfont"],
                    }}
                    onClick={this.handleSubmitData}
                  />
                )}
              </div>
            </form>
          </div>
        ) : (
          this.handleChangePage(this.state.currentPage)
        )}
      </>
    );
  }
}
export default Login;
