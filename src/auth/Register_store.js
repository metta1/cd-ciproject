import React, { Component } from "react";
import axios from "axios";
import withRequest from "../lib/withRequest";
import { Route, Link, Redirect } from "react-router-dom";
import { Form, Well, Button, FormGroup, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Field, reduxForm } from "redux-form";
import $ from "jquery";
import SweetAlert from "react-bootstrap-sweetalert";
import Cookies from "universal-cookie";
import { config } from "../config";
import queryString from "query-string";
import Swal from "sweetalert2";
import moment from "moment";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import th from "date-fns/locale/th"; // the locale you want
import validate from "jquery-validation";
import InputMask from "react-input-mask";
import {getParam,inArray, readURL, getSizeFromImgDataURL, validateEmail} from '../lib/Helper';
import PasswordStrengthBar from 'react-password-strength-bar';

registerLocale("th", th); // register it with the name you want

const required = (value) => (value ? undefined : "Required");
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[
  currentURLArray.length - 1
].toLowerCase();
const registerRecommend = sessionStorage.getItem("registerRecommend");

class Login extends Component {
  constructor(props) {
    //เริ่มต้น run component
    super(props);
    this.handleRequestOTP = this.handleRequestOTP.bind(this);

    let locationSearch = this.props.location.search;
    let getParam = queryString.parse(locationSearch);
    let refer_url = "/home";
    if (getParam.referer !== undefined && getParam.referer !== "") {
      refer_url = "/" + getParam.referer;
    }
    let registerTel = sessionStorage.getItem("registerTel");
    this.state = {
      displayName: "",
      lineid: "",
      pictureUrl: "assets/images/profile.svg",
      statusMessage: "",
      refer_url: refer_url,
      userid: null,
      fname: "",
      lname: "",
      email: "",
      usertel: null,
      password:'',
      recommend:
        registerRecommend !== undefined && registerRecommend !== null
          ? registerRecommend
          : undefined,
      waitingData: "Y",
      birthday: "",
      gender: "",
      currentPage: "",
      timeRequestOTP: 60,
      validateOtpSection: 1,
      envDev: false,
      otp1:'',
      otp2:'',
      otp3:'',
      otp4:'',
      otp5:'',
      otp6:'',
    };

    this.initialize = this.initialize.bind(this);
    this.getUserByTel = this.getUserByTel.bind(this);
  }

  modalPasswordCondition = () => {
    Swal.fire({
      title: "เงื่อนไขการตั้งรหัสผ่าน",
      html: `<ul class="text-left pl-0">
                <li>ต้องมีตัวอักษรพิมพ์เล็กและพิมพ์ใหญ่</li>
                <li>มีอักขระพิเศษ</li>
                <li>มีมากกว่า 6 ตัวขึ้นไป</li>
              </ul>`,
      icon: "info",
      confirmButtonText: "ตกลง",
      customClass: {
        confirmButton: "btn-darkblue",
      },
    });
  }

  countDownTimer() {
    const _this = this;
    var refreshId = setInterval(function () {
      _this.setState({
        timeRequestOTP: _this.state.timeRequestOTP - 1,
      });
      if (_this.state.timeRequestOTP < 1) {
        clearInterval(refreshId);
      }
    }, 1000);
  }

  setStatePage = (page) => {
    this.setState({
      currentPage: page,
    });
  };

  handleChangePage = (page) => {
    //console.log(page);]
    $(".digit-group")
      .find("input")
      .each(function () {
        //$(this).attr('maxlength', 1);
        $(this).on("keyup", function (e) {
          var parent = $($(this).parent());
          if (e.keyCode) {
            $("#log").val(e.keyCode);
            var next = parent.find("input#" + $(this).data("next"));
            if (next.length) {
              $(next).select();
            }
          }
        });
      });

    switch (page) {
      case "otp":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h2 className="text-center mt-4">ใส่รหัสยืนยัน</h2>
                <div className="rows digit-group text-center">
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp1"
                    name="otp1"
                    data-next="otp2"
                    value={this.state.otp1}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp2"
                    name="otp2"
                    data-next="otp3"
                    data-previous="otp1"
                    value={this.state.otp2}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp3"
                    name="otp3"
                    data-next="otp4"
                    data-previous="otp2"
                    value={this.state.otp3}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp4"
                    name="otp4"
                    data-next="otp5"
                    data-previous="otp3"
                    value={this.state.otp4}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp5"
                    name="otp5"
                    data-next="otp6"
                    data-previous="otp4"
                    value={this.state.otp5}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                  <input
                    type="number"
                    className="form cols text-center"
                    id="otp6"
                    name="otp6"
                    data-previous="otp5"
                    value={this.state.otp6}
                    onChange={this.handleChangeOTP}
                    maxLength="1"
                  />
                </div>
                <div className="w-100 text-center mt-4">
                  <button
                      type="button"
                      className="btn btn-success"
                      style={{ padding: "7px 30px" }}
                      onClick={
                        (e) => {
                          let state = this.state;
                          let otpcode = "";
                          otpcode =
                            state.otp1 +
                            state.otp2 +
                            state.otp3 +
                            state.otp4 +
                            state.otp5 +
                            state.otp6;
                            if(otpcode<6){
                              Swal.fire({
                                title: "ขออภัย",
                                html: "กรุณากรอก OTP อย่างน้อย 6 หลัก",
                                icon: "info",
                                confirmButtonText: "ตกลง",
                                customClass: {
                                  confirmButton: "btn-darkblue",
                                },
                              });
                            }else{
                              
                              this.handleSubmitOtp(e);
                            }
                          
                        }
                      }
                    >
                      ตกลง
                    </button>
                  </div>
                  <div className="mt-1">
                  {this.state.timeRequestOTP < 1 ? (
                    <>
                      <p
                        className="text-center text-primary link"
                        onClick={this.handleRequestOTP}
                      >
                        รับ OTP ใหม่อีกครั้ง
                      </p>
                      {this.state.validateOtpSection === 2 && (
                        <>
                          <p className="text-center">หรือ</p>
                          <p
                            className="text-center text-primary link"
                            onClick={() => {
                              this.setState({
                                currentPage: "formemail",
                              });
                            }}
                          >
                            ยืนยันตัวตนด้วยอีเมล
                          </p>
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <p className="text-center text-secondary">
                        รับ OTP ใหม่อีกครั้ง ({this.state.timeRequestOTP} วิ)
                      </p>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        );
        break;
        case "otpEmail":
          return (
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <h2 className="text-center mt-4">ยืนยันตัวตนด้วยอีเมล</h2>
                  <p className="text-center text-secondary">
                    <>
                      ระบบได้ส่งอีเมลยืนยันตัวตนให้คุณแล้ว<br/>
                      กรุณาตรวจสอบอีเมลของคุณ<br/>
                      
                      </>
                  </p>
                  <p className="text-center">{this.state.email}</p>
                  
                  
                  <div className="rows digit-group text-center">
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp1"
                      name="otp1"
                      data-next="otp2"
                      value={this.state.otp1}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp2"
                      name="otp2"
                      data-next="otp3"
                      data-previous="otp1"
                      value={this.state.otp2}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp3"
                      name="otp3"
                      data-next="otp4"
                      data-previous="otp2"
                      value={this.state.otp3}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp4"
                      name="otp4"
                      data-next="otp5"
                      data-previous="otp3"
                      value={this.state.otp4}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp5"
                      name="otp5"
                      data-next="otp6"
                      data-previous="otp4"
                      value={this.state.otp5}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                    <input
                      type="number"
                      className="form cols text-center"
                      id="otp6"
                      name="otp6"
                      data-previous="otp5"
                      value={this.state.otp6}
                      onChange={this.handleChangeOTP}
                      maxLength="1"
                    />
                  </div>
                  <div className="w-100 text-center mt-4">
                    <button
                      type="button"
                      className="btn btn-success"
                      style={{ padding: "7px 30px" }}
                      onClick={
                        (e) => {
                          let state = this.state;
                          let otpcode = "";
                          otpcode =
                            state.otp1 +
                            state.otp2 +
                            state.otp3 +
                            state.otp4 +
                            state.otp5 +
                            state.otp6;
                            if(otpcode<6){
                              Swal.fire({
                                title: "ขออภัย",
                                html: "กรุณากรอก OTP อย่างน้อย 6 หลัก",
                                icon: "info",
                                confirmButtonText: "ตกลง",
                                customClass: {
                                  confirmButton: "btn-darkblue",
                                },
                              });
                            }else{
                              
                              this.handleSubmitOtp(e);
                            }
                          
                        }
                      }
                    >
                      ตกลง
                    </button>
                  </div>
                  <div className="mt-1">
                    {this.state.timeRequestOTP < 1 ? (
                      <>
                        <p
                          className="text-center text-primary link"
                          onClick={() => {
                            this.setState({
                              currentPage: "formemail",
                            });
                          }}
                        >
                          รับรหัสใหม่อีกครั้ง
                        </p>
                       
                            <p className="text-center">หรือ</p>
                            <p
                              className="text-center text-primary link"
                              onClick={() => {
                                this.setState({
                                  currentPage: "",
                                });
                              }}
                            >
                              ยืนยันตัวตนด้วยเบอร์โทรศัพท์
                            </p>
                        
                      
                      </>
                    ) : (
                      <>
                        <p className="text-center text-secondary">
                        รับรหัสใหม่อีกครั้ง ({this.state.timeRequestOTP} วิ)
                        </p>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          );
          break;
      case "formemail":
        return (
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h2 className="text-center mt-4">ยืนยันตัวตนด้วยอีเมล</h2>
                <p className="text-center text-secondary">
                  กรุณาใส่อีเมลที่คุณต้องการยืนยันตัวตน
                </p>
                <form id="formemail">
                  <div className="form-group">
                    <input
                      type="text"
                      name="email"
                      className="form-control"
                      placeholder="อีเมล"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                  </div>
                </form>
                <div className="text-center">
                  <button className="btn btn-yellow w-25 mb-4" onClick={(e)=>this.handleSendConfirmViaEmail(e)} >ตกลง</button>
                </div>

                <p
                  className="text-center text-primary link"
                  onClick={() => {
                    this.setState({
                      currentPage: "",
                    });
                  }}
                >
                  ยืนยันตัวตนด้วยเบอร์โทรศัพท์
                </p>
              </div>
            </div>
          </div>
        );
        break;

      default:
        break;
    }
  };

  async componentDidMount() {
    this.initialize();
    //let registerTel = sessionStorage.getItem("registerTel");
    this.countDownTimer();

    $(".digit-group")
      .find("input")
      .each(function () {
        //alert();
        //$(this).attr('maxlength', 1);
        $(this).on("keyup", function (e) {
          var parent = $($(this).parent());
          if (e.keyCode) {
            $("#log").val(e.keyCode);
            var next = parent.find("input#" + $(this).data("next"));
            if (next.length) {
              $(next).select();
            }
          }
        });
      });

    ///this.getUserByTel(registerTel);

    if (this.state.lineid === "") {
      /*Swal.fire({
        position: "middle",
        title: "กรุณารอสักครู่",
        showConfirmButton: false,
        allowOutsideClick: false,
        customClass: "swal-wide",
      });*/
    }
  }

  async getUserByTel(registerTel) {
    const userData = await axios.get(
      config.api.base_url + "/api/userData/usertel/" + registerTel
    );
    console.log(userData);
    if (userData.status === 200) {
      let rowData = userData.data;
      this.setState({
        userid: rowData.userid,
        fname: rowData.fname,
        lname: rowData.lname,
        usertel: rowData.usertel,
        email: rowData.email,
        birthday:
          rowData.birthday !== "" ? moment(rowData.birthday).toDate() : "",
        gender: rowData.gender,
      });
    }
  }

  initialize() {
    liff.init(async (data) => {
      let profile = await liff.getProfile();
      this.setState({
        displayName: profile.displayName,
        lineid: profile.userId,
        pictureUrl: profile.pictureUrl,
        statusMessage: profile.statusMessage,
      });

      /*const result = await axios.get(config.api.base_url+'/api/userData/lineid/'+this.state.userId)
            if(result.status===200){
                let user_id = result.data.userid;
                sessionStorage.setItem('user_id',user_id)
                window.location='/home';
                
            }else{
                //window.location='/intro';

              
            }*/
    });
  }

  handleChange = (event) => {
    if (event.target.name == "agree") {
      this.setState({ agree: event.target.checked });
    } else if (event.target.name == "usertel") {
      let tel = event.target.value.replace(/[^\d]/g, "");
      this.setState({ [event.target.name]: tel });
    } else {
      this.setState({ [event.target.name]: event.target.value });
    }

    //console.log(this.state);
  };

  async handleChangeDate(date) {
    if (date !== null) {
      this.setState({ birthday: moment(date).toDate() });
    } else {
      this.setState({ birthday: "" });
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const _this = this;

    $.validator.addMethod(
      "checkConditionPassword",
      function (value, element) {
        let minLength = (value.length > 5) ? true : false;
        let upperAndLowerCase =  (value.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) ? true :false;
        let specialCharacter = (value.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) ? true :false;
        if(minLength && upperAndLowerCase && specialCharacter){
          return true;
        }else{
          return false;
        }
        
      },
      "ต้องมีตัวอักษรพิมพ์เล็กและพิมพ์ใหญ่ <br>มีอักขระพิเศษ<br>มีมากกว่า 6 ตัวขึ้นไป"
    );

    $("#myForm").validate({
      rules: {
        store_name: "required",
        fname: {
          required: true,
        },
        usertel: {
          required: true,
        },
        password: {
          required: true,
          minlength: 6,
          checkConditionPassword:true
        },
        repassword: {
          minlength: 6,
          equalTo: "#password",
        },
      },
      messages: {
        store_name: "กรุณากรอกชื่อร้านค้า",
        fname: {
          required: "กรุณากรอกชื่อผู้ติดต่อ",
        },
        usertel: {
          required: "กรุณากรอกเบอร์โทรศัพท์",
        },
        password: {
          required: "กรุณาตั้งรหัสผ่าน",
          minlength: "กรุณาตั้งรหัสผ่านอย่างน้อย 5 หลัก",
        },
        repassword: {
          equalTo: "กรุณาตั้งรหัสผ่านให้เหมือนกัน",
        },
      },

      errorPlacement: function (error, element) {
        $(element).parents(".form-group").append(error);
      },
    });

    if ($("#myForm").valid()) {
      if (!this.state.agree) {
        Swal.fire({
          title: "ขออภัย",
          html: "กรุณายอมรับนโยบายข้อมูลส่วนบุคคล ของบริษัท",
          icon: "info",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      } else if (this.state.usertel) {
        const userData = await axios.get(
          config.api.base_url + "/api/userData/usertel/" + this.state.usertel
        );

        console.log(userData);

        if (userData.status === 200) {
          Swal.fire({
            title: "เบอร์โทรศัพท์นี้ได้เป็นสมาชิก HO อยู่แล้ว",
            icon: "info",
            html: "คุณต้องการเปิดร้านโดยใช้บัญชีผู้ใช้เดิมหรือไม่",
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: "ใช่",
            cancelButtonText: "ไม่ใช่ เปลี่ยนหมายเลขโทรศัพท์",
            denyButtonText: "ต้องการสร้างบัญชีใหม่",
          }).then((result) => {
            if (result.isConfirmed) {
              //alert()
              _this.setState({
                userid: userData.data.userid,
                timeRequestOTP:60
              });
              _this.handleSendOTP(this.state.usertel);
            } else if (result.isDenied) {
              $("input[name='usertel']").focus();
              return;
            } else {
              $("input[name='usertel']").focus();
              return;
            }
          });
        } else {
          //alert()
          _this.setState({
            timeRequestOTP:60
          });
          _this.handleSendOTP(this.state.usertel);
        }
      } else {
        console.log(_this.state);
        Swal.fire({
          title: "ขออภัย",
          html: "กรุณากรอกข้อมูลให้ครบถ้วน",
          icon: "info",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      }
    } else {
    }
  };

  handleSendOTP = (usertel) => {
    //alert()
    const _this = this;
    sessionStorage.setItem("registerTel", usertel);
    let bodyFormData = new FormData();
    bodyFormData.set("usertel", usertel);
    
      axios({
        method: "post",
        url: config.api.base_url + "/api/sendotp",
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(function (response) {
          if (response.status === 200) {
            let otpid = response.data.otpid;
            console.log(otpid);

            _this.setState({
              otpid: otpid,
              currentPage: "otp",
            });
          } else {
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-darkblue",
              },
            });
          }
          //console.log(response);
        })
        .catch(function (response) {
          //handle error
          console.log(response);
        });
    
  };

  handleSendConfirmViaEmail = (event) => {
    event.preventDefault();
    const _this = this;
    $.validator.addMethod("validateEmail", function(value, element) {
      return validateEmail(value)
    }, 'Please enter a valid email address.');

    $("#formemail").validate({
      rules: {
       
        email: {
          required: true,
          validateEmail: true
        },
       
      },
      messages:{
        email: {
          required: 'กรุณาระบุอีเมลที่ต้องการยืนยันตัวตน',
          validateEmail: 'กรุณาระบุอีเมลให้ถูกต้อง'
        },
      },

      errorPlacement: function (error, element) {
        $(element).parents(".form-group").append(error);
      },
    });

    if ($("#formemail").valid()) {
        let bodyFormData = new FormData();
        bodyFormData.set("email", this.state.email);
        axios({
          method: "post",
          url: config.api.base_url + "/shop_register/api/sendValidateEmail",
          data: bodyFormData,
          headers: { "Content-Type": "multipart/form-data" },
        })
        .then(function (response) {
          if(response.status===200){
            
            _this.setState({
              timeRequestOTP: 90,
              otpemailid : response.data.otpid,
              currentPage:'otpEmail'
            });
            //_this.countDownTimer();
            

          }else{
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถส่ง OTP ได้ กรุณาทำรายการใหม่อีกครั้ง",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-darkblue",
              },
            });

          }
         
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });


    }

  }

  handleSubmitData = (event) => {
    //insert ลง DB
    event.preventDefault();
    const _this = this;
    //console.log(this.state)
    // if(this.state.lineid !==''){
    // check email format
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.fname &&
      this.state.store_name &&
      this.state.lineid &&
      this.state.usertel
    ) {
      if (this.state.userid) {
        //ใช้บัญชีเดิม ให้อัพเดท
        //alert()
        let bodyFormData = new FormData();
        bodyFormData.set("fname", this.state.fname);
        bodyFormData.set("lname", this.state.lname);
        bodyFormData.set("lineID", this.state.lineid);
        bodyFormData.set("email", this.state.email);
        bodyFormData.set("usertel", this.state.usertel);
        bodyFormData.set("userid", this.state.userid);
        bodyFormData.set("storename", this.state.store_name);
        bodyFormData.set("password", this.state.password);

        axios({
          method: "post",
          url: config.api.base_url + "/shop_register/api/register",
          data: bodyFormData,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            /************ insert card store ตัวเอง  */
            let bodyFormData = new FormData();
            bodyFormData.set("userid", this.state.userid);
            bodyFormData.set("storeid", response.data.storeid);
            axios({
              method: "post",
              url: config.api.base_url + "/api/card",
              data: bodyFormData,
              headers: { "Content-Type": "multipart/form-data" },
            })
            .then(function (response) {
              sessionStorage.setItem('showHome',0)
                if (response.status === 200 || response.status === 201) {
                  window.location = "/shop/" + response.data.storeid;
                } else {
                  window.location = _this.state.refer_url;
                }
              })
              .catch(function (response) {
                window.location = _this.state.refer_url;
            });
            
          })
          .catch(function (error) {
            console.log(error);
          });
      } else {
        let username = !this.state.email
          ? this.state.usertel
          : this.state.email;
        let bodyFormData = new FormData();
        bodyFormData.set("fname", this.state.fname);
        bodyFormData.set("lname", this.state.lname);
        bodyFormData.set("lineID", this.state.lineid);
        bodyFormData.set("email", this.state.email);
        bodyFormData.set("usertel", this.state.usertel);
        bodyFormData.set("userid", this.state.userid);
        bodyFormData.set("storename", this.state.store_name);
        bodyFormData.set("password", this.state.password);
        bodyFormData.set("recommend", this.state.recommend);
        axios({
          method: "post",
          url: config.api.base_url + "/users/api/register",
          data: bodyFormData,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            if (response.status === 200 || response.status === 201) {
              console.log(response);
              sessionStorage.removeItem("registerTel");
              let user_id = response.data.userid;
              sessionStorage.setItem("user_id", user_id);

              /***** เพิ่ม HO Card */
              let store_id = 4;
              let bodyFormDataCard = new FormData();
              bodyFormDataCard.set("userid", user_id);
              bodyFormDataCard.set("storeid", store_id);
              axios({
                method: "post",
                url: config.api.base_url + "/api/card",
                data: bodyFormDataCard,
                headers: { "Content-Type": "multipart/form-data" },
              })
                .then(function (response) {
                  bodyFormData.set("userid", user_id);

                  axios({
                    method: "post",
                    url: config.api.base_url + "/shop_register/api/register",
                    data: bodyFormData,
                    headers: { "Content-Type": "multipart/form-data" },
                  })
                    .then(function (response) {
                      /* window.location =
                        config.api.base_url +
                        "/auth/authToken?token=" +
                        response.data.apitoken; */
                      /* window.location='/shop/'+response.data.storeid
                      console.log(
                        config.api.base_url +
                          "/auth/authToken?token=" +
                          response.data.apitoken
                      );
                      console.log(response.data); */


                      /************ insert card store ตัวเอง  */
                      let bodyFormData = new FormData();
                      bodyFormData.set("userid", user_id);
                      bodyFormData.set("storeid", response.data.storeid);
                      axios({
                        method: "post",
                        url: config.api.base_url + "/api/card",
                        data: bodyFormData,
                        headers: { "Content-Type": "multipart/form-data" },
                      })
                      .then(function (response) {
                          if (response.status === 200 || response.status === 201) {
                            window.location = "/shop/" + response.data.storeid;
                          } else {
                            window.location = _this.state.refer_url;
                          }
                        })
                        .catch(function (response) {
                          window.location = _this.state.refer_url;
                      });

                        /****************** */
                    })
                    .catch(function (error) {
                      console.log(error);
                    });

                  console.log(response);

                  //window.location = "/interest";
                })
                .catch(function (response) {
                  console.log(response);
                  //window.location = "/interest";
                });
            } else {
              Swal.fire({
                title: "ขออภัย",
                html: "บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง",
                icon: "error",
                confirmButtonText: "ตกลง",
                customClass: {
                  confirmButton: "btn-darkblue",
                },
              });
            }
            //console.log(response);
          })
          .catch(function (response) {
            //handle error
            //console.log(response);
          });
      }
    } else {
      Swal.fire({
        title: "ขออภัย",
        html: "กรุณากรอกข้อมูลให้ครบถ้วน",
        icon: "info",
        confirmButtonText: "ตกลง",
        customClass: {
          confirmButton: "btn-darkblue",
        },
      });
    }

    // }else{
    //     Swal.fire({
    //         title : 'ขออภัย',
    //         html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
    //         icon:'error',
    //         confirmButtonText:'ตกลง',
    //         customClass: {
    //             confirmButton: 'btn-darkblue',
    //         },
    //     })
    // }
  };
  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve);
    });
  }

  handleChangeOTP = async (event) => {
    event.preventDefault();
    const { value, maxLength, name } = event.target;
    const inputValue = value.slice(0, maxLength);
    await this.setStateAsync({ [event.target.name]: inputValue });

    /* if (name == "otp6") {
      this.handleSubmitOtp(event);
    } */
  };

  handleSubmitOtp = (event) => {
    //validate OTP
    event.preventDefault();
    const _this = this;
    let state = this.state;
    let otpcode = "";
    otpcode =
      state.otp1 +
      state.otp2 +
      state.otp3 +
      state.otp4 +
      state.otp5 +
      state.otp6;
    //alert(otpcode);
    if (
      state.otp1 &&
      state.otp2 &&
      state.otp3 &&
      state.otp4 &&
      state.otp5 &&
      state.otp6
    ) {
      otpcode =
        state.otp1 +
        state.otp2 +
        state.otp3 +
        state.otp4 +
        state.otp5 +
        state.otp6;

      /* if(this.state.fakeOTP==otpcode){
            window.location='/register';
        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
        } */
      console.log(this.state.otpemailid)
      if (otpcode && (this.state.otpid||this.state.otpemailid)) {
        let bodyFormData = new FormData();
        let url_validate_otp = "";
        if (state.currentPage == 'otpEmail') {
          bodyFormData.set("otpcode", otpcode);
          bodyFormData.set("otpid", this.state.otpemailid);
          bodyFormData.set("email", this.state.email);
          url_validate_otp = config.api.base_url + "/shop_register/api/sendValidateEmail";
        } else {
          
          bodyFormData.set("otpcode", otpcode);
          bodyFormData.set("otpid", this.state.otpid);
          bodyFormData.set("usertel", this.state.usertel);
          url_validate_otp = config.api.base_url + "/api/validateotp";
        }
        axios({
          method: "post",
          url: url_validate_otp,
          data: bodyFormData,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              console.log(response)
              _this.handleSubmitData(event);
            } else {
              Swal.fire({
                title: "ขออภัย",
                html: "OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง",
                icon: "error",
                confirmButtonText: "ตกลง",
                customClass: {
                  confirmButton: "btn-darkblue",
                },
              });
            }
            //console.log(response);
          })
          .catch(function (response) {
            console.log(response);
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถเชื่อมต่อ กรุณาตรวจสอบอีกครั้ง (OTP)",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-darkblue",
              },
            });
          });
      } else {
        Swal.fire({
          title: "ขออภัย",
          html: "พบข้อผิดพลาดในการตรวจสอบ<br>กรุณาทำรายการใหม่อีกครั้ง",
          icon: "error",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      }
    } else {
      Swal.fire({
        title: "ขออภัย",
        html: "กรุณากรอกรหัส OTP ให้ครบ 6 หลัก",
        icon: "info",
        confirmButtonText: "ตกลง",
        customClass: {
          confirmButton: "btn-darkblue",
        },
      });
    }

    //console.log(otpcode);

    //window.location='/register';
  };

  handleRequestOTP = (event) => {
    event.preventDefault();
    let obj = this;

    let bodyFormData = new FormData();
    bodyFormData.set("usertel", this.state.usertel);
    axios({
      method: "post",
      url: config.api.base_url + "/api/resendotp",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          let responseotpid = response.data.otpid;
          obj.setState({
            otpid: responseotpid,
            validateOtpSection: 2,
          });
          obj.setState({
            timeRequestOTP: 60,
          });
          obj.countDownTimer();
        } else {
          Swal.fire({
            title: "ขออภัย",
            html: "ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง",
            icon: "error",
            confirmButtonText: "ตกลง",
            customClass: {
              confirmButton: "btn-darkblue",
            },
          });
        }
      })
      .catch(function (response) {
        console.log(response);
        Swal.fire({
          title: "ขออภัย",
          html: "ไม่สามารถเชื่อมต่อได้ กรุณาตรวจสอบอีกครั้ง",
          icon: "error",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-darkblue",
          },
        });
      });
  };

  render() {
    return (
      <>
        <link
          rel="stylesheet"
          type="text/css"
          href="/assets/css/register.css?v=4"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="/assets/css/register_store.css?v=4"
        />
        {!this.state.lineid ? (
          ""
        ) : !this.state.currentPage ? (
          <div class="container-mobile m-auto">
            <div class="row w-100 m-0">
              <div class="col-12 register-header">
                <img
                  class="logo"
                  src="/assets/images/logo-ho-white.png"
                  width="60"
                />
                <h2 className="mt-3">เริ่มต้นสมัครสมาชิก HO</h2>
              </div>
            </div>
            <form method="post" onSubmit={this.handleSubmit} id="myForm" style={{marginBottom:'250px'}}>
              <div className="container">
                <div className="register-container">
                  <div class="col-md-12">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="store_name"
                        placeholder="ชื่อร้านค้า"
                        value={this.state.store_name}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="fname"
                        placeholder="ชื่อผู้ติดต่อ"
                        value={this.state.fname}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <InputMask
                        mask="999-999-9999"
                        maskChar={null}
                        type="text"
                        name="usertel"
                        className="form-control"
                        placeholder="เบอร์โทรศัพท์"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="password"
                        className="form-control "
                        name="password"
                        id="password"
                        placeholder="รหัสผ่าน"
                        value={this.state.password}
                        onChange={this.handleChange}
                      />
                      <i class="fas fa-info-circle pr-2" style={{verticalAlign: 'top'}} onClick={this.modalPasswordCondition}></i>
                      
                    </div>
                      {/* {
                        this.state.password.length>0 &&(

                        <PasswordStrengthBar 
                          password={this.state.password} 
                          barColors={['#ddd', '#ef4836', '#f6b44d', '#2b90ef', '#25c281']}
                          scoreWords={['คาดเดาได้ง่าย', 'ง่าย', 'พอใช้', 'ดี', 'ดีมาก']}
                          shortScoreWord={'คุณต้องกำหนดรหัสผ่านอย่างน้อย 6 หลัก'}
                          minLength="6"
                        />
                        )
                      } */}
                    <div className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        name="repassword"
                        placeholder="ยืนยันรหัสผ่าน"
                        value={this.state.repassword}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>
                
                <div className="form-check text-center pt-3">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="exampleCheck1"
                    name="agree"
                    onChange={this.handleChange}
                    value="1"
                  />
                  <label
                    className="form-check-label text-grey link"
                    htmlFor="exampleCheck1"
                  >
                    ยอมรับเงื่อนไข&nbsp;
                  </label>
                  <Link
                    className="link"
                    to={{
                      pathname: "/privacy_policy",
                      state: { prevPath: this.props.location.pathname },
                    }}
                  >
                    นโยบายข้อมูลส่วนบุคคล&nbsp;
                  </Link>
                  ของบริษัท
                </div>
              </div>
              <div class="col-12 footer mx-auto shadow button-submit">
                <input
                  class="w-100 position-relative btn-register"
                  type="submit"
                  value="รับรหัส OTP"
                  onClick={this.handleSubmit}
                />
              </div>
            </form>
          </div>
        ) : (
          this.handleChangePage(this.state.currentPage)
        )}
      </>
    );
  }
}
export default Login;
