import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
//import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import {Route, Link, useParams} from 'react-router-dom'
import {config} from '../config';
import { Store_color } from '../template';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Interest extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        cateid : [],
        userid : sessionStorage.getItem('user_id'),
        store_id: localStorage.getItem('storageStoreID'),
        storeLogo : ''
    };
    


    async componentDidMount(){
        const obj = this;
        if (this.state.store_id) {

          await axios({
            method: "get",
            url:
              config.api.base_url +
              "/store/api/store/" +
              this.state.store_id,
          })
            .then(function (response) {
                console.log(response)
                if (response.status == 200) {
                    console.log(response)
                    obj.setState({
                        storeLogo: response.data[0] ? response.data[0].storelogo : ''
                    });
                }else{
                    
                }
            })
            .catch(function (response) {
                console.log(response)
            });
        }

        const userinterestresult = await axios.get(config.api.base_url+'/users/api/interest/'+this.state.userid)
        if(userinterestresult.status===200){
            
            userinterestresult.data.forEach(element => {
                if (element.userid !== null) {
                    this.state.cateid.push(element.cateid)
                }
            });
            this.setState({cateid:this.state.cateid})

            this.setState({data: userinterestresult.data});
        }


        
        
    }

 
    handleChange = (event,cateid) => {
        if(event.target.checked){
            this.handleChecked(cateid)
        }else{
            this.handleUnChecked(cateid)
        }

        console.log(this.state.cateid)
      
        
    }

    handleChecked(cateid){
        let objCateid = this.state.cateid;
        objCateid.push(cateid)
        this.setState({cateid:objCateid})
        
        
    }
    handleUnChecked(cateid){
        let objCateid = this.state.cateid;
        for(let i=0; i<objCateid.length;i++){
            if(objCateid[i]===cateid){
                objCateid.splice(i, 1);
            }
        }
        this.setState({cateid:objCateid})
       
        
    }
    
    handleSubmit = event => {
        event.preventDefault();
        const bodyFormData = new FormData();
        bodyFormData.set('userid',this.state.userid)
        bodyFormData.set('cateid',this.state.cateid)
        let obj = this
        axios({
            method: 'post',
            url: config.api.base_url+'/users/api/userinterest',
            data: bodyFormData,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                let registerStore = sessionStorage.getItem('registerStore');
                let toStoreID = localStorage.getItem('storageStoreID');
                sessionStorage.removeItem('registerStore')

                if(registerStore === undefined || registerStore === null || registerStore === ''){
                    registerStore = toStoreID;
                }
                
                if(registerStore !== undefined && registerStore !== null && registerStore !== ''){//สแกนจากร้าน
                    let bodyFormDataRegisCard = new FormData();
                    bodyFormDataRegisCard.set('userid',obj.state.userid)
                    bodyFormDataRegisCard.set('storeid',registerStore)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/api/card',
                        data: bodyFormDataRegisCard,
                        headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        if(response.status===200 || response.status===201){
                            window.location='/account_shop/'+registerStore
                        }else{
                            let toStoreID = localStorage.getItem('storageStoreID');
                            if(parseInt(toStoreID)){
                                window.location='/account_shop/'+toStoreID
                            }else{
                                window.location='/shop/4'
                            }
                        }
                    }).catch(function (response) {
                        let toStoreID = localStorage.getItem('storageStoreID');
                    if(parseInt(toStoreID)){
                        window.location='/account_shop/'+toStoreID
                    }else{
                        window.location='/shop/4'
                    }
                    });
                }else{
                    let toStoreID = localStorage.getItem('storageStoreID');
                    if(parseInt(toStoreID)){
                        window.location='/account_shop/'+toStoreID
                    }else{
                        window.location='/shop/4'
                    }
                    
                    
                }
                
                
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
        });
        
    }
    
    render() {

        return ( 
        <>
            <link rel="stylesheet" type="text/css" href="/assets/css/interest.css"/>
            {
                this.state.store_id &&(
                    <Store_color/>
                )
            }
            <div class="container-mobile m-auto bg-yellow">
              
                <div class="shadow header text-center" >
                        {
                            this.state.storeLogo  ? (
                                <>
                                <img src={this.state.storeLogo} alt="logo" className="logo"/>
                                </>
                            ):(
                                <>
                                <img src="/assets/images/logohofrist2.png" alt="logo" className="logo"/>
                                </>
                            )
                        }
                    
                </div>
                <div class="account p-4">
                    <form>
                        
                        <div className="row m-0">
                            {
                                this.state.data.map((row,key) => (
                                    <div className="col-6 p-2" key={key}>
                                        <div className="interest-box shadow">
                                            
                                            <div className={'interest-item'}>
                                                <div className="interest-checkbox">
                                                {row.userid !== null ?
                                                            <div className="custom-control form-control-lg custom-checkbox">  
                                                                <input type="checkbox" className="custom-control-input" id={`${key}`} checked="checked" value={row.cateid} name="cateid" onChange={(e) => (this.handleChange(e,row.cateid))}/>  
                                                                <label className="custom-control-label" htmlFor={`${key}`}/>
                                                            </div>
                                                        :
                                                            <div className="custom-control form-control-lg custom-checkbox">  
                                                                <input type="checkbox" className="custom-control-input" id={`${key}`} value={row.cateid} name="cateid" onChange={(e) => (this.handleChange(e,row.cateid))}/>  
                                                                <label className="custom-control-label" htmlFor={`${key}`}/>
                                                            </div>
                                                } 
                                                </div>
                                                <img className="" src={row.cateimg} alt={row.category}/>
                                                <p>{row.category}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div> 
                        <button className="btn ">
                            <p className="text-center" onClick={this.handleSubmit}>เริ่มกันเลย</p>
                        </button>
                    </form>
                </div>
            </div>
        </>
        )
    }
}
export default Interest;
