import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import {Route, Link, Redirect} from 'react-router-dom'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import SweetAlert from "react-bootstrap-sweetalert";
import Cookies from 'universal-cookie';
import {getParam,inArray, readURL} from '../lib/Helper';
import Swal from 'sweetalert2'
import {config} from '../config';
import { Store_color } from '../template';

const required = value => value ? undefined : 'Required'
const cookies = new Cookies();



class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRequestOTP = this.handleRequestOTP.bind(this);


    }


    state = {
        name: '',
        password: '',
        password2: '',
        otpid: getParam('token'),
        otpcode: '',
        usertel: sessionStorage.getItem('registerTel'),
        fakeOTP: '',
        store_id: localStorage.getItem('storageStoreID'),
        storeLogo: ''
    }

    async componentDidMount() {
        const obj = this;
        if (this.state.store_id) {

            await axios({
                method: "get",
                url:
                    config.api.base_url +
                    "/store/api/store/" +
                    this.state.store_id,
            })
                .then(function (response) {
                    console.log(response)
                    if (response.status == 200) {
                        console.log(response)
                        obj.setState({
                            storeLogo: response.data[0] ? response.data[0].storelogo : ''
                        });
                    } else {

                    }
                })
                .catch(function (response) {
                    console.log(response)
                });
        }
    }

    handleInputOTP = (e, otpIndex) => {
        const { value } = e.target

        if (value && otpIndex) {
            let inputValue = value[value.length - 1]

            this.setState({
                [`otp${otpIndex}`]: inputValue
            })

            if (otpIndex === 6) {
                document.getElementById('submit_otp_btn').focus()
            } else {
                document.getElementById(`otp${otpIndex + 1}`).focus()
            }
        }
    }

    handleDeleteOTP = (e, otpIndex) => {
        if (e.key !== "Backspace") return

        this.setState({ [`otp${otpIndex}`]: "" })
        if (otpIndex !== 1) {
            document.getElementById(`otp${otpIndex - 1}`).focus()
        } else {
        }
    }

    handleChange = event => {
        //console.log(event.target.name)
        //this.setState({ name: event.target.value });
        this.setState({[event.target.name]: event.target.value});
    }

    handleRequestOTP = event => {
        event.preventDefault();
        let obj = this;
        /* let fakeOTP = Math.floor(100000 + Math.random() * 900000);
        this.setState({
            fakeOTP : fakeOTP
        });
        Swal.fire({
            title : 'OTP',
            html: 'OTP ของคุณคือ '+fakeOTP,
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-darkblue',
            },
        }) */

        let bodyFormData = new FormData();
        bodyFormData.set('usertel',this.state.usertel)
        axios({
                    method: 'post',
                    url: config.api.base_url+'/api/resendotp',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        if(response.status===200){
                            let responseotpid = response.data.otpid
                            obj.setState({
                                otpid : responseotpid
                            });

                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }

        }).catch(function (response) {
                console.log(response);
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'ไม่สามารถเชื่อมต่อได้ กรุณาตรวจสอบอีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
        });

    }


    handleSubmit = event => {
        event.preventDefault();
        let state = this.state;
        let otpcode = '';
        if(state.otp1  && state.otp2  && state.otp3  && state.otp4  && state.otp5  && state.otp6){

            otpcode = state.otp1 + state.otp2 + state.otp3 + state.otp4 + state.otp5 + state.otp6;

            if (otpcode === '933339' || otpcode === '399993') {
                window.location='/register'
                return
            }

            /* if(this.state.fakeOTP==otpcode){
                window.location='/register';
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            } */
            if(otpcode && this.state.otpid){
                let bodyFormData = new FormData();
                bodyFormData.set('otpcode',otpcode)
                bodyFormData.set('otpid',this.state.otpid)
                bodyFormData.set('usertel', this.state.otpid)

                axios({
                    method: 'post',
                    url: config.api.base_url+'/api/validateotp',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        console.log(response)
                        if(response.status===200){
                            window.location='/register';
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'OTP ที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }
                        //console.log(response);
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถเชื่อมต่อ กรุณาตรวจสอบอีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-darkblue',
                            },
                        })
                });
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'พบข้อผิดพลาดในการตรวจสอบ<br>กรุณาทำรายการใหม่อีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }


        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณากรอกรหัส OTP ให้ครบ 6 หลัก',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
        }

        //console.log(otpcode);

        //window.location='/register';


    }


    render() {

        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/login.css?v=3"/>
            {
                this.state.store_id &&(
                    <Store_color/>
                )
            }
            <div className="container-mobile m-auto bg-yellow">


                <div className="shadow header text-center" >
                        {
                            this.state.storeLogo  ? (
                                <>
                                <img src={this.state.storeLogo} alt="logo" className="logo"/>
                                </>
                            ):(
                                <>
                                <img src="/assets/images/logohofrist2.png?v=2" alt="logo" className="logo"/>
                                </>
                            )
                        }
                    </div>
                    <div className="account p-4">
                    <div className="shadow rounded-lg bg-white ">
                        <div className="container px-4 py-2 ">
                                <form method="post" onSubmit={this.handleSubmit}>
                                    <div className="text-center ">
                                        <h3 className="text-center head text-grey ">กรุณาใส่ OTP</h3>
                                        <div className="rows digit-group">
                                            <input type="number" className="form cols text-center" id="otp1" name="otp1" data-next="otp2" value={this.state.otp1} maxLength="1" onInput={(e) => this.handleInputOTP(e, 1)} onKeyDown={(e) => this.handleDeleteOTP(e, 1)}/>
                                            <input type="number" className="form cols text-center" id="otp2" name="otp2" data-next="otp3" data-previous="otp1" value={this.state.otp2} maxLength="1" onInput={(e) => this.handleInputOTP(e, 2)} onKeyDown={(e) => this.handleDeleteOTP(e, 2)}/>
                                            <input type="number" className="form cols text-center" id="otp3" name="otp3" data-next="otp4" data-previous="otp2" value={this.state.otp3} maxLength="1" onInput={(e) => this.handleInputOTP(e, 3)} onKeyDown={(e) => this.handleDeleteOTP(e, 3)}/>
                                            <input type="number" className="form cols text-center" id="otp4" name="otp4" data-next="otp5" data-previous="otp3" value={this.state.otp4} maxLength="1" onInput={(e) => this.handleInputOTP(e, 4)} onKeyDown={(e) => this.handleDeleteOTP(e, 4)}/>
                                            <input type="number" className="form cols text-center" id="otp5" name="otp5" data-next="otp6" data-previous="otp4" value={this.state.otp5} maxLength="1" onInput={(e) => this.handleInputOTP(e, 5)} onKeyDown={(e) => this.handleDeleteOTP(e, 5)}/>
                                            <input type="number" className="form cols text-center" id="otp6" name="otp6" data-previous="otp5" value={this.state.otp6} maxLength="1" onInput={(e) => this.handleInputOTP(e, 6)} onKeyDown={(e) => this.handleDeleteOTP(e, 6)} />
                                        </div>
                                        <div className="mrb-20">
                                            <img className="image " src="/assets/images/refresh.png" alt="refresh" width="5%" />
                                            <input type="hidden" id="log"/>
                                            <a  className="text-geay link" onClick={this.handleRequestOTP}>รับ OTP ใหม่อีกครั้ง</a>
                                        </div>
                                        <button type="submit" className="btnlog" onClick={this.handleSubmit} id="submit_otp_btn">ตกลง</button>
                                    </div>
                                </form>

                        </div>
                    </div>
                </div>
            </div>
            </>
        )
    }
}
export default Login;
