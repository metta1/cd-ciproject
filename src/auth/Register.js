import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import {Route, Link, Redirect} from 'react-router-dom'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import SweetAlert from "react-bootstrap-sweetalert";
import Cookies from 'universal-cookie';
import {config} from '../config';
import queryString from 'query-string'
import Swal from 'sweetalert2'
import moment from 'moment';
import DatePicker, { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import th from "date-fns/locale/th"; // the locale you want
import { Store_color } from '../template';
registerLocale("th", th); // register it with the name you want

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const registerRecommend = sessionStorage.getItem('registerRecommend');



class Login extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
        let locationSearch = this.props.location.search
        let getParam       = queryString.parse(locationSearch);
        let refer_url      = '/home';
        if(getParam.referer !== undefined && getParam.referer !== ''){
            refer_url = '/'+getParam.referer;
        }
        let registerTel = sessionStorage.getItem('registerTel');
        let sessLineID = (sessionStorage.getItem('line_id') !== undefined && sessionStorage.getItem('line_id') !== '' && sessionStorage.getItem('line_id') != null) ? sessionStorage.getItem('line_id') : undefined;
        this.state = {
            displayName : '',
            lineid : sessLineID,
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
            refer_url : refer_url,
            userid:null,
            fname:'',
            lname:'',
            email:'',
            usertel:registerTel,
            recommend : (registerRecommend !== undefined && registerRecommend !== null) ? registerRecommend : undefined,
            waitingData : "Y",
            birthday : '',
            gender :  '',
            store_id: localStorage.getItem('storageStoreID'),
            storeLogo : '',
            btnDisable: false
        };

       
     

       
        this.initialize = this.initialize.bind(this);
        this.getUserByTel = this.getUserByTel.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);

        
       
    }


    async componentDidMount(){
        let registerTel = sessionStorage.getItem('registerTel');
        this.getUserByTel(registerTel);

        const obj = this;
        if (this.state.store_id) {

          await axios({
            method: "get",
            url:
              config.api.base_url +
              "/store/api/store/" +
              this.state.store_id,
          })
            .then(function (response) {
                console.log(response)
                if (response.status == 200) {
                    console.log(response)
                    obj.setState({
                        storeLogo: response.data[0] ? response.data[0].storelogo : ''
                    });
                }else{
                    
                }
            })
            .catch(function (response) {
                console.log(response)
            });
        }
        
        
        if (this.state.lineid === undefined) {
            Swal.fire({
                position: 'middle',
                title: 'กรุณารอสักครู่',
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: 'swal-wide'
            })
            await this.initialize();
        }
    }

    

    async getUserByTel(registerTel){
        const userData =  await axios.get(config.api.base_url+'/api/userData/usertel/'+registerTel)
        console.log(userData)
        if(userData.status===200){
            let rowData = userData.data;
            this.setState({
                userid : rowData.userid,
                fname  : rowData.fname,
                lname  : rowData.lname,
                usertel: rowData.usertel,
                email  : rowData.email,
                birthday  : rowData.birthday !== "" ? moment(rowData.birthday).toDate() : '',
                gender  : rowData.gender
            });
            
            
        }
    }

    async initialize() {
        liff.init(async (data) => {
            let profile = await liff.getProfile().then(profile =>{
                this.setState({
                    displayName : profile.displayName,
                    lineid : profile.userId,
                    pictureUrl : profile.pictureUrl,
                    statusMessage : profile.statusMessage
                });
                this.setState({waitingData : "N"})
                Swal.close()
                alert(this.state.lineid)
            }).catch(err =>{
                
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'เกิดข้อผิดพลาดเกี่ยวกับ LINE '+err+'<BR/>กรุณาลองใหม่อีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-black',
                    },
                }).then((result) => {
                    if (result.value) {
                        window.location.reload();
                    }
                })
            });
        }); 
    }

    handleChange = event => {
      
        if(event.target.name=='usertel'){
             let tel = (event.target.value).replace(/[^\d]/g, '');
             this.setState({[event.target.name]: tel});
        }else{
             this.setState({[event.target.name]: event.target.value});
        }
         
         //console.log(this.state)
     }

     async handleChangeDate(date) {
        if (date !== null) {
            this.setState({ birthday: moment(date).toDate() });
        } else {
            this.setState({ birthday: '' });
        }
    };

  
    handleSubmit = event => {
        event.preventDefault();
        //console.log(this.state)
        const obj = this;
        if(this.state.lineid !==undefined){
            obj.setState({
                btnDisable:true
            });
            // check email format
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if ( !re.test(this.state.email) ) {
                Swal.fire({
                    title : 'ผิดพลาด',
                    html: 'กรุณากรอกอีเมลให้ถูกต้อง',
                    icon:'error',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
                obj.setState({
                    btnDisable:false
                });
            } else {
                
                if(this.state.fname && this.state.lname && this.state.email && this.state.usertel && this.state.birthday && this.state.gender){
                    let bodyFormData = new FormData();
                    bodyFormData.set('fname',this.state.fname)
                    bodyFormData.set('lname',this.state.lname)
                    bodyFormData.set('lineID',this.state.lineid)
                    bodyFormData.set('email',this.state.email)
                    bodyFormData.set('usertel',this.state.usertel)
                    bodyFormData.set('userid',this.state.userid)
                    bodyFormData.set('birthday', moment(this.state.birthday).format('YYYY-MM-DD'))
                    bodyFormData.set('gender',this.state.gender)
                    bodyFormData.set('recommend',this.state.recommend)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/users/api/register',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            if(response.status===200 || response.status===201){
                                sessionStorage.removeItem('registerTel')
                                sessionStorage.removeItem('registerStore')
                                let user_id = response.data.userid;
                                sessionStorage.setItem('user_id',user_id)

                                /***** เพิ่ม HO Card */
                                let store_id = 4;
                                let bodyFormDataCard = new FormData();
                                bodyFormDataCard.set('userid',user_id)
                                bodyFormDataCard.set('storeid',store_id)
                                axios({
                                    method: 'post',
                                    url: config.api.base_url+'/api/card',
                                    data: bodyFormDataCard,
                                    headers: {'Content-Type': 'multipart/form-data' }
                                    })
                                    .then(function (response) {
                                        /***** เพิ่มบัตรร้าน */
                                        if(localStorage.getItem('storageStoreID')){
                                            let store_id2 = localStorage.getItem('storageStoreID');
                                            let bodyFormDataCard2 = new FormData();
                                            bodyFormDataCard2.set('userid',user_id)
                                            bodyFormDataCard2.set('storeid',store_id2)
                                            axios({
                                                method: 'post',
                                                url: config.api.base_url+'/api/card',
                                                data: bodyFormDataCard2,
                                                headers: {'Content-Type': 'multipart/form-data' }
                                                })
                                                .then(function (response) {
                                                    window.location='/account_shop/'+store_id2
                                            })
                                            .catch(function (response) {    
                                                window.location='/account_shop/'+store_id2
                                            });
                                        }else{
                                            window.location='/shop/4'
                                        }
                                })
                                .catch(function (response) {    
                                        /***** เพิ่มบัตรร้าน */
                                        if(localStorage.getItem('storageStoreID')){
                                            let store_id2 = localStorage.getItem('storageStoreID');
                                            let bodyFormDataCard2 = new FormData();
                                            bodyFormDataCard2.set('userid',user_id)
                                            bodyFormDataCard2.set('storeid',store_id2)
                                            axios({
                                                method: 'post',
                                                url: config.api.base_url+'/api/card',
                                                data: bodyFormDataCard2,
                                                headers: {'Content-Type': 'multipart/form-data' }
                                                })
                                                .then(function (response) {
                                                    window.location='/account_shop/'+store_id2
                                            })
                                            .catch(function (response) {    
                                                window.location='/account_shop/'+store_id2
                                            });
                                        }else{
                                            window.location='/shop/4'
                                        }
                                });


                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                                obj.setState({
                                    btnDisable:false
                                });
                            }
                            //console.log(response);
                        })
                        .catch(function (response) {
                            obj.setState({
                                btnDisable:false
                            });
                            //handle error
                            //console.log(response);
                    });
                }else{
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'กรุณากรอกข้อมูลให้ครบถ้วน',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-darkblue',
                        },
                    })
                    obj.setState({
                        btnDisable:false
                    });
                }
            }
            
        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'เกิดข้อผิดลาดเกี่ยวกับ Line กรุณาทำรายการใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
            obj.setState({
                btnDisable:false
            });
        }
        
      
        
    }
    

    render() {

        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/register.css?v=3"/>
            {
                this.state.store_id &&(
                    <Store_color/>
                )
            }
            <div class="container-mobile m-auto">
        <div class="row w-100 m-0 shadow bg-back">
            <div class="col-5 mt-back p-0">
                <a href="/login"><img class="mb-6" src="/assets/images/icon-back.png" width="30px"/><span class="text-back">ย้อนกลับ</span></a>
            </div>
            <div class="col-7 p-0">
                        {
                            this.state.storeLogo  ? (
                                <>
                                <img src={this.state.storeLogo} alt="logo" width="60"/>
                                </>
                            ):(
                                <>
                                <img  class="" src="/assets/images/logohofirst.png" width="60"/>
                                </>
                            )
                        }
                
            </div>
        </div>
        <form method="post" onSubmit={this.handleSubmit}>
            <div class="col-md-12 pd-23" style={{marginBottom:'450px'}}>
                <h1 class="title-regis mb-15">ลงทะเบียนผู้ใช้งาน</h1>
    
                
                    <label class="topic" for="fname">ชื่อจริง</label><span class="star">*</span>
                    <input type="text" id="fname" name="fname" placeholder="ชื่อจริง" value={this.state.fname} onChange={this.handleChange}/>
                
                    <label class="topic" for="lname">นามสกุลจริง</label><span class="star">*</span>
                    <input type="text" id="lname" name="lname" placeholder="นามสกุลจริง"  value={this.state.lname} onChange={this.handleChange}/>

                    <div className="form-group mb-0">
                        <div>
                            <span className="topic">วันเกิด</span><span className="star">*</span>
                        </div>
                        <DatePicker
                            selected={this.state.birthday}
                            onChange={this.handleChangeDate}
                            locale={th}
                            dateFormat="dd/MM/yyyy"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            className="form-control"
                            name="birthday"
                            id="birthday"
                        />
                    </div>
                    <div className="form-group">
                        <div>
                            <label className="topic">เพศ</label><span className="star">*</span>
                        </div>
                        <div className="row col-12 pl-0">
                            <div className="col-3 pr-0">
                                <input type="radio" id="genderMen" name="gender" value="1" onChange={this.handleChange} checked={this.state.gender === '1'}/> ชาย
                            </div>
                            <div className="col-3 pr-0 pl-1">
                                <input type="radio" id="genderWomen" name="gender" value="2" onChange={this.handleChange} checked={this.state.gender === '2'}/> หญิง
                            </div>
                            <div className="col-3 pl-0">
                                <input type="radio" id="genderOther" name="gender" value="3" onChange={this.handleChange} checked={this.state.gender === '3'}/> อื่นๆ
                            </div>
                        </div>
                    </div>
                
                    <label class="topic" for="email">อีเมลของคุณ</label><span class="star">*</span>
                    <input type="text" id="email" name="email" placeholder="อีเมล" value={this.state.email} onChange={this.handleChange}/>
                   {/*  <label class="topic" for="tel">เบอร์โทรศัพท์มือถือ</label><span class="star">*</span>
                    <input type="text" id="lname" name="usertel" placeholder="เบอร์โทรศัพท์มือถือ" value={this.state.usertel} onChange={this.handleChange}/> */}
                    
                

                
               
            </div>
            <div class="col-12 footer mx-auto shadow button-submit">
                {
                    this.state.btnDisable ? (
                        <input class="w-100 position-relative btn-register" type="submit" disabled value="กรุณารอสักครู่"/>
                    ) : (
                        <input class="w-100 position-relative btn-register" type="submit" value="ยืนยันการลงทะเบียน" onClick={this.handleSubmit}/>
                    )
                }
                
            </div>
        </form>
            
                
                            
            
    </div>  
            
        
            
            </>
        )
    }
}
export default Login;
