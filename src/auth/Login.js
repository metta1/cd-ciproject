import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import {Route, Link, Redirect} from 'react-router-dom'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import Cookies from 'universal-cookie';
import InputMask from 'react-input-mask';
import Swal from 'sweetalert2'
import {config} from '../config';
import { Store_color } from '../template';

const required = value => value ? undefined : 'Required'
const cookies = new Cookies();



class Login extends Component {
    constructor(){
        super();
        sessionStorage.removeItem('registerTel');

    }

    state = {
        name: '',
        password:'',
        password2:'',
        usertel:'',
        agree:false,
        store_id: localStorage.getItem('storageStoreID'),
        storeData:[],
        storeLogo:'',
        isVerifyOTP:true,
        loading: false
    }

    async componentDidMount() {
        //localStorage.setItem('storageStoreID',122)
        const obj = this;
        if (this.state.store_id) {

          await axios({
            method: "get",
            url:
              config.api.base_url +
              "/store/api/store/" +
              this.state.store_id,
          })
            .then(function (response) {
                if (response.status == 200) {

                    obj.setState({
                        pdpa_condition: response.data.pdpa_condition,
                        storeLogo: response.data[0] ? response.data[0].storelogo : ''
                    });
                }else{

                }
            })
            .catch(function (response) {

            });
        }else{

        }
    }

    handleChange = event => {
       if(event.target.name=='agree'){
        this.setState({agree: event.target.checked});
       }else if(event.target.name=='usertel'){
            let tel = (event.target.value).replace(/[^\d]/g, '');
            this.setState({[event.target.name]: tel});
       }else{
            this.setState({[event.target.name]: event.target.value});
       }

        //console.log(this.state)
    }

    readPolicy = event => {
        let terms = `<div className="text-left p-2">
                        <h4 className="m-0">
                            สิทธิพิเศษของบัตรสมาชิกโหการ์ด
                        </h4>
                    </div>
                    <div className="col p-3-3 w-100">
                            <ul>
                                <li>รับส่วนลดสมาชิกเพิ่มเมื่อซื้อสินค้าที่ร่วมรายการกับ โหหรือสินค้าชั้นนำที่โหคัดสรรมาให้  (ยกเว้นสินค้าลดล้างสต๊อค สินค้าฝากขาย สินค้าในโปรโมชั่น ซื้อ1แถม1 โปรโมชั่นลดราคาตั้งแต่ 50% ขึ้นไป และโปรโมชั่นสินค้าราคาพิเศษและคะแนนเพิ่มพิเศษเฉพาะสมาชิก)</li>
                                <li>ทุกๆยอดซื้อสินค้า 100 บาท รับคะแนนสะสม 1 คะแนนในบัตรสมาชิกโหการ์ดซิลเวอร์ </li>
                                <li>รับสิทธิซื้อสินค้าราคาสุดพิเศษเฉพาะสมาชิกและสินค้าโปรโมชั่นรับคะแนนเพิ่มพิเศษเฉพาะสมาชิก</li>
                                <li>รับสิทธิแลกรีวอร์ดตามเงื่อนไข</li>
                                <li>รับสิทธิพิเศษในเดือนเกิดและส่วนลดโปรโมชั่นส่งตรงถึงสมาชิกโหการ์ดทาง SMS line Broadcast หรือทาง Ho application</li>
                            </ul>


                    </div>
                    <div className="text-left p-2">
                        <h4 className="m-0">
                            เงื่อนไขของบัตรสมาชิกโหการ์ด
                        </h4>
                    </div>
                    <div className="col p-3-3 w-100 mb-5">
                            <p className="mb-1">
                            1. สิทธิพิเศษของบัตรสมาชิกโหการ์ดนี้ใช้ได้เฉพาะโหในประเทศไทย เท่านั้น
                            </p>
                            <p className="mb-0">
                            2. สิทธิพิเศษของบัตรสมาชิกโหการ์ดเป็นไปตามเงื่อนไขและรายละเอียดของแต่ละสิทธิพิเศษ
                            </p>
                            <ul>
                                <li>การสะสมคะแนนจากการซื้อสินค้าจากร้านค้าที่ร่วมรายการ</li>
                                <li>การสะสมคะแนนจากเงื่อนไขพิเศษอื่นๆของโห</li>
                                <li>การแลกคะแนนสะสมเป็นส่วนลดหรือรางวัล</li>

                            </ul>
                            <p className="mb-1">
                            3. บริษัท อิสระเสรี จำกัด ขอสงวนสิทธิโดยไม่ต้องแจ้งล่วงหน้าในการเปลี่ยนสินค้าโปรโมชั่น หรือเปลี่ยนแปลงที่เข้าร่วมการโปรโมชั่นต่างๆ ของบัตรสมาชิกโหการ์ด
                            </p>
                            <p className="mb-1">
                            4. บริษัท ฯ ขอสงวนสิทธิในการยกเลิกเปลี่ยนแปลงเงื่อนไขและสิทธิประโยชน์ของบัตรสมาชิกโหการ์ดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
                            </p>
                    </div>`;
        let terms2 =`<div className="feed p-3" style={{marginBottom: '4em'}}>

        <div className="text-left p-2">
            <h4 className="m-0">
                นโยบายความเป็นส่วนตัว
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                HO ได้จัดทำนโยบายคุ้มครองความเป็นส่วนตัวฉบับนี้ขึ้น เพื่อคุ้มครองข้อมูลส่วนบุคคลของผู้ใช้บริการทุกท่าน (Personal Information) ที่เข้ามายังระบบของ HO ดังนี้
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การเก็บรวบรวมข้อมูลส่วนบุคคล
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                1.	เพื่อความสะดวกในการให้บริการแก่ผู้ใช้บริการทุกท่านที่แวะมายังระบบ (ของหน่วยงาน) ทางระบบจึงได้จัดเก็บรวบรวมข้อมูลส่วนบุคคลของท่านไว้ เช่น อีเมล์แอดเดรส (Email Address) ชื่อ (Name) ที่อยู่หรือที่ทำงาน (Home or Work Address) รหัสไปรษณีย์ (ZIP Code) หรือหมายเลขโทรศัพท์ (Telephone Number) เป็นต้น
            </p>
            <p className="mb-1">
                2.	ในกรณีที่ท่านสมัคร (Sign up/Register) เพื่อสมัครสมาชิกหรือเพื่อใช้บริการอย่างใดอย่างหนึ่ง HO จะ เก็บรวบรวมข้อมูลส่วนบุคคลของท่านเพิ่มเติม เช่น เพศ (Sex) อายุ (Gender) สิ่งที่โปรดปราน/ความชอบ (Preferences/Favorites) ความสนใจ (Interests) และที่อยู่ในการแจ้งค่าใช้จ่าย (Billing Address) เป็นต้น
            </p>
            <p>
                3.	นอกจากนั้น เพื่อสำรวจความนิยมในการใช้บริการ อันจะเป็นประโยชน์ในการนำสถิติไปใช้ในการปรับปรุงคุณภาพในการให้บริการ HO จึงจำเป็นต้องจัดเก็บรวบรวมข้อมูลของท่านบางอย่างเพิ่มเติมด้วย ได้แก่ หมายเลขไอพี (IP Address) ชนิดของโปรแกรมค้นผ่าน (Browser Type) ชื่อโดเมน (Domain Name) บันทึกหน้าเว็บ (web page) ของระบบที่ผู้ใช้เยี่ยมชม เวลาที่เยี่ยมชมระบบ (Access Times) และระบบที่ผู้ใช้บริการเข้าถึงก่อนหน้านั้น (Referring Website Addresses)
            </p>
            <p>
                4.	HO ขอแนะนำให้ท่านตรวจสอบนโยบายความเป็นส่วนตัวของระบบอื่นที่เชื่อมโยงจาก ระบบนี้ เพื่อจะได้ทราบและเข้าใจว่าระบบดังกล่าวเก็บรวบรวม ใช้ หรือดำเนินการเกี่ยวกับข้อมูลส่วยบุคคลของท่านอย่างไร ทั้งนี้ HO ไม่ สามารถรองรับข้อความ หรือรับรองการดำเนินการตามที่ได้มีการประกาศไว้ในระบบดังกล่าวได้ และไม่ขอรับผิดชอบใดๆ หากระบบเหล่านั้นไม่ได้ปฏิบัติการหรือดำเนินการใดๆ ตามนโยบายความเป็นส่วนตัวที่ระบบดังกล่าวได้ประกาศไว้
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                นโยบายความเป็นส่วนตัว
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
                <p className="mb-1">
                HO ได้จัดทำนโยบายคุ้มครองความเป็นส่วนตัวฉบับนี้ขึ้น เพื่อคุ้มครองข้อมูลส่วนบุคคลของผู้ใช้บริการทุกท่าน (Personal Information) ที่เข้ามายังระบบของ HO ดังนี้
                </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การใช้ข้อมูลส่วนตัว
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                1.	HO จะใช้ข้อมูลส่วนบุคคลของท่านเพียงเท่าที่จำเป็น เช่น ชื่อ และที่อยู่ เพื่อใช้ในการติดต่อ ให้บริการ ประชาสัมพันธ์ หรือให้ข้อมูลข่าวสารต่างๆ รวมทั้งสำรวจความคิดเห็นของท่านในกิจการหรือกิจกรรมของ HO เท่านั้น
            </p>
            <p>
                2.	HO ขอรับรองว่าจะไม่นำข้อมูลส่วนบุคคลของท่านที่ HO ได้เก็บรวบรวมไว้ ไปขายหรือเผยแพร่ให้กับบุคคลภายนอกโดยเด็ดขาด เว้นแต่จะได้รับอนุญาตจากท่านเท่านั้น
            </p>
            <p>
                3.	ในกรณีที่ HO ได้ ว่าจ้างหน่วยงานอื่นเพื่อให้ดำเนินการเกี่ยวกับข้อมูลส่วนบุคคลของท่าน เช่น การจัดส่งพัสดุไปรษณีย์ การวิเคราะห์เชิงสถิติในกิจการหรือกิจกรรมของ HO เป็นต้น HO จะกำหนดให้หน่วยงานที่ได้ว่าจ้างให้ดำเนินการดังกล่าว เก็บรักษาความลับและความปลอดภัยของข้อมูลส่วนบุคคลของท่าน และกำหนดข้อห้ามมิให้มีการนำข้อมูลส่วนบุคคลดังกล่าวไปใช้นอกเหนือจาก กิจกรรมหรือกิจการของ HO
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                สิทธิในการควบคุมข้อมูลส่วนบุคคลของท่าน
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                เพื่อประโยชน์ในการรักษาความเป็นส่วนตัวของท่านๆ มีสิทธิที่จะให้มีการใช้หรือแชร์ข้อมูลส่วนบุคคลของท่าน หรืออาจเลือกที่จะไม่รับข้อมูลหรือสื่อทางการตลาดใดๆจาก HO ก็ได้ โดยเพียงแต่ท่านกรอกความจำนงดังกล่าวเพื่อแจ้งให้ HO เป็นลายลักษณ์อักษร หรือ ทาง อีเมล์แอดเดรส (Email Address)
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การรักษาความปลอดภัยสำหรับข้อมูลส่วนบุคคล
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                เพื่อประโยชน์ในการรักษาความลับและความปลอดภัยสำหรับข้อมูลส่วนบุคคลของท่าน HO จึง ได้กำหนดระเบียบภายในหน่วยงานเพื่อกำหนดสิทธิในการเข้าถึงหรือใช้ข้อมูลส่วน บุคคลของท่าน และเพื่อรักษาความลับและความปลอดภัยของข้อมูลบางอย่างที่มีความสำคัญอย่าง ยิ่ง เช่น หมายเลขบัตรเครดิต เป้นต้น HO จึงได้จัดให้มีช่องทางการสื่อสารแบบปลอดภัยสำหรับข้อมูลดังกล่าวด้วยการเข้า รหัสลับข้อมูลดังกล่าว เช่น จัดให้มีการใช้ Secure Socket Layer (SSL) protocol เป็นต้น
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การใช้คุกกี้ (Cookies)
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                “คุกกี้” คือ ข้อมูลที่ HO ส่ง ไปยังโปรแกรมค้นผ่านระบบ (Web browser) ของผู้ใช้บริการ และเมื่อมีการติดตั้งข้อมูลดังกล่าวไว้ในระบบของท่านแล้ว หากมีการใช้ “คุกกี้” ก็จะทำให้ระบบ (ของหน่วยงาน) สามารถบันทึกหรือจดจำข้อมูลของผู้ใช้บริการไว้ จนกว่าผู้ใช้บริการจะออกจากโปรแกรมค้นผ่านระบบ หรือจนกว่าผู้ใช้บริการจะทำการลบ “คุกกี้” นั้นเสีย หรือไม่อนุญาตให้ “คุกกี้” นั้นทำงานต่อไป
                หากท่านเลือกใช้ “คุกกี้” แล้ว ท่านจะได้รับความสะดวกสบายในการท่องระบบมากขึ้น เพราะ “คุกกี้” จะช่วยจดจำระบบที่ท่านแวะ, เยี่ยมชม หรือท่อง
                ทั้งนี้ HO จะนำข้อมูลที่ “คุกกี้” ได้บันทึกหรือเก็บรวบรวมไว้ไปใช้ในการวิเคราะห์เชิงสถิติ หรือในกิจกรรมอื่นของ HO เพื่อปรับปรุงคุณภาพการให้บริการของ HO ต่อไป
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การปรับปรุงนโยบายความเป็นส่วนตัว
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                HO อาจ ทำการปรับปรุงหรือแก้ไขนโยบายความเป็นส่วนตัวโดยไม่ได้แจ้งให้ท่านทราบล่วง หน้า ทั้งนี้ เพื่อความเหมาะสมและมีประสิทธิภาพในการให้บริการ ดังนี้ HO จึงขอแนะนำให้ผู้ใช้บริการอ่านนโยบายความเป็นส่วนตัวทุกครั้งที่แวะชม หรือเยี่ยมชม หรือมีการใช้บริการจากระบบ (ของหน่วยงาน)
            </p>
        </div>
        <div className="text-left p-2">
            <h4 className="m-0">
                การปฏิบัติตามนโยบายความเป็นส่วนตัวและการติดต่อกับ HO
            </h4>
        </div>
        <div className="col p-3-3 w-100 mb-5">
            <p className="mb-1">
                ในกรณีที่ท่านมีข้อสงสัย ข้อเสนอแนะ หรือข้อติชมใดๆ เกี่ยวกับนโยบายความเป็นส่วนตัว หรือการปฏิบัติตามนโยบายความเป็นส่วนตัวฉบับนี้ HO ยินดีที่จะตอบข้อสงสัย รับฟังข้อเสนอแนะ และคำติชมทั้งหลาย อันจะเป็นประโยชน์ต่อการปรับปรุงการให้บริการของ HO ต่อไป โดยท่านสามารถติดต่อกับ HO ตามที่ปรากฏข้างล่างนี้
            </p>
            <p>E-mail : hello@ho.app</p>
        </div>

    </div>`;
        Swal.fire({
            title : 'ข้อกำหนดและเงือนไข',
            html: terms2,
            animation: false,

            confirmButtonText:'ปิด',
            customClass: {
                confirmButton: 'btn-darkblue',
                content : 'swal-terms'
            },
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        const obj = this;
        obj.setState({ loading: true })

        if(this.state.usertel.length<9){
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })

            obj.setState({ loading: false })
        }else if(!this.state.agree){
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณายอมรับข้อกำหนดและเงื่อนไข',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })

            obj.setState({ loading: false })
        }else{

            if(this.state.usertel){
                sessionStorage.setItem('registerTel',this.state.usertel);
                //window.location='/authotp';
                if(this.state.isVerifyOTP){
                    let bodyFormData = new FormData();
                    bodyFormData.set('usertel',this.state.usertel)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/api/sendotp',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            if(response.status===200){
                                let otpid = response.data.otpid
                                window.location='/authotp?token='+otpid;

                                obj.setState({ loading: false })
                            }else{

                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })

                                obj.setState({ loading: false })
                            }
                            //console.log(response);
                        })
                        .catch(function (response) {
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถส่ง OTP ได้ กรุณาตรวจสอบอีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })

                            obj.setState({ loading: false })
                    });
                }else{
                    window.location='/register';
                    obj.setState({ loading: false })
                }
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณากรอกข้อมูลให้ครบถ้วน',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })

                this.setState({ loading: false })
            }
        }



    }


    render() {

        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/login.css?v=3"/>
            {
                this.state.store_id &&(
                    <Store_color/>
                )
            }

            <div className="container-mobile m-auto bg-yellow">
                <div className="shadow header text-center" >
                    {
                        this.state.storeLogo !== '' ? (
                            <>
                            <img src={this.state.storeLogo} alt="logo" className="logo"/>
                            </>
                        ):(
                            <>
                            <img src="/assets/images/logohofrist2.png?v=2" alt="logo" className="logo"/>
                            </>
                        )
                    }
                </div>
                <div className="account p-4">
                <div className="shadow rounded-lg bg-white ">
                    <div className="container px-4 py-2 ">
                                <div className="text-center ">
                                    <h3 className="text-center head text-grey " >เข้าสู่ระบบด้วยโทรศัพท์มือถือ</h3>
                                    <form method="POST" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                        <InputMask mask="999-999-9999" maskChar={null}
                                        name="usertel"
                                        className="form text-grey"
                                        placeholder="เบอร์โทรศัพท์"
                                        onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" id="exampleCheck1" name="agree"  onChange={this.handleChange} value="1"/>
                                            <label
                                            className="form-check-label text-grey link"
                                            htmlFor="exampleCheck1" >
                                            ยอมรับ
                                        </label>
                                            <Link className="link"
                                                to={{
                                                    pathname: "/privacy_policy",
                                                    state: { prevPath: this.props.location.pathname }
                                                }}
                                                >ข้อกำหนดและเงื่อนไข
                                            </Link>

                                        </div>


                                        <button type="button"
                                            className={`btnlog shadow ${this.state.loading ? 'disabled' : ''}`}
                                            disabled={this.state.loading}
                                            onClick={this.handleSubmit}>
                                                เข้าสู่ระบบ
                                        </button>
                                    </form>

                                </div>
                    </div>
                </div>
            </div>

            </div>
            </>
        )
    }
}
export default Login;
