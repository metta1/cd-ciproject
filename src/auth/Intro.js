import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import NumberFormat from 'react-number-format';
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import moment from 'moment';
import 'moment/locale/th';
import DateCountdown from 'react-date-countdown-timer';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import queryString from 'query-string'



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();






class Intro extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        let locationSearch = this.props.location.search
        let getParam       = queryString.parse(locationSearch);
        let refer_url      = '/home';
        if(getParam.referer !== undefined && getParam.referer !== ''){
            refer_url = '/'+getParam.referer;
        }

        this.state = {
            displayName : '',
            userId : '',
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
            refer_url : refer_url
        };
        
        

        /* sessionStorage.setItem('user_id',37) */
        let user_id = sessionStorage.getItem('user_id');
        if(user_id==null){
           
        }else{
            window.location=this.state.refer_url;
            
        }

        this.initialize = this.initialize.bind(this);
        

        
       
    }

    state = {
        couponData : [],
        specialpointData : [],
        feedData : []
    };
    


    async componentDidMount(){
        //let slideIndex = 1;
        //this.showSlides(slideIndex);
        
        //window.addEventListener('load', this.initialize);
         
    }

    initialize() {
        liff.init(async (data) => {
          let profile = await liff.getProfile();
          this.setState({
            displayName : profile.displayName,
            userId : profile.userId,
            pictureUrl : profile.pictureUrl,
            statusMessage : profile.statusMessage
          });
            const result = await axios.get(config.api.base_url+'/api/userData/lineid/'+this.state.userId)
            if(result.status===200){
                let user_id = result.data.userid;
                sessionStorage.setItem('user_id',user_id)
                window.location=this.state.refer_url;
                
            }else{

                let bodyFormData = new FormData();
                bodyFormData.set('name',this.state.displayName)
                bodyFormData.set('lastName','')
                bodyFormData.set('lineID',this.state.userId)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/api/register',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        let user_id = response.data.userid;
                        sessionStorage.setItem('user_id',user_id)
                        window.location=this.state.refer_url;
                    })
                    .catch(function (response) {
                        //handle error
                        //console.log(response);
                });
            }
        }); 
    }

    
    
    plusSlides(n) {
        let slideIndex = 1;
        this.showSlides(slideIndex += n);
    }
    
    currentSlide(n,e) {
        e.preventDefault();
        let slideIndex = 1;
        this.showSlides(slideIndex = n);
    }
    
    showSlides(n) {
      let slideIndex = 1;
      let i;
      let slides = document.getElementsByClassName("mySlides");
      let dots = document.getElementsByClassName("dot");
      if (n > slides.length) {slideIndex = 1}    
      if (n < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";  
          console.log(slides.length)
      }
      for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[n-1].style.display = "block";
      dots[n-1].className += " active";
      
     
     /* slides[slideIndex].style.display = "block";   */
     /* dots[slideIndex-1].className += " active"; */
    }

    toLogin = event => {
        window.location='/login';
    }
   



    
    handleSubmit = event => {
    
        
    }
    
    render(props) {
       
       
    
        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/auth.css?v=3"/>
            <div className="container-mobile m-auto center">
                
   
                <div class="row">
                    <div class="col-12 mt-75">
                    <div class="slideshow-container">
                    <CarouselProvider
                        naturalSlideWidth={100}
                        naturalSlideHeight={140}
                        aspect-ratio={16 / 9}
                        totalSlides={4}
                        
                        className="carousel slide pointer-event"
                    >
                        <Slider className="carousel-inner">
                            <Slide index={0}>
                                <img
                                className="d-block w-100 img-slide shadow-sm"
                                src="/assets/images/s1.png"
                                alt=""
                                
                                />
                            </Slide>
                            <Slide index={1}>
                                <img
                                className="d-block w-100 img-slide"
                                src="/assets/images/s2.png"
                                alt=""
                                
                                />
                            </Slide>
                            <Slide index={2}>
                                <img
                                className="d-block w-100 img-slide"
                                src="/assets/images/s3.png"
                                alt=""
                                
                                />
                            </Slide>
                            <Slide index={3}>
                                <img
                                className="d-block w-100"
                                src="/assets/images/s4.png"
                                alt=""
                                onClick={(e) => (this.toLogin(e))}
                                
                                />
                            </Slide>
                        </Slider>
                        <div className="w-100 text-center p-1">
                            <Dot slide={0} className="nav-dot">
                                <i class="fas fa-dot-circle"></i>
                            </Dot>
                            <Dot slide={1} className="nav-dot">
                                <i class="fas fa-dot-circle"></i>
                            </Dot>
                            <Dot slide={2} className="nav-dot">
                                <i class="fas fa-dot-circle"></i>
                            </Dot>
                            <Dot slide={3} className="nav-dot">
                                <i class="fas fa-dot-circle"></i>
                            </Dot>
                        </div>
                    </CarouselProvider>
                    </div>
                   
                        
                    </div>
                    <div class="col-3"></div>
                    <div class="col-6 button-start mgb-10">
                        <span class="text-start" onClick={(e) => (this.toLogin(e))}>เริ่มกันเลย</span>
                    </div>
                    <div class="col-3"></div>
                   

                </div>
       
    
            </div> 
            </>
           
        )
    }
}
export default Intro;
