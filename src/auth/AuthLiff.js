import React, { Component } from 'react'
import liff from '@line/liff';
import $ from 'jquery'
import {config} from '../config';
export default class AuthLiff extends Component {
    async componentDidMount(){
        liff.init(
            { liffId: config.liff.auth.id }, () => {
            if (liff.isLoggedIn()) {
                liff.getProfile().then(profile => {
                    $('#consoleLog').html(JSON.stringify(profile))
                  }).catch(err => {
                    $('#consoleLog').html(JSON.stringify(err))
                    console.error(err)
                });
            } else {
                liff.login();
            }
        }, err => {
            console.error(err.code, err.message)
            $('#consoleLog').html(JSON.stringify(err))
        });
    }

    lineLogout = e => {
        liff.logout();
        window.location='/';
    }

    render() {
        return (
            <div>
                <div id="consoleLog"></div>
                <button type="button" onClick={this.lineLogout}>logout</button>
            </div>
        )
    }
}
