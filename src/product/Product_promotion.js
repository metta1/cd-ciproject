import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import NumberFormat from 'react-number-format';
import $ from 'jquery'
import {Notification_num} from '../template'

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();



class Product_promotion extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleFilterProduct = this.handleFilterProduct.bind(this);
    }

    state = {
        data : [],
        activeSearchBox:false
    };

    async componentDidMount(){
        // // Product promotion
        const resultProductPromo = await axios.get(config.api.base_url+'/store_product/api/product_promotion')
        if(resultProductPromo.status===200){
            this.setState({data: resultProductPromo.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    async handleFilterProduct(e){
        let keyWord = e.target.value;
        const searchResultProduct = await axios.get(config.api.base_url+'/store_product/api/product_promotion?keywords='+keyWord)
        if(searchResultProduct.status===200){
            this.setState({data: searchResultProduct.data});
        } else {
            this.setState({data: []});
        }
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/product.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-4 pl-4">
                    {
                        this.state.activeSearchBox ? (
                            <div className="row w-100">
                                    
                                <div className="pl-5 w-100">
                                    <div className="icon-back" style={{left: '1rem'}}>
                                        <a onClick={this.handleBackPage}>
                                            <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle shadow ml-2 mt-1"/>
                                        </a>
                                    </div>
                                    <div className=" h-100 pl-3">
                                    <div className="input-group search-group mb-3">
                                        <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterProduct}/>
                                        <div className="input-group-append">
                                            <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                        
                                    </div>
                                        
                                </div>
                            </div>
                        ) : (
                        <div className="row">
                            <div className="col-7 head-text">
                                <Link to="/home">
                                    <img style={{marginTop:'0.1rem'}} src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </Link>
                                <h3 className="m-0 pl-2" style={{whiteSpace:'nowrap', fontWeight:600}}>สินค้าโปรโมชั่น</h3> 
                            </div>
                            <div className="col-5">
                                <div className="float-right">
                                    <div className="col p-1">
                                        <div className="icon-head bg-white rounded-circle text-center d-inline-block mr-1">
                                            <a href="#">
                                                <img className="h-50 w-100" src="/assets/images/icon-search.svg" alt="..." onClick={this.openSearchBox}/>
                                            </a>
                                        </div>
                                        <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                            <a href="/notifications">
                                                <img className="h-50 w-100" src="/assets/images/icon-noti.svg" alt="..." />
                                                <Notification_num/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        )
                    }
                    </div>
                    <div className="row m-0 ml-3 mt-3">
                        { this.state.data.map((row,key) => (
                            <div className="col-6 pl-0 mb-3 " key={key}>
                                <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                                    {
                                        row.point=== undefined || row.point=== '' || row.point=== null ? (
                                            <></>
                                        ):(
                                            <div className="flag">
                                                <img src="/assets/images/icon-flag.svg" alt=""/>
                                                <div className="flag-detail text-center p-1">
                                                    <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                    <p>POINTS</p>
                                                </div>
                                            </div>
                                        )
                                    }
                                    <div className="text-center w-100 product-img">
                                        <img className="img_thumb" src={row.product_img===''? '' : row.product_img} alt="" />
                                    </div>
                                    <div className="product-item-detail px-3 w-100 text-left">
                                        <h4 className="m-0">{row.productname}</h4>
                                        <p className="m-0">{row.catname}</p>
                                    </div>
                                    <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                        <div className="mt-auto">
                                            {
                                                row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null ? (
                                                    <small className="d-block"></small>
                                                ):(
                                                    <small className="d-block"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                )
                                            }
                                            <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                        </div>
                                        <div>
                                            <a href={'/product/'+row.storeid+'/'+row.productid}>
                                                <div className="product-add rounded-circle text-center">
                                                    <img className="w-50" src="/assets/images/icon-add.svg" alt="" />
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}
export default Product_promotion;
