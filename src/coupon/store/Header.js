import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
        
        
        <div className="head bg-black shadow">
            <div className="row w-100 h-100">
                <div className="col-10 h-100" style={{whiteSpace: 'nowrap'}}>
                    <div className=" h-100 p-3 pl-5">
                        <h1 className="m-0 text-white">COUPON</h1> 
                        <p className="text-red">รับส่วนลดพิเศษสำหรับคุณได้เลย</p>
                    </div>
                </div>
                <div className="col-2 h-100">
                    <div className="row float-right h-100 w-100">
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </>)
    
        

    }
}
export default Header;
