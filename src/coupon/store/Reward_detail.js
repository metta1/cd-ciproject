import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation_shop as Navigation} from '../../template'
import moment from 'moment';
import Store_color from '../../template/Store_color'
import {getParam,inArray, time} from '../../lib/Helper';
import {CopyToClipboard} from 'react-copy-to-clipboard';

var Barcode = require('react-barcode');
var QRCode = require('qrcode.react');



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();
let nextTenMin = new Date();
let getNewDate = new Date();
nextTenMin.setMinutes(nextTenMin.getMinutes()+10);
let nextThirtyMin = getNewDate.setMinutes(getNewDate.getMinutes()+30);


class Reward_detail extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        // this.getCoupon = this.getCoupon.bind(this);
        this.handleFilterCoupon = this.handleFilterCoupon.bind(this);
        this.goBack = this.goBack.bind(this);
        this.checkExpireReward = this.checkExpireReward.bind(this);
    }

    state = {
        data : [],
        storeCouponData : [],
        storeid : localStorage.getItem('storageStoreID'),
        userid : sessionStorage.getItem('user_id'),
        activeSearchBox:false,
        rewardData: [],
        itemid:this.props.match.params.itemid,
        point: 0,
        isOpen:false,
        dataModal: undefined,
        // expireScan: nextTenMin,
        expireScan: nextThirtyMin,
        addressresult : []
    };


    async componentDidMount(){
        console.log(localStorage.getItem('backPath'))
        this.checkExpireReward();

        const result = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.storeid+'/'+this.state.userid).catch(function (error) {
            if (error.response) {
              //console.log(error.response.status);
            }
        });
        if(result.status===200){
            console.log(result.data)
            this.setState({
                data: result.data,
            });
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/rewarddetail/'+this.state.storeid+'/'+this.state.itemid+'/'+this.state.userid)
        if(rewardRs.status===200){
            //console.log(rewardRs.data);
            let arrReward = [];
            arrReward.push(rewardRs.data[0])//แก้ไขแสดงซ้ำ ดักที่ API แล้วแต่ดักเผื่อ
            this.setState({
                rewardData: arrReward,
                dataModal : arrReward[0]
            });
        }
        console.log(this.state.rewardData)

        const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountcard/'+this.state.storeid+'/'+this.state.userid)
        if(cardRs.status===200){
            this.setState({
                point: parseFloat(cardRs.data[0]['point'])
            });
            
        }
        console.log(this.state.point)

        const addressresult = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.userid+'?active=1')
        if(addressresult.status===200){
            this.setState({addressdata: addressresult.data[0]});
        }
        console.log(this.state.addressdata);

        $(".selectedOffline").hide();

        if ((this.state.rewardData[0].redeemstatus === '0' && this.state.rewardData[0].reward_type === '2')
            || (this.state.rewardData[0].redeemtime_status === '1')) {
            $(".selectedOption").hide();
            $(".selectedOffline").show();
        }

    }

    async checkExpireReward() {
        const checkTimeExpire = await axios.get(config.api.base_url+'/store_reward/api/rewarddetail/'+this.state.storeid+'/'+this.state.itemid+'/'+this.state.userid)
        if(checkTimeExpire.status===200){
            let expireTime = new Date(checkTimeExpire.data[0].pointdate);
            if (expireTime !== undefined && expireTime !== null && expireTime !== "") {
                expireTime = expireTime.setMinutes(expireTime.getMinutes()+30);
                if (nowDate > expireTime) {
                    const formexpiredata = new FormData();
                    formexpiredata.set('point_id', checkTimeExpire.data[0].point_id);
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/store_reward/api/rewardexpire',
                        data: formexpiredata,
                        headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        console.log(response.data)
                    })
                    .catch(function (response) {
                            //handle error
                            //console.log(response);
                    });
                }
                this.setState({expireScan: expireTime})
            }
        }
    }

    async handleFilterCoupon(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.storeid+'/'+this.state.userid+'?keyword='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    openModal = async () => {
        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/rewarddetail/'+this.state.storeid+'/'+this.state.itemid+'/'+this.state.userid)
        console.log(rewardRs)
        if(rewardRs.status===200){
            //console.log(rewardRs.data);
            let arrReward = [];
            arrReward.push(rewardRs.data[0])//แก้ไขแสดงซ้ำ ดักที่ API แล้วแต่ดักเผื่อ
           
            this.setState({
                rewardData: arrReward,
                dataModal : arrReward[0]
            });
            if(arrReward[0].enddate){
                this.setState({
                    expireScan : arrReward[0].enddate
                })
            }
            //alert('ดึงค่าแสดงใหม่')
            this.setState({
                isOpen: true
            });
        }else{
            //alert('ไม่ดึงค่า')
            this.setState({
                isOpen: true
            });
        }

        console.log(this.state.expireScan)

        
     
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        window.location.reload();
        
    }

    toggleSelected = (e,selected,tabselected) => {
        e.preventDefault();
        $('.reward-code-wraper.offlineactive').removeClass('offlineactive');
        $('#'+selected).addClass('offlineactive')

        $('.tab-select.toggle-active').removeClass('toggle-active');
        $('.fix-bottom-modal .toggle .col div').removeClass('toggle-active');
        $('#'+tabselected).addClass('toggle-active')
    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    coudownTime(enddate){
        var countDownDate = new Date(enddate).getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            let timer = days + " วัน " + hours + " : " + minutes + " : " + seconds + " นาที";
            $('.popupExpireDate').html(timer)
            

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $('.popupExpireDate').html('หมดอายุ')
                //document.getElementById("demo").innerHTML = "EXPIRED";
            }
            }, 1000);
    }

    getCoupon(event,e) {
        const couponid = e.couponid;
        const storeid = this.state.storeid;
        const userid = this.state.userid;

        const data = new FormData();
        data.set('couponid', couponid);
        data.set('storeid', storeid);
        data.set('userid', userid);
        axios({
            method: 'post',
            url: config.api.base_url+'/store_coupon/api/coupon',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (response) {
            console.log(response.data)
        })
        .catch(function (response) {
                //handle error
                //console.log(response);
        });
        let minpurchase = (e.minpurchase).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.coudownTime(e.enddate)
        Swal.fire({
            html: `
            <div class="container-mobile m-auto">
            <div class="my-card mt-4" style="position: relative">
                <div class=" position-relative ">
                    <div id="user_coupon_detail">
                        <div class="coupon">
                            <div class="row w-100 m-0 mb-4" style="flex-wrap: wrap">
                                <div  class="col-12 p-0 pl-2 pr-2">
                                    <div class="position-relative">
                                        <img class="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                        <div class="coupon-detail row ml-1 h-100">
                                            <div class="col-4">
                                                <p class="mt-3 ml-2 mb-0" style="font-weight: normal;font-size: 1rem">${e.catename}</p>
                                                <div class="w-100 text-center mt-2">
                                                    <div class="coupon-detail-img rounded-circle mx-auto shadow">
                                                        <img class="" src=${e.storelogo} alt=""/>
                                                    </div>
                                                    <p class="mt-2" style="font-weight: normal;font-size: 1rem">${e.storename}</p>
                                                </div>
                                            </div>
                                            <div class="col-4 px-0 text-center mt-2 h-100">
                                                <p class="mt-2 mb-0">${e.couponname}</p>
                                                <h2>ลดราคา</h2>
                                                ${e.storebranch!== null ? (
                                                    `<small class="d-block" >สาขา : ${e.storebranch}</small>`
                                                ):(
                                                    <></>
                                                )}
                                                <small class="popupExpireDate" style="font-size: 0.9rem"></small>
                                            </div>
                                            <div class="col-4 p-0 text-center my-auto">
                                                <div class="coupon-detail-side ">
                                                <h1 class="m-0">${e.coupondiscount}${e.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                <h5>ขั้นต่ำ ${minpurchase} บาท</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            คุณได้รับดีลเรียบร้อย!
            `,
            confirmButtonText: '<a href="javascript: window.location=\'/shop/'+storeid+'\'" style="color:white;">เริ่มช็อปกันเลย!</a>',
        })
    }

    getReward(e,rewardData) {
        const _this = this
        e.preventDefault();
        console.log(rewardData)
        // localStorage.setItem('storageStoreID',rewardData.storeid);
        if (rewardData.rewardpoint > this.state.point)  {
            $('#validPoint').addClass('hidden')
            $('#nonValidPoint').removeClass('hidden')
        } else {
        // Swal.fire({
        //     title: 'ยืนยันการแลกคะแนน',
        //     showCloseButton: true,
        //     showCancelButton: true,
        //     reverseButtons :true,
        //     confirmButtonText:
        //       'ตกลง',
        //     focusConfirm: false,
        //     cancelButtonText:
        //       'ยกเลิก',
        //   }).then((result) => {
            // if (result.value) {
                let bodyFormData = new FormData();
                bodyFormData.set('user_id',this.state.userid)
                bodyFormData.set('store_id',this.state.storeid)
                bodyFormData.set('item_id',rewardData.itemid)
                bodyFormData.set('point',rewardData.rewardpoint)
                bodyFormData.set('reward_type',2)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/store_reward/api/pointuse',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                    _this.openModal()
                    //window.location = '/obtain_reward/'+rewardData.itemid;
                    // window.location.reload();
                })
                .catch(function (response) {
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถลบข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง'
                    })
                    
                    //handle error
                    //console.log(response);
                });
            // }
        // })
    }
    }


    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    useReward(e,data)  {
        e.preventDefault();
        // this.getReward(e,data);
        console.log(data)
        let obj = this
        obj.openModal()
        // const formdata = new FormData();
        // formdata.set('itemid', data.itemid);
        // formdata.set('storeid', this.state.storeid);
        // formdata.set('userid', this.state.userid);
        // formdata.set('point_id', data.point_id);
        // axios({
        //     method: 'post',
        //     url: config.api.base_url+'/store_reward/api/redeemreward',
        //     data: formdata,
        //     headers: {'Content-Type': 'multipart/form-data' }
        // })
        // .then(function (response) {
        //     //window.location = "/obtain_reward/"+data.itemid;
        //     obj.openModal()
        // })
        // .catch(function (response) {
        //         //handle error
        //         //console.log(response);
        // });
    }

    goBack(){
        const backUrl = localStorage.getItem('backPath');
        if (localStorage.getItem('isFromLogin') === 'Y') {
            if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+localStorage.getItem('storageStoreID');
            }
        } else if(backUrl !== undefined && backUrl !=='' && backUrl !== null){
            localStorage.removeItem('backPath');
            window.location=backUrl;
        }else{
            this.props.history.goBack();
        }
    }

    confirmOption(e, selected, rewardData) {
        e.preventDefault();
        // console.log(selected)
        let title = "";
        if (selected === 'offline') {
            title = "ยืนยันการแลกรางวัลหน้าร้าน";
        } else {
            title = "ยืนยันการแลกรางวัลออนไลน์";
        }
        Swal.fire({
            title: title,
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ตกลง',
            focusConfirm: false,
            cancelButtonText:
              'ยกเลิก',
          }).then((result) => {
            if (result.value) {
                if (selected === 'offline') {
                    this.setState({expireScan: nextThirtyMin})
                    $(".selectedOption").hide();
                    $(".selectedOffline").show();
                    if (rewardData.redeemtime_status === '0') {
                        this.getReward(e,rewardData);
                        //this.openModal();
                    }
                } else if (selected === 'online') {
                    if (rewardData.enddate !==  "" && (new Date(rewardData.enddate).getTime() < nowDate)) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'เกิดข้อผิดพลาด<BR/>หมดระยะเวลาแลกของรางวัล',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                    } else if (parseInt(rewardData.rewardStock) <= 0) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'เกิดข้อผิดพลาด<BR/>ของรางวัลหมด',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                    } else if (rewardData.rewardpoint > this.state.point)  {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'เกิดข้อผิดพลาด<BR/>สะสมคะแนนเพิ่มอีกนิด',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                    } else if (rewardData.userid !==null && rewardData.redeemstatus==='1' && rewardData.redeemtime_status === '1') {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'เกิดข้อผิดพลาด<BR/>แลกแล้ว',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                    } else {
                        let bodyFormData = new FormData();
                        bodyFormData.set('user_id',this.state.userid)
                        bodyFormData.set('store_id',this.state.storeid)
                        bodyFormData.set('item_id',rewardData.itemid)
                        bodyFormData.set('point',rewardData.rewardpoint)
                        bodyFormData.set('reward_type',1)
                        if (this.state.addressdata !== undefined) {
                            bodyFormData.set('addressID',this.state.addressdata.addressid)
                        }
                        axios({
                            method: 'post',
                            url: config.api.base_url+'/store_reward/api/pointuse',
                            data: bodyFormData,
                            headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            window.location = '/online_reward_detail/'+response.data.point_id;
                        })
                        .catch(function (response) {
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'เกิดข้อผิดพลาด<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง'
                            })
                        });
                    }
                } else {
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'เกิดข้อผิดพลาด<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง'
                    })
                }
            }
        })
    }
    
    render() {
        const couponlist = {
            flexWrap: 'wrap'
        };
        const useCouponButton = {
            backgroundColor: '#12a508',
            height: '2.1rem'
        };
        const getCouponButton = {
            height: '2.1rem'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0',
            height: '2.1rem'
        };
        const coupon = {
            marginTop: '5.5rem'
        };
        return (
            <div className="container-mobile m-auto">
                <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/reward.css?v=3"/>
                <Store_color/>
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="fix-bottom-modal">
                        <div className="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick={this.hideModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        
                            <div className="text-center">
                                {/* <small>แลกใช้เมื่อ</small> */}
                                {
                                    this.state.dataModal !== undefined ? (
                                        
                                        <>
                                            <div className="text-center">
                                                <div id="cardCselected" className="mx-auto w-100 reward-code-wraper offlineactive" style={{height:'220px'}}>
                                                    <h3 id="card-text">CODE</h3>
                                                    <br/>
                                                    <br/>  
                                                    <h2 className="code-text">{this.state.dataModal.rewardcode}</h2>
                                                    <br/>
                                                    <CopyToClipboard text={this.state.dataModal.rewardcode} onCopy={() => this.setState({copied: true})}>
                                                        <button className="btn btn-black">กดเพื่อคัดลอก</button>
                                                    </CopyToClipboard>
                                                </div>
                                                <div id="cardQRselected" className="mx-auto w-50 reward-code-wraper" style={{height:'220px'}}>
                                                    <h3 id="card-text">QR CODE</h3>
                                                    <QRCode value={config.api.base_url+'/reward/redeem?token='+btoa(this.state.dataModal.itemid+','+this.state.userid+','+this.state.dataModal.point_id+','+this.state.storeid+','+time())} size="500" className="w-100 "/>
                                                </div>
                                                <div id="cardBCselected" className="mx-auto w-100  reward-code-wraper" style={{height:'220px'}}>
                                                    <h3 id="card-text">BARCODE</h3>
                                                    <Barcode value={this.state.dataModal.rewardcode} height={'100%'} margin={0} text=''/>
                                                    
                                                </div>
                                                <br/>
                                                <br/>
                                                {/* <Switch onChange={this.handleChange} checked={this.state.checked} className="toggle-barcode" width={200} /> */}
                                               
                                                <div className="toggle mx-auto shadow mt-3 ">
                                                    <div className="row w-100 m-0">
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardCselected','cardCselected-text')}>
                                                            <div className="toggle-active" id="cardCselected-text" style={{fontSize:'20px',paddingTop:'20px'}}>1EA200</div>
                                                        </div>
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardBCselected','cardBCselected-text')}>
                                                            <div className="" id="cardBCselected-text" style={{fontSize:'40px',paddingTop:'13px'}}><i class="fas fa-barcode"></i></div>
                                                        </div>
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardQRselected','cardQRselected-text')}>
                                                            <div className="" id="cardQRselected-text" style={{fontSize:'40px',paddingTop:'13px'}}><i class="fas fa-qrcode"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <div className="text-center">
                                                <h1 className="date-countdown">เหลือเวลา <DateCountdown dateTo={new Date(this.state.expireScan)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                </h1>
                                            </div>
                                            <div className="text-center coupon-text-detail">
                                                <p>กรุณาแสดงรหัสต่อหน้าพนักงาน</p>
                                                <p>แลกรับ {this.state.dataModal.rewardname}</p>
                                            </div>
                                        </>
                                    ) : (
                                        <p>ไม่พบข้อมูล</p>
                                    )
                                }
                            </div>
                           
                        </div>
                </Modal>
                
               
                <div className="head bg-black shadow">
                {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back" style={{left: '1rem'}}>
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCoupon}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                        <div className="col-10 h-100" >
                            <div className="icon-back" style={{whiteSpace: 'nowrap'}}>
                                <a onClick={this.goBack}>
                                    <div className="icon rounded-circle shadow" style={{marginTop: '4px'}}>
                                        <img className="icon-back-discount" src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className=" h-100 p-3 pl-5">
                                    <h3 className="align-title align-title-discount m-0 text-white" style={{whiteSpace:'nowrap', fontSize:'1.75rem'}}>รับส่วนลดพิเศษสำหรับคุณได้เลย</h3>
                            </div>
                        </div>
                        {/*<div className="col-2 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col-3 p-1 mt-3">
                                    <div className="icon shop-coupon-icon rounded-circle">
                                        <img src="/assets/images/icon-search@2x.png" alt="" onClick={this.openSearchBox}/>
                                    </div>
                                </div>
                            </div>
                        </div>*/}
                    </div>
                )}
                </div>
                {/* <Header/> */}
                
                <div className="my-card-discount mt-1">
                    <div className=" position-relative ">
                        <div>
                        {
                            this.state.rewardData.map((row,key) => {
                                return (
                                <>
                                <div className="">
                                    <div className="text-center">
                                        <img className="" src={row.rewardbanner} alt="" style={{width: '100%'}} />
                                    </div>
                                    <div className="text-center">
                                        <h3>{row.rewardname}</h3>
                                    </div>
                                    <div className="p-4">
                                        <div className="row">
                                            {/* <tr className="">
                                                <td className="" style={{width: '40%'}}>
                                                    <p style={{margin: '0px'}}>Points</p>
                                                    <p style={{margin: '0px'}}>{row.rewardpoint}</p>
                                                </td>
                                                <td className="border-left" style={{width: '10%'}}></td>
                                                <td className="" style={{width: '50%'}}>
                                                    <p style={{margin: '0px'}}>Valid Date</p>
                                                    <p style={{margin: '0px'}}><span className="far fa-clock"></span> {moment(row.publicdate).format('DD MMMM Y')} - {moment(row.enddate).format('DD MMMM Y')}</p>
                                                </td>
                                            </tr> */}
                                            <div className="col-5">
                                                <p style={{margin: '0px'}}>Points</p>
                                                <p style={{margin: '0px'}}>{row.rewardpoint}</p>
                                            </div>
                                            <div className="col-1 border-left"></div>
                                            <div className="col-5 p-0">
                                                <p style={{margin: '0px'}}>ระยะเวลาแลกของรางวัล</p>
                                                {row.enddate != "" ?
                                                <p style={{margin: '0px'}}><span className="far fa-clock"></span> {moment(row.publicdate).add(543, 'year').format('D MMM Y')} - {moment(row.enddate).add(543, 'year').format('D MMM Y')}</p>
                                                :
                                                <p style={{margin: '0px'}}><span className="far fa-clock"></span> ไม่มีกำหนด</p>
                                                }
                                            </div>
                                        </div>
                                        <div className="border-top mt-1">
                                            <h4 style={{padding: '0px'}}>ไฮไลท์</h4>
                                            <p>{row.rewarddetail}</p>
                                        </div>
                                        <div className="border-top">
                                            <h4 style={{padding: '0px'}}>รายละเอียดและเงื่อนไข</h4>
                                            <p>
                                                <li>{row.rewardcodition}</li>
                                            </p>
                                        </div>
                                        <div className="selectedOption text-center">
                                            {
                                                (row.enddate !== "" && (new Date(row.enddate).getTime() < nowDate)) ?
                                                    <button className="btn btn-secondary text-center" disabled id="nonValidDate" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}>
                                                        <p style={{margin: '0px'}} className="p-2">หมดระยะเวลาแลกของรางวัล</p>
                                                    </button>
                                                :
                                                parseInt(row.rewardStock) <= 0 ?
                                                    <button className="btn btn-secondary text-center" disabled id="nonStock" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}>
                                                        <p style={{margin: '0px'}} className="p-2">ของรางวัลหมด</p>
                                                    </button>
                                                :
                                                <>
                                                <button className={row.offline=='1' ? 'btn btn-secondary text-center m-1' : 'btn btn-success text-center m-1'} disabled={row.offline=='1' ? true : false}
                                                    style={{width: '45%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={(e) => this.confirmOption(e, 'offline', row)}>
                                                    <p style={{margin: '0px'}} className="p-2">แลกรางวัลหน้าร้าน</p>
                                                </button>
                                                <button className={row.online=='1' ? 'btn btn-secondary text-center m-1' : 'btn btn-success text-center m-1'} disabled={row.online=='1' ? true : false}
                                                    style={{width: '45%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={(e) => this.confirmOption(e, 'online', row)}>
                                                    <p style={{margin: '0px'}} className="p-2">แลกรางวัลออนไลน์</p>
                                                </button>
                                                </>
                                            }
                                        </div>
                                        <div className="selectedOffline">
                                            {
                                                // (new Date(row.enddate).getTime() < nowDate) ?
                                                //     <button className="btn btn-secondary text-center" disabled id="nonValidDate" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}>
                                                //         <p style={{margin: '0px'}} className="p-2">หมดระยะเวลาแลกของรางวัล</p>
                                                //     </button>
                                                // :
                                                // parseInt(row.rewardStock) <= 0 ?
                                                //     <button className="btn btn-secondary text-center" disabled id="nonStock" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}>
                                                //         <p style={{margin: '0px'}} className="p-2">ของรางวัลหมด</p>
                                                //     </button>
                                                // :
                                                // row.userid !==null ? (
                                                     (
                                                        row.redeemstatus==='1' && row.redeemtime_status === '1' ? (
                                                            <>
                                                                <button className="btn btn-secondary text-center" disabled type="button" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}  data-primary={key}>
                                                                    <p style={{margin: '0px'}} className="p-2">แลกแล้ว</p>
                                                                   
                                                                </button>
                                                                
                                                            </>
                                                            ):(
                                                            row.redeemstatus==='2' ? (
                                                                <>
                                                                <button className="btn btn-secondary text-center" disabled type="button" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}  data-primary={key}>
                                                                    <p style={{margin: '0px'}} className="p-2">หมดเวลาแลกของรางวัล</p>
                                                                </button>
                                                                </>
                                                            ) : (
                                                            // row.redeemstatus==='0' && row.reward_type ==='2' ? (
                                                                <>
                                                                    {/* <a href={"/obtain_reward/"+row.itemid}> */}
                                                                    <button className="btn btn-success text-center"  style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}  data-primary={key} onClick={row.redeemstatus === "0" ? (e) => this.useReward(e,row) : ''}>
                                                                    {row.enddate != "" ?
                                                                        <>
                                                                        <p style={{margin: '0px'}}>แสดง QR</p>
                                                                        <small className="time-text-overflow" style={{padding: '0px'}}>
                                                                            <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                                            </p>
                                                                        </small>
                                                                        </>
                                                                        :
                                                                        <p style={{margin: '0px'}} className="p-2">แสดง QR</p>
                                                                    }
                                                                    </button>
                                                                    {/* </a> */}
                                                                </>
                                                            // ) : (
                                                            //     <>
                                                            //         <button className="btn btn-success text-center" id="validPoint" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={(e) => this.getReward(e,row)} data-primary={key}>
                                                            //             <p style={{margin: '0px'}}>แลกคะแนน</p>
                                                            //             <small className="time-text-overflow" style={{padding: '0px'}}>
                                                            //                 <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                            //                 </p>
                                                            //             </small>
                                                            //         </button>
                                                            //     </>
                                                            // ))
                                                            ))
                                                    )
                                                  
                                                    
                                                // ) : (
                                                //     <>
                                                //         <button className="btn btn-success text-center" id="validPoint" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={(e) => this.getReward(e,row)} data-primary={key}>
                                                //         {row.enddate != "" ?
                                                //             <>
                                                //             <p style={{margin: '0px'}}>แลกคะแนน</p>
                                                //             <small className="time-text-overflow" style={{padding: '0px'}}>
                                                //                 <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                //                 </p>
                                                //             </small>
                                                //             </>
                                                //             :
                                                //             <p style={{margin: '0px'}} className="p-2">แลกคะแนน</p>
                                                //         }
                                                //         </button>
                                                //     </>
                                                // )
                                            }
                                            
                                            <button className="btn btn-orange text-center hidden" id="nonValidPoint" style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }}>
                                            {row.enddate != "" ?
                                                <>
                                                <p style={{margin: '0px'}}>สะสมคะแนนเพิ่มอีกนิด</p>
                                                <small className="time-text-overflow" style={{padding: '0px'}}>
                                                    <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                    </p>
                                                </small>
                                                </>
                                                :
                                                <p style={{margin: '0px'}} className="p-2">สะสมคะแนนเพิ่มอีกนิด</p>
                                            }
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                </>
                                )   
                            })
                        }
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                    
                {/* <Navigation active={'deal'}/> */}

            </div>
        )
    }
}
export default Reward_detail;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;