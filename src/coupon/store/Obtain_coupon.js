import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation_shop as Navigation} from '../../template'
import moment from 'moment';
import Store_color from '../../template/Store_color'
import {getParam,inArray, time} from '../../lib/Helper';

var Barcode = require('react-barcode');
var QRCode = require('qrcode.react');




const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();
let nextTenMin = new Date();
nextTenMin.setMinutes(nextTenMin.getMinutes()+10);

class Obtain_coupon extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.goBack = this.goBack.bind(this);
    }

    state = {
        data : [],
        storeid : localStorage.getItem('storageStoreID'),
        userid : sessionStorage.getItem('user_id'),
        couponid:this.props.match.params.couponid
    };


    async componentDidMount(){
        // const couponRs = await axios.get(config.api.base_url+'/store_coupon/api/coupondetail/4/10/')
        const couponRs = await axios.get(config.api.base_url+'/store_coupon/api/coupondetail/'+this.state.storeid+'/'+this.state.couponid+"?user_id="+this.state.userid)
        if(couponRs.status===200){
            this.setState({
                data: couponRs.data,
            });
        }
        console.log(this.state.data)

    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }


    handleBackPage = e => {
        window.location.reload();
        
    }

    goBack(){
        if (localStorage.getItem('isFromLogin') === 'Y') {
            if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+localStorage.getItem('storageStoreID');
            }
        } else {
            this.props.history.goBack();
        }
    }

    toggleSelected = (e,selected,tabselected) => {
        e.preventDefault();
        $('.reward-code-wraper.offlineactive').removeClass('offlineactive');
        $('#'+selected).addClass('offlineactive')

        $('.tab-select.toggle-active').removeClass('toggle-active');
        $('#'+tabselected).addClass('toggle-active')
    }
    
    render() {
        return (
            <div className="container-mobile m-auto">
                <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
                
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        <div className="col-10 h-100" >
                            <div className="icon-back" style={{whiteSpace: 'nowrap'}}>
                                <a onClick={this.goBack}>
                                    <div className="icon rounded-circle shadow" >
                                        <img src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className=" h-100 p-3 pl-5">
                                <h3 className="align-title m-0 text-white">แลกของรางวัล</h3> 
                            </div>
                        </div>
                    </div>
                </div>
                {/* <Header/> */}
                
                <div style={{marginTop:'120px'}}>
                    {this.state.data === undefined || this.state.data === '' || this.state.data.length === 0 ? 
                        <></>
                    :
                    this.state.data.map((row,key) =>
                        <div key={key}>

                            <div className="text-center">
                                <div id="cardCselected" className="mx-auto w-100 reward-code-wraper offlineactive" style={{height:'220px'}}>
                                    <h5 id="card-text">CODE</h5>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <h3>{row.couponcode}</h3>
                                </div>
                                <div id="cardQRselected" className="mx-auto w-50 reward-code-wraper" style={{height:'220px'}}>
                                    <h5 id="card-text">QR CODE</h5>
                                    <QRCode value={config.api.base_url+'/reward/redeem?token='+btoa(row.couponid+','+this.state.userid+','+time())} size="500" className="w-100 "/>
                                </div>
                                <div id="cardBCselected" className="mx-auto w-100  reward-code-wraper" style={{height:'220px'}}>
                                    <h5 id="card-text">BARCODE</h5>
                                    <Barcode value={row.couponcode} height={'100%'} margin={0} text=''/>
                                    {/* <h5>{row.cardcode}</h5> */}
                                </div>
                                <br/>
                                <br/>
                                {/* <Switch onChange={this.handleChange} checked={this.state.checked} className="toggle-barcode" width={200} /> */}
                                <div className="toggle mx-auto shadow w-75 mt-3 rounded">
                                    <div className="row w-100 m-0">
                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardCselected','cardCselected-text')}>
                                            <h5 id="cardCselected-text" className="m-0 tab-select toggle-active">CODE</h5>
                                        </div>
                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardQRselected','cardQRselected-text')}>
                                            <h5 id="cardQRselected-text" className="m-0 tab-select">QR CODE</h5>
                                        </div>
                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardBCselected','cardBCselected-text')}>
                                            <h5 id="cardBCselected-text" className="m-0 tab-select">BARCODE</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div className="text-center">
                                <p><h1>เหลือเวลา <DateCountdown dateTo={new Date(nextTenMin)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                </h1></p>
                            </div>
                            <div className="text-center">
                                <p>กรุณาแสดงรหัสต่อหน้าพนักงาน</p>
                                <p>แลกรับ {row.couponname}</p>
                            </div>
                        </div>
                    )}
                </div>
                <br/>
                <br/>
                <br/>
                    
                {/* <Navigation active={'deal'}/> */}

            </div>
        )
    }
}
export default Obtain_coupon;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;