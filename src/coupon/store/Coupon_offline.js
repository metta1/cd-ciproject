import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation_shop as Navigation} from '../../template'
import moment from 'moment';
import Store_color from '../../template/Store_color'
import {getParam,inArray, time} from '../../lib/Helper';
import {CopyToClipboard} from 'react-copy-to-clipboard';

var Barcode = require('react-barcode');
var QRCode = require('qrcode.react');


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();
let nextTenMin = new Date();
nextTenMin.setMinutes(nextTenMin.getMinutes()+10);

sessionStorage.setItem('useOfflineCoupon', 'N');


class Coupon_offline extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        // this.getCoupon = this.getCoupon.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    state = {
        data : [],
        storeCouponData : [],
        storeid : localStorage.getItem('storageStoreID'),
        userid : sessionStorage.getItem('user_id'),
        couponOfflineData: [],
        couponid:this.props.match.params.couponid,
        isOpen:false,
        couponOfflineDataModal: undefined,
        expireScan: nextTenMin
    };


    async componentDidMount(){
        console.log("start : ",this.state.useOfflineCoupon)
        // const couponOfflineRs = await axios.get(config.api.base_url+'/store_coupon/api/coupondetail/4/10/')
        const couponOfflineRs = await axios.get(config.api.base_url+'/store_coupon/api/coupondetail/'+this.state.storeid+'/'+this.state.couponid+'?user_id='+this.state.userid)
        if(couponOfflineRs.status===200){
            this.setState({
                couponOfflineData: couponOfflineRs.data,
                couponOfflineDataModal: couponOfflineRs.data[0],
            });

            if(couponOfflineRs.data[0].getdate !== null && couponOfflineRs.data[0].getdate !== undefined){
                let expireScan = new Date();
                expireScan.setMinutes(expireScan.getMinutes()+30);
               /*  this.setState({
                    //expireScan : expireScan
                }) */

            }
        }

    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    coudownTime(enddate){
        var countDownDate = new Date(enddate).getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            let timer = days + " วัน " + hours + " : " + minutes + " : " + seconds + " นาที";
            $('.popupExpireDate').html(timer)
            

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $('.popupExpireDate').html('หมดอายุ')
                //document.getElementById("demo").innerHTML = "EXPIRED";
            }
            }, 1000);
    }


    handleBackPage = e => {
        window.location.reload();
        
    }

    goBack(){
        const backUrl = localStorage.getItem('backPath');
        if (localStorage.getItem('isFromLogin') === 'Y') {
            if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+localStorage.getItem('storageStoreID');
            }
        } else if(backUrl !== undefined && backUrl !=='' && backUrl !== null){
            localStorage.removeItem('backPath');
            window.location=backUrl;
        }else{
            this.props.history.goBack();
        }
        
    }

    confirmUserCoupon(e,state) {
        let obj = this;
        e.preventDefault();
        if (sessionStorage.getItem('useOfflineCoupon') === 'N') {
            Swal.fire({
                icon:'info',
                html: 'คูปองจะมีระยะเวลา 10 นาทีหลังจากกดใช้งานและจะไม่สามารถใช้งานคูปองนี้ได้อีก',
                showCancelButton: true,
                confirmButtonText: `ตกลง`,
                cancelButtonText: `ยกเลิก`,
            }).then((result) => {
                if (result.value) {
                    let nextTenMin = new Date();
                    nextTenMin.setMinutes(nextTenMin.getMinutes()+10);
                    
                    sessionStorage.setItem('useOfflineCoupon', 'Y')
                    obj.setState({expireScan: nextTenMin});
                    obj.useCoupon(e,state)
                }
            })
        } else {
            obj.useCoupon(e,state)
        }
    }

    useCoupon(e,state) {
        /*this.openModal()*/
        let obj = this
        e.preventDefault();
        localStorage.getItem('storageStoreID');
        const data = new FormData();
        data.set('couponid', state.couponid);
        data.set('storeid', this.state.storeid);
        data.set('userid', this.state.userid);
        // obj.openModal()
        axios({
            method: 'post',
            url: config.api.base_url+'/store_coupon/api/usecouponoffline',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(async function (response) {
            //window.location = "/obtain_coupon/"+state.couponid;
            /* const couponOfflineRs = await axios.get(config.api.base_url+'/store_coupon/api/coupondetail/'+obj.state.storeid+'/'+state.couponid+"?user_id="+obj.state.userid)
            if(couponOfflineRs.status===200){
                obj.setState({
                    couponOfflineDataModal: couponOfflineRs.data[0],
                });
                obj.openModal()
            
            } ไม่ต้องดึงใหม่แล้ว*/
            obj.openModal()
            
        })
        .catch(function (response) {
                //handle error 
                console.log(response);
        }); 
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
     
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        
    }

    toggleSelected = (e,selected,tabselected) => {
        e.preventDefault();
        $('.reward-code-wraper.offlineactive').removeClass('offlineactive');
        $('#'+selected).addClass('offlineactive')

        $('.tab-select.toggle-active').removeClass('toggle-active');
        $('.fix-bottom-modal .toggle .col div').removeClass('toggle-active');
        $('#'+tabselected).addClass('toggle-active')
    }

    getCoupon(event,e) {
        event.preventDefault();
        const couponid = e.couponid;
        const storeid = this.state.storeid;
        const userid = sessionStorage.getItem('user_id');

        const data = new FormData();
        data.set('couponid', couponid);
        data.set('storeid', storeid);
        data.set('userid', userid);
        axios({
            method: 'post',
            url: config.api.base_url+'/store_coupon/api/coupon',
            data: data,
            headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (response) {
            window.location.reload();
            //console.log(response);
        })
        .catch(function (response) {
                //handle error
                //console.log(response);
        });
    }
    
    render() {
        const couponlist = {
            flexWrap: 'wrap'
        };
        const useCouponButton = {
            backgroundColor: '#12a508',
            height: '2.1rem'
        };
        const getCouponButton = {
            height: '2.1rem'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0',
            height: '2.1rem'
        };
        const coupon = {
            marginTop: '5.5rem'
        };
        return (
            <div className="container-mobile m-auto">
                <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="fix-bottom-modal">
                        <div className="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick={this.hideModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        
                            <div className="text-center">
                               
                                {
                                    this.state.couponOfflineDataModal !== undefined ? (
                                        
                                        <>
                                            {
                                                this.state.couponOfflineDataModal.getdate !== null &&(
                                                    <small>แลกใช้เมื่อ {moment(this.state.couponOfflineDataModal.getdate).format('DD MMM Y H:mm')}</small>
                                                ) 
                                            }
                                            <div className="text-center">
                                                <div id="cardCselected" className="mx-auto w-100 reward-code-wraper offlineactive" style={{height:'220px'}}>
                                                    <h3 id="card-text">CODE</h3>
                                                    <br/>
                                                    <br/>  
                                                    <h2 className="code-text">{this.state.couponOfflineDataModal.couponcode}</h2>
                                                    <br/>
                                                    <CopyToClipboard text={this.state.couponOfflineDataModal.couponcode} onCopy={() => this.setState({copied: true})}>
                                                        <button className="btn btn-black">กดเพื่อคัดลอก</button>
                                                    </CopyToClipboard>
                                                </div>
                                                <div id="cardQRselected" className="mx-auto w-50 reward-code-wraper" style={{height:'220px'}}>
                                                    <h3 id="card-text">QR CODE</h3>
                                                    {this.state.couponOfflineDataModal.isStorePos ?
                                                        <QRCode value={this.state.couponOfflineDataModal.couponcode} size="500" className="w-100 "/>
                                                    :
                                                        <QRCode value={config.api.base_url+'/reward/redeem?token='+btoa(this.state.couponOfflineDataModal.couponid+','+this.state.userid+','+time())} size="500" className="w-100 "/>
                                                    }
                                                    {/* <QRCode value={config.api.base_url+'/reward/redeem?token='+btoa(this.state.couponOfflineDataModal.couponid+','+this.state.userid+','+time())} size="500" className="w-100 "/> */}
                                                    {/* <QRCode value={this.state.couponOfflineDataModal.couponcode} size="500" className="w-100 "/> */}
                                                </div>
                                                <div id="cardBCselected" className="mx-auto w-100  reward-code-wraper" style={{height:'220px'}}>
                                                    <h3 id="card-text">BARCODE</h3>
                                                    <Barcode value={this.state.couponOfflineDataModal.couponcode} height={'100%'} margin={0} text=''/>
                                                    {/* <h5>{this.state.couponOfflineDataModal.cardcode}</h5> */}
                                                </div>
                                                <br/>
                                                <br/>
                                                {/* <Switch onChange={this.handleChange} checked={this.state.checked} className="toggle-barcode" width={200} /> */}
                                               
                                                <div className="toggle mx-auto shadow mt-3 ">
                                                    <div className="row w-100 m-0">
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardCselected','cardCselected-text')}>
                                                            <div className="toggle-active" id="cardCselected-text" style={{fontSize:'20px',paddingTop:'20px'}}>1EA200</div>
                                                        </div>
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardBCselected','cardBCselected-text')}>
                                                            <div className="" id="cardBCselected-text" style={{fontSize:'40px',paddingTop:'13px'}}><i class="fas fa-barcode"></i></div>
                                                        </div>
                                                        <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardQRselected','cardQRselected-text')}>
                                                            <div className="" id="cardQRselected-text" style={{fontSize:'40px',paddingTop:'13px'}}><i class="fas fa-qrcode"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <div className="text-center">
                                                <h1 className="date-countdown">เหลือเวลา <DateCountdown dateTo={new Date(this.state.expireScan)} callback={() => window.location.reload()} locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                </h1>
                                            </div>
                                            <div className="text-center coupon-text-detail">
                                                <p>กรุณาแสดงรหัสต่อหน้าพนักงาน</p>
                                                <p>แลกรับ {this.state.couponOfflineDataModal.couponname}</p>
                                            </div>
                                        </>
                                    ) : (
                                        <p>ไม่พบข้อมูล</p>
                                    )
                                }
                            </div>
                           
                        </div>
                </Modal>
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        <div className="col-10 h-100" >
                            <div className="icon-back" style={{whiteSpace: 'nowrap'}}>
                                {/* <a href={'/deal/'+this.state.storeid}> */}
                                <a onClick={this.goBack}>
                                    <div className="icon rounded-circle shadow " >
                                        <img className="icon-back-discount" src="/assets/images/back.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className=" h-100 p-3 pl-5">
                                <h3 className="align-title align-title-discount m-0 text-white"style={{fontSize:'1.75rem', whiteSpace:'nowrap'}}>ส่วนลดพิเศษสำหรับคุณ</h3> 
                            </div>
                        </div>
                    </div>
                </div>
                {/* <Header/> */}
                
                <div className="my-card-discount mt-1">
                    <div className=" position-relative ">
                        <div>
                        {
                            this.state.couponOfflineData.map((row) => {
                                return (
                                <>
                                <div className="">
                                    <div className="text-center">
                                        <img className="" src={row.couponabnner} alt="" style={{width: '100%'}} />
                                    </div>
                                    
                                    <div className="p-4">
                                        <div className="text-center">
                                            <h3>{row.couponname}</h3>
                                        </div>
                                        <div >
                                            <p style={{margin: '0px'}}>Valid Date</p>
                                            <p style={{margin: '0px'}}><span className="far fa-clock"></span> {moment(row.publicdate).format('DD MMMM Y')} - {moment(row.enddate).format('DD MMMM Y')}</p>    
                                        </div>
                                        <hr></hr>
                                        <div>
                                            <h4 style={{padding: '0px'}}>ไฮไลท์</h4>
                                            <p>{row.coupondetail}</p>
                                        </div>
                                        <div>
                                            <h4 style={{padding: '0px'}}>สาขา</h4>
                                            <p>{row.couponbranch}</p>
                                        </div>
                                        <div className="border-top">
                                            <h4 style={{padding: '0px'}}>รายละเอียด และ เงื่อนไข</h4>
                                            <p>
                                                <li>{row.couponondition}</li>
                                            </p>
                                        </div>
                                        <div>
                                            {row.coupon_status === undefined ?
                                            <button className="btn btn-orange text-center text-white" 
                                                style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={(e) => this.getCoupon(e,row)}>
                                                <p style={{margin: '0px'}}>รับคูปอง</p>
                                                <small className="time-text-overflow" style={{padding: '0px'}}>
                                                    <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                    </p>
                                                </small>
                                            </button>
                                            :
                                            <button className={row.coupon_status === "0" ? "btn btn-success text-center" : "btn text-center btn-secondary"} 
                                                style={{width: '100%', fontSize: '1.4rem', borderRadius: '10px', padding: '0px' }} onClick={row.coupon_status === "0" ? (e) => this.confirmUserCoupon(e,row) : ''}>
                                                <p style={{margin: '0px'}}>สแกนคูปอง</p>
                                                <small className="time-text-overflow" style={{padding: '0px'}}>
                                                    <p style={{margin: '0px'}}>เวลาที่เหลือ <DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  />
                                                    </p>
                                                </small>
                                            </button>
                                            }
                                        </div>
                                    </div>
                                </div>
                                </>
                                )   
                            })
                        }
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                    
                {/* <Navigation active={'deal'}/> */}

            </div>
        )
    }
}
export default Coupon_offline;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;