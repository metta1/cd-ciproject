import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../../config';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2'
import $ from 'jquery'
import Modal from 'react-bootstrap4-modal';
import {Navigation_shop as Navigation, Notification_num} from '../../template'



const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();


class Coupon_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        // this.getCoupon = this.getCoupon.bind(this);
        this.handleFilterCoupon = this.handleFilterCoupon.bind(this);
    }

    state = {
        data : [],
        rewardData : [],
        storeCouponData : [],
        storeid : localStorage.getItem('storageStoreID'),
        userid : sessionStorage.getItem('user_id'),
        activeSearchBox:false,
        point:0,
        colorData : [],
        numNoti:0,
        waitingData : true
    };


    styleButtonCoupon = (txtE,obj,key) => {
        const couponid = obj.couponid
        if(txtE==='add' && obj.coupontype=='1'){
            
            $("button[data-couponid='"+couponid+"'").css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
            $('body').addClass('modal-open')
            $('<div class="modal-backdrop fade show"></div>').appendTo('body');
            $('#couponModal-'+key).css({'display':'block'})
        }
        
    }

    closeCouponModal = (e,key) => {
        e.preventDefault();
        $('#couponModal-'+key).css({'display':'none'})
        $('body').removeClass('modal-open')
        $('.modal-backdrop').remove();
        window.location.reload();
    }

   

    async componentDidMount(){
        
        const obj = this
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.storeid)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
        if(this.state.colorData !== undefined){
            let coloricon   = this.state.colorData['coloricon'];
        }

         //Noti
         const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.storeid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                console.log(storeNotiResult)
                this.setState({
                    numNoti:numNoti
                })
            }
        } 
        const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.storeid);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })
           
        }
        //====
        const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountcard/'+this.state.storeid+'/'+this.state.userid)
        console.log(cardRs)
        if(cardRs.status===200){
            this.setState({
                point: parseFloat(cardRs.data[0]['point'])
            });
            
        }

        const result = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.storeid+'/'+this.state.userid).catch(function (error) {
            if (error.response) {
              //console.log(error.response.status);
            }
        });
        if(result.status===200){
            console.log(result.data)
            this.setState({data: result.data});
        }
        console.log(this.state.data)

        const rewardResult = await axios.get(config.api.base_url+'/store_reward/api/reward/'+this.state.storeid+'?user_id='+this.state.userid).catch(function (error) {
            if (error.response) {
              //console.log(error.response.status);
            }
        });
        console.log(rewardResult)
        if(rewardResult.status===200){
            console.log(rewardResult.data)
            this.setState({rewardData: rewardResult.data});
        }

        

        

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })

        
        this.setState({waitingData: false})
    }

    async handleFilterCoupon(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.storeid+'/'+this.state.userid+'?keyword='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-coupon').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-coupon').css({"padding-top": 'unset'})

        this.componentDidMount();
    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    coudownTime(enddate){
        var countDownDate = new Date(enddate).getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            let timer = days + " วัน " + hours + " : " + minutes + " : " + seconds + " นาที";
            $('.popupExpireDate').html(timer)
            

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $('.popupExpireDate').html('หมดอายุ')
                //document.getElementById("demo").innerHTML = "EXPIRED";
            }
            }, 1000);
    }

    getCoupon(event,e,key='') {
        const _this = this
        const couponid = e.couponid;
        const storeid = this.state.storeid;
        const userid = this.state.userid;
        

        if (e.coupontype==="1") {
            const data = new FormData();
            data.set('couponid', couponid);
            data.set('storeid', storeid);
            data.set('userid', userid);
            axios({
                method: 'post',
                url: config.api.base_url+'/store_coupon/api/coupon',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                _this.styleButtonCoupon('add',e,key)
                console.log(response.data)
            })
            .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        }else{
            localStorage.setItem('storageStoreID',storeid);
            localStorage.setItem('backPath',window.location.pathname);
            window.location = "/coupon_detail/"+couponid;
        }
        /* let minpurchase = (e.minpurchase).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.coudownTime(e.enddate)
        Swal.fire({
            html: `
            <div class="container-mobile m-auto">
            <div class="my-card mt-4" style="position: relative">
                <div class=" position-relative ">
                    <div id="user_coupon_detail">
                        <div class="coupon">
                            <div class="row w-100 m-0 mb-4" style="flex-wrap: wrap">
                                <div  class="col-12 p-0 pl-2 pr-2">
                                    <div class="position-relative">
                                        <img class="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                        <div class="coupon-detail row ml-1 h-100">
                                            <div class="col-4">
                                                <p class="mt-3 ml-2 mb-0" style="font-weight: normal;font-size: 1rem">${e.catename}</p>
                                                <div class="w-100 text-center mt-2">
                                                    <div class="coupon-detail-img rounded-circle mx-auto shadow">
                                                        <img class="" src=${e.storelogo} alt=""/>
                                                    </div>
                                                    <p class="mt-2" style="font-weight: normal;font-size: 1rem">${e.storename}</p>
                                                </div>
                                            </div>
                                            <div class="col-4 px-0 text-center mt-2 h-100">
                                                <p class="mt-2 mb-0">${e.couponname}</p>
                                                <h2>ลดราคา</h2>
                                                ${e.storebranch!== null ? (
                                                    `<small class="d-block" >สาขา : ${e.storebranch}</small>`
                                                ):(
                                                    <></>
                                                )}
                                                <small class="popupExpireDate" style="font-size: 0.9rem"></small>
                                            </div>
                                            <div class="col-4 p-0 text-center my-auto">
                                                <div class="coupon-detail-side ">
                                                <h1 class="m-0">${e.coupondiscount}${e.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                <h5>ขั้นต่ำ ${minpurchase} บาท</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            คุณได้รับดีลเรียบร้อย!
            `,
            confirmButtonText: '<a href="javascript: window.location=\'/shop/'+storeid+'\'" style="color:white;">เริ่มช็อปกันเลย!</a>',
        }) */
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    handleChangeReward = (e,itemid) => {
        localStorage.setItem('backPath',window.location.pathname);
        window.location='/reward_detail/'+itemid
    }

    async couponDetail(e,data) {
        e.preventDefault();
        
        if (data.coupontype==="0" && data.couponused==="1") {
            console.log(data);
            localStorage.setItem('storageStoreID',data.storeid);
            window.location = "/coupon_detail/"+data.couponid;
        }
    }

    useCoupon(e,data) {
        e.preventDefault();
        // localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
    }
    
    render() {
        const couponlist = {
            flexWrap: 'wrap'
        };
        const useCouponButton = {
            backgroundColor: '#12a508',
            height: '2.1rem'
        };
        const getCouponButton = {
            height: '2.1rem'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0',
            height: '2.1rem'
        };
        const coupon = {
            marginTop: '5.5rem'
        };
        const textColor = {
            color: '#' + this.state.colorData['colorfont']
        }
        return (
            <div className="container-mobile m-auto">
                <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal" >
                    <div className="modal-body">
                        <div className="form-group">
                            <label>ค้นหาคูปอง</label>
                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCoupon}/>
                        </div>
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                </Modal>
                <div className="head bg-black shadow">
                {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back" style={{left: '1rem'}}>
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCoupon}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                        <div className="col-8 h-100" style={{whiteSpace: 'nowrap'}}>
                            <div className=" h-100 pt-3 pl-0 ml-20">
                                <h1 className="m-0 h mt-1 coupon-text" style={textColor}>REWARD&#38;COUPON</h1> 
                                <p className="" style={textColor}>รับส่วนลดพิเศษสำหรับคุณได้เลย</p>
                            </div>
                        </div>
                        <div className="col-4 h-100">
                            <div className="row float-right h-100 w-100">
                                <div className="col p-2 my-auto">
                                    <div className="icon rounded-circle" style={{marginLeft:'0.5rem'}}>
                                        <img className="w-100" src="/assets/images/icon-search.svg" alt="" onClick={this.openSearchBox}/>
                                    </div>
                                </div>
                                <div className="col p-2 my-auto">
                                    <div className="icon rounded-circle">
                                        <a href="/notifications">
                                            <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                            {
                                                this.state.numNoti>0 && (
                                                    <div className="alert alert-primary noti-num-border big-alert" role="alert">
                                                        {this.state.numNoti}
                                                    </div>
                                                )
                                            }
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                )}
                </div>
                {/* <Header/> */}
                
                <div className="my-card mt-1 position-relative" style={{zIndex: 'auto'}}>
                    <div className=" position-relative ">
                        <div id="user_coupon_detail">
                        {
                            this.state.waitingData ? <></>
                            :
                            this.state.data.length === 0 && this.state.rewardData.length === 0 ?
                                <div className="align-items-center text-center" style={{height: '80vh'}}>
                                    <h1 className="font-color" style={{ top: '40%', position: 'relative'}}>Coming Soon</h1>
                                </div>
                            :
                            <div className="coupon shop-coupon mt-2">
                                <div className="row w-100 m-0 mb-4" style={couponlist}>
                                {
                                    this.state.data.length === 0 ? (
                                        <div/>
                                    ) : (
                                    this.state.data.map((row,key) => (
                                        ((new Date(row.enddate).getTime() > nowDate) && (new Date(row.startdate).getTime() < nowDate) && row.couponstatus == "1") ? 
                                            <div  className="col-12 p-0 pl-2 pr-2 container-coupon">
                                                <div className="position-relative">
                                                    <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-4 align-coupon">
                                                            <p className="mt-3 mb-0 text-overflow text-center" style={{fontWeight: 'normal',fontSize: '1rem'}}>{row.catename}</p>
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                    <img className="" src={row.storelogo} alt=""/>
                                                                </div>
                                                                <p className="mt-2 text-overflow" style={{fontWeight: 'normal',fontSize: '1rem'}}>{row.storename}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 px-0 text-center h-100" style={{marginTop:'1.2rem'}}>
                                                            <p className="mt-2 mb-0 text-overflow">{row.couponname}</p>
                                                            <h2>ลดราคา</h2>
                                                            {row.branchName!== null ? (
                                                                <small className="d-block" >สาขา : {row.branchName}</small>
                                                            ):(
                                                                <></>
                                                            )}
                                                            <small className="time-text-overflow" style={{fontSize: '0.9rem'}}><DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                            {row.statususer === "1" && row.couponused === "1" ? ( 
                                                                // <button className="btn btn-orange p-0 mt-2" style={usedCouponButton} disabled data-primary="cp-1"> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                                <button className="btn btn-orange p-0 mt-1" style={usedCouponButton} onClick={(e) => this.couponDetail(e,row)} data-primary={key}> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                            ) : row.statususer === "1" ? (
                                                                // <button className="btn btn-orange p-0 mt-2" style={useCouponButton} data-primary="cp-1">
                                                                //     {row.coupontype==='1' ? (
                                                                //         <a href={"/shop/"+localStorage.getItem('storageStoreID')}><h5 className="m-0">ใช้คูปอง</h5></a>
                                                                //     ) : (
                                                                //         <a href={'/coupon_detail/'+row.couponid}><h5 className="m-0">ใช้คูปอง</h5></a>
                                                                //     )}
                                                                    
                                                                // </button>
                                                                <button className="btn btn-orange p-0 mt-1 btn-usecoupon" onClick={(e) => this.useCoupon(e,row)} style={useCouponButton} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}>
                                                                    <h5 className="m-0">ใช้คูปอง</h5>
                                                                </button>
                                                            ) : (
                                                                // <button className="btn btn-orange p-0 mt-2" style={getCouponButton} data-primary="cp-1" onClick={(e) => this.getCoupon(e,row)}> <h5 className="m-0">รับคูปอง</h5></button>
                                                                <div key={key}>
                                                                    {/* <button className="btn shop-home-coupon-btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key}> <h5 className="m-0">รับคูปอง</h5></button> */}
                                                                    <button className="btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row,key)} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}> <h5 className="m-0">รับคูปอง</h5></button>
                                                                </div>
                                                            )}
                                                            {/* MODAL COUPON */}
                                                            <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                                <div className="modal-dialog modal-dialog-centered" role="document">
                                                                <div className="modal-content w-75 mx-auto border-0">
                                                                    <div className="modal-header bg-yellow">
                                                                    <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                                        <div className="position-relative">
                                                                            <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                            <div className="coupon-detail row ml-1 h-100">
                                                                                <div className="col-4">
                                                                                    <p className="mt-3 mb-0 product-name">{row.catename}</p>
                                                                                    <div className="w-100 text-center mt-2">
                                                                                        <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                            <img className="" src={row.storelogo} alt=""/>
                                                                                        </div>
                                                                                        <p className="mt-2">{row.storename}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-4 px-0 text-center mt-2 h-100">
                                                                                    <p className="mt-2 mb-0">{row.couponname}</p>
                                                                                    <h2>ลดราคา</h2>
                                                                                    {row.branchName!== null ? (
                                                                                        <small className="d-block">สาขา : {row.branchName}</small>
                                                                                    ):(
                                                                                        <></>
                                                                                    )}
                                                                                    <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                                    <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                                </div>
                                                                                <div className="col-4 p-0 text-center my-auto">
                                                                                    <div className="coupon-detail-side mt-2" style={{whiteSpace: 'nowrap'}}>
                                                                                        <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                                        <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div></h5>
                                                                    <button type="button" className="close bg-grey rounded-circle px-2 p-0" onClick={(e) => this.closeCouponModal(e,key)} data-dismiss="modal" data-key={key} aria-label="Close">
                                                                        <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                                    </button>
                                                                    </div>
                                                                    <div className="modal-body text-center">
                                                                        <h4>คุณได้รับดีลเรียบร้อย!</h4>
                                                                        <button  className="btn bg-yellow w-75">
                                                                            <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 p-0 text-center my-auto">
                                                            <div className="coupon-detail-side ">
                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                                <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        : 
                                        <></>
                                    ))
                                    )
                                }

                                </div>
                            {
                                this.state.rewardData.length === 0 ? (
                                        <div/>
                                ) : (
                                    this.state.rewardData.map(row => (
                                <div class="col-12 p-0 pl-2 pr-2">
                                    <div class="position-relative">
                                        <img class="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                        <div class="coupon-detail row ml-1 h-100">
                                            <div class="col-5">
                                                <div class="w-100 text-center mt-2">
                                                    <div class="row w-100">
                                                        <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                            <img className="" src={row.storelogo} alt=""/>
                                                        </div>
                                                        <p class="mt-3 ml-1 mb-0 store-name">{row.storename}</p>
                                                    </div>
                                                        <p class="mb-0 fs-40" style={{marginTop: '-1rem'}}><h2>{row.itempoint}<br/>คะแนน</h2></p>
                                                    
                                                {
                                                    // (parseFloat(row.itempoint) <= this.state.point) ? (
                                                    //     // row.redeemstatus==='1' || row.redeemstatus==='0' ? (
                                                    //     //     <button class="btn coupon-btn btn-secondary w-100 p-10" style = {{position:'relative', bottom: 9}} onClick={(e) => this.handleChangeReward(e,row.itemid)}> <p class="m-0 font-tf">แลกแล้ว</p></button>
                                                    //     // ) : (
                                                    //     //     <button class="btn coupon-btn btn-green p-10" style = {{position:'relative', bottom: 9}} onClick={(e) => this.handleChangeReward(e,row.itemid)}> <p class="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                    //     // )
                                                    //     <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                    //         { row.redeemstatus === "1" ? 
                                                    //         <p className="m-0 font-tf">แลกแล้ว</p>
                                                    //         :
                                                    //         <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                    //         }
                                                    //     </button>
                                                    // ) : (
                                                    //     <button class="btn coupon-btn btn-orange p-10 mt-3" style = {{position:'relative', bottom: 10}}onClick={(e) => this.handleChangeReward(e,row.itemid)}> <p class="m-0 font-tf" style={{lineHeight:'20px'}}>สะสมคะแนนเพิ่ม</p></button>
                                                    // )
                                                    (parseFloat(row.itempoint) <= row.user_point) ? (
                                                        (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                            <button className="btn btn-green coupon-green-btn p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                            { row.redeemtime_status === "1" ? 
                                                            <p className="m-0 font-tf">แลกแล้ว</p>
                                                            :
                                                            <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                            }
                                                            </button>
                                                        ):(
                                                            <button className="btn coupon-green-btn btn-green p-10" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                        )
                                                    ) : (
                                                        <button className="btn coupon-btn btn-orange p-10 mt-0" > <p className="m-0 font-tf ">สะสมคะแนนเพิ่ม</p></button>
                                                    )
                                                }
                                                    
                                                </div>
                                            </div>
                                            <div class="col-7 p-0 text-center mt-2">
                                                <p class="m-0 align-text">{row.catename}</p>
                                                <img class="coupon-pic" src={row.itemimg} alt=""/>
                                                <p class="m-0 pt-2 px-3">{row.itemname}</p>
                                                {/* <p class="m-0 pl-3 pr-3 f-value"><h3>มูลค่า 1,750 บาท</h3></p> */}
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                    ))
                                )
                            }


                            </div>
                        }
                        </div>
                    </div>

                </div>
                <br/>
                <br/>
                    
                <Navigation active={'deal'}/>

                
               
                 
            </div>
        )
    }
}
export default Coupon_list;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/
/*

                                <div class="row w-100 m-0">
                                    <div id="cp-4" class="col-12 p-0 pl-2 pr-2">
                                        <div class="position-relative">
                                        <img class="w-100" src="images/fullbggift.png" alt=""/>
                                            <div class="coupon-detail row ml-1 h-100">
                                                <div class="col-5">
                                                    <div class="w-100 text-center mt-2">
                                                        <p class="mt-0 mb-0">MESTYLE</p>
                                                        <p class="mt-0 mb-10">THAILAND</p>
                                                        <p class="mt-2 mb-10 fs-40">70</p>
                                                        <p class="mt-2 mb-10 fs-40">คะแนน</p>
                                                        <p class="mt-2 mb-0" style="color: red;">สิทธิ์คงเหลือ 10/10</p>
                                                        <button class="btn btn-orange p-10 mt-0" onclick="getCoupon(this,'cp-4')"> <p class="m-0 font-tf">สะสมคะแนนเพิ่ม</p></button>
                                                    </div>
                                                </div>
                                                <div class="col-7 p-0 text-center mt-2">
                                                    <p class="m-0 mr-3">Men Accessories</p>
                                                    <img class="" src="images/Rectangle 124.png" width="43%" alt=""/>
                                                    <p class="m-0 pl-3 pr-3">VICTORIANOX SWISS ARMY Climber Red</p>
                                                    <p class="m-0 pl-3 pr-3 f-value">มูลค่า 1990 บาท</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

<div class="row w-100 m-0">
                                    <div id="cp-3" class="col-12 p-0 pl-2 pr-2">
                                        <div class="position-relative">
                                        <img class="w-100" src="images/fullbggift.png" alt=""/>
                                            <div class="coupon-detail row ml-1 h-100">
                                                <div class="col-5">
                                                    <div class="w-100 text-center mt-2">
                                                        <p class="mt-3 mb-0">MESTYLE</p>
                                                        <p class="mt-0 mb-10">THAILAND</p>
                                                        <p class="mt-2 mb-10 fs-40">70</p>
                                                        <p class="mt-2 mb-10 fs-40">คะแนน</p>
                                                        <button class="btn btn-green p-10 mt-0" onclick="getCoupon(this,'cp-3')"> <p class="m-0 font-tf">แลกรางวัลที่นี้</p></button>
                                                    </div>
                                                </div>
                                                <div class="col-7 p-0 text-center mt-2">
                                                    <p class="m-0 mr-3" style="text-align: end; font-size: 14px;">Men Accessories</p>
                                                    <img class="" src="images/water.png" width="43%" alt=""/>
                                                    <p class="m-0 pl-3 pr-3" style="text-align: start;">HYDRP FLASK รุ่น WIDE MOUTH 32 OZ.</p>
                                                    <p class="m-0 pl-3 pr-3 f-value" style="text-align: start;">มูลค่า 1750 บาท</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row w-100 m-0">
                                    <div id="cp-4" class="col-12 p-0 pl-2 pr-2">
                                        <div class="position-relative">
                                        <img class="w-100" src="images/fullbggift.png" alt=""/>
                                            <div class="coupon-detail row ml-1 h-100">
                                                <div class="col-5">
                                                    <div class="w-100 text-center mt-2">
                                                        <p class="mt-0 mb-0">MESTYLE</p>
                                                        <p class="mt-0 mb-10">THAILAND</p>
                                                        <p class="mt-2 mb-10 fs-40">70</p>
                                                        <p class="mt-2 mb-10 fs-40">คะแนน</p>
                                                        <p class="mt-2 mb-0" style="color: red;">สิทธิ์คงเหลือ 10/10</p>
                                                        <button class="btn btn-orange p-10 mt-0" onclick="getCoupon(this,'cp-4')"> <p class="m-0 font-tf">สะสมคะแนนเพิ่ม</p></button>
                                                    </div>
                                                </div>
                                                <div class="col-7 p-0 text-center mt-2">
                                                    <p class="m-0 mr-3" style="text-align: end; font-size: 14px;">Men Accessories</p>
                                                    <img class="" src="images/Rectangle 124.png" width="43%" alt=""/>
                                                    <p class="m-0 pl-3 pr-3" style="text-align: start;">VICTORIANOX SWISS ARMY Climber Red</p>
                                                    <p class="m-0 pl-3 pr-3 f-value" style="text-align: start;">มูลค่า 1990 บาท</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
*/
//export default User;