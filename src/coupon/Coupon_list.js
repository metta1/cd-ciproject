import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import DateCountdown from 'react-date-countdown-timer';
import NumberFormat from 'react-number-format';
import $ from 'jquery'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Coupon_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        userCouponData : [],
        usedCouponData : [],
        userRewardData : [],
        usedRewardData : [],
        userid:sessionStorage.getItem('user_id'),
    };


    async componentDidMount(){
        //click menu card
        $('#used_coupon_detail').hide();
        $("#user_coupon").click(function(){
            $("#user_coupon").removeClass("not-active");
            $('#used_coupon').addClass("not-active");

            $('#user_coupon_detail').show();
            $('#used_coupon_detail').hide();
        });
        $('#used_coupon').click(function(){
            $("#used_coupon").removeClass("not-active");
            $('#user_coupon').addClass("not-active");

            $('#used_coupon_detail').show();
            $('#user_coupon_detail').hide();

        });

        const rUserCoupon = await axios.get(config.api.base_url+'/store_coupon/api/couponuser/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rUserCoupon.status===200){
            this.setState({userCouponData: rUserCoupon.data});
        }
        console.log(this.state.userCouponData)

        const rUsedCoupon = await axios.get(config.api.base_url+'/store_coupon/api/couponused/'+this.state.userid).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rUsedCoupon.status===200){
            this.setState({usedCouponData: rUsedCoupon.data});
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id')+"&redeem_status=0")
        if(rewardRs.status===200){
            this.setState({userRewardData: rewardRs.data});
            
        }
        console.log(this.state.userRewardData)

        const usedRewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id')+"&redeem_status=1")
        if(usedRewardRs.status===200){
            this.setState({usedRewardData: usedRewardRs.data});
        }

        $('.btn-getcoupon, .btn-usecoupon').click(function(){
            let key = $(this).data('primary');
            let storeid = $(this).data('storeid');
            let couponid = $(this).data('couponid');
            let coupontype = $(this).data('coupontype');

            if (coupontype === 1) {
                $(this).css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
                $('body').addClass('modal-open')
                $('<div class="modal-backdrop fade show"></div>').appendTo('body');
                $('#couponModal-'+key).css({'display':'block'})
            } else {
                localStorage.setItem('storageStoreID',storeid);
                window.location = "/coupon_detail/"+couponid;
            }
        })

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })

    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    useCoupon(e,data) {
        e.preventDefault();
        localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
    }

    handleChangeReward = (e,itemid,storeid) => {
        e.preventDefault();
        localStorage.setItem('storageStoreID',storeid);
        window.location='/reward_detail/'+itemid;
    }

    render() {
        const couponlist = {
            flexWrap: 'wrap'
        };
        const useCouponButton = {
            backgroundColor: '#12a508',
            height: '2.1rem'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0',
            position: 'absolute',
            zIndex: '999',
            width: '30%'
        };
        const btnCoupon = {
            paddingBottom: '15px'
        };
        const tabButton = {
            paddingTop: '10px',
            color: '#010101'
        };
        const coupon = {
            marginTop: '5px'
        };
        const usedCoupon = {
            filter: 'brightness(50%)'
        };

        return (
            <>
            <link rel="stylesheet" type="text/css" href="/assets/css/home.css?v=3"/>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>

                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative my-coupon-head">
                        <div className="icon-back">
                            <a href={"/account?v="+new Date().getTime()}>
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h3 class="align-title m-0" style={{fontSize:'1.75rem'}}>คูปองของฉัน</h3>
                        </div>
                    </div>
                    <div className="my-card p-0 position-relative" style={{zIndex: 'auto'}}>
                        <div className="row w-100 mt-4 ml-0">
                            <div className="col-6 px-2">
                                <div id="user_coupon"  className="btn-card text-center shadow " style={btnCoupon}>
                                    <h5 style={tabButton}>คูปองและ Reward ของคุณ</h5>
                                </div>
                            </div>
                            <div className="col-6 px-2">
                                <div id="used_coupon" className="btn-card text-center shadow not-active" style={btnCoupon}>
                                    <h5 style={tabButton}>คูปองและ Reward ที่ใช้แล้ว</h5>
                                </div>
                            </div>
                        </div>
                        <div className="select-card position-relative " style={{zIndex: 'auto'}}>
                            <div id="user_coupon_detail">
                            {
                                <div className="coupon" style={coupon}>
                                    <div className="row w-100 m-0" style={couponlist}>
                                    {
                                        (this.state.userCouponData === undefined || this.state.userCouponData.length === 0) && 
                                            (this.state.userRewardData === undefined || this.state.userRewardData.length === 0) ? (
                                            <div>
                                                <img src="/assets/images/No-Coupon-img.png" className="ml-1 w-100"/>
                                                <div className="mt-3 text-center text-grey">
                                                    <h2>คุณยังไม่มีคูปอง</h2>
                                                    <h2>เพิ่มคูปองของคุณเลย</h2>
                                                    <a className="btn btn-primary p-0 mt-2 w-50" href={'/promotion_category'}>
                                                        <span style={{fontSize: '30px', color: 'white'}}>คลิก</span>
                                                    </a>
                                                </div>
                                            </div>
                                        ) : (
                                            <></>
                                        )
                                    }
                                    { (this.state.userCouponData === undefined || this.state.userCouponData.length === 0) ? <></> :
                                        this.state.userCouponData.map((row,key) => (
                                            <div  className="col-12 p-0 pl-2 pr-2">
                                                <div className="position-relative">
                                                    <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-4">
                                                            <p className="mt-3 ml-2 mb-0 text-overflow" style={{fontWeight: 'normal',fontSize: '1rem', whiteSpace: 'nowrap', overflow: 'hidden'}}>{row.catename}</p>
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                    <img className="" src={row.storelogo} alt=""/>
                                                                </div>
                                                                <p className="mt-2" style={{fontWeight: 'normal',fontSize: '1rem'}}>{row.storename}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 px-0 text-center mt-2 h-100">
                                                            <p className="mt-2 mb-0">{row.couponname}</p>
                                                            <h2>ลดราคา</h2>
                                                            {row.branchName!== null ? (
                                                                <small className="d-block" >สาขา : {row.branchName}</small>
                                                            ):(
                                                                <></>
                                                            )}
                                                            <small className="" style={{fontSize: '0.9rem'}}><DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                            <button className="btn btn-orange p-0 mt-2 btn-usecoupon" style={useCouponButton}  data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}>
                                                                <h5 className="m-0">ใช้คูปอง</h5>
                                                            </button>
                                                        </div>
                                                        <div className="col-4 p-0 text-center my-auto">
                                                            <div className="coupon-detail-side ">
                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                                <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* MODAL COUPON */}
                                                <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                    <div className="modal-dialog modal-dialog-centered" role="document">
                                                    <div className="modal-content w-75 mx-auto border-0">
                                                        <div className="modal-header bg-yellow">
                                                        <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                            <div className="position-relative">
                                                                <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                <div className="coupon-detail row ml-1 h-100">
                                                                    <div className="col-4">
                                                                        <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                        <div className="w-100 text-center mt-2">
                                                                            <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                <img className="" src={row.storelogo} alt=""/>
                                                                            </div>
                                                                            <p className="mt-2">{row.storename}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-4 px-0 text-center mt-2 h-100">
                                                                        <p className="mt-2 mb-0">{row.couponname}</p>
                                                                        <h2>ลดราคา</h2>
                                                                        {row.branchName!== null ? (
                                                                            <small className="d-block">สาขา : {row.branchName}</small>
                                                                        ):(
                                                                            <></>
                                                                        )}
                                                                        <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                        <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                    </div>
                                                                    <div className="col-4 p-0 text-center my-auto">
                                                                        <div className="coupon-detail-side ">
                                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                            <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div></h5>
                                                        <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                            <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                        </button>
                                                        </div>
                                                        <div className="modal-body text-center">
                                                            <h4>คุณได้รับคูปองเรียบร้อย!</h4>
                                                            <button className="btn bg-yellow w-75">
                                                                <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        ))
                                    }
                                    { (this.state.userRewardData === undefined || this.state.userRewardData.length === 0)  ? <></> :
                                        this.state.userRewardData.map(row => (
                                            (row.redeemstatus === "0") ?
                                            <div className="col-12 p-0 pl-2 pr-2">
                                                <div className="position-relative">
                                                    <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-5">
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="row w-100">
                                                                    <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                    </div>
                                                                    <p className="mt-3 ml-2 mb-0">{row.storename}</p>
                                                                </div>
                                                                <p className="mb-0 fs-40 " style={{marginTop: '-0.5rem'}}><h2 className="mb-2">{row.itempoint}<br/>คะแนน</h2></p>
                                                            {
                                                                // (parseFloat(row.itempoint) <= row.user_point) ? (
                                                                //     row.redeemstatus==='1' || row.redeemstatus==='0' ? (
                                                                //         <button className="btn coupon-green-btn btn-secondary w-100 p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                //             { row.redeemstatus === "1" ? 
                                                                //             <p className="m-0 font-tf">แลกแล้ว</p>
                                                                //             :
                                                                //             <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                //             }
                                                                //         </button>
                                                                //     ):(
                                                                //         <button className="btn coupon-green-btn btn-green p-10" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                                //     )
                                                                // ) : (
                                                                //     <button className="btn coupon-btn btn-orange p-10 mt-0" > <p className="m-0 font-tf ">สะสมคะแนนเพิ่มอีกนิด</p></button>
                                                                // )
                                                                (parseFloat(row.itempoint) <= row.user_point) ? (
                                                                    (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                                        <button className="btn coupon-green-btn btn-secondary w-100 p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                        { row.redeemtime_status === "1" ? 
                                                                        <p className="m-0 font-tf">แลกแล้ว</p>
                                                                        :
                                                                        <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                        }
                                                                        </button>
                                                                    ):(
                                                                        <button className="btn coupon-green-btn btn-green p-10" onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                                    )
                                                                ) : (
                                                                    <button className="btn coupon-btn btn-orange p-10 mt-0" > <p className="m-0 font-tf ">สะสมคะแนนเพิ่ม</p></button>
                                                                )
                                                            }
                                                            </div>
                                                        </div>
                                                        <div className="col-7 p-0 text-center mt-2">
                                                            <p className="m-0 align-text">{row.catename}</p>
                                                                <img className="coupon-pic" src={row.itemimg} width="43%" alt=""/>
                                                            <p className="m-0 px-3 pt-2 text-center">{row.itemname}</p>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            :<></>
                                        ))
                                    }
                                    </div>
                                </div>
                            }
                            </div>
                            <div id="used_coupon_detail" >
                            {
                                <div className="coupon" style={coupon}>
                                    <div className="row w-100 m-0" style={couponlist}>
                                    {
                                        (this.state.usedCouponData === undefined || this.state.usedCouponData.length === 0) &&
                                            (this.state.usedRewardData === undefined || this.state.usedRewardData.length === 0) ? (
                                            <div>
                                                <img src="/assets/images/No-Coupon-img.png" className="ml-1 w-100"/>
                                                <div className="mt-3 text-center text-grey">
                                                    <h2>คุณยังไม่มีคูปอง</h2>
                                                    <h2>เพิ่มคูปองของคุณเลย</h2>
                                                    <a className="btn btn-primary p-0 mt-2 w-50" href={'/promotion_category'}>
                                                        <span style={{fontSize: '30px', color: 'white'}}>คลิก</span>
                                                    </a>
                                                </div>
                                            </div>
                                        ) : (
                                            <></>
                                        )
                                    }
                                    { (this.state.usedCouponData === undefined || this.state.usedCouponData.length === 0) ? <></> :
                                        this.state.usedCouponData.map(row => (
                                            <div  className="col-12 p-0 pl-2 pr-2" >
                                                <label className="btn btn-orange p-1 mt-3 btn-getcoupon ml-3" style={usedCouponButton} data-primary="cp-1"> <h5 className="m-0">ถูกใช้ไปแล้ว</h5></label>
                                                <div className="position-relative" style={usedCoupon}>
                                                    <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-4">
                                                            <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                    <img className="" src={row.storelogo} alt=""/>
                                                                </div>
                                                                <p className="mt-2">{row.storename}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 px-0 text-center mt-2 h-100">
                                                            <p className="mt-2 mb-0">{row.couponname}</p>
                                                            <h2>ลดราคา</h2>
                                                            {row.branchName!== null ? (
                                                                <small className="d-block">สาขา : {row.branchName}</small>
                                                            ):(
                                                                <></>
                                                            )}
                                                            <small className=""><DateCountdown dateTo={new Date(row.enddate)}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                        </div>
                                                        <div className="col-4 p-0 text-center my-auto">
                                                            <div className="coupon-detail-side ">
                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='1' ? (' %') :(' บาท')}</h1>
                                                                <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }
                                    { (this.state.usedRewardData === undefined || this.state.usedRewardData.length === 0)  ? <></> :
                                        this.state.usedRewardData.map(row => (
                                            (row.redeemstatus === "1") ?
                                            <div className="col-12 p-0 pl-2 pr-2">
                                                <label className="btn btn-orange p-1 mt-3 btn-getcoupon ml-3" style={usedCouponButton} data-primary="cp-1"> <h5 className="m-0">ถูกใช้ไปแล้ว</h5></label>
                                                <div className="position-relative" style={usedCoupon}>
                                                    <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                                    <div className="coupon-detail row ml-1 h-100">
                                                        <div className="col-5">
                                                            <div className="w-100 text-center mt-2">
                                                                <div className="row w-100">
                                                                    <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                    </div>
                                                                    <p className="mt-3 ml-2 mb-0">{row.storename}</p>
                                                                </div>
                                                                <p className="mb-0 fs-40 " style={{marginTop: '-0.5rem'}}><h2 className="mb-2">{row.itempoint}<br/>คะแนน</h2></p>
                                                            </div>
                                                        </div>
                                                        <div className="col-7 p-0 text-center mt-2">
                                                            <p className="m-0 align-text">{row.catename}</p>
                                                                <img className="coupon-pic" src={row.itemimg} width="43%" alt=""/>
                                                            <p className="m-0 px-3 pt-2 text-center">{row.itemname}</p>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            :<></>
                                        ))
                                    }
                                    </div>
                                </div>
                            } 
                            </div>
                        </div>
                    </div>
                </div>  
                {/* <!-- container --> */}
            </div>
            </>
        )
    }
}
export default Coupon_list;
