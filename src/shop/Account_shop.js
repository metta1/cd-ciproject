import React, { Component } from 'react'
import Footer from './Footer'
import axios from 'axios'
import $ from 'jquery'
import {config} from '../config';
import NumberFormat from 'react-number-format';
import {Navigation_shop as Navigation, Notification_num} from '../template'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Card_expire_detail from './Card_expire_detail';
//import Switch from "../customcomponent/react-switch";
// import {Navigation_shop as Navigation} from '../template'
import Store_color from '../template/Store_color'
var QRCode = require('qrcode.react');
var ReactDOM = require('react-dom');
var Barcode = require('react-barcode');


class Account_shop extends Component {

    constructor(props){//เริ่มต้น run component
        super(props);
        this.addActiveClass = this.addActiveClass.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggleInfo = this.toggleInfo.bind(this);
        this.editAccount = this.editAccount.bind(this);

        if(parseInt(localStorage.getItem('storageStoreID'))===0 || parseInt(sessionStorage.getItem('user_id')===0)){
            window.location='/auth?referer=home'
        }

    }

    state = {
        data : [],
        rewardData : [],
        cardData: [],
        storeDetailData : [],
        storeSocialData : [],
        storeStampData : [],
        stampExpireDetail : [],
        point:0,
        store_id:localStorage.getItem('storageStoreID'),
        user_id : sessionStorage.getItem('user_id'),
        checked: false,
        displayMainEmail :true,
        stampRewarData: [],
        usertel:undefined,
        colorData : [],
        numNoti:0,
        storelogo: '',
        rowsStamp: [],
        usersStamp: [],
        stampPoint: 0,
        memberPoint: 0
    };

    toggleInfo(){
        this.setState({ info: !this.state.info })
    }

    handleChange(checked) {
        this.setState({ checked });
    }


    async componentDidMount(){
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }

        //Noti
        const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.user_id+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
            //   console.log(error.response.status);
            }
        });
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                this.setState({
                    numNoti:numNoti
                })
            }
        }
        const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.store_id);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })

        }

        // GET preview store color
        if(getParam('displayTabbar') !== undefined){
            let colortabbar = getParam('displayTabbar');
            if (Array.isArray(colortabbar)) {
                colortabbar = getParam('displayTabbar')[getParam('displayTabbar').length-1];
            }
            $(".footer" ).css( "background-color", "#"+colortabbar );
            $(".footer-bg" ).css( "color", "#"+colortabbar );
            $('.head.bg-black, .bg-black, .mestyle__wrapper').css("background-color", "#"+colortabbar);
            $('.btn-add').css('background-color','#'+colortabbar)
        }
        if(getParam('displayFont') !== undefined){
            let colorfont = getParam('displayFont');
            if (Array.isArray(colorfont)) {
                colorfont = getParam('displayFont')[getParam('displayFont').length-1];
            }
            $('.img-border-color').css( "fill", "#"+colorfont );
            $('.img-border-color-stroke').css( "stroke", "#"+colorfont );
        }
        if(getParam('displayIcon') !== undefined){
            let coloricon = getParam('displayIcon');
            if (Array.isArray(coloricon)) {
                coloricon = getParam('displayIcon')[getParam('displayIcon').length-1];
            }
            $('.link__box .tab, .status_and_qrcode__box, .detail__box .tab').css("background-color", "#"+coloricon);
            $('.account-img-bg-color' ).css( "fill", "#"+coloricon );
            $('.account-img-stroke').css( "stroke", "#"+coloricon );
            // $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg, .footer.menu-shop .menu-item.active .img-bg-color" ).css( "fill", "#"+coloricon );
        }
        //====
        if(getParam('token') !== undefined){
            /* userid,storeid,time*/
            let decodeToken = atob(getParam('token'));
            let arrToken    = decodeToken.split(",")
            let tokenUserID = arrToken[0]
            let tokenStoreID = arrToken[1]
            if(tokenUserID && tokenStoreID){
                this.setState({
                    user_id: tokenUserID,
                    store_id : tokenStoreID
                });
                localStorage.setItem('storageStoreID',tokenStoreID);
            }

            /****** REMOVE สิ่งที่ไม่อยากให้มี *********/
            $("a").each(function() {
                $(this).removeAttr("href");
            });
            setTimeout(function(){
                $("a").each(function() {
                    var attributes = $.map(this.attributes, function(item) {
                      return item.name;
                    });
                    $(this).removeAttr("href");
                });
            }, 2000);




            const result = await axios.get(config.api.base_url+'/store/api/store/'+tokenStoreID)
            if(result.status===200){
                this.setState({data: result.data[0]});
            }

            const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountstore/'+tokenStoreID+'/'+tokenUserID)
            // console.log(cardRs)
            if(cardRs.status===200){
                this.setState({
                    cardData: cardRs.data,
                    point: parseFloat(cardRs.data[0]['point'])
                });
            }


        }else{
            const result = await axios.get(config.api.base_url+'/store/api/store/'+this.state.store_id)
            if(result.status===200){
                this.setState({data: result.data[0]});
            }

            const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountcard/'+this.state.store_id+'/'+this.state.user_id)
            // console.log(cardRs.data)
            if(cardRs.status===200){

                this.setState({
                    cardData: cardRs.data,
                    point: parseFloat(cardRs.data[0]['point']),
                    usertel:cardRs.data[0]['tel'],
                    storelogo: cardRs.data[0].storelogo != '' ? cardRs.data[0].storelogo : "/assets/images/icon-ho-new.png",
                    memberPoint: parseFloat(cardRs.data[0]['memberPoint'])
                });
            }
        }



        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward/'+this.state.store_id)
        if(rewardRs.status===200){
            this.setState({rewardData: rewardRs.data});
        }



        const storeDetailRs = await axios.get(config.api.base_url+'/store/api/storedetail/'+this.state.store_id)
        if(storeDetailRs.status===200){

            this.setState({
                storeDetailData: storeDetailRs.data,
            });
        }

        const storeSocialRs = await axios.get(config.api.base_url+'/store/api/storesocial/'+this.state.store_id)
        if(storeSocialRs.status===200){
            this.setState({
                storeSocialData: storeSocialRs.data,
            });
            this.state.storeSocialData.map((row,key) => {
                if(row.socialtype==='Email' && row.socialdetail !== ''){
                    this.setState({
                        displayMainEmail: false,
                    });
                }
            })

        }

        //const storeStampRs = await axios.get('http://dev.backend.ho.com/store_stamp/api/userstamp/'+this.state.store_id+'/'+this.state.user_id)
        const storeStampRs = await axios.get(config.api.base_url+'/store_stamp/api/userstamp/'+this.state.store_id+'/'+this.state.user_id)
        if(storeStampRs.status===200){
            this.setState({
                storeStampData: storeStampRs.data
            });
            if(storeStampRs.data.length>0){
                let storeStampData = storeStampRs.data[0];
                this.setState({ stampPoint : storeStampRs.data[0].userstamppoint });
                if(storeStampData.stampExpireDetail !== null){
                    this.setState({
                        stampExpireDetail : storeStampData.stampExpireDetail
                    })

                }

            }

            let rowsStamp = [];
            let usersStamp = [];
            for (let i = 1; i <= parseInt(storeStampRs.data[0].stamppoint); i++) {
                rowsStamp.push(i);
            }
            for (let y = 1; y <= parseInt(storeStampRs.data[0].userstamppoint); y++) {
                usersStamp.push(y);
            }
            this.setState({
                rowsStamp: rowsStamp,
                usersStamp: usersStamp,
            });


        }
        // console.log(this.state.stampExpireDetail.length)
        //console.log(this.state.storeStampData)

        const stampRewardRs = await axios.get(config.api.base_url+'/store_stamp_reward/api/stampreward/'+this.state.store_id)
        if(stampRewardRs.status===200){
            this.setState({
                stampRewarData: stampRewardRs.data,
            });
        }
        //console.log(this.state.stampRewarData)



        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.cardData)


        this.addActiveClass()




    }

    addActiveClass = e => {
        //e.preventDefault();
        /************ JQUERY SELECTOR ***************/
        $('#cardQR:not(#cardQR.active)').click(function(){
            $(this).addClass('active');
            // $(this).attr("src", "/assets/images/icon-arrow.svg");
            $('#card-click').removeClass('d-none');
            $('.cardClickArrowTop').removeClass('d-none');
            $('.qrCardDisplay').addClass('d-none');
        })

        $('#cardQR.active').click(function(){
            $(this).removeClass('active');
            // $(this).attr("src", "/assets/images/icon-qr-code.svg");
            $('#card-click').addClass('d-none')
            $('.cardClickArrowTop').addClass('d-none');
            $('.qrCardDisplay').removeClass('d-none');
        })

        $('.stampClick:not(.stampClick.active)').click(function(){
            let key = $(this).data('key');
            $(this).addClass('active');
            // $(this).attr("src", "/assets/images/icon-arrow.svg");
            $('.stamp-click').removeClass('d-none');
            $('.stampClickArrowTop').removeClass('d-none');
            $('.qrStampDisplay').addClass('d-none');
        })

        $('.stampClick.active').click(function(){
            let key = $(this).data('key');
            $(this).removeClass('active');
            // $(this).attr("src", "/assets/images/icon-qr-code.svg");
            $('.stamp-click').addClass('d-none')
            $('.stampClickArrowTop').addClass('d-none');
            $('.qrStampDisplay').removeClass('d-none');
        })

    }



    toggleSelected = (e,selected,hidden) => {
        e.preventDefault();
        $('#'+selected).removeClass('d-none')
        $('#'+selected+'-text').addClass('toggle-active')
        $('#'+hidden).addClass('d-none')
        $('#'+hidden+'-text').removeClass('toggle-active')
    }

    handleSubmit = event => {


    }

    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1&callback=shop/'+this.state.store_id+'&store='+this.state.store_id, '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm?callback=shop/'+this.state.store_id+'&store='+this.state.store_id;
        }
    }

    editAccount = e => {
        e.preventDefault();
        // this.setState({})
        window.location = '/edit_account';
    }

    displaySuggestFriend = () => {
        const { storeDetailData } = this.state
        const isShowSuggest = storeDetailData[0] && storeDetailData[0].config_getfriend

        if (isShowSuggest === '0')  { return }

        return (
            <a href={config.base_url+"/share?openExternalBrowser=1&re="+this.state.usertel+'&s='+this.state.store_id} target="_blank">
                <div className="link__box">
                    <div className="left">

                        <div className="tab"></div>
                        {/* <img src="/assets/images/icon-suggest-friend.svg" alt="icon" className="icon"/> */}
                        <svg xmlns="http://www.w3.org/2000/svg" width="79.418" height="44.303" viewBox="0 0 79.418 44.303" className="icon w-8">
                            <g transform="translate(-156.789 -1999)">
                                <g transform="translate(158.791 2009)">
                                    <path d="M878.791,1832.453l6.994-14.515h13.551l6.557,14.515" transform="translate(-875.184 -1798.349)" className="account-img-bg-color" fill="#cd001c"/>
                                    <circle cx="8.5" cy="8.5" r="8.5" transform="translate(8.209 0)" className="account-img-bg-color" fill="#cd001c"/>
                                    <path d="M878.791,1832.453l6.994-14.515h13.551l6.557,14.515" transform="translate(-878.791 -1800.152)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                    <g transform="translate(5.209 0)" fill="none" stroke="#000" stroke-width="3">
                                        <circle cx="8.5" cy="8.5" r="8.5" stroke="none"/>
                                        <circle cx="8.5" cy="8.5" r="7" fill="none"/>
                                    </g>
                                </g>
                                <g transform="translate(202.619 2006)">
                                    <path d="M878.791,1833.813l7.65-15.876h14.821l7.172,15.876" transform="translate(-874.846 -1796.709)" className="account-img-bg-color" fill="#cd001c"/>
                                    <ellipse cx="9" cy="9.5" rx="9" ry="9.5" transform="translate(9.381)" className="account-img-bg-color" fill="#cd001c"/>
                                    <path d="M878.791,1833.813l7.65-15.876h14.821l7.172,15.876" transform="translate(-878.791 -1798.682)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                    <g transform="translate(5.381)" fill="none" stroke="#000" stroke-width="3">
                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none"/>
                                        <circle cx="9.5" cy="9.5" r="8" fill="none"/>
                                    </g>
                                </g>
                                <g transform="translate(176.264 1999)">
                                    <path d="M878.791,1837.124l9.245-19.187h17.913l8.667,19.187" transform="translate(-876.023 -1793.02)" className="account-img-bg-color" fill="#cd001c"/>
                                    <circle cx="11" cy="11" r="11" transform="translate(10.736)" className="account-img-bg-color" fill="#cd001c"/>
                                    <path d="M878.791,1837.124l9.245-19.187h17.913l8.667,19.187" transform="translate(-878.791 -1795.404)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                    <g transform="translate(5.736)" fill="none" stroke="#000" stroke-width="3">
                                        <ellipse cx="11.5" cy="11" rx="11.5" ry="11" stroke="none"/>
                                        <ellipse cx="11.5" cy="11" rx="10" ry="9.5" fill="none"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <p className="title">แนะนำเพื่อนเพื่อรับคะแนน</p>
                    </div>
                    <div className="right">
                        <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                    </div>
                </div>
            </a>
        )
    }

    displayAddressDeliver = () => {
        const { storeDetailData } = this.state
        const isShowSuggest = storeDetailData[0] && storeDetailData[0].config_getfriend

        if (isShowSuggest === '0')  { return }

        return (
            <a href="/order_history">
            <div className="link__box">
                <div className="left">
                    <div className="tab"></div>
                    {/* <img src="/assets/images/icon-address-delivery.svg" alt="icon" className="icon"/> */}
                    <svg xmlns="http://www.w3.org/2000/svg" width="73.396" height="76.641" viewBox="0 0 73.396 76.641" className="icon">
                        <g transform="translate(-156.5 -1513)">
                            <g transform="translate(177 1513)" className="account-img-bg-color" fill="#cd001c" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                                <circle cx="17" cy="17" r="17" stroke="none"/><circle cx="17" cy="17" r="15.5" fill="none"/>
                            </g>
                            <path d="M-16739,10203.709l15.193,30.074,13.414-30.074" transform="translate(16918.666 -8667.485)" className="account-img-bg-color" fill="#cd001c" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                            <g transform="translate(187 1521)" fill="#fff" stroke="#cd001c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1">
                                <circle cx="7.5" cy="7.5" r="7.5" stroke="none"/>
                                <circle cx="7.5" cy="7.5" r="7" fill="none"/>
                            </g>
                            <path d="M167.13,589.762v-.008l.008-.008L177.191,572.8h4.733" transform="translate(-7.805 973.704)" fill="none" stroke="#000" stroke-width="3"/>
                            <path d="M237.076,572.8h4.649l7.4,16.13" transform="translate(-24.357 973.704)" fill="none" stroke="#000" stroke-width="3"/>
                            <path d="M238.3,594.789v19.125a3.642,3.642,0,0,1-3.641,3.64H173.294a3.642,3.642,0,0,1-3.641-3.64V594.789a3.642,3.642,0,0,1,3.641-3.641h15.674v4.573a7.279,7.279,0,0,0,7.28,7.28h15.464a7.283,7.283,0,0,0,7.28-7.28v-4.573h15.667A3.642,3.642,0,0,1,238.3,594.789Z" transform="translate(-8.403 969.362)" className="account-img-bg-color" fill="#cd001c"/>
                            <path d="M234.04,597.567v19.125a3.641,3.641,0,0,1-3.64,3.64H169.035a3.641,3.641,0,0,1-3.64-3.64V597.567a3.641,3.641,0,0,1,3.64-3.64H184.71V598.5a7.279,7.279,0,0,0,7.28,7.28h15.463a7.283,7.283,0,0,0,7.28-7.28v-4.572H230.4A3.641,3.641,0,0,1,234.04,597.567Z" transform="translate(-7.395 967.808)" fill="none" stroke="#000" stroke-width="3"/>
                        </g>
                    </svg>
                    <p className="title">จัดการที่อยู่การจัดส่ง</p>
                </div>
                <div className="right">
                    <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                </div>
            </div>
            </a>
        )
    }

    render() {
        const textColor = {
            color: '#' + this.state.colorData['colorfont']
        }
        const show = (this.state.info) ? "show" : "" ;
        const unshow = (!this.state.info) ? "show" : "" ;
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/account_shop.css?v=3"/>
        <div class="container-mobile m-auto ">
            <div className="head bg-black shadow">
                <div className="row w-100 h-100">
                    <div className="col-9 h-100">
                        <div className=" h-100 p-3 pl-5">
                            <h1 className="m-0 h" style={textColor}>Account</h1>
                            <p className="text-nowrap" style={textColor}>บัตรสะสมคะแนน แสตมป์ ข้อมูลร้านค้าของคุณ</p>
                        </div>
                    </div>
                    <div className="col-3 h-100">
                        <div className="row float-right h-100 w-100">
                            <div className="col-8 p-1 my-auto">
                            <a href="/notifications">
                                <div className="icon rounded-circle">
                                    <img className="" src="/assets/images/icon-noti.svg" width="86%" alt=""/>
                                    {
                                        this.state.numNoti>0 && (
                                            <div className="alert alert-primary noti-num-border noti-acc big-alert" role="alert">
                                                {this.state.numNoti}
                                            </div>
                                        )
                                    }
                                </div>
                            </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
             {/* End Hend*/}
    {
        // this.state.data(row => (
            <>

    {this.state.cardData.length==0 ? (
        <div/>
    ) : (
        this.state.cardData.map((row,key) => {
            let point = row.point;
            let rangePoint = parseInt(row.cardPoint)-parseInt(row.oldCardPoint)
            if (point => row.oldCardPoint) {
                point = point-row.oldCardPoint;
            }
            // let progresspoint = parseFloat(point)*100/parseFloat(row.cardPoint);
            let progresspoint = parseFloat(point)*100/parseFloat(rangePoint);
            if (progresspoint > 100) {
                progresspoint = 100;
            }

            return (
            <>
            <Store_color/>
            <div className="card shadow px-4 pb-4">
                <div className="mestyle__wrapper bg-transparent pt-0 px-0">
                    <div className="status_and_qrcode__box new-point shadow">
                        <div className="left">
                            <p className="label text-overflow fontcolor">POINTS</p>
                            <p className="value text-nowrap fontcolor">{this.state.point.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                        </div>
                        <div className="right text-center">
                            <a onClick={this.handleClickScanQRCode}>
                                <p className="text-black">SCAN</p>
                                <svg xmlns="http://www.w3.org/2000/svg" width="56.104" height="56.215" viewBox="0 0 56.104 56.215" className="icon mb-1 mt-0">
                                    <g transform="translate(3 3)">
                                        <path d="M267,533.34h9a5.783,5.783,0,0,1,5.781,5.781v9" transform="translate(-231.675 -533.34)" fill="none" className="" stroke="#000" stroke-linecap="round" stroke-width="6"/>
                                        <path d="M161.424,548.12v-9a5.783,5.783,0,0,1,5.781-5.781h9" transform="translate(-161.424 -533.34)" fill="none" className="" stroke="#000" stroke-linecap="round" stroke-width="6"/>
                                        <path d="M281.78,639.3v9A5.783,5.783,0,0,1,276,654.08h-9" transform="translate(-231.675 -603.864)" fill="none" className="" stroke="#000" stroke-linecap="round" stroke-width="6"/>
                                        <path d="M176.2,654.08h-9a5.783,5.783,0,0,1-5.781-5.781v-9" transform="translate(-161.424 -603.864)" fill="none" className="" stroke="#000" stroke-linecap="round" stroke-width="6"/>
                                        <line x2="32.906" transform="translate(9.132 24.923)" fill="none" className="" stroke="#000" stroke-linecap="round" stroke-width="6"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div className="w-100 position-relative">
                    <img className="w-100 card-img shadow" src={row.cardimg == "" ? "/assets/images/card_default.png" : row.cardimg} alt=""/>
                    <div className="row w-100 card-header-account-shop">
                        <div className="col-3">
                            <img className="top-left ml-2 mt-2 p-1" src={row.storelogo != "" ? row.storelogo : "/assets/images/icon-ho-new.png"} width="100%" alt="Logo shop"/>
                        </div>
                        <div className="col-9 pl-0">
                            <div className="top-left-nameshop mt-2 pt-1">
                                <p className="name-shop shadow text-overflow">{row.storename}</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-detail">
                        <div className="w-75 text-center mx-auto border rounded bg-white px-2 py-1 m-2">
                            {/* <h2 className="m-0">{row.point}</h2> */}
                            <div className="row w-100 m-0">
                                <div className="col-4 text-left font-color">
                                    <p>{row.oldCardName}</p>
                                </div>
                                <div className="col-4">
                                    {/* <p>คะแนน</p> */}
                                </div>
                                <div className="col-4 text-right">
                                    <p>{row.cardName}</p>
                                </div>
                            </div>
                            <div className="w-100">
                                <div className="position-relative">
                                    <div className="bar">
                                    </div>
                                    <div className="bar-status shadow" style={{width:progresspoint+"%"}} >

                                    </div>
                                </div>

                            </div>
                            <div className="row w-100 m-0">
                                <div className="col-4 text-left">
                                    <p>{row.oldCardPoint}</p>
                                </div>
                                <div className="col-4">
                                    <p className="font-weight-bold h5">{this.state.memberPoint}</p>
                                </div>
                                <div className="col-4 text-right">
                                    <p>{row.cardPoint}</p>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between">
                            <div>
                                <p className="card-id mb-2">{row.username}</p>
                                <p className="card-id">{'No.'+row.cardcode}</p>
                            </div>
                            <div className="align-self-end">
                                {row.dateexpire!==null ?
                                    <p className="mt-2 card-expire">Exp. {row.dateexpire}</p>
                                : <></>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div id="card-click" className="card-qr text-center mt-3 d-none">
                    <div id="cardQRselected" className="mx-auto w-50" style={{height:'220px'}}>
                        <h5 id="card-text">QR CODE</h5>
                        {
                            row.cardcode &&(
                                <QRCode value={row.cardcode} size="500" className="w-100 "/>
                            )
                        }

                    </div>
                    <div id="cardBCselected" className="mx-auto w-100 d-none" style={{height:'220px'}}>
                        <h5 id="card-text">BARCODE</h5>
                        <Barcode value={row.cardcode} height={'100%'} margin={0} text=''/>
                        {/* <h5>{row.cardcode}</h5> */}
                    </div>
                    {/* <Switch onChange={this.handleChange} checked={this.state.checked} className="toggle-barcode" width={200} /> */}
                    <div className="toggle mx-auto shadow w-75 mt-3 rounded">
                        <div className="row w-100 m-0">
                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardQRselected','cardBCselected')}>
                                <h5 id="cardQRselected-text" className="m-0 toggle-active">QR CODE</h5>
                            </div>
                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardBCselected','cardQRselected')}>
                                <h5 id="cardBCselected-text" className="m-0">BARCODE</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div className="card-qr-code bg-white text-center mx-auto p-2 border shadow">
                {/* <img id="cardQR"  className="" src="/assets/images/icon-qr-code.svg" alt="" onClick={this.addActiveClass}/> */}
                <div className="cardClick" id="cardQR" data-key={key} onClick={this.addActiveClass}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="63.809" height="63.81" viewBox="0 0 63.809 63.81" className="qrCardDisplay">
                        <path d="M739.224,1363.618h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m9.115,0h3.039v-3.038h-3.039v3.038m9.116,0h3.038v-3.038H784.8v3.038m-45.578-3.038h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m6.076,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0H784.8v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5H745.3v3.038m3.039,0h3.038V1354.5H748.34v3.038m3.038,0h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5h-3.039v3.038m3.039,0h3.039V1354.5H766.57v3.038m3.039,0h3.038V1354.5h-3.038v3.038m6.076,0h3.039V1354.5h-3.039v3.038m6.077,0H784.8V1354.5h-3.039v3.038m9.116,0h3.039V1354.5h-3.039v3.038m3.039,0h3.039V1354.5h-3.039v3.038m-54.694-3.038h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m-54.694-3.039h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m12.155,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m-57.732-3.039h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m27.347,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m6.077,0h3.039v-3.039h-3.039v3.039m-54.694-3.039h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m6.076,0h3.039v-3.038h-3.039v3.038m12.155,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m-30.385-3.038h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m9.116,0h3.038v-3.039H784.8v3.039m9.116,0h3.039v-3.039h-3.039v3.039m6.076,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m6.076,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m9.116,0h3.039v-3.038H766.57v3.038m3.039,0h3.038v-3.038h-3.038v3.038m6.076,0h3.039v-3.038h-3.039v3.038m9.116,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038h-3.038v3.038m-54.694-3.038H745.3v-3.039h-3.039v3.039m6.077,0h3.038v-3.039H748.34v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m6.076,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m-54.694-3.039H745.3v-3.039h-3.039v3.039m12.154,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m9.116,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.076,0h3.039v-3.039h-3.039v3.039m-57.732-3.039H745.3v-3.038h-3.039v3.038m6.077,0h3.038v-3.038H748.34v3.038m6.076,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m6.076,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m21.27,0h3.038v-3.038h-3.038v3.038m3.038,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m9.115,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038h-3.038v3.038m-30.385-3.038h3.039v-3.039H766.57v3.039m-27.347-3.039h3.039v-3.039h-3.039v3.039m3.039,0H745.3v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039V1315h-3.039v3.039m18.231,0h3.039V1315h-3.039v3.039m6.077,0h3.039V1315h-3.039v3.039m9.115,0h3.039V1315h-3.039v3.039m9.116,0H784.8V1315h-3.039v3.039m18.231,0h3.039V1315h-3.039v3.039M739.224,1315h3.039v-3.038h-3.039V1315m6.077,0h3.039v-3.038H745.3V1315m3.039,0h3.038v-3.038H748.34V1315m3.038,0h3.039v-3.038h-3.039V1315m6.077,0h3.039v-3.038h-3.039V1315m9.116,0h3.039v-3.038H766.57V1315m9.115,0h3.039v-3.038h-3.039V1315m6.077,0H784.8v-3.038h-3.039V1315m6.077,0h3.039v-3.038H787.84V1315m3.039,0h3.039v-3.038h-3.039V1315m3.039,0h3.039v-3.038h-3.039V1315m6.076,0h3.039v-3.038h-3.039V1315m-60.77-3.038h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m9.115,0h3.039v-3.038h-3.039v3.038m6.077,0H784.8v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H787.84v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.076,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m12.155,0h3.038v-3.038h-3.038v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0H784.8v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H787.84v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.076,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039v-3.039h-3.039v3.039m3.039,0H745.3v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m12.155,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039Z" transform="translate(-739.224 -1299.808)" fill="#000" className="qrColor"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="78.846" height="48.407" viewBox="0 0 78.846 48.407" className="cardClickArrowTop d-none">
                        <g transform="translate(-64.032 -15.306)">
                            <line y1="30.022" x2="30.022" transform="translate(73.225 24.498)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="13" className="arrowTopColor"/>
                            <line x1="30.439" y1="30.022" transform="translate(103.247 24.498)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="13" className="arrowTopColor"/>
                        </g>
                    </svg>
                </div>
            </div>
            </>
            )
        })

    )
    }

    {
        this.state.storeStampData.length==0 ? (
            <div/>
        ) : (
            this.state.storeStampData.map((row,key) => {
                let rowsStamp = [];
                let usersStamp = [];
                for (let i = 1; i <= parseInt(row.stamppoint); i++) {
                    rowsStamp.push(i);
                }
                for (let y = 1; y <= parseInt(row.userstamppoint); y++) {
                    usersStamp.push(y);
                }

                return (


                <>
                <Store_color/>
            <div className="stamp px-4 pt-4" key={key}>
                <div className="stamp-bg w-100 rounded shadow py-2 px-4" id="stamp-bg">
                        <h5 className="font-color">STAMP {row.storename}</h5>
                        <div className="w-100">
                            {
                                rowsStamp.map((row,key) => {
                                    return (
                                        <>
                                    {/* <div className="stamp-item" style={usersStamp[key] === row ? {background: 'white'} : {background: 'transparent'}}> */}
                                    <div className={usersStamp[key] === row ? 'stamp-item stamp-logo' : 'stamp-item stamp-num'}>
                                        {/* <div className="stamp-item-dt"> */}
                                        <div className={((key+1)==(rowsStamp.length) && usersStamp[key] !== (rowsStamp.length)) ? "stamp-item-dt stamp-goal w-100 text-center" : "stamp-item-dt stamp-gift w-100 text-center"}>
                                            <h2 className="font-color">
                                                {((key+1)==(rowsStamp.length) && usersStamp[key] !== (rowsStamp.length)) ?
                                                    // <img src="/assets/images/icon-stamp-goal.svg" alt="" style={{width:'80%'}}/>
                                                    <span className="d-flex stampGoalText" >goal</span>
                                                :
                                                    usersStamp[key] === row  ? <img src={this.state.storelogo} alt=""/>
                                                :   row
                                                }
                                            </h2>
                                        </div>
                                    </div>
                                    </>
                                    )
                                })
                            }
                        </div>
                        <p className="font-color">เงื่อนไขการให้บริการ</p>
                        <ul>
                            <li className="font-color">ซื้อครบ  <NumberFormat value={row.stampthb} displayType={'text'} thousandSeparator={true}/> บาท รับแสตมป์ 1 ดวง</li>
                            <li className="font-color">เฉพาะร้านค้า {row.storename} เท่านั้น </li>
                            {
                                row.cardexpire_desc ? <li className="font-color">{row.cardexpire_desc}</li> : ''
                            }

                        </ul>
                </div>
                <div  className="card-qr text-center d-none py-3 mx-3 shadow rounded-bottom stamp-click" data-key={key}>
                    <div id="stampQRselected" className="mx-auto w-50" style={{height:'220px'}}>
                        <h5 id="card-text">QR CODE</h5>
                        <QRCode value={row.stampcode} size="500" className="w-100 "/>
                    </div>
                    <div id="stampBCselected" className="mx-auto w-100 d-none" style={{height:'220px'}}>
                        <h5 id="card-text">BARCODE</h5>
                        <Barcode value={row.stampcode} height={'100%'} margin={0} text=''/>
                    </div>
                    <div className="toggle mx-auto shadow w-75 mt-3 rounded">
                        <div className="row w-100 m-0">
                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'stampQRselected','stampBCselected')}>
                                <h5 id="stampQRselected-text" className="m-0 toggle-active">QR CODE</h5>
                            </div>
                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'stampBCselected','stampQRselected')}>
                                <h5 id="stampBCselected-text" className="m-0">BARCODE</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="stamp-qr-code bg-black text-center mx-auto p-2">
                {/* <img className="stampClick"  data-key={key} src="/assets/images/icon-qr-code.svg" alt="" onClick={this.addActiveClass}/> */}
                <div className="stampClick" data-key={key} onClick={this.addActiveClass}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="63.809" height="63.81" viewBox="0 0 63.809 63.81" className="qrStampDisplay">
                        <path d="M739.224,1363.618h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m9.115,0h3.039v-3.038h-3.039v3.038m9.116,0h3.038v-3.038H784.8v3.038m-45.578-3.038h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m6.076,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0H784.8v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5H745.3v3.038m3.039,0h3.038V1354.5H748.34v3.038m3.038,0h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5h-3.039v3.038m6.077,0h3.039V1354.5h-3.039v3.038m3.039,0h3.039V1354.5H766.57v3.038m3.039,0h3.038V1354.5h-3.038v3.038m6.076,0h3.039V1354.5h-3.039v3.038m6.077,0H784.8V1354.5h-3.039v3.038m9.116,0h3.039V1354.5h-3.039v3.038m3.039,0h3.039V1354.5h-3.039v3.038m-54.694-3.038h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m-54.694-3.039h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m12.155,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m-57.732-3.039h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m27.347,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m6.077,0h3.039v-3.039h-3.039v3.039m-54.694-3.039h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m6.076,0h3.039v-3.038h-3.039v3.038m12.155,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m-30.385-3.038h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m9.116,0h3.038v-3.039H784.8v3.039m9.116,0h3.039v-3.039h-3.039v3.039m6.076,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m6.076,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m9.116,0h3.039v-3.038H766.57v3.038m3.039,0h3.038v-3.038h-3.038v3.038m6.076,0h3.039v-3.038h-3.039v3.038m9.116,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038h-3.038v3.038m-54.694-3.038H745.3v-3.039h-3.039v3.039m6.077,0h3.038v-3.039H748.34v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m6.076,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m-54.694-3.039H745.3v-3.039h-3.039v3.039m12.154,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m9.116,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.076,0h3.039v-3.039h-3.039v3.039m-57.732-3.039H745.3v-3.038h-3.039v3.038m6.077,0h3.038v-3.038H748.34v3.038m6.076,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m6.076,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m21.27,0h3.038v-3.038h-3.038v3.038m3.038,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.038h-3.039v3.038m3.039,0H745.3v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m9.115,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038H784.8v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.038v-3.038h-3.038v3.038m-30.385-3.038h3.039v-3.039H766.57v3.039m-27.347-3.039h3.039v-3.039h-3.039v3.039m3.039,0H745.3v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m6.077,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039V1315h-3.039v3.039m18.231,0h3.039V1315h-3.039v3.039m6.077,0h3.039V1315h-3.039v3.039m9.115,0h3.039V1315h-3.039v3.039m9.116,0H784.8V1315h-3.039v3.039m18.231,0h3.039V1315h-3.039v3.039M739.224,1315h3.039v-3.038h-3.039V1315m6.077,0h3.039v-3.038H745.3V1315m3.039,0h3.038v-3.038H748.34V1315m3.038,0h3.039v-3.038h-3.039V1315m6.077,0h3.039v-3.038h-3.039V1315m9.116,0h3.039v-3.038H766.57V1315m9.115,0h3.039v-3.038h-3.039V1315m6.077,0H784.8v-3.038h-3.039V1315m6.077,0h3.039v-3.038H787.84V1315m3.039,0h3.039v-3.038h-3.039V1315m3.039,0h3.039v-3.038h-3.039V1315m6.076,0h3.039v-3.038h-3.039V1315m-60.77-3.038h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038H766.57v3.038m9.115,0h3.039v-3.038h-3.039v3.038m6.077,0H784.8v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H787.84v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.076,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H745.3v3.038m3.039,0h3.038v-3.038H748.34v3.038m3.038,0h3.039v-3.038h-3.039v3.038m6.077,0h3.039v-3.038h-3.039v3.038m12.155,0h3.038v-3.038h-3.038v3.038m3.038,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.077,0H784.8v-3.038h-3.039v3.038m6.077,0h3.039v-3.038H787.84v3.038m3.039,0h3.039v-3.038h-3.039v3.038m3.039,0h3.039v-3.038h-3.039v3.038m6.076,0h3.039v-3.038h-3.039v3.038m-60.77-3.038h3.039v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m6.077,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H766.57v3.039m3.039,0h3.038v-3.039h-3.038v3.039m6.076,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m18.231,0h3.039v-3.039h-3.039v3.039m-60.77-3.039h3.039v-3.039h-3.039v3.039m3.039,0H745.3v-3.039h-3.039v3.039m3.039,0h3.039v-3.039H745.3v3.039m3.039,0h3.038v-3.039H748.34v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m12.155,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m6.077,0H784.8v-3.039h-3.039v3.039m3.039,0h3.038v-3.039H784.8v3.039m3.038,0h3.039v-3.039H787.84v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.039v-3.039h-3.039v3.039m3.039,0h3.038v-3.039h-3.038v3.039m3.038,0h3.039v-3.039h-3.039v3.039Z" transform="translate(-739.224 -1299.808)" fill="#000" className="qrColor"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="78.846" height="48.407" viewBox="0 0 78.846 48.407" className="stampClickArrowTop d-none">
                        <g transform="translate(-64.032 -15.306)">
                            <line y1="30.022" x2="30.022" transform="translate(73.225 24.498)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="13" className="arrowTopColor"/>
                            <line x1="30.439" y1="30.022" transform="translate(103.247 24.498)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="13" className="arrowTopColor"/>
                        </g>
                    </svg>
                </div>
            </div>

                </>
                )
            })
        )
    }




    {
    this.state.stampRewarData.length== 0 ? (
        <div/>
    ) : (
        <div className="rewards mt-3">
                <h3 className="text-center">รางวัลที่ได้รับ</h3>
                    <div className="row w-100 mx-auto stampreward pb-3">
                        {
                            this.state.stampRewarData.map((row,key) => {
                                return (
                                <>
                                <div className="col-5 position-relative mt-3" key={key}>
                                    <a href={"/reward_stamp/"+row.rewardid}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="103" height="121" viewBox="0 0 103 121" className="reward-flag">
                                            <path d="M0,0H103V121L52.526,93.45,0,121Z" fill="#da1111" className="reward-flag-color"/>
                                        </svg>
                                        <div className="reward-point">
                                            <div className="text-center">
                                                <h4 className="m-0">{row.stampamount}</h4>
                                                <p className="m-0">STAMP</p>
                                            </div>
                                        </div>
                                        <div className="reward-item shadow rounded" style={{height:'77%'}}>
                                            <img className="" src={row.rewardimg} alt=""/>
                                            <h5>{row.rewardname}</h5>
                                        </div>
                                    </a>

                                    {
                                        //Needs to change the compare. No user data in stampRewarData
                                        (parseFloat(row.stampamount) <= parseFloat(this.state.stampPoint)) ? (
                                            <button className="w-100 mt-2 btn btn-green" onClick={(e) => window.location='/reward_stamp/'+row.rewardid}>
                                                <p className="m-0 font-tf">แลกรางวัล</p>
                                            </button>
                                        ):(
                                            <button className="w-100 mt-2 btn btn-gray">
                                                <p className="m-0 font-tf">สะสมเพิ่ม</p>
                                            </button>
                                        )
                                    }

                                </div>
                                </>
                                )

                            })
                        }



                    </div>
        </div>
    )
    }


    {
        this.state.stampExpireDetail.length== 0 ? (

            <div/>
        ) : (
            <Card_expire_detail stampExpireDetailData={this.state.stampExpireDetail}/>

        )
    }



            <div className="mestyle__wrapper">
                {/* <div className="status_and_qrcode__box">
                    <div className="left">
                        <p className="label text-overflow">{this.state.data.storename}</p>
                        <p className="value text-nowrap">{this.state.point.toLocaleString(navigator.language, { minimumFractionDigits: 0 })} POINT</p>
                    </div>
                    <div className="right text-center">
                        <a onClick={this.handleClickScanQRCode}>
                            <p>SCAN</p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="56.104" height="56.215" viewBox="0 0 56.104 56.215" className="icon mb-1 mt-0">
                                <g transform="translate(3 3)">
                                    <path d="M267,533.34h9a5.783,5.783,0,0,1,5.781,5.781v9" transform="translate(-231.675 -533.34)" fill="none" className="account-img-stroke" stroke="#cd001c" stroke-linecap="round" stroke-width="6"/>
                                    <path d="M161.424,548.12v-9a5.783,5.783,0,0,1,5.781-5.781h9" transform="translate(-161.424 -533.34)" fill="none" className="account-img-stroke" stroke="#cd001c" stroke-linecap="round" stroke-width="6"/>
                                    <path d="M281.78,639.3v9A5.783,5.783,0,0,1,276,654.08h-9" transform="translate(-231.675 -603.864)" fill="none" className="account-img-stroke" stroke="#cd001c" stroke-linecap="round" stroke-width="6"/>
                                    <path d="M176.2,654.08h-9a5.783,5.783,0,0,1-5.781-5.781v-9" transform="translate(-161.424 -603.864)" fill="none" className="account-img-stroke" stroke="#cd001c" stroke-linecap="round" stroke-width="6"/>
                                    <line x2="32.906" transform="translate(9.132 24.923)" fill="none" className="account-img-stroke" stroke="#cd001c" stroke-linecap="round" stroke-width="6"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div> */}

                { this.displaySuggestFriend() }
                { this.displayAddressDeliver() }

                <a href={'/shop_history/'+this.state.store_id}>
                <div className="link__box">
                    <div className="left">
                        <div className="tab"></div>
                        {/* <img src="/assets/images/icon-history-log.svg" alt="icon" className="icon"/> */}
                        <svg xmlns="http://www.w3.org/2000/svg" width="65" height="76.027" viewBox="0 0 65 76.027" className="icon">
                            <g transform="translate(-157 -1554.973)">
                                <path className="account-img-bg-color" d="M618.656,1828.512v-58.966l9.785,7.879,8.969-7.879,7.882,7.879,8.7-7.879,7.61,7.879,8.7-7.879v58.966Z" transform="translate(-454.501 -206)" fill="#cd001c"/>
                                <rect width="60" height="18" rx="9" transform="translate(162 1613)" fill="#cd001c" className="account-img-bg-color"/>
                                <g transform="translate(157 1607)" fill="none" stroke="#000" stroke-width="3">
                                    <rect width="60" height="18" rx="9" stroke="none"/>
                                    <rect x="1.5" y="1.5" width="57" height="15" rx="7.5" fill="none"/>
                                </g>
                                <g transform="translate(157 1607)" fill="none" stroke="#000" stroke-width="3">
                                    <ellipse cx="7.5" cy="9" rx="7.5" ry="9" stroke="none"/>
                                    <ellipse cx="7.5" cy="9" rx="6" ry="7.5" fill="none"/>
                                </g>
                                <line x2="4" transform="translate(169.5 1592.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="4" transform="translate(169.5 1579.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="13" transform="translate(169.5 1598.877)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="13" transform="translate(169.5 1585.877)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="15" transform="translate(186.5 1598.877)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="15" transform="translate(186.5 1585.877)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="2" transform="translate(199.5 1592.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="2" transform="translate(199.5 1579.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="16" transform="translate(178.5 1592.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <line x2="16" transform="translate(178.5 1579.441)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
                                <path d="M660.155,1815.907c-.586,3.525,0-54.74,0-54.74l9.5,7.149,8.911-7.149,7.932,7.149,8.128-7.149,7.736,8.128,9.792-8.128V1811.5h-46.22S660.741,1812.382,660.155,1815.907Z" transform="translate(-501 -203)" fill="none" stroke="#000" stroke-width="3"/>
                            </g>
                        </svg>
                        <p className="title">ประวัติการใช้งาน</p>
                    </div>
                    <div className="right">
                        <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                    </div>
                </div>
                </a>
                <a onClick={(e) => this.editAccount(e)}>
                    <div className="link__box">
                        <div className="left">
                            <div className="tab"></div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="56.647" height="53.437" viewBox="0 0 56.647 53.437" className="icon">
                                <g transform="translate(-974.084 -344.694)">
                                    <g transform="translate(51 2)">
                                        <path d="M3729.419,353.436H3697.5v42.695h44.06V366.272" transform="translate(-2769.912)" fill="#fff" className="account-img-bg-color"/>
                                        <g transform="translate(942.188 376.566) rotate(-45)" fill="#fff" className="account-img-bg-color">
                                            <path d="M 42.00467300415039 8.425962448120117 L 42.0013313293457 8.425952911376953 L 1.725972294807434 8.237632751464844 C 1.346902370452881 8.235862731933594 1.03707230091095 7.926022529602051 1.035302400588989 7.546962738037109 L 1.007842302322388 1.673462748527527 C 1.007002353668213 1.494702696800232 1.075192332267761 1.327282667160034 1.19984233379364 1.202042698860168 C 1.324502348899841 1.076802730560303 1.491522312164307 1.007822751998901 1.670132398605347 1.007822751998901 L 41.94883346557617 1.196152687072754 C 42.32790374755859 1.197922706604004 42.63773345947266 1.507762670516968 42.63950347900391 1.886822700500488 L 42.66697311401367 7.760322570800781 C 42.66781234741211 7.939082622528076 42.5996208190918 8.106502532958984 42.4749641418457 8.231742858886719 C 42.35030364990234 8.356983184814453 42.18328094482422 8.425962448120117 42.00467300415039 8.425962448120117 Z" stroke="none"/>
                                            <path d="M 2.009414672851562 2.009417057037354 L 2.03387451171875 7.239060401916504 L 41.66537857055664 7.424376487731934 L 41.64093017578125 2.194725036621094 L 2.009414672851562 2.009417057037354 M 1.670135498046875 0.007823944091796875 C 1.672847747802734 0.007823944091796875 1.675426483154297 0.007829666137695312 1.678142547607422 0.007843017578125 L 41.95351409912109 0.1961631774902344 C 42.88032150268555 0.2005023956298828 43.63516235351562 0.9553422927856445 43.63949203491211 1.882152557373047 L 43.66695404052734 7.755642890930176 C 43.67127990722656 8.679737091064453 42.92771530151367 9.425961494445801 42.00466918945312 9.425961494445801 C 42.00195693969727 9.425961494445801 41.9993782043457 9.425955772399902 41.99666213989258 9.425942420959473 L 1.721302032470703 9.237622261047363 C 0.7944831848144531 9.233283042907715 0.039642333984375 8.478443145751953 0.03531265258789062 7.551632881164551 L 0.007843017578125 1.678142547607422 C 0.0035247802734375 0.7540483474731445 0.7470817565917969 0.007823944091796875 1.670135498046875 0.007823944091796875 Z" stroke="none" fill="#fff" className="account-img-bg-color"/>
                                        </g>
                                        <g transform="translate(940.188 392.133) rotate(-135)" fill="#fff" className="account-img-bg-color">
                                            <path d="M 8.300490379333496 7.999996662139893 L 1.699509978294373 7.999996662139893 L 4.999711036682129 2.059646606445312 L 5.000293254852295 2.059646606445312 L 8.300479888916016 7.999996662139893 L 8.300490379333496 7.999996662139893 Z" stroke="none"/>
                                            <path d="M 5.000002384185791 4.118246555328369 L 3.39903450012207 6.999996662139893 L 6.60097074508667 6.999996662139893 L 5.000002384185791 4.118246555328369 M 5 1.059128761291504 C 5.341827392578125 1.059128761291504 5.68365478515625 1.230581283569336 5.874159812927246 1.573486328125 L 9.174639701843262 7.51435661315918 C 9.544939994812012 8.180887222290039 9.062970161437988 8.999996185302734 8.300490379333496 8.999996185302734 L 1.699509620666504 8.999996185302734 C 0.9370298385620117 8.999996185302734 0.4550600051879883 8.180887222290039 0.8253602981567383 7.51435661315918 L 4.125840187072754 1.573486328125 C 4.31634521484375 1.230581283569336 4.658172607421875 1.059128761291504 5 1.059128761291504 Z" stroke="none" fill="#fff"/>
                                        </g>
                                    </g>
                                    <g transform="translate(48 -1)">
                                        <g transform="translate(942.188 376.566) rotate(-45)" fill="none">
                                            <path d="M1.678.008,41.954.2a1.7,1.7,0,0,1,1.686,1.686l.027,5.873A1.659,1.659,0,0,1,42,9.426L1.721,9.238A1.7,1.7,0,0,1,.035,7.552L.008,1.678A1.659,1.659,0,0,1,1.678.008Z" stroke="none"/>
                                            <path d="M 3.014125823974609 3.014119148254395 L 3.029232025146484 6.24370288848877 L 40.66067123413086 6.419660568237305 L 40.64557266235352 3.190082550048828 L 3.014125823974609 3.014119148254395 M 1.670135498046875 0.007823944091796875 C 1.672847747802734 0.007823944091796875 1.675426483154297 0.007829666137695312 1.678142547607422 0.007843017578125 L 41.95351409912109 0.1961631774902344 C 42.88032150268555 0.2005023956298828 43.63516235351562 0.9553422927856445 43.63949203491211 1.882152557373047 L 43.66695404052734 7.755642890930176 C 43.67127990722656 8.679737091064453 42.92771530151367 9.425961494445801 42.00466918945312 9.425961494445801 C 42.00195693969727 9.425961494445801 41.9993782043457 9.425955772399902 41.99666213989258 9.425942420959473 L 1.721302032470703 9.237622261047363 C 0.7944831848144531 9.233283042907715 0.039642333984375 8.478443145751953 0.03531265258789062 7.551632881164551 L 0.007843017578125 1.678142547607422 C 0.0035247802734375 0.7540483474731445 0.7470817565917969 0.007823944091796875 1.670135498046875 0.007823944091796875 Z" stroke="none" fill="#707070"/>
                                        </g>
                                        <g transform="translate(940.188 392.133) rotate(-135)" fill="none">
                                            <path d="M4.126,1.573a1,1,0,0,1,1.748,0l3.3,5.941A1,1,0,0,1,8.3,9H1.7A1,1,0,0,1,.825,7.514Z" stroke="none"/>
                                            <path d="M 5 1.059128761291504 C 5.341827392578125 1.059128761291504 5.68365478515625 1.230581283569336 5.874159812927246 1.573486328125 L 9.174639701843262 7.51435661315918 C 9.544939994812012 8.180887222290039 9.062970161437988 8.999996185302734 8.300490379333496 8.999996185302734 L 1.699509620666504 8.999996185302734 C 0.9370298385620117 8.999996185302734 0.4550600051879883 8.180887222290039 0.8253602981567383 7.51435661315918 L 4.125840187072754 1.573486328125 C 4.31634521484375 1.230581283569336 4.658172607421875 1.059128761291504 5 1.059128761291504 Z" stroke="none" fill="#707070"/>
                                        </g>
                                        <path d="M3729.419,353.436H3697.5v42.695h44.06V366.272" transform="translate(-2769.912)" fill="none" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                </g>
                            </svg>
                            <p className="title">แก้ไขข้อมูลส่วนตัว</p>
                        </div>
                        <div className="right">
                            <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                        </div>
                    </div>
                </a>
            </div>

            <div className="shop_detail__wrapper">
                <h3>รายละเอียดร้านค้า</h3>
                {/* <a href="/Shop_search"> */}
                {/* <a target="_BLANK" href={'https://www.google.com/maps/search/?api=1&query='+(this.state.data.location !== undefined && this.state.data.location !== null && this.state.data.location !== '' ?  this.state.data.location : this.state.data.storename)+'&openExternalBrowser=1'}> */}
                <a href="/store_maps">
                <div className="link__box">
                    <div className="left">
                        <div className="tab"></div>
                        {/* <img src="/assets/images/icon-marker.svg" alt="icon" className="icon"/> */}
                        <svg xmlns="http://www.w3.org/2000/svg" width="41" height="66.499" viewBox="0 0 41 66.499" className="icon">
                            <g transform="translate(-177 -1513)">
                                <g transform="translate(177 1513)" className="account-img-bg-color" fill="#cd001c" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                                    <circle cx="20.5" cy="20.5" r="20.5" stroke="none"/><circle cx="20.5" cy="20.5" r="19" fill="none"/>
                                </g>
                                <path d="M-16739,10203.709l18.354,36.678,16.205-36.678" transform="translate(16919.25 -8662.387)" className="account-img-bg-color" fill="#cd001c" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                <g transform="translate(189 1523)" fill="#fff" stroke="#cd001c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1">
                                    <circle cx="9" cy="9" r="9" stroke="none"/>
                                    <circle cx="9" cy="9" r="8.5" fill="none"/>
                                </g>
                            </g>
                        </svg>
                        <p className="title">ค้นหาร้านค้า</p>
                    </div>
                    <div className="right">
                        <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                    </div>
                </div>
                </a>
                <div className="detail__box">
                    <div className="tab"></div>
                    <div className="content__wrapper">
                    {this.state.storeDetailData.length==0 ? (
                            <div/>
                        ):(
                            this.state.storeDetailData.map((row,key) => {

                                return (
                                    <>
                                        <a className="link__box" href={'tel:'+row.tel}>
                                            <div className="box">
                                                <img src="/assets/images/icon-shop-phone.svg" alt="icon" className="icon"/>
                                                <p>{row.tel}</p>
                                            </div>
                                        </a>
                                        {/* {
                                            row.email !== '' && this.state.displayMainEmail=== true ? (
                                                <a className="link__box" href={'mailto:'+row.email} target="_BLANK">
                                                    <div className="box">
                                                        <img src="/assets/images/icon-shop-email.svg" alt="icon" className="icon"/>
                                                        <p>{row.email}</p>
                                                    </div>
                                                </a>
                                            ) : (
                                                <div/>
                                            )
                                        } */}
                                        {

                                            row.lineoa !== '' ? (
                                                <a className="link__box" href={'https://line.me/ti/p/~'+row.lineoa}>
                                                <div className="box">
                                                    <img src="/assets/images/icon-shop-line.svg" alt="icon" className="icon"/>
                                                    <p>{row.lineoa}</p>
                                                </div>
                                                </a>
                                            ):(
                                                <div/>
                                            )
                                        }

                                    </>
                                )
                            })
                        )

                    }

                    {this.state.storeSocialData.length==0 ? (
                            <div/>
                        ):(
                            this.state.storeSocialData.map((row,key) => {
                                let label;
                                let link;
                                let icon;
                                switch(row.socialtype) {
                                    case "Website":
                                        label = row.socialdetail;
                                        link  = row.socialdetail !== '' ? row.socialdetail+'?openExternalBrowser=1' : '';
                                        icon  = '/assets/images/icon-shop-website.svg';
                                      break;
                                    case "Facebook":
                                      label = row.socialdetail;
                                      link  = row.socialdetail !== '' ? row.socialdetail+'?openExternalBrowser=1' : '';
                                      icon  = '/assets/images/icon-shop-facebook.svg';
                                        break;
                                    case "Instagram":
                                        label = row.socialdetail;
                                        link  = row.socialdetail !== '' ? 'https://www.instagram.com/'+row.socialdetail+'?openExternalBrowser=1' : '';
                                        icon  = '/assets/images/icon-shop-instagram.svg';
                                        break;
                                    case "Line":
                                        label = row.socialdetail;
                                        link  = row.socialdetail !== '' ? 'https://line.me/ti/p/~'+row.socialdetail : '';
                                        icon  = '/assets/images/icon-shop-line.svg';
                                        break;
                                    case "Email":
                                        label = row.socialdetail;
                                        link  = row.socialdetail !== '' ? 'mailto:'+row.socialdetail : '';
                                        icon  = '/assets/images/icon-shop-email.svg';
                                        break;

                                    default:
                                        label = row.socialdetail;
                                        link = '';
                                        icon = '/assets/images/icon-shop-phone.svg';

                                }

                                return (
                                    <>
                                    {
                                        link!=='' ? (
                                            <a className="link__box" key={key} href={link} target="_BLANK">
                                                <div className="box">
                                                    <img src={icon} alt="icon" className="icon"/>
                                                    <p>{label}</p>
                                                </div>
                                            </a>
                                        ) : (
                                            <div/>
                                        )
                                    }

                                    </>
                                )
                            })
                        )

                    }


                    </div>

                </div>
                <a href="/Shop_about">
                <div className="link__box black_theme">
                    <div className="left">
                        <div className="tab"></div>
                        <img src="/assets/images/icon-shop-about.svg" alt="icon" className="icon"/>
                        <p className="title">เกี่ยวกับ {this.state.data.storename}</p>
                    </div>
                    <div className="right">
                        <img src="/assets/images/icon-arrow-right.svg" alt="Arrow right icon" className="to_link__icon"/>
                    </div>
                </div>
                </a>
            </div>
            <br/>
        <br/>
        <br/>
        <br/>
        </>
        // ))
    }

        <Navigation active={'account'} storeid={this.state.store_id}/>

        </div>

        </>


        )
    }
}
export default Account_shop;
