import React, { Component } from 'react'
import Footer from './Footer'
import axios from 'axios'

class Account_shop extends Component {

    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        store_id:localStorage.getItem('storageStoreID'),
    };


    async componentDidMount(){
        
        const result = await axios.get('https://backend-ho.dreamnolimit.com/store/api/store/'+this.state.store_id)
        if(result.status===200){
            this.setState({data: result.data});
        }
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
        
    }

    render() {
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/account_shop_v1.css?v=3"/>
        {/* Head */}
        <div className="head bg-black shadow">
            <div className="row w-100 h-100">
                <div className="col-10 h-100">
                    <div className=" h-100 p-3 pl-5">
                        <h1 className="m-0 text-white">Account</h1> 
                        <p className="text-red">บัตรสะสมคะแนน แสตมป์ ข้อมูลร้านค้าของคุณ</p>
                    </div>
                </div>
                <div className="col-2 h-100">
                    <div className="row float-right h-100 w-100">
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* End Hend*/}
        {
            this.state.data.map(row => (
                                    
                                
        <div className="h-100 w-100">
            <div className="container-mobile m-auto ">
            <div className="text-center p-4 w-100">
                <div className="col p-1 my-auto">
                    <img className="w-100" src={row.store_card} alt=""/>
                </div>
            </div>

            <div className="list_menu">
                <div className="account-menu-header w-100 shadow  mt-3 rounded-lg d-flex flex-row ">
                    <div className="account-menu-detail pl-3 h-100 w-75 ">
                        <span className="pl-3">{row.storename} COIN</span>
                    </div>
                    <div className="account-menu-detail pl-3 h-100 w-75" style={{textAlign: "right"}}>
                        <span className="pl-3">100 COIN</span>
                    </div>
                </div>
                <a href="#">
                    <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                        <div className="box rounded-left "></div>
                        <div className="account-menu-detail pl-3 h-100 w-75 ">
                            <img className="menu-icon" src="/assets/images/icon-ft-shop-account.svg" alt=""/>
                            <span className="pl-3">แนะนำเพื่อนเพื่อรับคะแนน</span>
                        </div>
                        <div className="account-menu-detail ml-auto">
                            <img className="h-100 p-1" src="/assets/images/icon-shop-go.svg" alt=""/>
                        </div>
                    </div>
                </a>
                <a href="/order_history">
                    <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                        <div className="box rounded-left "></div>
                        <div className="account-menu-detail pl-3 h-100 w-75 ">
                            <img className="menu-icon" src="/assets/images/icon-shop-map-marker.svg" alt=""/>
                            <span className="pl-3">จัดการที่อยู่การจัดส่ง</span>
                        </div>
                        <div className="account-menu-detail ml-auto">
                            <img className="h-100 p-1" src="/assets/images/icon-shop-go.svg" alt=""/>
                        </div>
                    </div>
                </a>
                <a href={'/shop_history/'+localStorage.getItem('storageStoreID')}>
                    <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                        <div className="box rounded-left "></div>
                        <div className="account-menu-detail pl-3 h-100 w-75 ">
                            <img className="menu-icon" src="/assets/images/icon-shop-history.svg" alt=""/>
                            <span className="pl-3">ประวัติการใช้งาน</span>
                        </div>
                        <div className="account-menu-detail ml-auto">
                            <img className="h-100 p-1" src="/assets/images/icon-shop-go.svg" alt=""/>
                        </div>
                    </div>
                </a>
            </div>

            
            <div className="list_detail">
                <h3>รายละเอียดร้านค้า</h3>
                <a href="">
                    <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                        <div className="box rounded-left "></div>
                        <div className="account-menu-detail pl-3 h-100 w-75 ">
                            <img className="menu-icon" src="/assets/images/icon-shop-map-marker.svg" alt=""/>
                            <span className="pl-3">ค้นหาร้านค้า</span>
                        </div>
                        <div className="account-menu-detail ml-auto">
                            <img className="h-100 p-1" src="/assets/images/icon-shop-go.svg" alt=""/>
                        </div>
                    </div>
                </a>
                <br/>
                <div className="contact-menu">
                    <div className="box rounded-left"></div>
                    <a href="#">
                        <div className="contact-menu-detail ">
                            <img className="menu-icon" src="/assets/images/icon-shop-mobile.svg" alt=""/>
                            <span className="pl-3">{row.storeTel}</span>
                        </div>        
                    </a>
                    <a href="#">
                        <div className="contact-menu-detail">
                            <img className="menu-icon" src="/assets/images/icon-shop-envelope.svg" alt=""/>
                            <span className="pl-3">{row.storeEmail}</span>
                        </div>            
                    </a>
                    {/* <a href="#">
                        <div className="contact-menu-detail ">
                            <img className="menu-icon" src="/assets/images/icon-shop-globe.svg" alt=""/>
                            <span className="pl-3">https://www.mestyleth.com</span>
                        </div>            
                    </a>
                    <a href="#">
                        <div className="contact-menu-detail ">
                            <img className="menu-icon" src="/assets/images/icon-shop-line.svg" alt=""/>
                            <span className="pl-3">mestyleth</span>
                        </div>            
                    </a>
                    <a href="#">
                        <div className="contact-menu-detail">
                            <img className="menu-icon" src="/assets/images/icon-shop-facebook.svg" alt=""/>
                            <span className="pl-3">https://www.facebook.co/mestyleth/</span>
                        </div>            
                    </a>
                    <a href="#">
                        <div className="contact-menu-detail">
                            <img className="menu-icon" src="/assets/images/icon-shop-instagram.svg" alt=""/>
                            <span className="pl-3">Mestyleth</span>
                        </div>            
                    </a> */}
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            </div>  
        </div>  
        ))
    }
        {/* Footer */}
        <div className="footer mx-auto">
            <img className="w-100 position-relative" src="/assets/images/footer_account_shop.svg" alt=""/>
            <div className="menu position-absolute w-100">
               <div className="menu-item px-2 pt-2 ">
                   <a href="/home">
                       <img className="" src="/assets/images/icon-ft-shop-home.svg" alt=""/><br/>
                       <span>HOME</span>
                    </a>
                </div>
                <div className="menu-item px-2 pt-2">
                    <a href={'/store_feed/'+localStorage.getItem('storageStoreID')}>
                       <img className="position-relative" src="/assets/images/icon-ft-shop-feed.svg" alt=""/><br/>
                       <span>FEED</span>
                   </a>
                </div>
                <div className="menu-item px-2 pt-2">
                    <a href={'/shop/'+localStorage.getItem('storageStoreID')}>
                        <img className="" src="/assets/images/icon-ft-shop-shop.svg" alt=""/> <br/>
                        <span>SHOP</span>
                    </a>  
                </div>
                <div className="menu-item px-2 pt-2">
                    <a href={'/deal/'+localStorage.getItem('storageStoreID')}>
                       <img className="" src="/assets/images/icon-ft-shop-deal.svg" alt=""/> <br/>
                       <span>DEAL</span>
                    </a>
                </div>
                <div className="menu-item px-2 pt-2">
                    <div className="menu-item-active bg-black rounded-circle shadow" style={{marginLeft:0+'%'}}>
                        <div className="menu-item-active-ab">
                            <a href={'/account_shop/'+localStorage.getItem('storageStoreID')}>
                                <img className="position-relative" src="/assets/images/icon-ft-shop-account.svg" alt=""/> <br/>
                                <span>MY ACCOUNT</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
        {/* End Footer */}
        </>


        )
    }
}
export default Account_shop;
