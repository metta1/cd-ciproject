import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header_back'
import Footer from './Footer'
import Swal from 'sweetalert2'
import {Navigation_shop as Navigation} from '../template'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();



class Product_category extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleFilterCate = this.handleFilterCate.bind(this);
       
    }

    
    
    state = {
        data : [],
        categoryData:[],
        store_id:localStorage.getItem('storageStoreID'),
        back_url:"/shop/"+localStorage.getItem('storageStoreID'),
        searchCate: "",
        userid:sessionStorage.getItem('user_id'),
        numNoti:0,
        numProductInCart:0
    };
    


    async componentDidMount(){
        const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                this.setState({
                    numNoti:numNoti
                })
            }
        } 

        const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.store_id);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })
           
        }

        const resultCart = await axios.get(config.api.base_url+'/order/api/cart/'+this.state.store_id+'/'+sessionStorage.getItem('user_id'));
        if(resultCart.status===200){
            if(resultCart.data.cartAmount>0){
                this.setState({
                    numProductInCart : resultCart.data.cartAmount
                })
            }
        }
        
        
        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id)
        if(result.status===200){
            this.setState({data: result.data});
        }

        const resultCat = await axios.get(config.api.base_url+'/store_product/api/storecategory/'+this.state.store_id)
        if(resultCat.status===200){
            this.setState({categoryData: resultCat.data});
        }

     


        
        
        

        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
        
    handleToCart = (proid,event) => {
        event.preventDefault();
        if(proid!==null){
                    let user_id = sessionStorage.getItem('user_id');
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('proid',proid)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/order/api/addToCart',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            if(response.status==201){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    customClass: 'swal-mobile',
                                    title: 'เพิ่มไปยังรถเข็นแล้ว',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    
                                });
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                })
                            }
                            console.log(response)
                        })
                        .catch(function (response) {
                            //console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                },
                            })
                            
                           
                    });

            

        }
    }
    
    handleSubmit = event => {
    
        
    }

    handleSearchInput = event => {
        this.setState({searchCate: event.target.value});
    }

    async handleFilterCate(e){
        let keyWord = this.state.searchCate;
        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }

        const resultCat = await axios.get(config.api.base_url+'/store_product/api/storecategory/'+this.state.store_id+'?keywords='+keyWord)
        if(resultCat.status===200){
            this.setState({categoryData: resultCat.data});
        } else {
            this.setState({categoryData: []});
        }
    }
    
    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1', '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm';
        }
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <Header data={this.state}/>
                    <div className="shop-home">
                        
                        <div className="row p-2 mt-1 m-0 w-100 h-100 ">
                            <div className="col-9">
                                    <div className="position-relative ">
                                        <input type="text" className="search w-100 rounded form-control shadow" placeholder="Search category" onKeyUp={this.handleSearchInput}/>
                                        <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" onClick={this.handleFilterCate}/>
                                    </div>

                            </div>
                            <div className="col-3 row head2-icon p-0">
                                <div className="icon-new ">
                                    <a onClick={this.handleClickScanQRCode}>
                                        <img className="" src="/assets/images/icon-scan-shadow.png" alt="..." />
                                    </a>
                                </div>
                                <div className="icon-new " style={{left:'0.35rem'}}>
                                    <a href="/notifications">
                                        <img className="" src="/assets/images/icon-notification-shadow.png" alt="..." />
                                        {
                                            this.state.numNoti>0 && (
                                                <div className="alert alert-primary noti-num-border small-alert" role="alert">
                                                    {this.state.numNoti}
                                                </div>
                                            )
                                        }
                                    </a>
                                </div>
                                <div className="icon-new" style={{left:'0.7rem'}}>
                                    <a href={'/cart/'+this.state.store_id}>
                                        <img class="" src="/assets/images/icon-basket-shadow.png" alt="..." />
                                        {
                                            this.state.numProductInCart>0 && (
                                                <div className="alert alert-primary noti-num-border small-alert" role="alert">
                                                    {this.state.numProductInCart}
                                                </div>
                                            )
                                        }
                                    </a>
                                </div>
                            </div>
                        </div>
                        {this.state.categoryData === undefined ? (
                            <></>
                        ):(
                                        <div className="shop-home-cat-detail px-3">
                                            <div className="d-flex bd-highlight ">
                                                <div className="px-2 py-0  bd-highlight">
                                                    <h4 className="m-0">หมวดหมู่</h4>
                                                </div>
                                                <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                                   
                                                </div>
                                            </div>
                            
                                            <div className="row w-100 m-0 text-center">
                                {

                                    
                                this.state.categoryData.map((row,key) => (
                                    <>
                                            
                                            <div className="col-6 p-2" key={key}>
                                                <a href={'/products_incategory/'+this.state.store_id+'/'+row.cateid}>
                                                    <div className="cat-card rounded shadow">
                                                        <img className="img-category" src={row.product_img} alt=""/>
                                                        <h5 className="p-1 m-0 fw-600">{row.catename}</h5>
                                                    </div>
                                                </a>
                                            </div>
                                    </>
                                                
                                                
                                ))
                                }
                                
                                            
                                            </div>
                                        </div>
                        )}



      


        
                        <br/>
                        
                        <br/>
                        <br/>


                    
                    </div>
                    
                    {/* end shophome */}

                    <Navigation active={'shop'}/>
            </div>
                 
        
        )
    }
}
export default Product_category;
