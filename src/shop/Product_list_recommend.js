import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header_back'
import Footer from './Footer'
import Swal from 'sweetalert2'
import {Navigation_shop as Navigation} from '../template'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();



class Product_list_recommend extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        localStorage.setItem('storageStoreID',this.props.match.params.store_id);
        this.handleFilterProduct = this.handleFilterProduct.bind(this);
    }

    
    
    state = {
        data : [],
        back_url:"/shop/"+localStorage.getItem('storageStoreID'),
        store_id:this.props.match.params.store_id,
        searchProduct: "",
        colorData: [],
    };
    


    async componentDidMount(){
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
        if(this.state.colorData !== undefined){
            let coloricon   = this.state.colorData['coloricon'];
            if(coloricon){
                $('.chat-bg' ).css( "fill", "#"+coloricon );
            }
        }
        
        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?recommend=1')
        if(result.status===200){
            this.setState({data: result.data});
        }

     


        
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
        
    handleToCart = (proid,event) => {
        event.preventDefault();
        if(proid!==null){
                    let user_id = sessionStorage.getItem('user_id');
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('proid',proid)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/order/api/addToCart',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            if(response.status==201){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    customClass: 'swal-mobile',
                                    title: 'เพิ่มไปยังรถเข็นแล้ว',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    
                                });
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                })
                            }
                            console.log(response)
                        })
                        .catch(function (response) {
                            //console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                },
                            })
                            
                           
                    });

            

        }
    }
    
    handleSubmit = event => {
    
        
    }

    handleSearchInput = event => {
        this.setState({searchProduct: event.target.value});
    }

    async handleFilterProduct(e){
        let keyWord = this.state.searchProduct;
        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?recommend=1&keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1', '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm';
        }
    }
    
    render() {
        const addBtnBackground = {
            backgroundColor: '#' + this.state.colorData['colortabbar']
        }
        const addColor = {
            stroke: '#' + this.state.colorData['coloricon']
        }
        return (
          
            <div className="container-mobile m-auto shop-page">
                <Header data={this.state}/>
                    <div className="shop-home">
                        
                        <div className="row p-2 mt-1 m-0 w-100 h-100 ">
                            <div className="col-9">
                                    <div className="position-relative ">
                                        <input type="text" className="search w-100 rounded form-control shadow" placeholder="Search product" onKeyUp={this.handleSearchInput}/>
                                        <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" onClick={this.handleFilterProduct}/>
                                    </div>

                            </div>
                            <div className="col-3 row head2-icon p-0">
                                <div className="icon-new ">
                                    <a onClick={this.handleClickScanQRCode}>
                                        <img className="" src="/assets/images/icon-scan-shadow.png" alt="..." />
                                    </a>
                                </div>
                                <div className="icon-new " style={{left:'0.35rem'}}>
                                    <a href="/notifications">
                                        <img className="" src="/assets/images/icon-notification-shadow.png" alt="..." />
                                    </a>
                                </div>
                                <div className="icon-new" style={{left:'0.7rem'}}>
                                    <a href={'/cart/'+this.state.store_id}>
                                        <img class="" src="/assets/images/icon-basket-shadow.png" alt="..." />
                                    </a>
                                </div>
                            </div>
                        </div>


        {this.state.data === undefined ? (
            <></>
        ):(
                        <div className="product p-3">
                            <div className="d-flex bd-highlight">
                                <div className="px-2 py-0  bd-highlight">
                                    <h4 className="m-0">สินค้าแนะนำสำหรับคุณ</h4>
                                </div>
                                <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                   
                                </div>
                            </div>
                            <div className="row m-0 mt-2 w-100">
                {

                    
                    this.state.data.map((row,key) => (
                                
                                <div className="col-6 pl-0 mb-3" key={key}>
                                    <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                            {
                                    row.point=== undefined || row.point=== '' || row.point=== null ? (
                                            <></>
                                        ):(
                                            <div className="flag">
                                                <img src="/assets/images/icon-flag.svg" alt=""/>
                                                <div className="flag-detail text-center p-1">
                                                    <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                    <p>POINTS</p>
                                                </div>
                                            </div>
                                    )
                            }
                                        
                                        <div className="text-center">
                                            <img className="img-thumb-shop" src={row.product_img===''? '' : row.product_img} alt=""/>
                                        </div>
                                        <div className="product-item-detail px-3" style={{width:'100%', height: '100%'}}>
                                            <h4 className="m-0 product-name">{row.productname}</h4>
                                            <p className="m-0 product-cat-name">{row.catname}</p>
                                        </div>
                                        <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                            <div className="mt-auto">
                                                {
                                                    row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null ? (
                                                        <small className="d-block"></small>
                                                    ):(
                                                        <small className="d-block product-discount-price"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                    )
                                                }
                                                
                                                <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                            </div>
                                            <div>
                                                
                                                {/* <div className="product-add rounded-circle text-center" onClick={(e) => this.handleToCart(row.productid, e)}>
                                                    <img className="w-50" src="/assets/images/icon-add.svg" alt="" />
                                                </div> */}
                                                <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                    <div className="product-add rounded-circle text-center" style={addBtnBackground}>
                                                        <svg className="w-50" xmlns="http://www.w3.org/2000/svg" width="49.299" height="50.683" viewBox="0 0 49.299 50.683">
                                                            <g transform="translate(-398.932 -2712.768)">
                                                                <line style={addColor} y2="45.683" transform="translate(423.582 2715.268)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                <line style={addColor} x2="44.299" transform="translate(401.432 2737.417)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </a>
                                            

                                            </div>
                                        </div>
                                    </div>
                                </div>
                    ))
                }
                                
                            </div>
                        </div>
        )}


        
                        <br/>
                        
                        <br/>
                        <br/>


                    
                    </div>
                    {/* <div className="add">
                        <a href={'line://app/'+config.liff.chat.id+'?u='+this.state.userid+'&s='+this.state.store_id}  className="btn btn-chat btn-add rounded-circle text-center shadow">
                            <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="94.365" height="72.944" viewBox="0 0 94.365 72.944">
                                <g id="Group_2451" data-name="Group 2451" transform="translate(1911.24 -485.317)">
                                    <g id="Group_2448" data-name="Group 2448" transform="translate(-1907.594 485.317)">
                                    <g id="Group_2447" data-name="Group 2447" transform="translate(0)">
                                        <path className="img-bg-color chat-bg" id="Path_5870" data-name="Path 5870" d="M-1862.682,520.545h-31.924a16.17,16.17,0,0,1-8.672,5.608,29.044,29.044,0,0,0,2.689-5.608h-2.4a3.41,3.41,0,0,1-3.322-3.472V490.789a3.41,3.41,0,0,1,3.322-3.472h40.306a3.41,3.41,0,0,1,3.322,3.472v26.284A3.41,3.41,0,0,1-1862.682,520.545Z" transform="translate(1906.31 -487.317)" fill="#da1111"/>
                                    </g>
                                    </g>
                                    <g id="Group_2450" data-name="Group 2450" transform="translate(-1911.24 489.428)">
                                    <g id="Group_2449" data-name="Group 2449" transform="translate(0 0)">
                                        <path className="img-bg-color" id="Path_5871" data-name="Path 5871" d="M-1899.536,524.478h31.924a3.41,3.41,0,0,0,3.322-3.472V494.722a3.41,3.41,0,0,0-3.322-3.472h-40.306a3.41,3.41,0,0,0-3.322,3.472v26.284a3.41,3.41,0,0,0,3.322,3.472h2.4" transform="translate(1911.24 -491.25)" fill="none"/>
                                        <path className="img-bg-color" id="Path_5872" data-name="Path 5872" d="M-1902.9,553.17a29.05,29.05,0,0,1-2.688,5.608,16.169,16.169,0,0,0,8.672-5.608" transform="translate(1908.622 -519.942)" fill="none"/>
                                    </g>
                                    </g>
                                    <circle className="img-bg-color" id="Ellipse_408" data-name="Ellipse 408" cx="3" cy="3" r="3" transform="translate(-1900.24 500.317)"/>
                                    <path className="img-bg-color" id="Path_6938" data-name="Path 6938" d="M3,0A3,3,0,1,1,0,3,3,3,0,0,1,3,0Z" transform="translate(-1901.24 502.317)" fill="none"/>
                                    <g id="Group_2452" data-name="Group 2452" transform="translate(1 -2)">
                                    <path className="img-bg-colo chat-bg" id="Path_5873" data-name="Path 5873" d="M-1820.534,555.7h-50.9a5.307,5.307,0,0,1-5.291-5.292V510.361a5.307,5.307,0,0,1,5.291-5.291h64.259a5.306,5.306,0,0,1,5.291,5.291v40.045a5.307,5.307,0,0,1-5.291,5.292H-1811" transform="translate(-15.996 -8.226)" fill="#da1111"/>
                                    <path className="img-bg-color chat-bg" id="Path_5874" data-name="Path 5874" d="M-1762.482,599.415a43.218,43.218,0,0,0,4.292,8.543,25.9,25.9,0,0,1-13.827-8.543" transform="translate(-64.513 -51.944)" fill="#da1111"/>
                                    </g>
                                    <path className="img-bg-color" id="Path_5875" data-name="Path 5875" d="M-1824.167,559.884h-50.9a5.307,5.307,0,0,1-5.291-5.291V514.548a5.307,5.307,0,0,1,5.291-5.292h64.258a5.307,5.307,0,0,1,5.292,5.292v40.045a5.307,5.307,0,0,1-5.292,5.291h-3.828" transform="translate(-14.312 -10.166)" fill="none"/>
                                    <path className="img-bg-color" id="Path_5876" data-name="Path 5876" d="M-1766.115,603.6a43.228,43.228,0,0,0,4.292,8.543,25.892,25.892,0,0,1-13.826-8.543" transform="translate(-62.829 -53.883)" fill="none"/>
                                    <circle className="img-bg-color" id="Ellipse_402" data-name="Ellipse 402" cx="5.5" cy="5.5" r="5.5" transform="translate(-1878.24 515.317)"/>
                                    <circle className="img-bg-color" id="Ellipse_403" data-name="Ellipse 403" cx="5.5" cy="5.5" r="5.5" transform="translate(-1860.24 515.317)"/>
                                    <circle className="img-bg-color" id="Ellipse_404" data-name="Ellipse 404" cx="5.5" cy="5.5" r="5.5" transform="translate(-1842.24 515.317)"/>
                                    <path className="img-bg-color" id="Path_6935" data-name="Path 6935" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1880.24 518.317)" fill="none"/>
                                    <path className="img-bg-color" id="Path_6936" data-name="Path 6936" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1862.24 518.317)" fill="none"/>
                                    <path className="img-bg-color" id="Path_6937" data-name="Path 6937" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1844.24 518.317)" fill="none"/>
                                </g>
                            </svg>
                            <svg className="img-border" xmlns="http://www.w3.org/2000/svg" width="96.365" height="74.943" viewBox="0 0 96.365 74.943">
                                <g id="Group_2407" data-name="Group 2407" transform="translate(-124.5 -2252.606)">
                                    <path className="img-border-color-stroke" id="Path_102" data-name="Path 102" d="M2920.721,2277.168" transform="translate(-2741.565 35.588)" fill="none" stroke="#000" stroke-width="5"/>
                                    <g id="Group_2451" data-name="Group 2451" transform="translate(2037.24 1767.789)">
                                    <g id="Group_2448" data-name="Group 2448" transform="translate(-1907.594 485.317)">
                                        <g id="Group_2447" data-name="Group 2447" transform="translate(0)">
                                        <path className="img-border-color-stroke" id="Path_5870" data-name="Path 5870" d="M-1862.682,520.545h-31.924a16.17,16.17,0,0,1-8.672,5.608,29.044,29.044,0,0,0,2.689-5.608h-2.4a3.41,3.41,0,0,1-3.322-3.472V490.789a3.41,3.41,0,0,1,3.322-3.472h40.306a3.41,3.41,0,0,1,3.322,3.472v26.284A3.41,3.41,0,0,1-1862.682,520.545Z" transform="translate(1906.31 -487.317)" fill="none" stroke="#000" stroke-width="1"/>
                                        </g>
                                    </g>
                                    <g id="Group_2450" data-name="Group 2450" transform="translate(-1911.24 489.428)">
                                        <g id="Group_2449" data-name="Group 2449" transform="translate(0 0)">
                                        <path className="img-border-color-stroke" id="Path_5871" data-name="Path 5871" d="M-1899.536,524.478h31.924a3.41,3.41,0,0,0,3.322-3.472V494.722a3.41,3.41,0,0,0-3.322-3.472h-40.306a3.41,3.41,0,0,0-3.322,3.472v26.284a3.41,3.41,0,0,0,3.322,3.472h2.4" transform="translate(1911.24 -491.25)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="3"/>
                                        <path className="img-border-color-stroke" id="Path_5872" data-name="Path 5872" d="M-1902.9,553.17a29.05,29.05,0,0,1-2.688,5.608,16.169,16.169,0,0,0,8.672-5.608" transform="translate(1908.622 -519.942)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        </g>
                                    </g>
                                    <circle className="img-border-color-stroke" id="Ellipse_409" data-name="Ellipse 409" cx="3" cy="3" r="3" transform="translate(-1901.24 502.317)" fill="#fff"/>
                                    <g id="Group_2452" data-name="Group 2452" transform="translate(1 -2)">
                                        <path className="img-border-color-stroke" id="Path_5873" data-name="Path 5873" d="M-1820.534,555.7h-50.9a5.307,5.307,0,0,1-5.291-5.292V510.361a5.307,5.307,0,0,1,5.291-5.291h64.259a5.306,5.306,0,0,1,5.291,5.291v40.045a5.307,5.307,0,0,1-5.291,5.292H-1811" transform="translate(-15.996 -8.226)" fill="none" stroke="#000" stroke-width="1"/>
                                        <path className="img-border-color-stroke" id="Path_5874" data-name="Path 5874" d="M-1762.482,599.415a43.218,43.218,0,0,0,4.292,8.543,25.9,25.9,0,0,1-13.827-8.543" transform="translate(-64.513 -51.944)" fill="none" stroke="#000" stroke-width="1"/>
                                    </g>
                                    <path className="img-border-color-stroke" id="Path_5875" data-name="Path 5875" d="M-1824.167,559.884h-50.9a5.307,5.307,0,0,1-5.291-5.291V514.548a5.307,5.307,0,0,1,5.291-5.292h64.258a5.307,5.307,0,0,1,5.292,5.292v40.045a5.307,5.307,0,0,1-5.292,5.291h-3.828" transform="translate(-14.312 -10.166)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="3"/>
                                    <path className="img-border-color-stroke" id="Path_5876" data-name="Path 5876" d="M-1766.115,603.6a43.228,43.228,0,0,0,4.292,8.543,25.892,25.892,0,0,1-13.826-8.543" transform="translate(-62.829 -53.883)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    <circle className="img-border-color-stroke" id="Ellipse_405" data-name="Ellipse 405" cx="5.5" cy="5.5" r="5.5" transform="translate(-1880.24 518.317)" fill="#fff"/>
                                    <path className="img-border-color-stroke" id="Path_6935" data-name="Path 6935" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1862.24 518.317)" fill="#fff"/>
                                    <path className="img-border-color-stroke" id="Path_6936" data-name="Path 6936" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1844.24 518.317)" fill="#fff"/>
                                    </g>
                                </g>
                            </svg>
                            CHAT
                        </a>
                    
                    </div> */}
                    {/* end shophome */}

                    <Navigation active={'shop'}/>
            </div>
                 
        
        )
    }
}
export default Product_list_recommend;
