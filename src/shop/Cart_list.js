import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import {getParam} from '../lib/Helper';
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');
let address_active = localStorage.getItem('address_active');
let rAddres = '';
let deleveryid = localStorage.getItem('deleveryid');
const get_producttype_text = {
    1 : 'รับสินค้าที่หน้าร้าน',
    2 : 'จัดส่งสินค้า'
}
const payType_text = {
    1 : 'ชำระเงินผ่านบัญชีธนาคาร',
    2 : 'ชำระเงินปลายทาง',
    3 : 'ชำระเงินสด'
}

class Cart_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
    
 
    }


    state = {
        data : [],
        store_id:this.props.match.params.store_id,
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        get_producttype: localStorage.getItem('getProductType') === null ? '1' : localStorage.getItem('getProductType'),
        userAddressData : [],
        address_active : address_active,
        deleveryid: deleveryid,
        deleveryData:[],
        payType: localStorage.getItem('payType') === null ? '1' : localStorage.getItem('payType'),
        couponid: getParam('couponid'),
        couponOnUseData: []
    };

    


    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/order/api/cart/'+localStorage.getItem('storageStoreID')+'/'+this.state.user_id);
      
        if(result.status===200){
            this.setState({data: result.data});
        }
        //console.log(this.state.data)
        if(this.state.address_active !== null){
            rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.user_id+'/'+this.state.address_active)
            //console.log(rAddres)
        }else{
            rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.user_id+'?active=1')
            //console.log(2)
        }
        
        if(rAddres.status===200){
            this.setState({userAddressData: rAddres.data});
           
        }

        if(this.state.couponid !== undefined){
            const couponRs =  await axios.get(config.api.base_url+'/store_coupon/api/couponuser/'+sessionStorage.getItem('user_id')+'/'+this.state.couponid);
            if(couponRs.status===200){
                this.setState({couponOnUseData: couponRs.data});
               
            }
        }
       

       if(this.state.deleveryid !==null && this.state.store_id !== null){
            const deleveryResult = await axios.get(config.api.base_url+'/store_delivery/api/delivery/'+localStorage.getItem('storageStoreID')+'/'+this.state.deleveryid);
            console.log(deleveryResult)
            if(deleveryResult.status===200){
                this.setState({deleveryData: deleveryResult.data});

                
            }
        }

        if(this.state.couponOnUseData[0] === undefined){
            let totalPrice       = parseFloat(this.state.data['totalPrice']);
            let totalPriceResult = 0;
            if(this.state.deleveryData[0] !== undefined){
                totalPriceResult = totalPrice + parseFloat(this.state.deleveryData[0]['price'])
            }else{
                totalPriceResult = totalPrice
            }
            
            $('.totalPriceShow').html(totalPriceResult.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
        }else{
            let discount         = this.state.couponOnUseData[0]['coupondiscount']
            let discountStatus   = this.state.couponOnUseData[0]['discountstatus']
            let totalPrice       = parseFloat(this.state.data['totalPrice']);
            let priceAfterDiscount = '';
            let discountPrice    = 0;
            if(discountStatus==='0'){//บาท
                discountPrice      = parseFloat(discount);
                priceAfterDiscount = parseFloat(this.state.data['totalPrice'])-parseFloat(discount);
            }else{//%
                discountPrice = (parseFloat(this.state.data['totalPrice'])*parseFloat(discount))/100;
                priceAfterDiscount  = totalPrice-discountPrice;
                if(priceAfterDiscount<1){
                    priceAfterDiscount = 0;
                }
            }

            let totalPriceResult = 0;
            if(this.state.deleveryData[0] !== undefined){
                totalPriceResult = priceAfterDiscount + parseFloat(this.state.deleveryData[0]['price'])
            }else{
                totalPriceResult = priceAfterDiscount
            }
            $('.totalPriceShow').html(totalPriceResult.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            $('.discountPriceShow').html(discountPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
        }
        
        
        


        
     
        
  
      
       

       

        
       


        
        
        

        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

  

   
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
        
    
    
    handleSubmit = event => {
        let user_id   = sessionStorage.getItem('user_id');
        let gettypeid = this.state.get_producttype;
        let addressid = this.state.address_active;
        let storeid   = localStorage.getItem('storageStoreID');
        let paymenttypeid = this.state.payType;
        let couponid = this.state.couponid === undefined ? 0 : this.state.couponid;
        let delivery_id = this.state.deleveryid === undefined ? 0 : this.state.deleveryid;
        if(user_id!==null){
            if(gettypeid==2 && addressid==null){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณาเลือกที่อยู่จัดส่งก่อนกดยินยัน',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-black',
                    },
                })
            }else{

            
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('gettypeid',gettypeid)
                    bodyFormData.set('addressid',addressid)
                    bodyFormData.set('storeid',storeid)
                    bodyFormData.set('paymenttypeid',paymenttypeid)
                    bodyFormData.set('couponid',couponid)
                    bodyFormData.set('delivery_id',delivery_id)
                   
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/order/api/ordering',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            if(response.status==201){
                                //window.location('/cart');
                                Swal.fire({
                                    title : 'ขอบคุณค่ะ',
                                    html: 'ระบบได้รับออเดอร์ของคุณแล้ว',
                                    icon:'success',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                }).then((result) => {
                                    window.location='/shop/'+storeid;
                                });
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                })
                            }
                            console.log(response)
                        })
                        .catch(function (response) {
                            //console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                },
                            })
                            
                           
                    });
            }
        }
    
        /* Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        }) */
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-12 h-100">
                            <div className="icon-back">
                                <a href={"/cart/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">ทำการสั่งซื้อสินค้าของคุณ</h4> 
                               
                            </div>
                        </div>
                        {/* <div className="col-3 h-100">
                            <div className="row float-right h-100 w-100">
                               
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div>
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            
                        </div> */}
                    </div>
                </div>

    
           
           <div className="cart-detail mb-5">
               <div className="row mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">

                       {
            this.state.data.cartDetail === undefined ? (
                <div/>
            ) : (
                                    
                                    <ul className="cart-product">
                    {
                   
                        this.state.data.cartDetail.map((row,key) => (
                           
                                <li key={key} className="" data-detail={row.detailid} >
                                <div className="row pl-2 py-2 w-100">
                                    <div className="col-5 pr-3">
                                        <img className="img-fluid img-product" src={row.product_img}/>
                                    </div>
                                    <div className="col-7 pl-0">
                                        <div class="product-item-detail px-2 py-2">
                                      
                                            <h4 class="m-0 cart-item-title ">{row.productname}</h4>
                                            {
                                                row.variantname=== null ? (
                                                    <></>
                                                ) : (
                                                    
                                                        <h5 className="mt-1">ตัวเลือก
                                                            <div>{row.variantname}</div>
                                                        </h5>
                                                    
                                                )
                                            }
                                                <h5 className="m-0">
                                                <NumberFormat value={row.unitprice} displayType={'text'} thousandSeparator={true}/> THB x {row.quantity}
                                                <input type="hidden" className="unitPrice" value={parseFloat(row.unitprice)}/>
                                                </h5>
                                            
                                        </div>
                                    </div>
                                </div>
                                    
                                </li>
                            
       
                        )
                        )
                    }
                                    <a href="/getProductType" className="w-100">
                                        <li className="product-item-detail p-2">
                                            <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 pl-2">การจัดส่ง</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    
                                                        <h4 className="m-0 text-right text-gray">{get_producttype_text[this.state.get_producttype]} <i class="fas fa-chevron-right"></i></h4>
                                                    
                                                    
                                                </div>

                                            </div>  
                                        </li>
                                    </a>
                                {
                                    this.state.get_producttype==2 ? (
                                        <>
                                        <a href="/OrderShipingtype" className="w-100">
                                            <li className="product-item-detail p-2">
                                                <div className="row w-100">
                                                    <div className="col-4">
                                                        <h4 className="m-0 pl-2">จัดส่งแบบ</h4>
                                                    </div>
                                                    <div className="col-8 pr-0">
                                                    {
                                                        this.state.deleveryData === undefined ? (
                                                            <h4 className="m-0 text-gray">กรุณาเลือก <i class="fas fa-chevron-right"></i></h4>
                                                        ) : (        
                                                            
                                                            this.state.deleveryData.map((row,key) => (
                                                            <>
                                                           <h4 className="m-0 text-right text-gray">{row.deliverryname+' '+row.price+' THB'} <i class="fas fa-chevron-right"></i></h4>
                                                            </>
                                                            )
                                                            )
                                                        )
                                                    }
                                                          {/* <h4 className="m-0 text-right text-gray"><i class="fas fa-chevron-right"></i></h4>   */}
                                                        
                                                        
                                                    </div>
                                                    {/* <div className="col-2">
                                                      
                                                            <h4 className="m-0 text-right text-gray"><i class="fas fa-chevron-right"></i></h4>
                                                        
                                                        
                                                    </div> */}

                                                </div>  
                                            </li>
                                        </a>
                                        <a href="/order_address" className="w-100">
                                        <li className="product-item-detail p-2">
                                            <div className="row w-100">
                                                <div className="col-12">
                                                    <h4 className="m-0 pl-2">ที่อยู่การจัดส่ง</h4>
                                                </div>
                                            </div>
                                            <div className="row w-100">
                                                <div className="col-10">
                                                    <h4 className="m-0 pl-2 text-gray">
                                                    {
                                                        this.state.userAddressData === undefined ? (
                                                            <div/>
                                                        ) : (        
                                                            
                                                            this.state.userAddressData.map((row,key) => (
                                                            <>
                                                            {row.name}
                                                            <br/>
                                                            {row.tel+' '}
                                                            {row.detail+', '}
                                                            {row.subdistrictname+', '}
                                                            {row.districtname+', '}
                                                            {row.provincename+' '}
                                                            {row.zipcode}
                                                            </>
                                                            )
                                                            )
                                                        )
                                                    }
                                                    

                                                    </h4>
                                                </div>
                                                <div className="col-2 pr-0">
                                                    <h4 className="m-0  text-gray text-right"><i class="fas fa-chevron-right"></i></h4>
                                                </div>

                                            </div>  
                                        </li>
                                    </a>
                                    </>
                                    ) : (
                                        <div/>
                                    )
                                    
                                }
                                        <a href="/OrderCoupon">
                                        <li className="product-item-detail p-2">
                                            <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 pl-2">โค้ดส่วนลด</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    <h4 className="m-0 text-right text-gray">
                                        {this.state.couponid !== undefined ?  (
                                                        this.state.couponOnUseData=== undefined ? 
                                                        'เลือกโค้ดส่วนลด ' : ('ส่วนลด '+
                                                            this.state.couponOnUseData.map((row,key) => (
                                                                
                                                                    (row.discountstatus==='0' ? (row.coupondiscount+' บาท ') :(row.coupondiscount+'% '))
                                                                   
                                                        
                                                               
                                                                
                                                                
                                                            ))
                                                        )
                                                    
                                                    ) : 'เลือกโค้ดส่วนลด '
                                        } 
                                                    <i class="fas fa-chevron-right"></i></h4>
                                                </div>

                                            </div>  
                                        </li>
                                        </a>
                                       
                                        <li className="product-item-detail p-2">
                                            <a href="/OrderPayType" className="w-100">
                                            <div className="row pb-3 w-100">
                                                <div className="col-4">
                                                    <h4 className="m-0 pl-2">ชำระเงิน</h4>
                                                </div>
                                                <div className="col-8 pr-0">
                                                    <h4 className="m-0 text-right text-gray">{payType_text[this.state.payType]} <i class="fas fa-chevron-right"></i></h4>
                                                </div>

                                            </div>  
                                            </a>
                                            <hr className="mt-0"/>
                                            <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 text-gray font-weight-light pl-2">รวมคำสั่งซื้อ</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    <h4 className="m-0 text-right font-weight-light text-gray">
                                                    <NumberFormat value={parseFloat(this.state.data['totalPrice']).toFixed(2)} displayType={'text'} thousandSeparator={true} /> THB
                                                    </h4>
                                                </div>

                                            </div>  
                                {
                                    this.state.couponOnUseData[0] === undefined ? (
                                        <></>
                                    ):(
                                        <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 text-gray font-weight-light">ส่วนลด</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    <h4 className="m-0 text-right font-weight-light text-gray"><span className="discountPriceShow"></span> THB</h4>
                                                </div>

                                        </div>  
                                    )
                                }
                                            
                                            <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 text-gray font-weight-light pl-2">การจัดส่ง</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    {
                                                        this.state.deleveryData === undefined ? (
                                                            <h4 className="m-0 text-gray">กรุณาเลือกรูปแบบการจัดส่ง </h4>
                                                        ) : (        
                                                            
                                                            this.state.deleveryData.map((row,key) => (
                                                            <>
                                                           <h4 className="m-0 text-right text-gray">{row.price+' THB'} </h4>
                                                            </>
                                                            )
                                                            )
                                                        )
                                                    }
                                                    <h4 className="m-0 text-right font-weight-light text-gray"></h4>
                                                </div>

                                            </div>  
                                            
                                            <div className="row w-100">
                                                <div className="col-6">
                                                    <h4 className="m-0 text-gray pl-2">ยอดรวมทั้งหมด</h4>
                                                </div>
                                                <div className="col-6 pr-0">
                                                    <h4 className="m-0 text-right ">
                                                       <span className="totalPriceShow"></span> THB
                                                    </h4>
                                                </div>

                                            </div>   
                                            
                                        </li>
                                        
                                       
                                      
                                    </ul>
                                    
            )
        }
              
                       </div>

                   </div>
                   
               </div>
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                   
                  
                <div className="footer footer-product-detail">
                    <div className="row m-0">
                        <div className="col-6 mt-2">
                            <div className="detail">รวมทั้งหมด :</div>
                            <div className="detail display-price"><span className="totalPriceShow"></span> THB</div>
                            <input type="hidden" value={parseFloat(this.state.data['totalPrice'])} id="totalPrice"/>
                        </div>
                        <div className="col-6 p-3">
                            <button className="btn btn-light btn-col p-0" onClick={this.handleSubmit}><h4 class="mt-2">ยืนยันการสั่งซื้อ</h4></button>
                        </div>
                    </div>
                </div>
                    
            </div>
                 
        
        )
    }
}
export default Cart_list;
