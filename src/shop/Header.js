import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../config';

class Header extends Component {

    constructor(props){//เริ่มต้น run component
        super(props);
    }
    
    state = {
        data : []
    }

    async componentDidMount(){
        const rsStore = await axios.get(config.api.base_url+'/store/api/store/'+localStorage.getItem('storageStoreID'))
        if(rsStore.status===200){
            this.setState({data: rsStore.data});
        }
        
        
        // const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?limit=2')
        // if(result.status===200){
        //     this.setState({data: result.data});
        // }

        // const resultRecommend = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?limit=6&recommend=1')
        // if(resultRecommend.status===200){
        //     this.setState({recommendData: resultRecommend.data});
        // }
    }

    render() {
        return (
        <>
        {/* <link rel="stylesheet" type="text/css" href="/assets/css/new-home.css?v=3"/> */}
        <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
        
        
        <div className="bg-black shadow">
            <div className="w-100 h-100">
                { this.state.data.length === 0 ? (
                    <></> 
                ) : (
                    this.state.data.map((row, key) =>
                        <div className="w-100" key={key}>
                            <div className="p-2 w-100 text-center">
                                <h1 className="m-0 mt-2 text-white">{row.storename}</h1>
                            </div>
                            {row.store_banner ==="" ? (
                                <></>
                            ) : (
                                <div>
                                    <img src={row.store_banner} width="100%" style={{maxHeight: '250px'}}/>
                                </div>
                            )}
                        </div>
                    )
                )}
            </div>
            {/* <div className="row w-100 h-100">
                <div className="col-8 h-100">
                    <div className=" h-100 p-3 pl-5">
                        <h1 className="m-0 text-white">SHOP</h1> 
                        <p className="text-red">เลือกสินค้าของคุณได้เลย</p>
                    </div>
                </div>
                <div className="col-4 h-100">
                    <div className="row float-right h-100 w-100">
                        <div className="col p-1 my-auto">
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div> */}
        </div>
        </>)
    
        

    }
}
export default Header;
