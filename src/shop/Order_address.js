import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import {getParam, readURL} from '../lib/Helper';
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');
let address_active = localStorage.getItem('address_active');


class Order_address extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
        this.goBack = this.goBack.bind(this);
 
    }


    state = {
        data : [],
        user_id : user_id,
        userAddressData : [],
        address_active : address_active
    };


    async componentDidMount(){
        
        const rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.user_id)
        if(rAddres.status===200){
            this.setState({userAddressData: rAddres.data});
        }
        

       

     
        
    }

  

   
    handleChange = event => {
        
        //console.log(event.target.name)
        localStorage.setItem('address_active',event.target.value);
        this.setState({[event.target.name]:event.target.value});
        //console.log(localStorage.getItem('address_active'))
        if(getParam('orderid') !== undefined){
            window.location='/order_editaddress/'+event.target.value+'?orderid='+getParam('orderid');
        }else if(getParam('point_id') !== undefined){
            window.location='/order_editaddress/'+event.target.value+'?point_id='+getParam('point_id');
        }else{
            window.location='/order_editaddress/'+event.target.value;
        }
        
        
        
    }
    
    handleNew = event => {
        if(getParam('point_id') !== undefined){
            window.location='/order_addaddress?point_id='+getParam('point_id');
        } else {
        window.location='/order_addaddress';
        }
        /* Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        }) */
    }

    goBack(){
        this.props.history.goBack();
    }


    handleBackPage = event => {
        if(getParam('orderid') !== undefined){
            window.location='/order_detail/'+getParam('orderid')
        }else if(getParam('point_id') !== undefined){
            let bodyFormData = new FormData();
            bodyFormData.set('point_id',getParam('point_id'))
            bodyFormData.set('addressID',localStorage.getItem('address_active'))
            axios({
                method: 'post',
                url: config.api.base_url+'/store_reward/api/updateuserreward',
                data: bodyFormData,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                window.location = '/online_reward_detail/'+getParam('point_id');
            })
            .catch(function (response) {
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'เกิดข้อผิดพลาด<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                    icon:'error',
                    confirmButtonText:'ตกลง'
                })
                
                //handle error
                //console.log(response);
            });
        }else{
            window.location='/cart_list';
        }
    }
        
    
    
    handleSubmit = event => {
    
        Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        })
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-9 h-100">
                            <div className="icon-back">
                                <a onClick={this.goBack}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">การจัดส่งสินค้าของคุณ</h4> 
                               
                            </div>
                        </div>
                        {/* <div className="col-3 h-100">
                            <div className="row float-right h-100 w-100">
                               
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div>
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            
                        </div> */}
                    </div>
                </div>

    
           
           <div className="cart-detail mb-5 ">
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">

                                    <ul className="cart-product">
                        {
                                this.state.userAddressData === undefined ? (
                                                                <div/>
                                ) : (        
                                                                
                                    this.state.userAddressData.map((row,key) => (
                                            <>
                                                <li className="product-item-detail p-2" key={key}>
                                                    <div className="row w-100">
                                                        <div className="col-1 pr-0">
                                                            <input type="radio" id={'radio'+row.addressid} name="address_active" value={row.addressid} onClick={this.handleChange} checked={this.state.address_active===row.addressid ? 'checked' : ''}/>
                                                        </div>
                                                        <div className="col-11 pl-2">
                                                            <h4 className="m-0"><label for={'radio'+row.addressid}>ที่อยู่การจัดส่ง</label></h4>
                                                        </div>
                                                    </div>
                                                    <div className="row w-100">
                                                        <div className="col-1">
                                                        </div>
                                                        <div className="col-11 pl-4">
                                                            <label for={'radio'+row.addressid}>
                                                                <h5 className="m-0  text-gray">
                                                                
                                                                    {row.name} {row.active === "1" ? <span style={{color: 'green'}}>ที่อยู่ตั้งต้น</span> : ""}
                                                                    <br/>
                                                                    {row.tel+' '}
                                                                    {row.detail+', '}
                                                                    {row.subdistrictname+', '}
                                                                    {row.districtname+', '}
                                                                    {row.provincename+' '}
                                                                    {row.zipcode}

                                                                </h5>
                                                            </label>
                                                        </div>
                                                        

                                                    </div>  
                                            </li>
                                                                
                                            </>
                                        )
                                    )
                                )
                        }
                                      
                                      
                                        <li className="product-item-detail p-2" onClick={this.handleNew}>
                                            <div className="row w-100">
                                                <div className="col-10">
                                                    <h4 className="m-0">เพิ่มที่อยู่ใหม่</h4>
                                                </div>
                                                <div className="col-2 pr-0">
                                                    <h4 className="m-0 text-right text-gray"><i class="fas fa-plus"></i></h4>
                                                </div>

                                            </div>  
                                        </li>
                                       
                                       
                                      
                                    </ul>
                                    
        
              
                       </div>

                   </div>
                   
               </div>
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                   
                  
                <div className="footer footer-product-detail">
                        <a onClick={this.handleBackPage} className="w-100"><button className="btn btn-buy">ยืนยัน</button></a>
                          
                   
                </div>
                    
            </div>
                 
        
        )
    }
}
export default Order_address;
