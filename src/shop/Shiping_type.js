import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');


class Shiping_type extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    
 
    }


    state = {
        data : [],
        store_id:this.props.match.params.store_id,
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        deleveryid: localStorage.getItem('deleveryid') === null ? '2' : localStorage.getItem('deleveryid')
    };


    async componentDidMount(){
        
        const result = await axios.get(config.api.base_url+'/store_delivery/api/delivery/'+localStorage.getItem('storageStoreID'));
        if(result.status===200){
            this.setState({data: result.data});
        }
        
        console.log(this.state.data)

       
    

       

        
     
     
        
    }

  

   
    handleChange = (event,key) => {
        //console.log(event.target.name)
        localStorage.setItem('deleveryid',event.target.value);
        this.setState({'deleveryid':event.target.value});
        
        
        
    }
    
        
    
    
    handleSubmit = event => {
    
        Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        })
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-9 h-100">
                            <div className="icon-back">
                                <a href={"/cart_list/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">เลือกการจัดส่งของคุณ</h4> 
                               
                            </div>
                        </div>
                        <div className="col-3 h-100">
                            {/*<div className="row float-right h-100 w-100">
                               
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div>
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                    </div>
                                </div>
                        </div>*/}
                            
                        </div>
                    </div>
                </div>

    
           
           <div className="cart-detail mb-5 ">
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">
                {
                        this.state.data === undefined ? (
                             <div/>
                        ) : (
                                    
                                    <ul className="cart-product">
                                {
                                    this.state.data.map((row,key) => (
                                        <li className="product-item-detail p-2" key={key}>
                                            <div className="row w-100">
                                                <div className="col-7">
                                                    <h4 className="m-0 ml-2"><input type="radio" id={"customRadio"+key} name="deleveryid" value={row.deliveryid} onClick={(e) => this.handleChange(e,key)} checked={this.state.deleveryid===(row.deliveryid) ? 'checked' : ''}/> <label className="m-0 pl-2" for={"customRadio"+key}>{row.deliverryname}</label></h4>
                                                </div>
                                                <div className="col-5 pr-0">
                                                    <h4 className="m-0 text-right text-gray"><NumberFormat value={parseFloat(row.price)} displayType={'text'} thousandSeparator={true} /> THB</h4>
                                                </div>
                                             
                                                
                                               

                                            </div>  
                                        </li>
                                    )
                                    )
                                }
                                        
                                       
                                       
                                      
                                    </ul>
                        )
                }
                                    
        
              
                       </div>

                   </div>
                   
               </div>
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                   
                  
                <div className="footer footer-product-detail">
                        <a href="/cart_list" className="w-100"><button className="btn btn-buy">ยืนยัน</button></a>
                          
                   
                </div>
                    
            </div>
                 
        
        )
    }
}
export default Shiping_type;
