import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery'
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import DateCountdown from 'react-date-countdown-timer';
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Moment from 'moment';
import 'moment/min/moment-with-locales' 

import InputMask from 'react-input-mask';
import Modal from 'react-bootstrap4-modal';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import th from "date-fns/locale/th"; // the locale you want
import Select from 'react-select'

import Store_color from '../template/Store_color'

// var resizebase64 = require('resize-base64');



// let optionHour = [];
// for(let i=0;i<=23;i++){
//     let data = {value: ("0" + i).slice(-2), label: ("0" + i).slice(-2) }
//     optionHour.push(data)
// }

// let optionMinute = [];
// for(let i=0;i<=59;i++){
//     let data = { value: ("0" + i).slice(-2), label: ("0" + i).slice(-2) }
//     optionMinute.push(data)
// }

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');
const allowCancelStatus = [1,2];
const allowUpdatePaytypeStatus = [1];
let slip_default = <i class="fas fa-file-upload slip-default"></i>
const d = new Date();


class OnlineRewardDetail extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }


    state = {
        dataColor : [],
        store_id:localStorage.getItem('storageStoreID'),
        user_id : user_id,
        point_id : this.props.match.params.pointid,
        user_id : sessionStorage.getItem('user_id'),
        demodata : [],
        addressdata : []
    };


    async componentDidMount(){
        const demoresult = await axios.get(config.api.base_url+'/store_reward/api/onlinerewarddetail/'+this.state.point_id)
        if(demoresult.status===200){
            this.setState({demodata: demoresult.data[0]});
        }
        console.log(this.state.demodata)

        if (this.state.demodata.addressID !== "0") {
            const addressresult = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.user_id+'/'+this.state.demodata.addressID)
            if(addressresult.status===200){
                this.setState({addressdata: addressresult.data[0]});
            }
        }
        // const addressresult = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.user_id+'/'+this.state.demodata.addressID)
        // if(addressresult.status===200){
        //     this.setState({addressdata: addressresult.data[0]});
        // }
        console.log(this.state.addressdata)

        const colorResult = await axios.get(config.api.base_url+'/store/api/storeColor/'+localStorage.getItem('storageStoreID'))
        if(colorResult.status===200){
            this.setState({dataColor: colorResult.data});
        }
        if(this.state.dataColor !== undefined){
            let colortabbar = this.state.dataColor['colortabbar'];
            let coloricon   = this.state.dataColor['coloricon'];
            let colorfont   = this.state.dataColor['colorfont'];
            if(colortabbar != undefined && coloricon!= undefined && colorfont!= undefined){
                $(".img-bg-color" ).css( "fill", "#"+coloricon );
                $(".footer" ).css( "background-color", "#"+colortabbar );
                $('.img-border-color').css( "fill", "#"+colorfont );
                $('.img-border-color-stroke').css( "stroke", "#"+colorfont );
                $('.footer .menu-item span, .notifications h3, .nav-link').css("color", "#"+colorfont);
                $('.head.bg-black, .bg-black, .btn-buy, .header, .active').css("background-color", "#"+colortabbar);
                $(".swal2-styled.swal2-confirm, .btn.btn-primary.btn-shop, swal2-confirm.swal2-styled.btn-shop").css("background-color", "#"+colortabbar,"border-color", "#"+colortabbar );
                this.setState({
                    colortabbar:colortabbar
                })
            }else{
                $(".img-bg-color" ).css( "fill", "#DA1111" );
                $(".footer" ).css( "background-color", "#000000" );
                $('.img-border-color').css( "fill", "#FFFFFF" );
                $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
            }

        }else{
            
            $(".img-bg-color" ).css( "fill", "#DA1111" );
            $(".footer" ).css( "background-color", "#000000" );
            $('.img-border-color').css( "fill", "#FFFFFF" );
            $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    gotoNoti() {
        window.location = '/notifications?activetab=2';
    }
    
    backHome(e) {
        e.preventDefault();
        if (this.state.store_id !== "") {
            window.location = "/shop/"+this.state.store_id;
        } else {
            window.location = "/home";
        }
    }

    render() {
        return (
            <>
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        <div className="col-12 h-100">
                            <div className="icon-back" onClick={this.gotoNoti}>
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </div>
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">รายละเอียดคำสั่งซื้อ</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="cart-detail mb-5 ">
                    <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                        <div className="col-12 p-0">
                            <div className="">
                                <ul className="cart-product">
                                    <li className="product-item-detail order-list">
                                        { this.state.demodata.length === '0' ? <></> :
                                        <>
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-3 ">
                                                    <div class="w-100 text-center pb-2 pt-2">
                                                        <img class="img-fluid img-product" src={this.state.demodata['reward_img']}/>
                                                    </div>
                                                </div>
                                                <div className="col-9 mx-auto pt-2 p-0 pr-4">
                                                    <h3 className="mb-1 pr-4">รหัส Reward : {this.state.demodata['rewardcode']}</h3>
                                                    <h4 className="mb-1 text-gray" style={{fontWeight: '100'}}>{Moment(this.state.demodata['createdate']).format('D MMM YYYY HH:mm')}</h4>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-12">
                                                    <h3 className="mb-1 pr-4">รายละเอียดสินค้า</h3>
                                                </div>
                                                <div className="col-12 ">
                                                    <h3 className="mb-1 text-gray" style={{fontWeight: '100', lineBreak: 'anywhere'}}> {this.state.demodata['rewardname']}</h3>
                                                </div>
                                            </div>
                                            <div className="row pb-3">
                                                <div className="col-12">
                                                    <h3 className="mb-1 pr-4">คะแนนที่แลก</h3>
                                                </div>
                                                <div className="col-12">
                                                    <h3 className="mb-1 text-gray" style={{fontWeight: '100'}}>{this.state.demodata['rewardpoint']} คะแนน</h3>
                                                </div>
                                            </div>
                                        </div>
                                        </>
                                        }
                                    </li>
                                    <li className="product-item-detail order-list">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-1 ">
                                                    <div class="w-100 text-center   pb-2 pt-2">
                                                        <h3 className="mb-1 w-100"><i class="fa fa-map-marker-alt" aria-hidden="true"></i></h3>
                                                    </div>
                                                </div>
                                                <div className="col-11 ">
                                                    <div class="w-100    pb-2 pt-2">
                                                        {/* <h3 className="mb-1 w-100">ที่อยู่การจัดส่ง {inArray(this.state.data['statusid'],allowUpdatePaytypeStatus) ? (<a href={"/order_address?orderid="+this.state.data['orderid']} className="link text-right">แก้ไขที่อยู่</a>) : (<></>)}</h3> */}
                                                        <h3 className="mb-1 w-100">ที่อยู่การจัดส่ง {this.state.demodata['redeem_status'] === '0' ? (<a href={"/order_address?point_id="+this.state.point_id} className="link text-right h6">แก้ไขที่อยู่</a>) : <></> }</h3>
                                                        <div className="text-gray">
                                                        {
                                                            this.state.addressdata.length === 0 ? (
                                                                <></>
                                                            ):(
                                                                <>
                                                                <h3 class="text-gray" style={{fontWeight: '100'}}>
                                                                    {this.state.addressdata['name']} | <NumberFormat value={this.state.addressdata['tel']} displayType={'text'} format="###-###-####" />
                                                                    <br/>
                                                                    {this.state.addressdata['detail']+' ,'}
                                                                    <br/>
                                                                    {this.state.addressdata['subdistrictname']+' , '+this.state.addressdata['districtname']}
                                                                    <br/>
                                                                    {this.state.addressdata['provincename']+' '+this.state.addressdata['zipcode']}
                                                                </h3>
                                                                </>
                                                            )
                                                        }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div className="p-1">
                                    <button class="btn btn-primary btn-shop w-100 border rounded" onClick={(e) => this.backHome(e)}><h5 className="p-1 mb-0">กลับหน้าหลัก</h5></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/><br/><br/>
                </div>
            </div>
            </>
        )
    }
}
export default OnlineRewardDetail;
