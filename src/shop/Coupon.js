import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import DateCountdown from 'react-date-countdown-timer';
import {getParam} from '../lib/Helper';
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');


class Coupon extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    
 
    }


    state = {
        data : [],
        store_id:localStorage.getItem('storageStoreID'),
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        deleveryid: localStorage.getItem('deleveryid') === null ? '2' : localStorage.getItem('deleveryid'),
        totalCartPrice : 0,
        colorData : [],
       
    };


    async componentDidMount(){
        const resultCart = await axios.get(config.api.base_url+'/order/api/cart/'+localStorage.getItem('storageStoreID')+'/'+this.state.user_id);
      
        if(resultCart.status===200){
            this.setState({totalCartPrice: parseFloat(resultCart.data.totalPrice)});
            
        }
        const result = await axios.get(config.api.base_url+'/store_coupon/api/couponuser/'+sessionStorage.getItem('user_id'));
        if(result.status===200){
            this.setState({data: result.data});
            
        }
        
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }


    }

  

   
    handleChange = (event,key) => {
        //console.log(event.target.name)
        localStorage.setItem('deleveryid',event.target.value);
        this.setState({'deleveryid':event.target.value});
        
        
        
    }
    
        
    useCoupon(e,couponid,data){
        if(data !== undefined){
            let minpurchase = parseFloat(data.minpurchase);
            let storeid     = parseInt(data.storeid);
           
            if((this.state.totalCartPrice>=minpurchase) && (parseInt(this.state.store_id) === storeid || storeid===0)){
                window.location='/cart_list?couponid='+couponid;
            }else if((this.state.totalCartPrice<minpurchase)){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'ยอดสั่งซื้อไม่ถึงราคาขั้นต่ำ',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-black',
                    },
                })
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'คูปองที่คุณเลือกไม่เข้าเงื่อนไขการใช้งาน',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-black',
                    },
                })
            }
            
           
        }
      
    }
    
    handleSubmit = event => {
    
        Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        })
    }
    
    render() {
        const btnBackground = {
            backgroundColor: '#' + this.state.colorData['colortabbar'],
            borderColor: '#' + this.state.colorData['colortabbar'],
            color: '#' + this.state.colorData['coloricon'],
            borderRadius: '0px 5px 5px 0px'
        }
        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-12 h-100">
                            <div className="icon-back">
                                <a href={"/cart_list/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">โค้ดส่วนลดของคุณ</h4> 
                               
                            </div>
                        </div>
                        
                    </div>
                </div>

            
            <div className="search-bar">
               
                <div className="col-12">
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="กรุณาใส่โค้ดส่วนลดของคุณ"  aria-describedby="basic-addon2"/>
                        <div className="input-group-append">
                            <button className="btn px-4" style={btnBackground} type="button">เพิ่ม</button>
                        </div>
                    </div>
                </div>
                
            </div>
           
           <div className="cart-detail mb-5 ">
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="m-2">
                {
                        this.state.data === undefined ? (
                             <div/>
                        ) : (                                    
                            <ul className="cart-product">
                            {
                                this.state.data.map((row,key) => (
                                    
        
                                    
                                    <li className="product-item-detail" key={key}>
                                        <div className="row w-100">
                                            <div className="col-3 ">
                                                <div class="w-100 text-center  bl-dotted-2 br-dotted-2 p-2">
                                                    <div class="coupon-detail-img rounded-circle mx-auto  border">
                                                        <div className="">
                                                        <img className="" src={row.storelogo} alt=""/>
                                                        </div>
                                                    </div>
                                                        
                                                </div>
                                            </div>
                                            <div className="col-6 mx-auto pt-3 p-0">
                                                <h4 className="mb-1 coupon-use-name">ส่วนลด {row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')} เมื่อซื้อสินค้าขั้นต่ำ  <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h4>
                                                <h4 className="m-0 coupon-use-expiredate mb-2">หมดเขต : เหลือเวลา <DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></h4>
                                            </div>
                                            <div className="col-3 mx-auto text-right pl-0 pr-0">
                                                <button type="button" className={(this.state.totalCartPrice >= parseFloat(row.minpurchase)) && (parseInt(this.state.store_id) === parseInt(row.storeid) || row.storeid===0) ? 'btn btn-xs btn-secondary  btn-use-coupon btn-available mr-2' : 'btn btn-xs btn-use-coupon mr-2 text-white'} onClick={(e) => this.useCoupon(e,row.couponid,row)}>ใช้คูปอง</button>
                                            </div>
                                        </div>  
                                    </li>
                                )
                                )
                            }
                            </ul>
                        )
                }
                       </div>
                   </div>                   
               </div>
                   <br/>                   
                   <br/>
                   <br/>
           </div>
    


                
                    
            </div>
                 
        
        )
    }
}
export default Coupon;
