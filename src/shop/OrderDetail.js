import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery'
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import DateCountdown from 'react-date-countdown-timer';
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Moment from 'moment';
import 'moment/min/moment-with-locales' 

import InputMask from 'react-input-mask';
import Modal from 'react-bootstrap4-modal';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import th from "date-fns/locale/th"; // the locale you want
import Select from 'react-select'

import Store_color from '../template/Store_color'

var resizebase64 = require('resize-base64');



let optionHour = [];
for(let i=0;i<=23;i++){
    let data = {value: ("0" + i).slice(-2), label: ("0" + i).slice(-2) }
    optionHour.push(data)
}

let optionMinute = [];
for(let i=0;i<=59;i++){
    let data = { value: ("0" + i).slice(-2), label: ("0" + i).slice(-2) }
    optionMinute.push(data)
}

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');
const allowCancelStatus = [1,2];
const allowUpdatePaytypeStatus = [1];
let slip_default = <i class="fas fa-file-upload slip-default"></i>
const d = new Date();


class Coupon extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        this.handleChangeDatePicker = this.handleChangeDatePicker.bind(this);
        this.handleChangeFile       = this.handleChangeFile.bind(this);
        this.goBack = this.goBack.bind(this);
        
 
    }


    state = {
        order_id : this.props.match.params.orderid,
        data : [],
        dataColor : [],
        paymentData:[],
        store_id:localStorage.getItem('storageStoreID'),
        user_id : user_id,
        deleveryid: localStorage.getItem('deleveryid') === null ? '2' : localStorage.getItem('deleveryid'),
        slip_canvas : slip_default,
        slip_file : undefined,
        slip_base64 : undefined,
        isOpen: false,
        startDate: Moment().toDate(),
        state_money : 0,
        colortabbar:undefined
        
       
    };


    async componentDidMount(){
        console.log(d)
        console.log(Moment(d).locale('el'))

        this.checkUpdate();

        
        
   

        const result = await axios.get(config.api.base_url+'/order/api/orderdetail/'+sessionStorage.getItem('user_id')+'/'+this.state.order_id);
        if(result.status===200){
            this.setState({data: result.data});
            if(this.state.data['paymenttypeid']==1){
                const paymentRS = await axios.get(config.api.base_url+'/store_payment/api/payment/'+localStorage.getItem('storageStoreID'));
                if(paymentRS.status===200){
                    this.setState({paymentData: paymentRS.data});
                }else{
                    this.setState({paymentData: []});
                }
            }else{
                this.setState({paymentData: []});
            }
           

        }else{
            this.setState({data: []});
        }
        
        console.log(this.state.data)
        
        localStorage.setItem('storageStoreID',this.state.data.storeid);

        const colorResult = await axios.get(config.api.base_url+'/store/api/storeColor/'+localStorage.getItem('storageStoreID'))
        if(colorResult.status===200){
            this.setState({dataColor: colorResult.data});
        }
        if(this.state.dataColor !== undefined){
            let colortabbar = this.state.dataColor['colortabbar'];
            let coloricon   = this.state.dataColor['coloricon'];
            let colorfont   = this.state.dataColor['colorfont'];
            if(colortabbar && coloricon && colorfont){
                $(".img-bg-color" ).css( "fill", "#"+coloricon );
                $(".footer" ).css( "background-color", "#"+colortabbar );
                $('.img-border-color').css( "fill", "#"+colorfont );
                $('.img-border-color-stroke').css( "stroke", "#"+colorfont );
                $('.footer .menu-item span, .notifications h3, .nav-link').css("color", "#"+colorfont);
                $('.head.bg-black, .bg-black, .btn-buy, .header, .active').css("background-color", "#"+colortabbar);
                $(".swal2-styled.swal2-confirm, .btn.btn-primary.btn-shop, swal2-confirm.swal2-styled.btn-shop").css("background-color", "#"+colortabbar,"border-color", "#"+colortabbar );
                this.setState({
                    colortabbar:colortabbar
                })
            }else{
                $(".img-bg-color" ).css( "fill", "#DA1111" );
                $(".footer" ).css( "background-color", "#000000" );
                $('.img-border-color').css( "fill", "#FFFFFF" );
                $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
            }
            
        }else{
            
            $(".img-bg-color" ).css( "fill", "#DA1111" );
            $(".footer" ).css( "background-color", "#000000" );
            $('.img-border-color').css( "fill", "#FFFFFF" );
            $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
        }

       

        
     
     
        
    }


    async checkUpdate() {
        if(getParam('updateaddress') !== undefined){
            let order_id = this.state.order_id
            let bodyFormData = new FormData();
            bodyFormData.set('userid',this.state.user_id)
            bodyFormData.set('orderid',this.state.order_id)
            bodyFormData.set('addressid',getParam('updateaddress'))
            await axios({
                method: 'post',
                url: config.api.base_url+'/order/api/updateorder/address',
                data: bodyFormData,
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                
                    if(response.status==200){
                        //window.location('/cart');
                        
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            customClass: 'swal-mobile',
                            title: 'อัพเดทที่อยู่จัดส่งแล้ว',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            window.location.replace("/order_detail/"+order_id);
                        });
                    }else{
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถอัพเดทที่อยู่ได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-black',
                            },
                        }).then((result) => {
                            window.location.replace("/order_detail/"+order_id);
                        });
                        
                    }
                    console.log(response)
                })
                .catch(function (response) {
                    console.log(response)
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-black',
                        },
                    }).then((result) => {
                        window.location.replace("/order_detail/"+order_id);
                    });
                    
                   
            });
        }else if(getParam('updatepaytype') !== undefined){
            let order_id = this.state.order_id
            let bodyFormData = new FormData();
            bodyFormData.set('userid',this.state.user_id)
            bodyFormData.set('orderid',this.state.order_id)
            bodyFormData.set('paymenttypeid',getParam('updatepaytype'))
            await axios({
                method: 'post',
                url: config.api.base_url+'/order/api/updateorder/paymenttype',
                data: bodyFormData,
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                
                    if(response.status==200){
                        //window.location('/cart');
                        
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            customClass: 'swal-mobile',
                            title: 'อัพเดทช่องทางการชำระเงินแล้ว',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            window.location.replace("/order_detail/"+order_id);
                        });
                    }else{
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถเปลี่ยนช่องทางชำระเงินได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-black',
                            },
                        }).then((result) => {
                            window.location.replace("/order_detail/"+order_id);
                        });
                        
                    }
                    //console.log(response)
                })
                .catch(function (response) {
                    console.log(response)
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-black',
                        },
                    }).then((result) => {
                        window.location.replace("/order_detail/"+order_id);
                    });
                    
                   
            });
        }
    }

    openModal = (event,order_id,order_no) => {
        this.setState({
          isOpen: true
        });
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        
    }
    
   
    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
        
        
    }

    handleChangeFile = (e) => {
        e.preventDefault();
        let file = e.target.files[0];
        let reader = new FileReader();
        if(file){
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                let trueSize = getSizeFromImgDataURL(reader.result,'MB');
                if(trueSize>=1.4){
                    /* ต้อง resize */
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'หลักฐานโอนเงินต้องมีขนาดไม่เกิน 1 MB',
                        icon:'info',
                        confirmButtonText:'ตกลง',
                        customClass: {
                            confirmButton: 'btn-black',
                        },
                    })
                    
                    $('#slip_file').val(null)
                    this.setState({
                        slip_file: undefined,
                        slip_base64: undefined,
                        slip_canvas: slip_default
                    });
                }else{
                    this.setState({
                        slip_file: file,
                        slip_base64: reader.result,
                        slip_canvas: <img src={reader.result} className="w-100 slip-default"/>
                    });
                }
                
            };
        }else{
            this.setState({
                slip_file: undefined,
                slip_base64: undefined,
                slip_canvas: slip_default
            });
        }

    }

    
        
    useCoupon(e,couponid){
        window.location='/cart_list?couponid='+couponid;
    }

  

    handlePayment = (e,orderid) => {
        window.location="/order_detail/"+orderid;
    }

    goBack(){
        // this.props.history.goBack();
        window.location="/notifications";
    }

    historyBack = event => {
        window.history.back();
    }

    select_canvas = (e) => {
        $('#slip_file').click();
    }

    handleUploadSip = (e) => {
        let bodyFormData = new FormData();
        let transfer_date = document.getElementById("transfer_date").value;
        let transfer_time = this.state.transfer_hour+':'+this.state.transfer_minute;
        
        if(transfer_date === '' || transfer_date === undefined || this.state.transfer_hour === undefined || this.state.transfer_minute === undefined){
            
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณาระบุวันเวลาที่โอนเงิน',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
        }else if(parseFloat(this.state.state_money) < 0 || parseFloat(this.state.state_money) === NaN){
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณาระบุจำนวนเงินที่โอนเงิน',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
            
        }else if(this.state.slip_base64 === undefined){
            Swal.fire({
                title : 'ขออภัย',
                html: 'กรุณาแนบหลักฐานการโอนเงิน',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
        }else{
        
            bodyFormData.append("slipdatauri", this.state.slip_base64);
            bodyFormData.append("transferdate", transfer_date);
            bodyFormData.append("transfertime", transfer_time);
            bodyFormData.append("orderid", this.state.order_id);
            bodyFormData.append("userid", this.state.user_id);
            bodyFormData.append("storeid", this.state.store_id);
            bodyFormData.append("statemoney",parseFloat(this.state.state_money))

            axios({
                method: 'post',
                url: config.api.base_url+'/order/api/uploadslip',
                data: bodyFormData,
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                    if(response.status==200){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            /*customClass: 'swal-mobile',*/
                            title: 'แจ้งโอนเรียบร้อยแล้ว ขอบคุณค่ะ',
                            showConfirmButton: false,
                            timer: 2500
                        }).then(() => {
                            window.location.reload();
                        });
                    }else{
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'บันทึกข้อมูลไม่สำเร็จ<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                    }
                    
                })
                .catch(function (response) {
                    Swal.fire({
                        title : 'ขออภัย',
                        html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                        icon:'error',
                        confirmButtonText:'ตกลง'
                    })
                    
                    
            });
        }
        
     
        

         
            
            
            
            
    }
    

    handleChangeDatePicker(date) {
        this.setState({
          startDate: date
        });
        console.log(this.state)
        

      }
    
    handleCancel = (event,order_id,order_no) => {
        Swal.fire({
            title: 'ต้องการยกเลิกคำสั่งซื้อ',
            html:'ORDER ID : '+order_no,
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ใช่',
            focusConfirm: false,
            cancelButtonText:
              'ไม่ใช่',
              customClass: {
                confirmButton: 'btn-shop',
                cancelButton: '',
              
            },
            focusConfirm:true
          }).then((result) => {
            if (result.value) {
                let user_id = sessionStorage.getItem('user_id');
                let store_id = localStorage.getItem('storageStoreID');
                let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('storeid',store_id)
                bodyFormData.set('orderid',order_id)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/order/api/cancel',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        if(response.status==200){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                customClass: 'swal-mobile',
                                title: 'ยกเลิกคำสั่งซื้อเรียบร้อยแล้ว',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                window.location.replace("/order_history");
                            });
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถยกเลิกคำสั่งซื้อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง'
                            })
                        }
                        
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                        
                        
                    });
            }
        })
        if(this.state.colortabbar !== undefined){
            $(".swal2-styled.swal2-confirm, .btn.btn-primary.btn-shop, swal2-confirm.swal2-styled.btn-shop").css("background-color", "#"+this.state.colortabbar,"border-color", "#"+this.state.colortabbar );
        }
        
    }
    
    render() {

        return (
            <>


            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                    <div className="modal-header">
                        <h4>แจ้งชำระเงิน</h4>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label>วันที่โอน</label>
                                    <DatePicker
                                        selected={this.state.startDate}
                                        onChange={this.handleChangeDatePicker}
                                        locale={th}
                                        //showTimeSelect
                                        dateFormat="dd/MM/yyyy"
                                        maxDate={new Date()}
                                        className="form-control"
                                        name="transfer_date"
                                        id="transfer_date"
            
                                       
                                    />
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="form-group">
                                    <label>เวลาที่โอน</label>
                                    <div className="input-group">
                                        <select name="transfer_hour" onChange={this.handleChange} className="form-control"  style={{width:'59%'}}>
                                            <option value="">ชม.</option>
                                            {
                                                optionHour.map((row,key) => {
                                                    return (
                                                        <>
                                                            <option value={row.value}>{row.label}</option>
                                                        </>
                                                    )
                                                    
                                                })
                                            }
                                        </select>
                                        <select name="transfer_minute" onChange={this.handleChange} className="form-control" style={{width:'59%'}}>
                                            <option value="">นาที</option>
                                            {
                                                optionMinute.map((row,key) => {
                                                    return (
                                                        <>
                                                            <option value={row.value}>{row.label}</option>
                                                        </>
                                                    )
                                                    
                                                })
                                            }
                                        </select>
                                    {/* <Select options={optionHour} isSearchable={false} name="transfer_hour"  placeholder="ชม." onChange={this.handleChange}/>
                                    <Select options={optionMinute} isSearchable={false} name="transfer_minute" placeholder="นาที" onChange={this.handleChange} /> */}
                                    </div>
                                   
                                    
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="form-group">
                                    <label>จำนวนเงิน</label>
                                    <input type="number" name="state_money" id="state_money" className="form-control" onChange={this.handleChange}/>
                                </div>
                            </div>

                            
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label>แนบหลักฐาน (ขนาดไม่เกิน 1 MB)</label>
                                     <div id="slip_canvas text-center" onClick={(e) => this.select_canvas(e)}>
                                    
                                    {this.state.slip_canvas}
                                
                                    </div> 
                                   <input type="file" name="slip_file" id="slip_file" className="form-control"  accept="image/*" onChange={this.handleChangeFile}/>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        
                    </div>
                    <div className="modal-footer">
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.handleUploadSip}>ตกลง</button>
                            <button className="btn btn-outline-dark bg-white" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                    
                </Modal>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-12 h-100">
                            <div className="icon-back" onClick={this.goBack}>
                                
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">รายละเอียดคำสั่งซื้อ</h4> 
                               
                            </div>
                        </div>
                        
                    </div>
                </div>

            
           
           
           <div className="cart-detail mb-5 ">
               
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">
                {
                        this.state.data.orderdetail === undefined ? (
                             <div/>
                        ) : (

                            
                                    
                                    <ul className="cart-product">
                                        
                                {
                                    this.state.data.orderdetail.map((row,key) => (
                                        <li className="product-item-detail order-list" key={key}>
                                            <div className="container">

                                            <div className="row">
                                                <div className="col-3 ">
                                                    <div class="w-100 text-center   pb-2 pt-2">
                                                        <img class="img-fluid img-product" src={row.productimg==='' ? '/assets/images/slide.png' : row.productimg}/>
                                                            
                                                    </div>
                                                </div>
                                                <div className="col-9 mx-auto pt-2 p-0 pr-4">
                                                   
                                                    <h4 className="mb-1 text-gray pr-4">ชื่อ {row.productname}</h4>
                                                    <h4 className="mb-1 text-gray">ตัวเลือกสินค้า {row.variantname}</h4>
                                                    <h4 className="mb-1 text-gray">จำนวน X{row.amount}</h4>
                                                    <h4 className="mb-1 ">ราคา <NumberFormat value={row.unitprice} displayType={'text'} thousandSeparator={true}/> THB</h4>
                                                    {
                                                        row.point===0 ? (
                                                            <div/>
                                                        ) : (
                                                            <div className="flag-point">+ <NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/><br/>POINT</div>
                                                        )
                                                    }
                                                       
                                                     
                                                </div>
                                                
                                            </div>  
                                           
                                            </div>
                                        </li>
                                    )
                                    )
                                }
                                        
                                       
                                       
                                        <li className="product-item-detail order-list" >
                                            <div className="container">

                                                <div className="row">
                                                    <div className="col-1 ">
                                                        <div class="w-100 text-center   pb-2 pt-2">
                                                        <h4 className="mb-1 w-100"><i class="fa fa-map-marker" aria-hidden="true"></i></h4>
                                                                
                                                        </div>
                                                    </div>
                                                    <div className="col-11 ">
                                                        <div class="w-100    pb-2 pt-2">
                                                            <h4 className="mb-1 w-100">ที่อยู่การจัดส่ง {inArray(this.state.data['statusid'],allowUpdatePaytypeStatus) ? (<a href={"/order_address?orderid="+this.state.data['orderid']} className="link text-right">แก้ไขที่อยู่</a>) : (<></>)}</h4>
                                                            <div className="text-gray">
                                                            {
                                                               this.state.data=== undefined ? (
                                                                   <div></div>
                                                               ):(
                                                                   <>
                                                                   {/* {row.name} */}
                                                                    {this.state.data['orderreceiver']} | <NumberFormat value={this.state.data['ordertel']} displayType={'text'} format="###-###-####" />
                                                                    <br/>
                                                                    {this.state.data['orderaddress']+' '+this.state.data['subdistrictname']+' '+this.state.data['districtname']}
                                                                    <br/>
                                                                    {this.state.data['provincename']+' '+this.state.data['zipcode']}
                                                                 
                                                                   </>
                                                               )
                                                           }
                                                            </div>
                                                                
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div>  
                                           
                                            </div>
                                        </li>
                                        <li className="product-item-detail order-list" >
                                            <div className="container">

                                                <div className="row">
                                                    
                                                    <div className="col-12 ">
                                                        <div class="w-100    pb-2 pt-2">
                                                            <h4 className="mb-1 w-100">ช่องทางชำระเงิน {inArray(this.state.data['statusid'],allowUpdatePaytypeStatus) ? ( <a href={"/OrderPayType?orderid="+this.state.data['orderid']+'&orderpaytype='+this.state.data['paymenttypeid']} className="link text-right">เปลี่ยนช่องทางชำระเงิน</a> ) : (<></>)}</h4>
                                                            <div className="text-gray">
                                                            {
                                                               this.state.data=== undefined ? (
                                                                   <div></div>
                                                               ):(
                                                                   <>
                                                                 
                                                                    {this.state.data['paymenttypename']} 
                                                                 
                                                                   </>
                                                               )
                                                           }
                                                            </div>
                                                                
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div> 
                            {
                                this.state.data['paymenttypeid']==1 ? (
                                    <>
                                                <hr className="mt-1 mb-1"/>
                                                <div className="row">
                                                
                                                    <div className="col-12 ">
                                                        <div class="w-100    pb-2 pt-2">
                                                            
                                                            <div className="text-gray">
                                                            {
                                                            this.state.paymentData=== undefined ? (
                                                                <div></div>
                                                            ):(
                                                                this.state.paymentData.map((row,key) => (
                                                                    <>
                                                                        <h4 className="mb-1 w-100 text-gray dark">{row.bankname}</h4>
                                                                        {"ชื่อบัญชี : "+row.bankaccount}
                                                                        <br/>
                                                                        {"เลขที่บัญชี : "}<NumberFormat value={row.bankno} displayType={'text'} format="###-####-####" />
                                                                    
                                                                
                                                                    </>
                                                                ))
                                                                
                                                            )
                                                        }
                                                            </div>
                                                                
                                                        </div>
                                                    </div>
                                                </div> 
                                    </>
                                ) : (
                                    <div/>
                                )
                            }
                                                
                                           
                                            </div>
                                        </li>
                                        <li className="product-item-detail order-list" >
                                            <div className="container mb-3 mt-3">

                                                <div className="row">
                                                    <div className="col-5 ">
                                                        <div class="w-100  ">
                                                        <h4 className="mb-0 w-100 text-gray dark">หมายเลขคำสั่งซื้อ</h4>     
                                                        </div>
                                                    </div>
                                                    <div className="col-7 ">
                                                        <div class="w-100 ">
                                                        <h4 className="mb-0 w-100 text-gray font-weight-light text-right">{this.state.data['orderno']}</h4>         
                                                        </div>
                                                    </div>
                                                </div>  

                                                <div className="row mt-2">
                                                    <div className="col-5 ">
                                                        <div class="w-100  ">
                                                        <h4 className="mb-0 w-100 text-gray dark">รวมคำสั่งซื้อ</h4>     
                                                        </div>
                                                    </div>
                                                    <div className="col-7 ">
                                                        <div class="w-100 ">
                                                        <h4 className="mb-0 w-100 text-gray font-weight-light text-right"><NumberFormat value={this.state.data['price']} displayType={'text'} thousandSeparator={true}/> THB</h4>         
                                                        </div>
                                                    </div>
                                                </div>  

                                    {
                                        this.state.data['couponid']=== 0 ? (
                                            <div/>
                                        ) : (
                                                <div className="row mt-2">
                                                    <div className="col-5 ">
                                                        <div class="w-100  ">
                                                        <h4 className="mb-0 w-100 text-gray dark">ส่วนลด</h4>     
                                                        </div>
                                                    </div>
                                                    <div className="col-7 ">
                                                        <div class="w-100 ">
                                                        <h4 className="mb-0 w-100 text-gray font-weight-light text-right"><NumberFormat value={this.state.data['discount']} displayType={'text'} thousandSeparator={true}/> THB</h4>         
                                                        </div>
                                                    </div>
                                                </div>
                                        )
                                    }

                                    {
                                        this.state.data['gettypeid']== 2 && this.state.data['deliveryid'] != 0 ? (
                                                <div className="row mt-2">
                                                    <div className="col-5 ">
                                                        <div class="w-100  ">
                                                        <h4 className="mb-0 w-100 text-gray dark">ค่าจัดส่ง</h4>     
                                                        </div>
                                                    </div>
                                                    <div className="col-7 ">
                                                        <div class="w-100 ">
                                                        <h4 className="mb-0 w-100 text-gray font-weight-light text-right">{this.state.data['deliverryname']} <NumberFormat value={this.state.data['deliverryprice']} displayType={'text'} thousandSeparator={true}/> THB</h4>         
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        ) : (
                                                <div/>
                                        )
                                    }
                                                 <div className="row mt-2">
                                                    <div className="col-5 ">
                                                        <div class="w-100  ">
                                                        <h4 className="mb-0 w-100 ">ยอดรวมทั้งหมด</h4>     
                                                        </div>
                                                    </div>
                                                    <div className="col-7 ">
                                                        <div class="w-100 ">
                                                        <h4 className="mb-0 w-100 text-right"><NumberFormat value={parseFloat(this.state.data['totalprice'])+parseFloat(this.state.data['deliverryprice'])} displayType={'text'} thousandSeparator={true}/> THB</h4>         
                                                        </div>
                                                    </div>
                                                </div> 
                                           
                                            </div>
                                        </li>
                                    </ul>
                        )
                }
                                    
        

                       </div>

                   </div>
                   
               </div>
    {
        this.state.data['paymenttypeid']==1 && this.state.data['statusid']==1 ? (
            <>
            {this.state.data['thirdPartyOrderID'] === '0' ?
                <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                    <div className="container text-center">
                       <h5>เก็บหลักฐานการโอนเงินและอัพโหลดภายใน {Moment(this.state.data['paywithin']).format('DD-MM-YYYY')}<br/>
                       เวลา {Moment(this.state.data['paywithin']).format('H:mm')} น. หลังจากเวลานี้ออเดอร์ของคุณจะถูกยกเลิก</h5>
                    </div>
                    <div className="container text-center">
                        <button type="button" className="btn btn-outline-dark w-100 bg-white" onClick={(e) => this.openModal(e,this.state.data['orderid'],this.state.data['orderno'])}>อัพโหลดหลักฐานการโอนเงิน</button>
                    </div>
                    <div className="container text-center mt-3">
                        <button type="button" className="btn btn-primary btn-shop w-100" onClick={(e) => this.handleCancel(e,this.state.data['orderid'],this.state.data['orderno'])}>ยกเลิกคำสั่งซื้อ</button>
                    </div>
                </div>
            : <></> }
            </>
        ) : (
            <>
            {this.state.data['thirdPartyOrderID'] === '0' ?
                inArray(this.state.data['statusid'],allowCancelStatus) ? ( 
                    <div className="row  m-0 w-100 h-100 ">
                        <div className="container text-center">
                            <button type="button" className="btn btn-primary btn-shop w-100" onClick={(e) => this.handleCancel(e,this.state.data['orderid'],this.state.data['orderno'])}>ยกเลิกคำสั่งซื้อ</button>
                        </div>
                    </div>
                ) : (
                    <div/>
                )
            : <></> }
            </>
        )
    }


    
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                
                    
            </div>
                 
            </>
        )
    }
}
export default Coupon;
