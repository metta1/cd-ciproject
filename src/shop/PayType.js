import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import {getParam} from '../lib/Helper';
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');
let payType = getParam('orderpaytype') !== undefined ? getParam('orderpaytype') : (localStorage.getItem('payType') === null ? '1' : localStorage.getItem('payType'))

class PayType extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        console.log(localStorage.getItem('getProductType'))

        this.goBack = this.goBack.bind(this);
 
    }


    state = {
        data : [],
        store_id:this.props.match.params.store_id,
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        get_producttype: localStorage.getItem('getProductType') === null ? '1' : localStorage.getItem('getProductType'),
        payType : payType
    };

    goBack(){
        if (localStorage.getItem('isFromLogin') === 'Y') {
            if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+localStorage.getItem('storageStoreID');
            }
        } else {
            this.props.history.goBack();
        }
    }


    async componentDidMount(){
       

       

        
     
     
        
    }

  

   
    handleChange = event => {
        this.setState({'payType':event.target.value});
        
        
    }

    handleBackPage = event => {
        if(getParam('orderid') !== undefined){
            window.location='/order_detail/'+getParam('orderid')
        }else{
            window.location='/cart_list/'+localStorage.getItem('storageStoreID');
            
        }
    }
    
        
    
    
    handleSubmit = event => {
        if(this.state.payType){
            if(getParam('orderid') !== undefined){
                window.location='/order_detail/'+getParam('orderid')+'?updatepaytype='+this.state.payType;
            }else{
                localStorage.setItem('payType',this.state.payType);
                window.location='/cart_list'
            }
            
        }else{
            Swal.fire({
                title : 'ขออภัยค่ะ',
                html: 'กรุณาเลือกช่องทางการชำระเงิน',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
        }
    
       
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-12 h-100">
                            <div className="icon-back">
                                <a onClick={this.goBack}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">เลือกช่องทางการชำระเงินของคุณ</h4> 
                               
                            </div>
                        </div>
                        
                    </div>
                </div>

    
           
           <div className="cart-detail mb-5 ">
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">

                                    <ul className="cart-product">
                                        <li className="product-item-detail p-2">
                                            <div className="row w-100">
                                                <div className="col-12">
                                                    <h4 className="m-0"><input type="radio" id="customRadio1" name="payType" value="1" onClick={this.handleChange} checked={this.state.payType==1 ? 'checked' : ''}/> <label className="m-0 pl-2" for="customRadio1">โอน/ชำระเงินผ่านบัญชีธนาคาร</label></h4>
                                                </div>
                                            </div>  
                                        </li>
                                        <li className={this.state.get_producttype==1 ? 'product-item-detail p-2 disabled' : 'product-item-detail p-2'}>
                                            <div className="row w-100">
                                                <div className="col-12">
                                                    <h4 className="m-0"><input disabled={this.state.get_producttype==1 ? 'disabled' : ''}  type="radio" id="customRadio2" name="payType" value="2" onClick={this.handleChange} checked={this.state.payType==2 ? 'checked' : ''}/> <label className="m-0 pl-2" for="customRadio2">ชำระเงินปลายทาง</label></h4>
                                                </div>
                                            </div>  
                                        </li>
                                        <li className={this.state.get_producttype==2 ? 'product-item-detail p-2 disabled' : 'product-item-detail p-2'} >
                                            <div className="row w-100">
                                                <div className="col-12">
                                                    <h4 className="m-0"><input disabled={this.state.get_producttype==2 ? 'disabled' : ''} type="radio" id="customRadio3" name="payType" value="3" onClick={this.handleChange} checked={this.state.payType==3 ? 'checked' : ''}/> <label className="m-0 pl-2" for="customRadio3">ชำระเงินสด</label></h4>
                                                </div>
                                            </div>  
                                        </li>
                                      
                                       
                                       
                                      
                                    </ul>
                                    
        
              
                       </div>

                   </div>
                   
               </div>
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                   
                  
                <div className="footer footer-product-detail">
                        <button className="btn btn-buy" onClick={this.handleSubmit}>ยืนยัน</button>
                          
                   
                </div>
                    
            </div>
                 
        
        )
    }
}
export default PayType;
