import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Promotion_detail extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        promotion_id:this.props.match.params.promotion_id
    };


    async componentDidMount(){
        
        const result = await axios.get('https://backend-ho.dreamnolimit.com/api/promotion/'+this.state.cat_id+'/'+this.state.promotion_id)
        if(result.status===200){
            this.setState({data: result.data});
        }
        
        console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/promotion_category.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-3 pl-4">
                        <div className="row">
                            <div className="col-6">
                                <img className="w-75" src="/assets/images/logo.png" alt=""/>
                            </div>
                            <div className="col-6">
                                <div className="float-right">
                                    <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                        <a href="">
                                            <img className="h-50" src="/assets/images/icon-search.svg" alt="..." />
                                        </a>
                                    </div>
                                    <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                        <a href="">
                                            <img className="h-50" src="/assets/images/icon-scan.svg" alt="..." />
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                    
                    <div className="promotion p-2" style={{marginTop: "20%"}}>

                        <span className="p-2 m-0 text-grey">โปรโมชั่น</span>
                        <div className="row m-0 mb-5">


                    {


                        this.state.data.map(row => (
                            
                            <div className="col-12 p-2">
                               
                                    <div className="promotion-box shadow rounded">
                                        <div className={'promotion-item'}>
                                                <img className="mt-4" src={row.promotion_img} alt={row.promotionname}/> <br/>
                                                <h4 className="mt-2">{row.promotionname}</h4>
                                                <p className="mt-2 text-left" style={{margin: "10px"}}>{row.promotiondetail}</p>
                                        </div>
                                    </div>
                               
                            </div>
                         ))


                    }
                            

                        </div> 
                        {/* <!-- row --> */}
                    </div>
                    {/* <!-- promotion --> */}

                    <div className="footer mx-auto">
                            <img className="w-100 position-relative" src="/assets/images/footer_promotion.svg" alt=""/>
                            <div className="menu position-absolute w-100">
                                <div className="menu-item px-2 pt-2 ">
                                    <a href="/user_card">
                                        <img className="position-relative" src="/assets/images/icon-credit.png" alt=""/> <br/>
                                        <span>CARD</span>
                                    </a>
                                    
                                </div>
                                <div className="menu-item px-2 pt-2">
            
                                    <a href="">
                                        <img className="position-relative" src="/assets/images/icon-inbox.png" alt=""/><br/>
                                        <span className="position-absolute menu-item-num rounded-circle">
                                            +1
                                        </span>
                                        <span>INBOX</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="">
                                        <img className="" src="/assets/images/icon-home.png" alt=""/><br/>
                                        <span>HOME</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 ">
                                    <div className="menu-item-active bg-white rounded-circle shadow">
                                        <div className="menu-item-active-ab">
                                            <a href="/promotion_category">
                                                <img className="" src="/assets/images/icon-gift.png" alt=""/> <br/>
                                                <span>PROMOTION</span>
                                            </a>
                                        </div>
                                    </div>
                
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="/account">
                                        <img className="" src="/assets/images/icon-user.png" alt=""/> <br/>
                                        <span>ACCOUNT</span>
                                    </a>
                                </div>
                            </div>

                    </div>
                </div>
                 
            </div>
        )
    }
}
export default Promotion_detail;
