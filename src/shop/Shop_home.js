import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import DateCountdown from 'react-date-countdown-timer';
import {Navigation_shop as Navigation} from '../template'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();

const hiddenMenuShopArray = [
    83,
    139,
    140,
    141,
    162
]

class Shop_home extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        localStorage.setItem('isFromLogin','N');
        localStorage.setItem('storageStoreID',this.props.match.params.store_id);

        if(inArray(this.props.match.params.store_id,hiddenMenuShopArray)){
            window.location='/store_feed/'+this.props.match.params.store_id
        }

        this.handleSearch = this.handleSearch.bind(this);
    }

    
    
    state = {
        data : [],
        recommendData : [],
        categoryData : [],
        couponData : [],
        productPromoData : [],
        productNewData : [],
        storeData: [],
        rewardData:[],
        store_id:this.props.match.params.store_id,
        userid:sessionStorage.getItem('user_id'),
        lineid:sessionStorage.getItem('line_id'),
        inputsearch : '',
        colorData : [],
        numNoti:0,
        numProductInCart:0
    };
    


    async componentDidMount(){
        /* NOTI */
    
        const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        
        if(storeNotiResult.status===200){
            if(storeNotiResult.data !== undefined){
                const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                this.setState({
                    numNoti:numNoti
                })
            }
        } 

        const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.store_id);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })
           
        }

        const resultCart = await axios.get(config.api.base_url+'/order/api/cart/'+this.state.store_id+'/'+sessionStorage.getItem('user_id'));
        if(resultCart.status===200){
            if(resultCart.data.cartAmount>0){
                this.setState({
                    numProductInCart : resultCart.data.cartAmount
                })
            }
        }

        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
        if(this.state.colorData !== undefined){
            let coloricon   = this.state.colorData['coloricon'];
            if(coloricon){
                $('.chat-bg' ).css( "fill", "#"+coloricon );
            }
        }
        const rsStore = await axios.get(config.api.base_url+'/store/api/store/'+this.state.store_id)
        if(rsStore.status===200){
            this.setState({storeData: rsStore.data[0]});
        }
      
        const resultCat = await axios.get(config.api.base_url+'/store_product/api/storecategory/'+this.state.store_id)
        if(resultCat.status===200){
            this.setState({categoryData: resultCat.data});
        }
        
        
        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id)
        if(result.status===200){
            this.setState({data: result.data});
        }

        const resultRecommend = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?limit=6&recommend=1')
        if(resultRecommend.status===200){
            this.setState({recommendData: resultRecommend.data});
        }

        // Product promotion
        const resultProductPromo = await axios.get(config.api.base_url+'/store_product/api/product_promotion/'+this.state.store_id)
        if(resultProductPromo.status===200){
            this.setState({productPromoData: resultProductPromo.data});
        }
        // Product new
        const resultProductNew = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?orderby=date_create')
        if(resultProductNew.status===200){
            this.setState({productNewData: resultProductNew.data});
        }
        // Coupon
        const resultCoupon = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.store_id+'/'+this.state.userid)
        if(resultCoupon.status===200){
            this.setState({couponData: resultCoupon.data});
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward/'+this.state.store_id+'?user_id='+sessionStorage.getItem('user_id'))
        if(rewardRs.status===200){
            this.setState({rewardData: rewardRs.data});
        }
        //console.log(this.state.rewardData);
        
        $('.btn-getcoupon, .btn-usecoupon').click(function(){
            let key = $(this).data('primary');
            let storeid = $(this).data('storeid');
            let couponid = $(this).data('couponid');
            let coupontype = $(this).data('coupontype');

            if (coupontype === 1) {
                $(this).css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
                $('body').addClass('modal-open')
                $('<div class="modal-backdrop fade show"></div>').appendTo('body');
                $('#couponModal-'+key).css({'display':'block'})
            } else {
                localStorage.setItem('storageStoreID',storeid);
                window.location = "/coupon_detail/"+couponid;
            }
        })

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })

        
       
        //this.setState({data: result.data});
        //console.log(this.state.data)
    }
 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
        
    handleToCart = (proid,event) => {
        event.preventDefault();
        if(proid!==null){
                    let user_id = sessionStorage.getItem('user_id');
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('proid',proid)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/order/api/addToCart',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            if(response.status==201){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    customClass: 'swal-mobile',
                                    title: 'เพิ่มไปยังรถเข็นแล้ว',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    
                                });
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                })
                            }
                            console.log(response)
                        })
                        .catch(function (response) {
                            //console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                },
                            })
                            
                           
                    });

            

        }
    }
    
    handleSubmit = event => {
    
        
    }

    getCoupon(event,e) {
        const couponid = e.couponid;
        const storeid = e.storeid;
        const userid = sessionStorage.getItem('user_id');

        if (e.coupontype==="1") {
            const data = new FormData();
            data.set('couponid', couponid);
            data.set('storeid', storeid);
            data.set('userid', userid);
            axios({
                method: 'post',
                url: config.api.base_url+'/store_coupon/api/coupon',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data)
            })
            .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        }
    }



    async handleSearch(e){
        e.preventDefault();

        let user_id = sessionStorage.getItem('user_id');
        // let keyWord = this.state.inputsearch;
        let keyWord = e.target.value

        const resultProductPromo = await axios.get(config.api.base_url+'/store_product/api/product_promotion/'+this.state.store_id+'?keywords='+keyWord)
        if(resultProductPromo.status===200){
            this.setState({productPromoData: resultProductPromo.data});
        } else {
            this.setState({productPromoData: []});
        }

        const resultCoupon = await axios.get(config.api.base_url+'/store_coupon/api/coupon/'+this.state.store_id+'/'+this.state.userid+'?keyword='+keyWord)
        if(resultCoupon.status===200){
            this.setState({couponData: resultCoupon.data});
        } else {
            this.setState({couponData: []});
        }

        const resultProductNew = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?orderby=date_create&keywords='+keyWord)
        if(resultProductNew.status===200){
            this.setState({productNewData: resultProductNew.data});
        } else {
            this.setState({productNewData: []});
        }

        const resultCat = await axios.get(config.api.base_url+'/store_product/api/storecategory/'+this.state.store_id+'?keywords='+keyWord)
        if(resultCat.status===200){
            this.setState({categoryData: resultCat.data});
        } else {
            this.setState({categoryData: []});
        }

        const result = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }

        const resultRecommend = await axios.get(config.api.base_url+'/store_product/api/product/'+this.state.store_id+'?limit=6&recommend=1&keywords='+keyWord)
        if(resultRecommend.status===200){
            this.setState({recommendData: resultRecommend.data});
        } else {
            this.setState({recommendData: []});
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward/'+this.state.store_id+'?user_id='+sessionStorage.getItem('user_id')+'&keywords='+keyWord)
        if(rewardRs.status===200){
            this.setState({rewardData: rewardRs.data});
        } else {
            this.setState({rewardData: []});
        }
    }

    useCoupon(e,data) {
        e.preventDefault();
        // localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
    }

    handleChangeReward = (e,itemid,storeid) => {
        e.preventDefault();
        localStorage.setItem('storageStoreID',storeid);
        window.location='/reward_detail/'+itemid;
    }

    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1&callback=shop/'+this.state.store_id+'&store='+this.state.store_id, '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm?callback=shop/'+this.state.store_id+'&store='+this.state.store_id;
        }
    }

    async couponDetail(e,data) {
        e.preventDefault();
        
        if (data.coupontype==="0" && data.couponused==="1") {
            console.log(data);
            localStorage.setItem('storageStoreID',data.storeid);
            window.location = "/coupon_detail/"+data.couponid;
        }
    }
    
    render() {
        const useCouponButton = {
            backgroundColor: '#12a508'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0'
        };
        const addBtnBackground = {
            backgroundColor: '#' + this.state.colorData['colortabbar']
        }
        const addColor = {
            stroke: '#' + this.state.colorData['coloricon']
        }
        const textColor = {
            color: '#' + this.state.colorData['colorfont']
        }
        return (
            
            <div className="container-mobile m-auto shop-page">
                {/* <Header/> */}
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <div className="bg-black shadow">
                    <div className="w-100 h-100">
                        { this.state.storeData.length === 0 ? (
                            <></> 
                        ) : (
                            <div className="w-100">
                                <div className="p-2 w-100 text-center">
                                    <h1 className="m-0 h" style={textColor}>{this.state.storeData.storename}</h1>
                                </div>
                                {this.state.storeData.store_banner ==="" ? (
                                    <></>
                                ) : (
                                    <div>
                                        <img src={this.state.storeData.store_banner} width="100%" style={{maxHeight: '250px'}}/>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </div>
                    <div className="shop-home">
                        <div className="row p-2 mt-1 m-0 w-100 h-100 ">
                            <div className="col-9">
                                    <div className="position-relative align-searchbox">
                                        <input type="text" className="search w-100 rounded form-control shadow " placeholder="Search" style={{height: '80%', marginLeft: '-0.5rem'}} name="inputsearch" onKeyUp={this.handleSearch}/>
                                        <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" /*onClick={this.handleSearch}*//>
                                    </div>

                            </div>
                            <div className="col-3 row head2-icon p-0">
                                <div className="icon-new ">
                                    <a onClick={this.handleClickScanQRCode}>
                                        <img className="" src="/assets/images/icon-scan-shadow.png" alt="..." />
                                    </a>
                                </div>
                                <div className="icon-new " style={{left:'0.35rem'}}>
                                    <a href="/notifications">
                                        <img className="" src="/assets/images/icon-notification-shadow.png" alt="..." />
                                        {
                                            this.state.numNoti>0 && (
                                                <div className="alert alert-primary noti-num-border small-alert" role="alert">
                                                    {this.state.numNoti}
                                                </div>
                                            )
                                        }
                                        
                                    </a>
                                </div>
                                <div className="icon-new" style={{left:'0.7rem'}}>
                                    <a href={'/cart/'+this.state.store_id}>
                                        <img class="" src="/assets/images/icon-basket-shadow.png" alt="..." />
                                        {
                                            this.state.numProductInCart>0 && (
                                                <div className="alert alert-primary noti-num-border small-alert" role="alert">
                                                    {this.state.numProductInCart}
                                                </div>
                                            )
                                        }
                                    </a>
                                </div>
                            </div>
                        </div>
                        {/* Promotion */}
                        { this.state.productPromoData.length === 0 ? (
                            <div/>
                        ):(
                            <div className="product product-promotion px-3">
                                <div className="d-flex bd-highlight">
                                    <div className="px-2 py-0  bd-highlight">
                                        <h4 className="m-0">สินค้าโปรโมชั่น</h4>
                                    </div>
                                    <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                        <a className="text-blue" href={'/products/'+this.state.store_id+'?type=promotion'}>
                                            <u> เพิ่มเติม </u>
                                        </a>
                                    </div>
                                </div>
                                <div className="row m-0 mt-2 w-100">
                                { this.state.productPromoData.map((row,key) => (
                                    <div className="col-6 pl-0 mb-3 " key={key}>
                                        <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                                            {
                                                row.point=== undefined || row.point=== '' || row.point=== null ? (
                                                    <></>
                                                ):(
                                                    <div className="flag">
                                                        <img src="/assets/images/icon-flag.svg" alt=""/>
                                                        <div className="flag-detail text-center p-1">
                                                            <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                            <p>POINTS</p>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            <div className="text-center w-100">
                                                <img className="img-thumb-shop" src={row.product_img===''? '' : row.product_img} alt="" />
                                            </div>
                                            <div className="product-item-detail px-3 w-100 text-left">
                                                <h4 className="m-0 product-name">{row.productname}</h4>
                                                <p className="m-0 product-cat-name">{row.catname}</p>
                                            </div>
                                            <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                                <div className="mt-auto">
                                                    {
                                                        row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null || row.pricebeforediscount === '0'  ? (
                                                            <small className="d-block"></small>
                                                        ):(
                                                            <small className="d-block product-discount-price"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                        )
                                                    }
                                                    
                                                    <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                                </div>
                                                <div>
                                                    <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                        <div className="product-add rounded-circle text-center" style={addBtnBackground}>
                                                        <svg className="w-50" xmlns="http://www.w3.org/2000/svg" width="49.299" height="50.683" viewBox="0 0 49.299 50.683">
                                                            <g transform="translate(-398.932 -2712.768)">
                                                                <line style={addColor} y2="45.683" transform="translate(423.582 2715.268)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                <line style={addColor} x2="44.299" transform="translate(401.432 2737.417)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                            </g>
                                                        </svg>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        )}
                        {/* Deal */}
                        {this.state.couponData.length === 0 && this.state.rewardData.length === 0 ? <></> :
                        <div className="coupon shadow pl-2 py-2 mt-2">
                            <div className="d-flex bd-highlight ">
                                <div className="px-2 py-0  bd-highlight">
                                    <h4 className="m-0">ดีลใหม่</h4>
                                </div>
                            </div>
            
                            <div className="row w-100 m-0 text-center">
                        {this.state.couponData.length === 0 ? (
                            <></>
                        ):(
                            // <div className="coupon shadow pl-3 py-2 mt-2">
                            //     <div className="d-flex bd-highlight ">
                            //         <div className="px-2 py-0  bd-highlight">
                            //             <h4 className="m-0">ดีลใหม่</h4>
                            //         </div>
                            //     </div>
                
                            //     <div className="row w-100 m-0 text-center">
                                     this.state.couponData.map((row,key) => (
                                        ((new Date(row.enddate).getTime() > nowDate) && (new Date(row.startdate).getTime() < nowDate) && row.couponstatus == "1") ?
                                        <div className="col-10 p-0 pl-0" key={key}>
                                            <div className="position-relative">
                                                <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                <div className="coupon-detail row ml-1 h-100">
                                                    <div className="col-4">
                                                        <p className="mt-3 ml-2 mb-0 text-overflow">{row.catename}</p>
                                                        <div className="w-100 text-center mt-2">
                                                            <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                            <p className="mt-2 text-overflow">{row.storename}</p>
                                                        </div>
                                                    </div>
                                                    <div className="col-4 px-0 text-center mt-2 h-100">
                                                        <p className="mt-2 mb-0 text-overflow">{row.couponname}</p>
                                                        <h2>ลดราคา</h2>
                                                        {row.branchName!== null ? (
                                                            <small className="d-block">สาขา : {row.branchName}</small>
                                                        ):(
                                                            <></>
                                                        )}
                                                    
                                                        <small className="time-text-overflow"><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                        {row.statususer === "1" && row.couponused === "1" ? ( 
                                                            // <button className="btn btn-orange p-0 mt-1" style={usedCouponButton} disabled data-primary="cp-1"> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                            <button className="btn btn-orange p-0 mt-1" style={usedCouponButton} onClick={(e) => this.couponDetail(e,row)} data-primary={key}> <h5 className="m-0">ใช้แล้ว</h5></button>
                                                        ) : row.statususer === "1" ? (
                                                            // <button className="btn btn-orange p-0 mt-1" style={useCouponButton} data-primary="cp-1" onClick={(e) => this.useCoupon(e,row)}>
                                                            //     <h5 className="m-0">ใช้คูปอง</h5>
                                                            // </button>
                                                            <button className="btn btn-orange p-0 mt-1 btn-usecoupon" style={useCouponButton} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}>
                                                                <h5 className="m-0">ใช้คูปอง</h5>
                                                            </button>
                                                        ) : (
                                                            <div key={key}>
                                                                {/* <button className="btn shop-home-coupon-btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key}> <h5 className="m-0">รับคูปอง</h5></button> */}
                                                                <button className="btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}> <h5 className="m-0">รับคูปอง</h5></button>
                                                            </div>
                                                        )}
                                                        {/* MODAL COUPON */}
                                                        <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                            <div className="modal-dialog modal-dialog-centered" role="document">
                                                            <div className="modal-content w-75 mx-auto border-0">
                                                                <div className="modal-header bg-yellow">
                                                                <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                                    <div className="position-relative">
                                                                        <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                        <div className="coupon-detail row ml-1 h-100">
                                                                            <div className="col-4">
                                                                                <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                                <div className="w-100 text-center mt-2">
                                                                                    <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                                    </div>
                                                                                    <p className="mt-2">{row.storename}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-4 px-0 text-center mt-2 h-100">
                                                                                <p className="mt-2 mb-0">{row.couponname}</p>
                                                                                <h2>ลดราคา</h2>
                                                                                {row.branchName!== null ? (
                                                                                    <small className="d-block">สาขา : {row.branchName}</small>
                                                                                ):(
                                                                                    <></>
                                                                                )}
                                                                                <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                                <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                            </div>
                                                                            <div className="col-4 p-0 text-center my-auto">
                                                                                <div className="coupon-detail-side mt-2" style={{whiteSpace: 'nowrap'}}>
                                                                                    <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                                    <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div></h5>
                                                                <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                                    <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                                </button>
                                                                </div>
                                                                <div className="modal-body text-center">
                                                                    <h4>คุณได้รับดีลเรียบร้อย!</h4>
                                                                    <button data-dismiss="modal" className="btn bg-yellow w-75">
                                                                        <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-4 p-0 text-center my-auto">
                                                        <div className="coupon-detail-side mt-2 " style={{whiteSpace: 'nowrap'}}>
                                                        <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                            <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                            <></>
                                        
                                        
                                    ))
                                    // {/* { this.state.categoryData.map((row,key) => (
                                    //     <div className="col-4 p-2" key={key}>
                                    //         <a href={'/products_incategory/'+this.state.store_id+'/'+row.cateid}>
                                    //             <div className="cat-card rounded shadow">
                                    //                 <img className="w-100" src={row.product_img} alt=""/>
                                    //                 <p className="p-1 m-0">{row.catename}</p>
                                    //             </div>
                                    //         </a>
                                    //     </div>
                                    // ))} */}
                            //     </div>
                            // </div>
                        )}
                        {this.state.rewardData === undefined || this.state.rewardData ==='' || this.state.rewardData.length === 0 ? (
                                    <div/>
                                ) : (
                                this.state.rewardData.map(row => (
                                    <div className="col-10 p-0 pl-2">
                                        <div className="position-relative">
                                            <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                            <div className="coupon-detail row ml-1 h-100">
                                                <div className="col-5">
                                                    <div className="w-100 text-center mt-2">
                                                        <div className="row w-100">
                                                            <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                            <p className="text-overflow mt-3 ml-2 mb-0">{row.storename}</p>
                                                        </div>
                                                        <p className="mt-0 mb-0 fs-40"><h5 className="mb-1">{row.itempoint}<br/>คะแนน</h5></p>
                                                    {
                                                        // (parseFloat(row.itempoint) <= row.user_point) ? (
                                                        //     <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                        //         { row.redeemstatus === "1" ? 
                                                        //         <p className="m-0 font-tf reward">แลกแล้ว</p>
                                                        //         :
                                                        //         <p className="m-0 font-tf reward">แลกรางวัลนี้</p>
                                                        //         }
                                                        //     </button>
                                                        // ) : (
                                                        //     <button className="btn btn-orange p-10 mt-0"> <p className="m-0 text-overflow font-tf">สะสมคะแนนเพิ่มอีกนิด</p></button>
                                                        // )
                                                        (parseFloat(row.itempoint) <= row.user_point) ? (
                                                            (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                                <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                { row.redeemtime_status === "1" ? 
                                                                <p className="m-0 font-tf">แลกแล้ว</p>
                                                                :
                                                                <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                }
                                                                </button>
                                                            ):(
                                                                <button className="btn btn-green p-10 mt-0" style={useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                            )
                                                        ) : (
                                                            <button className="btn coupon-btn btn-orange p-10" style = {{position:'relative', bottom: 10}}> <p className="m-0 font-tf">สะสมคะแนนเพิ่ม</p></button>
                                                        )
                                                    }
                                                    </div>
                                                </div>
                                                <div className="col-7 p-0 text-center mt-2">
                                                    <p className="m-0 align-text">{row.catename}</p>
                                                    <img className="img-thumb" src={row.itemimg} width="43%" alt=""/>
                                                    <p className="m-0 pl-3 pr-3 text-center">{row.itemname}</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    ))
                                )
                            }
                            </div>
                        </div>
                        }
                        {/* New Product */}
                        { this.state.productNewData.length === 0 ? (
                            <div/>
                        ):(
                            <div className="product product-new p-3">
                                <div className="d-flex bd-highlight">
                                    <div className="px-2 py-0  bd-highlight">
                                        <h4 className="m-0">สินค้าใหม่</h4>
                                    </div>
                                    <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                        <a className="text-blue" href={'/products/'+this.state.store_id+'?type=new'}>
                                            <u> เพิ่มเติม </u>
                                        </a>
                                    </div>
                                </div>
                                <div className="row m-0 mt-2 w-100">
                                { this.state.productNewData.map((row,key) => (
                                    <div className="col-6 pl-0 mb-3" key={key}>
                                        <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                                            {
                                                row.point=== undefined || row.point=== '' || row.point=== null ? (
                                                    <></>
                                                ):(
                                                    <div className="flag">
                                                        <img src="/assets/images/icon-flag.svg" alt=""/>
                                                        <div className="flag-detail text-center p-1">
                                                            <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                            <p>POINTS</p>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            <div className="text-center w-100">
                                                <img className="img-thumb-shop" src={row.product_img===''? '' : row.product_img} alt=""/>
                                            </div>
                                            <div className="product-item-detail px-3 w-100 text-left">
                                                <h4 className="m-0 product-name">{row.productname}</h4>
                                                <p className="m-0 product-cat-name">{row.catname}</p>
                                            </div>
                                            <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                                <div className="mt-auto">
                                                    {
                                                        row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null || row.pricebeforediscount === '0' ? (
                                                            <small className="d-block"></small>
                                                        ):(
                                                            <small className="d-block product-discount-price"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                        )
                                                    }
                                                    
                                                    <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                                </div>
                                                <div>
                                                    <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                        <div className="product-add rounded-circle text-center" style={addBtnBackground}>
                                                            <svg className="w-50" xmlns="http://www.w3.org/2000/svg" width="49.299" height="50.683" viewBox="0 0 49.299 50.683">
                                                                <g transform="translate(-398.932 -2712.768)">
                                                                    <line style={addColor} y2="45.683" transform="translate(423.582 2715.268)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                    <line style={addColor} x2="44.299" transform="translate(401.432 2737.417)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        )}

        {this.state.categoryData.length === 0 ? (
            <></>
        ):(
                        <div className="shop-home-cat shadow pl-3 py-2 mt-2">
                            <div className="d-flex bd-highlight ">
                                <div className="px-2 py-0  bd-highlight">
                                    <h4 className="m-0">หมวดหมู่</h4>
                                </div>
                                <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                    <a className="text-blue" href={'/product/category'}>
                                        <u> เพิ่มเติม </u>
                                    </a>
                                </div>
                            </div>
            
                            <div className="row w-100 m-0 text-center">
                {

                    
                this.state.categoryData.map((row,key) => (
                             
                                <div className="category-card-size p-2" key={key}>
                                    <a href={'/products_incategory/'+this.state.store_id+'/'+row.cateid}>
                                        <div className="cat-card shadow">
                                            <img className="img-thumb-shop" src={row.product_img} alt=""/>
                                            <p className="p-1 m-0 fw-600">{row.catename}</p>
                                        </div>
                                    </a>
                                </div>
                ))
                }
                
                              
                            </div>
                        </div>
        )}

        {this.state.data.length === 0 ? (
            <></>
        ):(
                        <div className="product product-all p-3">
                            <div className="d-flex bd-highlight">
                                <div className="px-2 py-0  bd-highlight">
                                    <h4 className="m-0">สินค้าทั้งหมด</h4>
                                </div>
                                <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                    <a className="text-blue" href={"/products/"+this.state.store_id}>
                                        <u> เพิ่มเติม </u>
                                    </a>
                                </div>
                            </div>
                            <div className="row m-0 mt-2 w-100">
                {

                    
                    this.state.data.map((row,key) => (
                                
                                <div className="col-6 pl-0 mb-3" key={key}>
                                    <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                            {
                                    row.point=== undefined || row.point=== '' || row.point=== null ? (
                                            <></>
                                        ):(
                                            <div className="flag">
                                                <img src="/assets/images/icon-flag.svg" alt=""/>
                                                <div className="flag-detail text-center p-1">
                                                    <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                    <p>POINTS</p>
                                                </div>
                                            </div>
                                    )
                            }
                                        
                                        <div className="text-center w-100">
                                            <img className="img-thumb-shop" src={row.product_img===''? '' : row.product_img} alt=""/>
                                        </div>
                                        <div className="product-item-detail px-3 w-100 text-left">
                                            <h4 className="m-0 product-name">{row.productname}</h4>
                                            <p className="m-0 product-cat-name">{row.catname}</p>
                                        </div>
                                        <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                            <div className="mt-auto">
                                                {
                                                    row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null || row.pricebeforediscount === '0' ? (
                                                        <small className="d-block"></small>
                                                    ):(
                                                        <small className="d-block product-discount-price"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                    )
                                                }
                                                
                                                <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                            </div>
                                            <div>
                                                
                                                {/* <div className="product-add rounded-circle text-center" onClick={(e) => this.handleToCart(row.productid, e)}>
                                                    <img className="w-50" src="/assets/images/icon-add.svg" alt="" />
                                                </div> */}
                                                <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                    <div className="product-add rounded-circle text-center" style={addBtnBackground}>
                                                        <svg className="w-50" xmlns="http://www.w3.org/2000/svg" width="49.299" height="50.683" viewBox="0 0 49.299 50.683">
                                                            <g transform="translate(-398.932 -2712.768)">
                                                                <line style={addColor} y2="45.683" transform="translate(423.582 2715.268)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                <line style={addColor} x2="44.299" transform="translate(401.432 2737.417)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </a>
                                            

                                            </div>
                                        </div>
                                    </div>
                                </div>
                    ))
                }
                                
                            </div>
                        </div>
        )}


        {this.state.recommendData.length === 0 ? (
            <></>
        ):(
            
                        <div className="product product-for-u px-3 "> {/*product-for-u*/}
                            <div className="d-flex bd-highlight">
                                <div className="px-2 py-0  bd-highlight">
                                    <h4 className="m-0">สินค้าแนะนำสำหรับคุณ</h4>
                                </div>
                                <div className="ml-auto px-2 pr-3 mt-1 bd-highlight ">
                                    <a className="text-blue" href={"/products_recommend/"+this.state.store_id}>
                                        {/* <u> เพิ่มเติม </u> */}
                                    </a>
                                </div>
                            </div>
                            <div className="row m-0 mt-2 w-100">
                            {

                    
                                this.state.recommendData.map((row,key) => (
                                            
                                            <div className="col-6 pl-0 mb-3" key={key}>
                                                <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                                        {
                                                row.point=== undefined || row.point=== '' ? (
                                                        <></>
                                                    ):(
                                                        <div className="flag">
                                                            <img src="/assets/images/icon-flag.svg" alt=""/>
                                                            <div className="flag-detail text-center p-1">
                                                                <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                                <p>POINTS</p>
                                                            </div>
                                                        </div>
                                                )
                                        }
                                                    
                                                    <div className="text-center w-100">
                                                        <img className="img-thumb-user" src={row.product_img===''? '' : row.product_img} alt=""/>
                                                    </div>
                                                    <div className="product-item-detail px-3 w-100 text-left">
                                                        <h4 className="m-0 product-name">{row.productname}</h4>
                                                        <p className="m-0 product-cat-name">{row.catname}</p>
                                                    </div>
                                                    <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                                        <div className="mt-auto">
                                                            {
                                                                row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null || row.pricebeforediscount === '0' ? (
                                                                    <small className="d-block"></small>
                                                                ):(
                                                                    <small className="d-block product-discount-price"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                                )
                                                            }
                                                            
                                                            <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                                        </div>
                                                        <div>
                                                        <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                            <div className="product-add rounded-circle text-center" style={addBtnBackground}>
                                                                <svg className="w-50" xmlns="http://www.w3.org/2000/svg" width="49.299" height="50.683" viewBox="0 0 49.299 50.683">
                                                                    <g transform="translate(-398.932 -2712.768)">
                                                                        <line style={addColor} y2="45.683" transform="translate(423.582 2715.268)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                        <line style={addColor} x2="44.299" transform="translate(401.432 2737.417)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                        </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                ))
                            }
                            </div>
                        </div>

                        
        )
    }
                        <br/>
                        
                        <br/>
                        <br/>


                    
                    </div>
                    {/* <div className="add">
                        <a href={'line://app/'+config.liff.chat.id+'?u='+this.state.userid+'&s='+this.state.store_id}  className="btn btn-add btn-chat rounded-circle text-center shadow">
                            <svg className="img-border" xmlns="http://www.w3.org/2000/svg" width="96.365" height="74.943" viewBox="0 0 96.365 74.943">
                                <g id="Group_2407" data-name="Group 2407" transform="translate(-124.5 -2252.606)">
                                    <path className="img-border-color-stroke" id="Path_102" data-name="Path 102" d="M2920.721,2277.168" transform="translate(-2741.565 35.588)" fill="none" stroke="#000" stroke-width="5"/>
                                    <g id="Group_2451" data-name="Group 2451" transform="translate(2037.24 1767.789)">
                                    <g id="Group_2448" data-name="Group 2448" transform="translate(-1907.594 485.317)">
                                        <g id="Group_2447" data-name="Group 2447" transform="translate(0)">
                                        <path className="img-border-color-stroke" id="Path_5870" data-name="Path 5870" d="M-1862.682,520.545h-31.924a16.17,16.17,0,0,1-8.672,5.608,29.044,29.044,0,0,0,2.689-5.608h-2.4a3.41,3.41,0,0,1-3.322-3.472V490.789a3.41,3.41,0,0,1,3.322-3.472h40.306a3.41,3.41,0,0,1,3.322,3.472v26.284A3.41,3.41,0,0,1-1862.682,520.545Z" transform="translate(1906.31 -487.317)" fill="none" stroke="#000" stroke-width="1"/>
                                        </g>
                                    </g>
                                    <g id="Group_2450" data-name="Group 2450" transform="translate(-1911.24 489.428)">
                                        <g id="Group_2449" data-name="Group 2449" transform="translate(0 0)">
                                        <path className="img-border-color-stroke" id="Path_5871" data-name="Path 5871" d="M-1899.536,524.478h31.924a3.41,3.41,0,0,0,3.322-3.472V494.722a3.41,3.41,0,0,0-3.322-3.472h-40.306a3.41,3.41,0,0,0-3.322,3.472v26.284a3.41,3.41,0,0,0,3.322,3.472h2.4" transform="translate(1911.24 -491.25)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="3"/>
                                        <path className="img-border-color-stroke" id="Path_5872" data-name="Path 5872" d="M-1902.9,553.17a29.05,29.05,0,0,1-2.688,5.608,16.169,16.169,0,0,0,8.672-5.608" transform="translate(1908.622 -519.942)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        </g>
                                    </g>
                                    <circle className="img-border-color-stroke" id="Ellipse_409" data-name="Ellipse 409" cx="3" cy="3" r="3" transform="translate(-1901.24 502.317)" fill="#fff"/>
                                    <g id="Group_2452" data-name="Group 2452" transform="translate(1 -2)">
                                        <path className="img-border-color-stroke" id="Path_5873" data-name="Path 5873" d="M-1820.534,555.7h-50.9a5.307,5.307,0,0,1-5.291-5.292V510.361a5.307,5.307,0,0,1,5.291-5.291h64.259a5.306,5.306,0,0,1,5.291,5.291v40.045a5.307,5.307,0,0,1-5.291,5.292H-1811" transform="translate(-15.996 -8.226)" fill="none" stroke="#000" stroke-width="1"/>
                                        <path className="img-border-color-stroke" id="Path_5874" data-name="Path 5874" d="M-1762.482,599.415a43.218,43.218,0,0,0,4.292,8.543,25.9,25.9,0,0,1-13.827-8.543" transform="translate(-64.513 -51.944)" fill="none" stroke="#000" stroke-width="1"/>
                                    </g>
                                    <path className="img-border-color-stroke" id="Path_5875" data-name="Path 5875" d="M-1824.167,559.884h-50.9a5.307,5.307,0,0,1-5.291-5.291V514.548a5.307,5.307,0,0,1,5.291-5.292h64.258a5.307,5.307,0,0,1,5.292,5.292v40.045a5.307,5.307,0,0,1-5.292,5.291h-3.828" transform="translate(-14.312 -10.166)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="3"/>
                                    <path className="img-border-color-stroke" id="Path_5876" data-name="Path 5876" d="M-1766.115,603.6a43.228,43.228,0,0,0,4.292,8.543,25.892,25.892,0,0,1-13.826-8.543" transform="translate(-62.829 -53.883)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    <circle className="img-border-color-stroke" id="Ellipse_405" data-name="Ellipse 405" cx="5.5" cy="5.5" r="5.5" transform="translate(-1880.24 518.317)" fill="#fff"/>
                                    <path className="img-border-color-stroke" id="Path_6935" data-name="Path 6935" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1862.24 518.317)" fill="#fff"/>
                                    <path className="img-border-color-stroke" id="Path_6936" data-name="Path 6936" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1844.24 518.317)" fill="#fff"/>
                                    </g>
                                </g>
                            </svg>
                            <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="94.365" height="72.944" viewBox="0 0 94.365 72.944">
                                <g id="Group_2451" data-name="Group 2451" transform="translate(1911.24 -485.317)">
                                    <g id="Group_2448" data-name="Group 2448" transform="translate(-1907.594 485.317)">
                                    <g id="Group_2447" data-name="Group 2447" transform="translate(0)">
                                        <path className="img-bg-color chat-bg" id="Path_5870" data-name="Path 5870" d="M-1862.682,520.545h-31.924a16.17,16.17,0,0,1-8.672,5.608,29.044,29.044,0,0,0,2.689-5.608h-2.4a3.41,3.41,0,0,1-3.322-3.472V490.789a3.41,3.41,0,0,1,3.322-3.472h40.306a3.41,3.41,0,0,1,3.322,3.472v26.284A3.41,3.41,0,0,1-1862.682,520.545Z" transform="translate(1906.31 -487.317)" fill="#da1111"/>
                                    </g>
                                    </g>
                                    <g id="Group_2450" data-name="Group 2450" transform="translate(-1911.24 489.428)">
                                    <g id="Group_2449" data-name="Group 2449" transform="translate(0 0)">
                                        <path className="img-bg-color" id="Path_5871" data-name="Path 5871" d="M-1899.536,524.478h31.924a3.41,3.41,0,0,0,3.322-3.472V494.722a3.41,3.41,0,0,0-3.322-3.472h-40.306a3.41,3.41,0,0,0-3.322,3.472v26.284a3.41,3.41,0,0,0,3.322,3.472h2.4" transform="translate(1911.24 -491.25)" fill="none"/>
                                        <path className="img-bg-color" id="Path_5872" data-name="Path 5872" d="M-1902.9,553.17a29.05,29.05,0,0,1-2.688,5.608,16.169,16.169,0,0,0,8.672-5.608" transform="translate(1908.622 -519.942)" fill="none"/>
                                    </g>
                                    </g>
                                    <circle className="img-bg-color" id="Ellipse_408" data-name="Ellipse 408" cx="3" cy="3" r="3" transform="translate(-1900.24 500.317)"/>
                                    <path className="img-bg-color" id="Path_6938" data-name="Path 6938" d="M3,0A3,3,0,1,1,0,3,3,3,0,0,1,3,0Z" transform="translate(-1901.24 502.317)" fill="none"/>
                                    <g id="Group_2452" data-name="Group 2452" transform="translate(1 -2)">
                                    <path className="img-bg-colo chat-bg" id="Path_5873" data-name="Path 5873" d="M-1820.534,555.7h-50.9a5.307,5.307,0,0,1-5.291-5.292V510.361a5.307,5.307,0,0,1,5.291-5.291h64.259a5.306,5.306,0,0,1,5.291,5.291v40.045a5.307,5.307,0,0,1-5.291,5.292H-1811" transform="translate(-15.996 -8.226)" fill="#da1111"/>
                                    <path className="img-bg-color chat-bg" id="Path_5874" data-name="Path 5874" d="M-1762.482,599.415a43.218,43.218,0,0,0,4.292,8.543,25.9,25.9,0,0,1-13.827-8.543" transform="translate(-64.513 -51.944)" fill="#da1111"/>
                                    </g>
                                    <path className="img-bg-color" id="Path_5875" data-name="Path 5875" d="M-1824.167,559.884h-50.9a5.307,5.307,0,0,1-5.291-5.291V514.548a5.307,5.307,0,0,1,5.291-5.292h64.258a5.307,5.307,0,0,1,5.292,5.292v40.045a5.307,5.307,0,0,1-5.292,5.291h-3.828" transform="translate(-14.312 -10.166)" fill="none"/>
                                    <path className="img-bg-color" id="Path_5876" data-name="Path 5876" d="M-1766.115,603.6a43.228,43.228,0,0,0,4.292,8.543,25.892,25.892,0,0,1-13.826-8.543" transform="translate(-62.829 -53.883)" fill="none"/>
                                    <circle className="img-bg-color" id="Ellipse_402" data-name="Ellipse 402" cx="5.5" cy="5.5" r="5.5" transform="translate(-1878.24 515.317)"/>
                                    <circle className="img-bg-color" id="Ellipse_403" data-name="Ellipse 403" cx="5.5" cy="5.5" r="5.5" transform="translate(-1860.24 515.317)"/>
                                    <circle className="img-bg-color" id="Ellipse_404" data-name="Ellipse 404" cx="5.5" cy="5.5" r="5.5" transform="translate(-1842.24 515.317)"/>
                                    <path className="img-bg-color" id="Path_6935" data-name="Path 6935" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1880.24 518.317)" fill="none"/>
                                    <path className="img-bg-color" id="Path_6936" data-name="Path 6936" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1862.24 518.317)" fill="none"/>
                                    <path className="img-bg-color" id="Path_6937" data-name="Path 6937" d="M5.5,0A5.5,5.5,0,1,1,0,5.5,5.5,5.5,0,0,1,5.5,0Z" transform="translate(-1844.24 518.317)" fill="none"/>
                                </g>
                                </svg>

                            CHAT
                        </a>

                    </div> */}
                    {/* end shophome */}

                    <Navigation active={'shop'}/>
            </div>
                 
        
        )
    }
}
export default Shop_home;
