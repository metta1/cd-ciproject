import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../config';

class Header extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        storeid : localStorage.getItem('storageStoreID'),
        colorData : [],
    };


    async componentDidMount(){
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.storeid)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
    }
    render() {
        const textColor = {
            color: '#' + this.state.colorData['coloricon']
        }
        return (
        <>
        <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
        
        <div className="head bg-black shadow">
            <div className="row w-100 h-100">
                <div className="col-8 h-100">
                    <div className="icon-back">
                        <a href={this.props.data.back_url !== undefined ? (this.props.data.back_url) : ("/shop/"+localStorage.getItem('storageStoreID'))}>
                            <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h1 className="m-0 mt-2 h" style={textColor}>SHOP</h1> 
                        <p className="" style={{whiteSpace:'nowrap'},textColor}>เลือกสินค้าของคุณได้เลย</p>
                    </div>
                </div>
                <div className="col-4 h-100">
                    {/*<div className="row float-right h-100 w-100">
                        <div className="col p-1 my-auto">
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                            </div>
                        </div>
                        <div className="col p-1 my-auto">
                            <div className="icon rounded-circle">
                                <a href="/notifications">
                                    <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>*/}
                    
                </div>
            </div>
        </div>
        </>)
    
        

    }
}
export default Header;
