import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <>
                
                <div className="footer mx-auto">
                   
                    <img className="w-100 position-relative" src="/assets/images/footer-shop.svg" alt=""/>
                    <div className="menu position-absolute w-100">
                        <div className="menu-item px-2 pt-2 ">
                            <a href="/home">
                                <img className="" src="/assets/images/icon-ft-shop-home.svg" alt=""/><br/>
                                <span>HOME</span>
                            </a>
                        </div>
                        <div className="menu-item px-2 pt-2">
    
                            <a href={'/store_feed/'+localStorage.getItem('storageStoreID')}>
                                <img className="position-relative" src="/assets/images/icon-ft-shop-feed.svg" alt=""/><br/>
                                <span>FEED</span>
                            </a>
                        </div>
                        <div className="menu-item px-2 pt-2">
                                <div className="menu-item-active bg-black rounded-circle shadow" style={{marginLeft:0+'%'}}>
                                    <div className="menu-item-active-ab">
                                        <a href={'/shop/'+localStorage.getItem('storageStoreID')}>
                                            <img className="" src="/assets/images/icon-ft-shop-shop.svg" alt=""/> <br/>
                                            <span>SHOP</span>
                                        </a>
                                    </div>
                                </div>
                        </div>
                        <div className="menu-item px-2 ">
                            <a href={'/deal/'+localStorage.getItem('storageStoreID')}>
                                <img className="" src="/assets/images/icon-ft-shop-deal.svg" alt=""/> <br/>
                                <span>DEAL</span>
                            </a>
        
                        </div>
                        <div className="menu-item px-2 pt-2">
                            <a href={'/account_shop/'+localStorage.getItem('storageStoreID')}>
                                <img className="position-relative" src="/assets/images/icon-ft-shop-account.svg" alt=""/> <br/>
                                <span>MY ACCOUNT</span>
                            </a>

                        </div>
                    </div>

        

                                

                </div>
        
            </>
        )
    
        

    }
}
export default Footer;
