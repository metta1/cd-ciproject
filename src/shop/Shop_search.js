import React, { Component} from 'react'
import Footer from './Footer'
import axios from 'axios'
import $ from 'jquery'
import {config} from '../config';
var QRCode = require('qrcode.react');
var ReactDOM = require('react-dom');
var Barcode = require('react-barcode');

class Shop_search extends Component {

    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        storeDetailData : [],
        store_id:localStorage.getItem('storageStoreID'),
        user_id : sessionStorage.getItem('user_id')
    };


    async componentDidMount(){
     
        const storeDetailRs = await axios.get(config.api.base_url+'/store/api/storedetail/'+this.state.store_id)
        if(storeDetailRs.status===200){
            this.setState({
                storeDetailData: storeDetailRs.data,
            });
        }



     

        
        //console.log(result)
        //this.setState({data: result.data});
        console.log(this.state.storeDetailData[0]['tel'])


        
        

        
    }

    
    handleSubmit = event => {
    
        
    }
   

    render() {
        return (
            <>
     
        <link rel="stylesheet" type="text/css" href="/assets/css/account_shop.css?v=3"/>
        <div className="container-mobile m-auto ">
            
            <div className="head bg-black shadow">
                <div className="row w-100 h-100">
                    <div className="col-12 h-100">
                        <div className="icon-back">
                                <a href={"/account_shop/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white">ค้นหา {this.state.storeDetailData[0]!== undefined ? this.state.storeDetailData[0]['storename'] : ''}</h4> 
                               
                        </div>
                       
                    </div>
                   
                </div>
            </div>
             {/* End Hend*/}
 
   

           

            <div className="shop_detail__wrapper">    
                <div className="link__box">
                    <div className="left">
                        <div className="tab"></div>
                        <img src="/assets/images/icon-marker.svg" alt="icon" className="icon"/>
                        <p className="title">ค้นหา {this.state.storeDetailData[0]!== undefined ? this.state.storeDetailData[0]['storename'] : ''}</p>
                    </div>
                    
                </div>
                <div className="detail__box ">
                    <div className="tab"></div>
                    <div className="content__wrapper">
            
                      
                        <a className="link__box">
                            <div className="box"><p>ที่อยู่ {this.state.storeDetailData[0] !== undefined ? (this.state.storeDetailData[0]['address']===null ? '-' : this.state.storeDetailData[0]['address']) : ''}</p></div>
                        </a>
                        <a className="link__box">
                            <div className="box"><p>ตำบล/แขวง {this.state.storeDetailData[0]!== undefined ? (this.state.storeDetailData[0]['subdistrict']===null ? '-' : this.state.storeDetailData[0]['subdistrict']) : ''}</p></div>
                        </a>
                        <a className="link__box">
                            <div className="box"><p>อำเภอ/เขต {this.state.storeDetailData[0]!== undefined ? (this.state.storeDetailData[0]['district']===null ? '-' : this.state.storeDetailData[0]['district']) : ''}</p></div>
                        </a>
                        <a className="link__box">
                            <div className="box"><p>จังหวัด {this.state.storeDetailData[0]!== undefined ? (this.state.storeDetailData[0]['province']===null ? '- ' : this.state.storeDetailData[0]['province'])+'  รหัสไปรษณี '+(this.state.storeDetailData[0]['zipcode']===null ? '- ' : this.state.storeDetailData[0]['zipcode']) : ''}</p></div>
                        </a>
                        
                       
                    <div>
                    </div>
                    </div>
                </div>
            </div>
            
            <br/>
        <br/>
        <br/>
        <br/>
        </div>
       
  

       </>
      


        )
    }
}
 
export default Shop_search;
