import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Header from './Header'
import Footer from './Footer'
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import qs from 'qs'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');


class Cart extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
 
    }


    state = {
        data : [],
        store_id : localStorage.getItem('storageStoreID'),
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        productType:getParam('type'),
        colorData : [],
        productName: "",
        variantExists: true,
        variantName: ""
    };


    async componentDidMount(){
        
        const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }
        if(this.state.colorData !== undefined){
            let colortabbar   = this.state.colorData['colortabbar'];
            if(colortabbar){
                $('.head' ).css( "color", "#"+colortabbar );
            }
        }
        console.log(this.state.colorData)
        
        
        const result = await axios.get(config.api.base_url+'/order/api/cart/'+localStorage.getItem('storageStoreID')+'/'+this.state.user_id);
        //const result = await axios.get('http://dev.backend.ho.com/order/api/cart/'+localStorage.getItem('storageStoreID')+'/'+this.state.user_id);
        if(result.status===200){
            this.setState({data: result.data});
        }
        console.log(this.state.data)
      
        this.handleChangeVariant = this.handleChangeVariant.bind(this);

        /***********************************/
        $(document).on('click', ".group-quantity .btn-quantity", function() {
            var totalPrice = parseFloat($('#totalPrice').val());
            var detailID = $(this).data('detail');
            var actions = $(this).data('action');
            var variantstock = $(this).data('stock');
            var value   = parseInt($('.group-quantity[data-detail="'+detailID+'"').find('.input-quantity').val());
            var unitPrice   = parseFloat($('li[data-detail="'+detailID+'"] .unitPrice').val());
            var quantity = 0;
           
            if(actions=='minus' && value>1){
                quantity = value-1;
                var newTotalPrice = totalPrice-unitPrice;
            }else if(actions=='plus'){
                quantity = value;
                var newTotalPrice = totalPrice;
                if (value < variantstock) {
                    quantity = value+1;
                    newTotalPrice = totalPrice+unitPrice;
                } else {
                    Swal.fire({
                        title: "ขออภัย",
                        html: "สินค้าเหลือแค่ "+variantstock+" ชิ้น",
                        icon: "error",
                        confirmButtonText: "ตกลง",
                        customClass: {
                          confirmButton: "btn-black",
                        },
                    });
                    quantity = variantstock
                    newTotalPrice = unitPrice*quantity;
                }
            }else if(value===1){
                quantity = 1
                var newTotalPrice = totalPrice;
            }else{
                var newTotalPrice = 0;
            }
            $('.group-quantity[data-detail="'+detailID+'"').find('.input-quantity').val(quantity);
            $('#totalPrice').val(newTotalPrice);
            var totalPriceShow = newTotalPrice;
            $('#totalPriceShow').html((totalPriceShow.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")));
            let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('quantity',quantity)
                bodyFormData.set('detailid',detailID)
            
            axios({
                    method: 'post',
                    url: config.api.base_url+'/order/api/updatecart',
                    //url: 'http://dev.backend.ho.com/order/api/updatecart',
                    data: bodyFormData,
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' }
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        console.log(response)
                    })
                    .catch(function (response) {
                        //handle error
                        //console.log(JSON.stringify(response));
            });

        });

        $('.cart-delete').click(function(){
            var detailID = $(this).data('detail');
            var colorBtn = $('.head').css("color");
            Swal.fire({
              title: 'ต้องการลบสินค้าใช่หรือไม่',
              html: '',
              type: 'question',
              showCancelButton: true,
              reverseButtons :true,
              confirmButtonText: 'ตกลง',
              confirmButtonColor: colorBtn,
              cancelButtonText: 'ยกเลิก',
              onOpen: () => document.activeElement.blur()
            }).then((result) => {
              console.log(result)
              if (result.value) {
                let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('detailid',detailID)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/order/api/deleteCart',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        if(response.status==200){
                            //$('.cart-product li[data-detail="'+response.data.detailid+'"').remove();
                            window.location.reload();
                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถลบสินค้าได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                }
                            }).then((result) => {
                                window.location.reload();
                            })
                        }
                      
                        console.log(response)
                    })
                    .catch(function (response) {
                        console.log(response)
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-black',
                            },
                        })
                        
                       
                });
             
              }
            })
            
        });
        
       
        if (this.state.data.cartDetail !== undefined) {
            for (let i=0; i<this.state.data.cartDetail.length; i++) {
                let productid = this.state.data.cartDetail[i]['productid'];
                let variantid = this.state.data.cartDetail[i]['variantid'];
                let productName = "";
                let isVariantExists = true;
                let variantName = "";

                const checkproductvariant = await axios.get(config.api.base_url+'/store_product/api/checkproductvariant/'+productid+'/'+variantid)
                if(checkproductvariant.status===200){
                    if (checkproductvariant.data[0]['variant_active'] === '1') {
                        isVariantExists = false;
                        variantName = checkproductvariant.data[0]['color'];
                        productName = checkproductvariant.data[0]['productname'];
                        this.setState({variantName: variantName});
                        this.setState({productName: productName});
                    }
                }

                if (!isVariantExists) {
                    this.setState({'variantExists' : false });
                    break;
                } else {
                    this.setState({'variantExists' : true });
                }

            }
        }


        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

  

    handleChangeVariant =  (variant_id,event) => {
        event.preventDefault();
       
        if(variant_id!==null){
            $(".product-variant li").removeClass('active')
            this.setState({variant_id: variant_id});
            event.target.parentNode.classList.add('active')
            //event.target.classList.add('active')
            
        }
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
        
    handleToCart = (proid,event) => {
        event.preventDefault();
        let user_id    = sessionStorage.getItem('user_id');
        let variant_id = this.state.variant_id;
       
 
        if((variant_id ===null || variant_id==='') && (this.state.data.variant != '')){
            Swal.fire({
                title : 'ขออภัยค่ะ',
                html: 'กรุณาเลือกตัวเลือกสินค้า ก่อนสั่งซื้อ',
                icon:'info',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
        }else if(proid!==null && user_id!==null){
                    let user_id = sessionStorage.getItem('user_id');
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',user_id)
                    bodyFormData.set('productid',proid)
                    bodyFormData.set('variantid',variant_id)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/order/api/addToCart',//config.api.base_url+
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                        
                            if(response.status==201){
                                //window.location('/cart');
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    customClass: 'swal-mobile',
                                    title: 'เพิ่มไปยังตะกร้าแล้ว',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    
                                });
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-black',
                                    },
                                })
                            }
                            console.log(response)
                        })
                        .catch(function (response) {
                            //console.log(response)
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-black',
                                },
                            })
                            
                           
                    });

            

        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                },
            })
        }
    }
    
    handleSubmit = event => {
        if (!this.state.variantExists) {
            Swal.fire({
                title : 'ขออภัย',
                html: 'ไม่มีสินค้า '+this.state.productName+' ตัวเลือก '+this.state.variantName+' นี้แล้ว',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-black',
                }
            })
        } else {
            if(this.state.data === undefined || this.state.data===''){
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'คุณไม่มีสินค้าในตะกร้า',
                    icon:'info',
                    confirmButtonText:'ไปเลือกสินค้า',
                    customClass: {
                        confirmButton: 'btn-black',
                    },
                }).then((result) => {
                    if (result.value) {
                        
                        if(localStorage.getItem('storageStoreID')=== undefined || localStorage.getItem('storageStoreID')=== 'null'){
                            localStorage.removeItem('storageStoreID')
                            window.location='/user_card'
                        }else{
                            window.location='/shop/'+localStorage.getItem('storageStoreID')
                        }
                    
                    }
                })
            }else{
                window.location='/cart_list'
            }
        }
     
        
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-8 h-100">
                            <div className="icon-back">
                                <a href={"/shop/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5">
                                <h4 className="mt-3 text-white" style={{fontWeight:600}}>ตะกร้าสินค้าของคุณ</h4> 
                               
                            </div>
                        </div>
                        <div className="col-4 h-100">
                            {/*<div className="row float-right h-100 w-100">
                                <div className="col p-1 my-auto">
                                </div>
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                    </div>
                                </div>
                                <div className="col p-1 my-auto">
                                    <div className="icon rounded-circle">
                                        <a href="/notifications">
                                            <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>*/}
                            
                        </div>
                    </div>
                </div>

    
           
           <div className="cart-detail mb-5">
               <div className="row mt-2 m-0 w-100">
                   <div className="col-12 p-0">
                       <div className="mt-0">

                       {
            this.state.data.cartDetail === undefined ? (
                <div/>
            ) : (
                                    
                                    <ul className="cart-product">
                    {
                   
                        this.state.data.cartDetail.map((row,key) => (
                            row.stock>0 ? (
                                <li key={key} className=""  >
                                <div className="row">
                                    <div className="col-5 pr-0">
                                        <img className="img-fluid img-product" src={row.product_img}/>
                                    </div>
                                    <div className="col-7">
                                        <div class="product-item-detail pt-2 pb-2 pr-2">
                                            <h4 class="m-0">Nike Tiempo Legend 8 Academy MG FG AG</h4>
                                            <h5 className="m-0"><NumberFormat value={this.state.data['price']} displayType={'text'} thousandSeparator={true}/> THB</h5>
                                            <div className="row">
                                                <div className="col-6">
                                                    <h5 className="mt-1">ตัวเลือก
                                                        <div>สีเขียว</div>
                                                    </h5>
                                                </div>
                                                <div className="col-6 p-0">
                                                    <h5 className="mt-1">
                                                        <div className="input-group">
                                                            <div className="input-group-prepend">
                                                                <span className="input-group-text">-</span>
                                                            </div>
                                                            <input type="text" class="form-control" readOnly  value={row.quantity}/>
                                                            <div className="input-group-append">
                                                                <span className="input-group-text">+</span>
                                                            </div>
                                                        </div>
                                                    </h5>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                </li>
                            ) : (
                                <li key={key} className="shadow" data-detail={row.detailid} >
                                <div className="row pl-3 py-3 w-100">
                                    <div className="col-5 pr-3">
                                        <div className="cart-flag text-center">
                                            <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                            <p>POINTS</p>
                                        </div>
                                        <img className="img-fluid img-product" src={row.product_img}/>
                                    </div>
                                    <div className="col-7 pl-0">
                                        <div class="product-item-detail pr-2">
                                        <button className="cart-delete" data-detail={row.detailid}>
                                            <img src="/assets/images/icon_x.png" alt=""/>
                                        </button>
                                            <h4 class="m-0 cart-item-title">{row.productname}</h4>
                                            <h5 className="m-0">
                                                <NumberFormat value={row.unitprice} displayType={'text'} thousandSeparator={true}/> THB
                                                <input type="hidden" className="unitPrice" value={parseFloat(row.unitprice)}/>
                                            </h5>
                                            <div className="row">
                                                <div className="col-6 pr-1">
                                        {
                                            row.variantname=== null ? (
                                                <></>
                                            ) : (
                                                
                                                    <h5 className="mt-1">ตัวเลือก
                                                        <div>{row.variantname}</div>
                                                    </h5>
                                                
                                            )
                                        }
                                                </div>
                                                <div className="col-6 p-0 col-group-quantity">
                                                    <h5 className="mt-1">
                                                        <div className="input-group group-quantity" data-detail={row.detailid}>
                                                            <div className="input-group-prepend btn-quantity" data-action="minus" data-detail={row.detailid} data-stock={row.variantstock}>
                                                                <span className="input-group-text">-</span>
                                                            </div>
                                                            <input type="text" className="form-control input-quantity quantity-font" data-detail={row.detailid} readOnly  value={row.quantity}/>
                                                            <div className="input-group-append btn-quantity" data-action="plus" data-detail={row.detailid} data-stock={row.variantstock}>
                                                                <span className="input-group-text">+</span>
                                                            </div>
                                                        </div>
                                                    </h5>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                </li>
                            )
       
                        )
                        )
                    }
                                        
                                    </ul>
            )
        }
              
                       </div>
                   </div>
               </div>
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                   
                  
                <div className="footer footer-product-detail">
                    <div className="row m-0">
                        <div className="col-6 mt-2">
                            <div className="detail">รวมทั้งหมด :</div>
                            <div className="detail display-price"><NumberFormat value={parseFloat(this.state.data['totalPrice']).toFixed(2)} displayType={'text'} thousandSeparator={true} id="totalPriceShow"/> THB</div>
                            <input type="hidden" value={parseFloat(this.state.data['totalPrice'])} id="totalPrice"/>
                        </div>
                        <div className="col-6 p-3">
                            <button className="btn btn-light btn-col p-0 buy-btn" onClick={(e) => (this.handleSubmit(e))}>ซื้อสินค้า</button>
                        </div>
                    </div>
                </div>
                    
            </div>
                 
        
        )
    }
}
export default Cart;
