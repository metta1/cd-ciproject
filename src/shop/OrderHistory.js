import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery'
import NumberFormat from 'react-number-format';
import {config} from '../config';
import Swal from 'sweetalert2'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext,Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import DateCountdown from 'react-date-countdown-timer';
import {getParam} from '../lib/Helper';
import Moment from 'moment';
import 'moment/min/moment-with-locales' 
import Store_color from '../template/Store_color'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
let user_id = sessionStorage.getItem('user_id');


class Coupon extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    
 
    }


    state = {
        data : [],
        colorData : [],
        store_id:this.props.match.params.store_id,
        product_id: this.props.match.params.product_id,
        variant_id:'',
        user_id : user_id,
        deleveryid: localStorage.getItem('deleveryid') === null ? '2' : localStorage.getItem('deleveryid'),
        colortabbar: '',
        coloricon: '',
        colorfont: '',
    };


    async componentDidMount(){
        

        /* const result = await axios.get(config.api.base_url+'/store_order/api/order/'+localStorage.getItem('storageStoreID')+'/'+sessionStorage.getItem('user_id')+'/1');
        if(result.status===200){
            this.setState({data: result.data});
        } */
        
        

        const colorresult = await axios.get(config.api.base_url+'/store/api/storeColor/'+localStorage.getItem('storageStoreID'))
        if(colorresult.status===200){
            this.setState({colorData: colorresult.data});
        }

        console.log(this.state.colorData);
        if(this.state.colorData !== undefined){
            let colortabbar = this.state.colorData['colortabbar'];
            let coloricon   = this.state.colorData['coloricon'];
            let colorfont   = this.state.colorData['colorfont'];
            this.setState({colortabbar: colortabbar});
            this.setState({coloricon: coloricon});
            this.setState({colorfont: colorfont});
        }
    

        this.activeStatus(this,1);

        
     
     
        
    }

  

   
    handleChange = (event,key) => {
        //console.log(event.target.name)
        localStorage.setItem('deleveryid',event.target.value);
        this.setState({'deleveryid':event.target.value});
        
        
        
    }
    
        
    useCoupon(e,couponid){
        window.location='/cart_list?couponid='+couponid;
    }

    async activeStatus(e,statusID) {
        statusID = statusID===2 ? '2-3' : statusID;

        $('.nav-order-historty .nav-link').click(function(){
            $('.nav-order-historty .nav-link').removeClass('active');
            $(".nav-link" ).css( "background-color", "#ddd");
            $(this).addClass('active')
        })

        const result =  await axios.get(config.api.base_url+'/store_order/api/order/'+localStorage.getItem('storageStoreID')+'/'+sessionStorage.getItem('user_id')+'/'+statusID);
        if(result.status===200){
            this.setState({data: result.data});
        }else{
            this.setState({data: []});
        }
        this.setState({statusActive: statusID});
        $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#"+this.state.colortabbar);
        $('.btn.btn-primary').attr('style', 'background-color:#'+this.state.colortabbar+'!important;'+
                                            'border-color:#'+this.state.colortabbar+'!important;'+
                                            'color:#'+this.state.colorfont+'!important;')
        //console.log(this.state.data)
        //console.log(this.state.statusActive)
        
        
    }

    handlePayment = (e,orderid) => {
        window.location="/order_detail/"+orderid;
    }
    
    handleSubmit = event => {
    
        Swal.fire({
            title : 'ขออภัยค่ะ',
            html: 'ระบบกำลังพัฒนา',
            icon:'info',
            confirmButtonText:'ตกลง',
            customClass: {
                confirmButton: 'btn-black',
            },
        })
    }
    
    render() {

        return (
          
            <div className="container-mobile m-auto shop-page">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
        
                <div className="head bg-black shadow">
                    <div className="row w-100 h-100">
                        
                        <div className="col-12 h-100">
                            <div className="icon-back">
                                <a href={"/account_shop"}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            
                            <div className=" h-100 p-3 pl-5 mt-1">
                                <h3 className="mt-2">แจ้งเตือนของคุณ</h3>
                               
                            </div>
                        </div>
                        
                    </div>
                </div>

            
            <div className="search-bar">
               
                <div className="col-12 p-0">
                    <div className="table-responsive">
                        <ul class="nav nav-pills nav-fill nav-order-historty">
                            <li class="nav-item" onClick={(e) => this.activeStatus(e,1)}>
                                <a class="nav-link active" href="#">สินค้าที่ต้องชำระ</a>
                            </li>
                            <li class="nav-item" onClick={(e) => this.activeStatus(e,2)}>
                                <a class="nav-link" href="#">สินค้าที่ต้องจัดส่ง</a>
                            </li>
                            <li class="nav-item" onClick={(e) => this.activeStatus(e,4)}>
                                <a class="nav-link" href="#">สินค้าที่ต้องได้รับ</a>
                            </li>
                            <li class="nav-item" onClick={(e) => this.activeStatus(e,6)}>
                                <a class="nav-link" href="#">สินค้าที่จำหน่ายสำเร็จ</a>
                            </li>
                            <li class="nav-item" onClick={(e) => this.activeStatus(e,5)}>
                                <a class="nav-link" href="#">สินค้าที่ถูกยกเลิก</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
            </div>
           
           <div className="cart-detail mb-5 ">
               
               <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                   <div className="col-12 p-0">
                       <div className="">
                {
                        this.state.data === undefined ? (
                             <div/>
                        ) : (

                            
                                    
                                    <ul className="cart-product">
                                {
                                    this.state.data.map((row,key) => (
                                        <li className="product-item-detail order-list" key={key} onClick={(e) => this.handlePayment(e,row.orderid)}>
                                            <div className="container">
                                            <div className="row">
                                                <div className="col-3 ">
                                                    <div class="w-100 text-center   pt-2 pb-2">
                                                        <div class="coupon-detail-img rounded-circle mx-auto  border">
                                                            <div className="">
                                                            <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                        </div>
                                                            
                                                    </div>
                                                </div>
                                                <div className="col-9 mx-auto pt-4 p-0">
                                                    <h3 className="mb-1">{row.storename}</h3>
                                                </div>
                                                
                                            </div>  
                                            <hr className="mb-0 mt-0"/>
                                            <div className="row">
                                                <div className="col-3 ">
                                                    <div class="w-100 text-center   pb-2 pt-2">
                                                        <img class="img-fluid img-product" src={row.productimg==='' ? '/assets/images/slide.png' : row.productimg}/>
                                                            
                                                    </div>
                                                </div>
                                                <div className="col-9 mx-auto pt-2 p-0">
                                                    <h3 className="mb-1 text-gray">{row.productname} X{row.amount}</h3>
                                                    <h4 className="mb-1">ORDER ID : {row.orderno}</h4>
                                                    <h4 className="mb-1 text-gray">{Moment(row.orderdate).format('D MMM YYYY H:mm')}</h4>
                                                    <h3 className="display-price"><NumberFormat value={row.totalprice} displayType={'text'} thousandSeparator={true}/> THB</h3>
                                                     
                                                </div>
                                                
                                            </div>  
                                            <hr className="mb-0 mt-0"/>
                                            <div className="row mb-2 mt-2">
                                            {
                                                    (() => {
                                                        switch (this.state.statusActive) {
                                                            case 1:   
                                                                return (
                                                                <>
                                                                <div className="col-8">
                                                                    <h4 className="text-gray">{row.paymenttypename}</h4>
                                                                    {
                                                                        row.paymenttypeid==2 || row.paymenttypeid ==3 || row.paymenttypeid==0 ? (
                                                                            <h4 className="text-gray">จะได้รับการยืนยันจากทางร้าน</h4>
                                                                        ) : (
                                                                            <h4 className="text-gray">ชำระเงินภายในวันที่ {Moment(row.orderdate).add(1, 'days').format('d MMM YYYY H:mm')}</h4>
                                                                        )
                                                                    }
                                                                </div>
                                                                <div className="col-4 pl-0 text-right">
                                                                    {
                                                                        row.paymenttypeid==2 || row.paymenttypeid ==3 || row.paymenttypeid==0 ? (
                                                                            <button type="button" className="btn btn-primary pl-2 pr-2 p-1 text-bold" onClick={(e) => this.handlePayment(e,row.orderid)}>สินค้ารอการยืนยัน</button>
                                                                        ) : (
                                                                            <button type="button" className="btn btn-primary pl-2 pr-2 p-1 text-bold" onClick={(e) => this.handlePayment(e,row.orderid)}>สินค้ารอชำระ</button>
                                                                        )
                                                                    }
                                                                
                                                                    
                                                                </div>
                                                                </>);
                                                            default:      
                                                                return "";
                                                        }
                                                    })()
                                            }
                                            
                                                
                                            </div>
                                            </div>
                                        </li>
                                    )
                                    )
                                }
                                        
                                       
                                       
                                      
                                    </ul>
                        )
                }
                                    
        
              
                       </div>

                   </div>
                   
               </div>
              

               
               
               

 
                   <br/>
                   
                   <br/>
                   <br/>


               
           </div>
    


                
                    {/* <Store_color/> */}
            </div>
            
                 
        
        )
    }
}
export default Coupon;
