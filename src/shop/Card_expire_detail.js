import React, { Component } from 'react'
import Moment from 'moment';
import 'moment/locale/th';

export default class Card_expire_detail extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);   
        this.toggleInfo = this.toggleInfo.bind(this);   
    }

    state = {
        data : [],
        stampExpireDetail : this.props.stampExpireDetailData,
        
    };

    toggleInfo(){
        this.setState({ info: !this.state.info })
        console.log(this.state.info)
    }

    componentDidMount(){
        //console.log(this.state.info)
    }

    render() {
        const show = (this.state.info) ? "show" : "" ;
        const unshow = (!this.state.info) ? "show" : "" ;
        return (
            <div>
                <div className="pl-4 pr-4">
                <div className="discount-info shadow">
            
                    {
                        
                        this.state.stampExpireDetail.map((row,key) => {
                            
                            return (
                                key<5 ? (
                                    <div>
                                        <p className="m-0 font-info">{'* '+Moment(row.expiredate).add(543,'year').format('D MMM YYYY')+' แสตมป์จะหมดอายุ '+row.amount+' ดวง'}</p>
                                        {key==4 &&(
                                            <label className={"w-100 m-0 collapse" + unshow} type="button" onClick={ this.toggleInfo }>
                                                <hr className="mt-1 mb-0"/>
                                                <p className="my-1 font-info text-center">
                                                    เพิ่มเติม
                                                    <img src="/assets/images/down.png" alt="down icon" className="center-img"/>
                                                </p>
                                                
                                            </label>
                                           
                                        )}
                                    </div>
                                ) : (
                                    <div>
                                    <div className={"collapse" + show}>
                                        <div className="">
                                            <p className="m-0 font-info">{'* '+Moment(row.expiredate).add(543,'year').format('D MMM YYYY')+' แสตมป์จะหมดอายุ '+row.amount+' ดวง'}</p>
                                        </div>
                                        
                                    </div>
                                    {
                                        (key+1)==this.state.stampExpireDetail.length &&(
                                            <div style={this.state.info===true ? ({display:'block'}) : ({display:'none'})}>
                                            <hr className="my-1"/>
                                                <label className="w-100 m-0" type="button" onClick={ this.toggleInfo }>
                                                <p className="mt-2 mb-0 font-info text-center">
                                                    <img src="/assets/images/up.png" alt="up icon" className="center-img"/>
                                                    กลับ
                                                </p>
                                            </label>
                                            </div>
                                        )

                                    }

                                    </div>
                                   
                                    
                                         
                                        
                                    

                                    
                                    
                                
                                )
                            
                            )
                        })
                    }
            
                </div>
            </div>
                
            </div>
        )
    }
}
