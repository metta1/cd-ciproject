import React, { Component} from 'react'
import Footer from './Footer'
import axios from 'axios'
import $ from 'jquery'
import {config} from '../config';
import Store_color from '../template/Store_color'

var QRCode = require('qrcode.react');
var ReactDOM = require('react-dom');
var Barcode = require('react-barcode');

class Account_shop extends Component {

    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        storeDetailData : [],
        store_id:localStorage.getItem('storageStoreID'),
        user_id : sessionStorage.getItem('user_id')
    };


    async componentDidMount(){
     
        const storeDetailRs = await axios.get(config.api.base_url+'/store/api/storedetail/'+this.state.store_id)
        if(storeDetailRs.status===200){
            this.setState({
                storeDetailData: storeDetailRs.data,
            });
        }



     

        
        //console.log(result)
        //this.setState({data: result.data});
        console.log(this.state.storeDetailData[0]['tel'])


        
        

        
    }

    
    handleSubmit = event => {
    
        
    }
   

    render() {
        return (
            <>
     
        <link rel="stylesheet" type="text/css" href="/assets/css/account_shop.css?v=3"/>
        <div className="container-mobile m-auto ">
            <Store_color/>
            
            <div className="head bg-black shadow">
                <div className="row w-100 h-100">
                    <div className="col-12 h-100">
                        <div className="icon-back">
                                <a href={"/account_shop/"+localStorage.getItem('storageStoreID')}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                        </div>
                        <div className=" h-100 p-3 pl-5 mt-1">
                                <h3 className="mt-2">เกี่ยวกับ {this.state.storeDetailData[0]!== undefined ? this.state.storeDetailData[0]['storename'] : ''}</h3>
                               
                        </div>
                       
                    </div>
                   
                </div>
            </div>
             {/* End Hend*/}
 
   

           

            <div className="shop_detail__wrapper">    
                <div className="link__box black_theme">
                    <div className="left">
                        <div className="tab"></div>
                        <img src="/assets/images/icon-shop-about.svg" alt="icon" className="icon"/>
                        <p className="title">เกี่ยวกับ {this.state.storeDetailData[0]!== undefined ? this.state.storeDetailData[0]['storename'] : ''}</p>
                    </div>
                    
                </div>
                <div className="detail__box black_theme">
                    <div className="tab"></div>
                    <div className="content__wrapper">
            
                        <a className="link__box">
                            <div className="box"><p>{this.state.storeDetailData[0]!== undefined ? this.state.storeDetailData[0]['storedetail'] : ''}</p></div>
                        </a>
                       
                    <div>
                    </div>
                    </div>
                </div>
            </div>
            
            <br/>
        <br/>
        <br/>
        <br/>
        </div>
       
  

       </>
      


        )
    }
}
 
export default Account_shop;
