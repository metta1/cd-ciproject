import React, { Component } from "react";
import axios from "axios";
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
//import { Field, reduxForm } from 'redux-form'
import $ from "jquery";
//import SweetAlert from "react-bootstrap-sweetalert";
import NumberFormat from "react-number-format";
import { config } from "../config";
import Header from "./Header";
import Footer from "./Footer";
import Swal from "sweetalert2";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  Dot,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import Store_color from '../template/Store_color'

const required = (value) => (value ? undefined : "Required");
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[
  currentURLArray.length - 1
].toLowerCase();
let user_id = sessionStorage.getItem("user_id");


class Product_detail extends Component {
  constructor(props) {
    //เริ่มต้น run component
    super(props);
    let storeid = this.props.match.params.store_id;
    if(storeid !== localStorage.getItem('storageStoreID')){
      localStorage.setItem('storageStoreID',storeid);
    }
  }

  state = {
    data: [],
    store_id: this.props.match.params.store_id,
    product_id: this.props.match.params.product_id,
    variant_id: "",
    user_id: user_id,
    isEmptyStock: true,
    variantStock: 0,
    quantitySelected: 1,
    colorData : [],
  };

  async componentDidMount() {
    const color = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(color.status===200){
        this.setState({colorData: color.data});
        }

    const result = await axios.get(
      config.api.base_url +
        "/store_product/api/productdetail/" +
        this.state.product_id
    ); //http://dev.backend.ho.com
    if (result.status === 200) {
      this.setState({ data: result.data });
      if(result.data.variant !== undefined){
        let variantData = result.data.variant;
        if(variantData.length > 0 && variantData[0] !== undefined){
          let default_variant_id = variantData[0]['variantid'];
          let default_stock      = variantData[0]['stock'];
          if(default_stock > 0){
            this.setState({ variant_id: default_variant_id });
            $(".product-variant li[data-variantid='"+default_variant_id+"'").addClass('active');
            this.setState({ variantStock: default_stock });
          }
          
        }
        let isEmptyStock = true;
        //console.log(parseInt(result.data.variant[0].stock))
        //console.log(variantData)
        let totalStock = 0;
        if(variantData.length > 0) {
          variantData.map(row =>
            {
              totalStock += parseFloat(row.stock) > 0 ? parseFloat(row.stock) : 0
            }
          )
          this.setState({ isEmptyStock: totalStock > 0 ? false : true });
        }
      }
    }

    this.handleChangeVariant = this.handleChangeVariant.bind(this);

    //console.log(result)
    //this.setState({data: result.data});
    console.log(this.state.data)
  }

  handleChangeVariant = (variant_id, stock, event) => {
    event.preventDefault();

    if (variant_id !== null) {
      $(".product-variant li").removeClass("active");
      this.setState({ variant_id: variant_id });
      this.setState({ variantStock: stock });
      this.setState({ quantitySelected: 1 });
      event.target.parentNode.classList.add("active");
      //event.target.classList.add('active')
    }
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleToCart = (proid, event) => {
    event.preventDefault();
    let user_id = sessionStorage.getItem("user_id");
    let variant_id = this.state.variant_id;
    let quantity = this.state.quantitySelected;

    if (
      (variant_id === null || variant_id === "") &&
      this.state.data.variant != ""
    ) {
      Swal.fire({
        title: "ขออภัยค่ะ",
        html: "กรุณาเลือกตัวเลือกสินค้า ก่อนสั่งซื้อ",
        icon: "info",
        confirmButtonText: "ตกลง",
        customClass: {
          confirmButton: "btn-black",
        },
      });
    } else if (proid !== null && user_id !== null) {
      let user_id = sessionStorage.getItem("user_id");
      let bodyFormData = new FormData();
      bodyFormData.set("userid", user_id);
      bodyFormData.set("productid", proid);
      bodyFormData.set("variantid", variant_id);
      bodyFormData.set("quantity", quantity);
      /* for(var pair of bodyFormData.entries()){
        console.log(pair[0]+','+pair[1])
      } */
      axios({
        method: "post",
        url: config.api.base_url + "/order/api/addToCart", //config.api.base_url+
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(function (response) {
          //console.log(response)
          if (response.status == 201) {
            window.location = "/cart";
            /* Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    customClass: 'swal-mobile',
                                    title: 'เพิ่มไปยังตะกร้าแล้ว',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    
                                }); */
          } else {
            Swal.fire({
              title: "ขออภัย",
              html: "ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง",
              icon: "error",
              confirmButtonText: "ตกลง",
              customClass: {
                confirmButton: "btn-black",
              },
            });
          }
          console.log(response);
        })
        .catch(function (response) {
          //console.log(response)
          Swal.fire({
            title: "ขออภัย",
            html: "ไม่สามารถเชื่อมต่อได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง",
            icon: "error",
            confirmButtonText: "ตกลง",
            customClass: {
              confirmButton: "btn-black",
            },
          });
        });
    } else {
      Swal.fire({
        title: "ขออภัย",
        html: "ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง",
        icon: "error",
        confirmButtonText: "ตกลง",
        customClass: {
          confirmButton: "btn-black",
        },
      });
    }
  };

  handleSubmit = (event) => {};

  handleChangeQuantity = (e, action) => {
    e.preventDefault();

    let actions = action;
    let value   = parseInt($('.input-quantity').val());
    let stock = parseInt(this.state.variantStock);
    let quantity = 1;
    if(actions=='minus' && value>1){
        quantity = value-1;
    }
    if(actions=='plus'){
      if (value < stock) {
        quantity = value+1;
      } else {
        Swal.fire({
          title: "ขออภัย",
          html: "สินค้าเหลือแค่ "+stock+" ชิ้น",
          icon: "error",
          confirmButtonText: "ตกลง",
          customClass: {
            confirmButton: "btn-black",
          },
        });
          quantity = stock;
      }
    }
    this.setState({ quantitySelected: quantity });

  }

  render() {
    const textColor = {
      color: '#' + this.state.colorData['coloricon']
    }
    return (
      <div className="container-mobile m-auto shop-page">
        <link
          rel="stylesheet"
          type="text/css"
          href="/assets/css/shop.css?v=3"
        />
        <Store_color/>

        <div className="head bg-black shadow">
          <div className="row w-100 h-100">
            <div className="col-8 h-100">
              <div className="icon-back">
                <a href={"/shop/" + localStorage.getItem("storageStoreID")}>
                  <div className="icon rounded-circle shadow">
                    <img src="/assets/images/back.svg" alt="" />
                  </div>
                </a>
              </div>

              <div className=" h-100 p-3 pl-5">
                <h1 className="m-0 mt-2 h" style={textColor}>SHOP </h1>
                <p className="" style={{whiteSpace:'nowrap'},textColor}>เลือกสินค้าของคุณได้เลย</p>
              </div>
            </div>
            <div className="col-4 h-100">
              {/*<div className="row float-right h-100 w-100">
                <div className="col p-1 my-auto"></div>
                <div className="col p-1 my-auto">
                  <div className="icon rounded-circle">
                    <img
                      className=""
                      src="/assets/images/icon-scan.svg"
                      alt=""
                    />
                  </div>
                </div>
                <div className="col p-1 my-auto">
                  <div className="icon rounded-circle">
                    <a href="/notifications">
                      <img
                        className=""
                        src="/assets/images/icon-noti.svg"
                        alt=""
                      />
                      </a>
                  </div>
                </div>
              </div>*/}
            </div>
          </div>
        </div>

        <div className="product-detail mb-5">
          <div className="row mt-1 m-0 w-100 h-100 ">
            <div className="w-100">
              <div className="">
                {this.state.data.gallery === undefined ? (
                  <div />
                ) : this.state.data.gallery.length == 0 ? (
                  <CarouselProvider
                    naturalSlideWidth={100}
                    naturalSlideHeight={100}
                    totalSlides={1}
                    className="carousel slide pointer-event"
                  >
                    <Slider className="carousel-inner">
                      <Slide index={0}>
                        <img
                          className="d-block w-100"
                          src={
                            this.state.data["product_img"] === ""
                              ? "/assets/images/slide.png"
                              : this.state.data["product_img"]
                          }
                          alt=""
                        />
                      </Slide>
                    </Slider>
                    <div className="w-100 text-center p-1">
                      <Dot slide={0} className="nav-dot">
                        <i class="fas fa-dot-circle"></i>
                      </Dot>
                    </div>
                  </CarouselProvider>
                ) : (
                  <CarouselProvider
                    naturalSlideWidth={100}
                    naturalSlideHeight={100}
                    totalSlides={this.state.data.gallery.length}
                    className="carousel slide pointer-event"
                  >
                    <Slider className="carousel-inner">
                      {this.state.data.gallery.map((rowGall, keyGall) => (
                        <Slide index={keyGall}>
                          <img
                            className="d-block w-100"
                            src={
                              rowGall.gallery === ""
                                ? "/assets/images/slide.png"
                                : rowGall.gallery
                            }
                            alt=""
                          />
                        </Slide>
                      ))}
                    </Slider>
                    <div className="w-100 text-center p-1">
                      {this.state.data.gallery.map((row, keyGall) => (
                        <>
                          <Dot slide={keyGall} className="nav-dot">
                            <i class="fas fa-dot-circle"></i>
                          </Dot>
                        </>
                      ))}
                    </div>
                  </CarouselProvider>
                )}
              </div>
            </div>
          </div>
          <div className="row p-2 mt-0 m-0 w-100 h-100 ">
            <div className="col-12">
              <div className="">
                <div className="product-item-detail px-3">
                  <h3 className="m-0 regular-font">{this.state.data["productname"]}</h3>
                  <p className="m-0">{this.state.data["catname"]}</p>
                  <div className="row" style={{ lineHeight: 1 }}>
                    <div className="col-8 d-flex align-items-start flex-column">
                    {this.state.data["pricebeforediscount"] !== '0' ?
                        (
                          <h4 className="price-before-discount m-0 mt-auto" >
                            <NumberFormat
                              value={this.state.data["pricebeforediscount"]}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                            {' '}THB
                          </h4>
                        ) : (
                          <div></div>
                        )
                      }
                      <h3 className="m-0 h mt-auto">
                        <NumberFormat
                          value={this.state.data["price"]}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                      </h3>
                    </div>
                    <div className="col-4 pr-0">
                      <div className="flag-point">
                        +
                        <NumberFormat
                          value={this.state.data["point"]}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                        <br />
                        POINT
                      </div>
                    </div>
                  </div>

                  <hr className="mt-0" />
                  <div className="row">
                    <div className="col-12">
                      {this.state.data.variant != "" ? (
                        <h4>ตัวเลือกสินค้า*</h4>
                      ) : (
                        <div></div>
                      )}
                      {this.state.data.variant === undefined ? (
                        <div />
                      ) : (
                        <ul className="product-variant">
                          {this.state.data.variant.map((row, key) =>
                            row.stock > 1 ? (
                              <li
                                data-variantid={row.variantid}
                                key={key}
                                className=""
                                onClick={(e) =>
                                  this.handleChangeVariant(row.variantid, row.stock, e)
                                }
                              >
                                <img
                                  className="img-fluid"
                                  src={row.variant_img}
                                />
                                <div>
                                  <br/>
                                  <div>{row.color!=='' ? 'สี: '+row.color: ''}</div>
                                  <div className="box">{row.size!=='' ? 'ไซส์: '+row.size: ''}</div>
                                </div>
                              </li>
                            ) : (
                              <li key={key} className="disabled">
                                <img
                                  className="img-fluid"
                                  src={row.variant_img}
                                />
                              </li>
                            )
                          )}
                        </ul>
                      )}
                    </div>
                  </div>
                  <div className="">
                    <h4>จำนวน</h4>
                    <div className="col-6 p-0 col-group-quantity">
                        <h5 className="mt-1">
                            <div className="input-group group-quantity">
                                <div className="input-group-prepend btn-quantity" onClick={(e) => this.handleChangeQuantity(e, 'minus')}>
                                    <span className="input-group-text bg-white">-</span>
                                </div>
                                <input type="text" class="form-control input-quantity text-center bg-white" readOnly  value={this.state.quantitySelected}/>
                                <div className="input-group-append btn-quantity" onClick={(e) => this.handleChangeQuantity(e, 'plus')}>
                                    <span className="input-group-text bg-white">+</span>
                                </div>
                            </div>
                        </h5>
                    </div>
                  </div>
                  <p style={{whiteSpace: 'pre-line'}}>
                    <h3>รายละเอียด</h3>
                    {this.state.data["description"]}
                  </p>
                </div>
              </div>
            </div>
          </div>

          <br />

          <br />
          <br />
        </div>

        <div className={!this.state.isEmptyStock ? "footer footer-product-detail" : "footer footer-product-detail bgGrey"}>
          {!this.state.isEmptyStock ?
            <button
              className="btn btn-buy"
              onClick={(e) => this.handleToCart(this.state.data["productid"], e)}
            >
              สั่งซื้อสินค้า
            </button>
          :
            <button
              className="btn btn-buy bgGrey"
            >
              สินค้าหมด
            </button>
          }
        </div>
      </div>
    );
  }
}
export default Product_detail;
