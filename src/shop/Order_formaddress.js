import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Link} from 'react-router-dom'
import Switch from "react-switch";
import InputMask from 'react-input-mask';
import {getParam} from '../lib/Helper';
import Store_color from '../template/Store_color'
import $ from 'jquery'
import validate from 'jquery-validation'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

class Order_formaddress extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.isActiveChange = this.isActiveChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeProvince = this.changeProvince.bind(this);
        this.changeDistrict = this.changeDistrict.bind(this);
        this.getUserAddress = this.getUserAddress.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    state = {
        data : [],
        proviceData : [],
        districtData : [],
        subDistrictData : [],
        userid:sessionStorage.getItem('user_id'),
        checked: false,
        addressid: this.props.match.params.addressid,
        province: null,
        district: null,
        subdistrict: null,
        zipcode: null,
        name:null,
        tel:null,
        detail:null
    };


    async componentDidMount(){
        const rProvince = await axios.get(config.api.base_url+'/address/api/province').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rProvince.status===200){
            this.setState({proviceData: rProvince.data});
        }

        if (this.state.addressid != null) {
            const rAddres = await axios.get(config.api.base_url+'/users/api/useraddress/'+this.state.userid+"/"+this.state.addressid).catch(function (error) {
                if (error.response) {
                console.log(error.response.status);
                }
            });
            if(rAddres.status===200){
                {
                    rAddres.data.map(row => (
                        this.setState({name: row.name}),
                        this.setState({tel: row.tel}),
                        this.setState({zipcode: row.zipcode}),
                        this.setState({detail: row.detail}),
                        this.setState({checked: row.active==="1" ? true  : false })
                    ))
                    this.getUserAddress(rAddres.data[0].province,rAddres.data[0].district,rAddres.data[0].subdistrict);
                }
            }
        }
    }

    async getUserAddress(pId,dId,sdId) {
        const rDistrict = await axios.get(config.api.base_url+'/address/api/district/'+pId).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rDistrict.status===200){
            this.setState({districtData: rDistrict.data});
        }

        const rSubDistrict = await axios.get(config.api.base_url+'/address/api/subdistrict/'+dId).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rSubDistrict.status===200){
            this.setState({subDistrictData: rSubDistrict.data});
        }

        this.setState({province: pId})
        this.setState({district: dId});
        this.setState({subdistrict: sdId});
    }

    async changeProvince(event){
        this.setState({province: event.target.value});
        this.setState({subDistrictData: []});
        this.setState({districtData: []});
        this.setState({zipcode: ""});

        const rDistrict = await axios.get(config.api.base_url+'/address/api/district/'+event.target.value).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rDistrict.status===200){
            this.setState({districtData: rDistrict.data})
        }
    }

    async changeDistrict(event){
        this.setState({district: event.target.value});

        const rSubDistrict = await axios.get(config.api.base_url+'/address/api/subdistrict/'+event.target.value).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(rSubDistrict.status===200){
            this.setState({subDistrictData: rSubDistrict.data});
            this.setState({zipcode: rSubDistrict.data[0].zipcode});
        }
    }

    handleChange = event => {
        if(event.target.name=='telInput'){
            let tel = (event.target.value).replace(/[^\d]/g, '');
            //console.log((tel))
            this.setState({'tel': tel});
        }else{
            this.setState({[event.target.name]: event.target.value});
        }
        
        
    }

    goBack(){
        this.props.history.goBack();
    }

    handleBackPage = event => {
        if(getParam('orderid') !== undefined){
            window.location='/order_address?orderid='+getParam('orderid')
        }else{
            window.location='/order_address';
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        const data = new FormData(event.target);
        data.set('active', data.get('active') === "on" ? 1 : 0);

        // if (this.state.addressid != null) {
        //     axios({
        //         method: 'put',
        //         url: config.api.base_url+'/users/api/useraddress',
        //         data: data,
        //         headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8"',
        //                     'X-Requested-With': 'XMLHttpRequest' }
        //         })
        //         .then(function (response) {
        //             window.location="/address/"+data.get('userid')+"?v="+ new Date().getTime();
        //         })
        //         .catch(function (response) {
        //             //handle error
        //             //console.log(response);
        //     });
        // } else {
        $.extend($.validator.messages, {
            required: "กรุณาระบุ",
        });
        $("#myForm").validate({
            rules: {
                name: {
                    required: true,
                   
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                telInput : {
                    required: true,
                },
                province : {
                    required:true
                },
                district : {
                    required :true
                },
                subdistrict : {
                    required :true
                },
                detail : {
                    required :true
                }
            },
           
            errorPlacement: function(error, element) {
                $(element).parents('.input-wrap').append(error)
            },
        });

        if($("#myForm").valid()){
            axios({
                method: 'post',
                url: config.api.base_url+'/users/api/useraddress',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                    if(response.status==201 || response.status==200){
                        localStorage.setItem('address_active',response.data.addressid);
                        
                        if(getParam('orderid') !== undefined){
                            window.location='/order_detail/'+getParam('orderid')+'?updateaddress='+response.data.addressid;
                        }else if(getParam('point_id') !== undefined){
                            let bodyFormData = new FormData();
                            bodyFormData.set('point_id',getParam('point_id'))
                            bodyFormData.set('addressID',localStorage.getItem('address_active'))
                            axios({
                                method: 'post',
                                url: config.api.base_url+'/store_reward/api/updateuserreward',
                                data: bodyFormData,
                                headers: {'Content-Type': 'multipart/form-data' }
                            })
                            .then(function (response) {
                                window.location = '/online_reward_detail/'+getParam('point_id');
                            })
                            .catch(function (response) {
                                //handle error
                                console.log(response);
                            });
                        }else{
                            window.location="/cart_list/";
                        }
                    }
                    
                    
                })
                .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        // }
        }
    }

    isActiveChange(event) {
        console.log(event)
        this.setState({ checked: event });
    }

    render() {
        const displayFlex = {
            display: 'flex'
        };
        const inputField = {
            right: 0,
            textAlign:'left',
            backgroundColor: 'initial',
            outline: 'none'
        };
        const submitButton = {
            background: 'none',
            outline: 'none',
            color: 'white',
            border: 0
        };
        let selectField = null
        if (isChrome) {
            selectField = {
                right: '0',
                textAlign:'left',
                backgroundColor: 'initial',
                height: '2em',
                position: 'absolute',
                border:'1px solid #ddd',
                textAlignLast: 'left'
            }
        } else {
            selectField = {
                right: '0',
                textAlign:'left',
                backgroundColor: 'initial',
                height: '2em',
                position: 'absolute',
                border:'1px solid #ddd',
                // direction: 'rtl'
            }
        }

        return (
            <>
                
            <div className="container-mobile m-auto ">
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <Store_color/>
            
                    <div className="head bg-black shadow">
                        <div className="row w-100 h-100">
                            
                            <div className="col-12 h-100">
                                <div className="icon-back">
                                    <a onClick={this.goBack}>
                                            <div className="icon rounded-circle shadow">
                                                <img src="/assets/images/back.svg" alt=""/>
                                            </div>
                                    </a>
                                </div>
                                
                                <div className=" h-100 p-3 pl-5">
                                    <h4 className="mt-3 text-white">
                                    {
                                        this.state.addressid ==null ? (
                                            'เพิ่มที่อยู่ใหม่'
                                        ):(
                                            'แก้ไขที่อยู่ของคุณ'
                                        )
                                    }
                                        
                                        </h4> 
                                
                                </div>
                            </div>
                        </div>
                    </div> 
                    <form onSubmit={this.handleSubmit} id="myForm">
                        <input type="hidden" name="userid" id="userid" value={this.state.userid}/>
                        <input type="hidden" name="addressid" id="addressid" value={this.state.addressid}/>
                        <div className="px-4 py-2 border-bottom" style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>ชื่อ-นามสกุล </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <input type="text" id="name" name="name" className="position-absolute border-0 w-100" style={inputField} placeholder="ชื่อ-นามสกุล" value={this.state.name} onChange={this.handleChange}/>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>หมายเลขโทรศัพท์ </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <InputMask mask="999-999-9999" maskChar={null}  onChange={this.handleChange} name="telInput" width="100%" className="position-absolute border-0 w-100" style={inputField} value={this.state.tel} placeholder="หมายเลขโทรศัพท์"/>
                                <input type="hidden" id="tel" name="tel" className="position-absolute border-0 w-100" style={inputField} placeholder="หมายเลขโทรศัพท์" value={this.state.tel}/>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>จังหวัด </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <select onChange={this.changeProvince} name="province" value={this.state.province} style={selectField} className="w-100">
                                    <option key="0" value="">-- เลือกจังหวัด --</option>
                                    {this.state.proviceData.map(row => (
                                        <option key={row.provinceid} value={row.provinceid}>
                                        {row.provicename}
                                        </option>
                                    ))}
                                </select>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>อำเภอ/เขต </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <select onChange={this.changeDistrict} name="district" value={this.state.district} style={selectField} className="w-100">
                                    <option key="0" value="">-- เลือกอำเภอ/เขต --</option>
                                    {this.state.districtData.map(row => (
                                        <option key={row.districtid} value={row.districtid}>
                                        {row.districtname}
                                        </option>
                                    ))}
                                </select>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>ตำบล </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <select onChange={this.handleChange} name="subdistrict" value={this.state.subdistrict} style={selectField} className="w-100">
                                    <option key="0" value="">-- เลือกตำบล --</option>
                                    {this.state.subDistrictData.map(row => (
                                        <option key={row.subdistrictid} value={row.subdistrictid}>
                                        {row.subdistrictname}
                                        </option>
                                    ))}
                                </select>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <div className="col-5 p-0">
                                <h5>รหัสไปรษณีย์ </h5>
                            </div>
                            <div className="col-7 p-0">
                                <div className="input-wrap">
                                <input type="text" id="zipcode" readOnly name="zipcode" width="100%" className="position-absolute border-0 w-100" style={inputField} value={this.state.zipcode}></input>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom ">
                            <h5 style={{marginBottom: '0'}}>รายละเอียดที่อยู่เพิ่มเติม </h5>
                            <h6 >(เลขที่ห้อง, เลขที่บ้าน, ตึก, ชื่อถนน)</h6>
                            <div className="input-wrap">
                            <textarea id="detail" name="detail" className="w-100" value={this.state.detail} onChange={this.handleChange}></textarea>
                            </div>
                        </div>
                        <div className="px-4 py-2 border-bottom " style={displayFlex}>
                            <h5>ตั้งเป็นที่อยู่ตั้งต้น </h5>
                            <div className="position-absolute" style={inputField}><Switch name="active" onChange={this.isActiveChange} checked={this.state.checked}/></div>
                        </div>
                        <div className="px-4 py-2 border-bottom ">
                            <h5 style={{marginBottom: '0'}}>เลือกพื้นที่สำหรับจัดส่ง </h5>
                            <h6 >เพื่อเพิ่มความถูกต้องของที่อยู่การจัดส่ง</h6>
                        </div><br/><br/><br/>
                        <div className="footer footer-product-detail">
                              <button className="btn btn-buy">ยืนยัน</button>
                                
                        
                        </div>
                    </form>
                </div>  
                {/* <!-- container --> */}
            </>
        )
    }
}
export default Order_formaddress;
