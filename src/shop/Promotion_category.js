import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
//import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Field, reduxForm } from 'redux-form'
//import $ from 'jquery'
//import SweetAlert from "react-bootstrap-sweetalert";

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Promotion_category extends Component {
    constructor(){//เริ่มต้น run component
        super();
       
    }

    state = {
        data : []
    };


    async componentDidMount(){
        const result = await axios.get('https://backend-ho.dreamnolimit.com/api/category').catch(function (error) {
        //const result = await axios.get('http://dev.backend.ho.com/admin_api').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
        }
        
        //console.log(result)
        //this.setState({data: result.data});
        //console.log(this.state.data)
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="assets/css/promotion_category.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-3 pl-4">
                        <div className="row">
                            <div className="col-6">
                                <img className="w-75" src="assets/images/logo.png" alt=""/>
                            </div>
                            <div className="col-6">
                                <div className="float-right">
                                    <div className="icon-head icon-promo bg-white rounded-circle text-center d-inline-block">
                                        <a href="">
                                            <img className="h-50" src="assets/images/icon-search.svg" alt="..." />
                                        </a>
                                    </div>
                                    <div className="icon-head icon-promo bg-white rounded-circle text-center d-inline-block">
                                        <a href="">
                                            <img className="h-50" src="assets/images/icon-scan.svg" alt="..." />
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                    
                    <div className="promotion p-2" style={{marginTop: "20%"}}>

                        <span className="p-2 m-0 text-grey">โปรโมชั่น</span>
                        <div className="row m-0">


                    {


                        this.state.data.map(row => (
                            
                            <div className="col-4 p-2">
                                <a className="" href={parseInt(row.promo_count)>0 ? '/promotion/'+row.cateid : '#'}>
                                    <div className="promotion-box shadow rounded">
                                        <div className={parseInt(row.promo_count)>0 ? 'promotion-item' : 'promotion-item unactive'}>
                                                <img className="mt-4" src={row.cate_img} alt={row.catename}/> <br/>
                                                <p className="mt-2">{row.catename}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                         ))


                    }
                            

                        </div> 
                        {/* <!-- row --> */}
                    </div>
                    {/* <!-- promotion --> */}

                    <div className="footer mx-auto">
                            <img className="w-100 position-relative" src="assets/images/footer_promotion.svg" alt=""/>
                            <div className="menu position-absolute w-100">
                                <div className="menu-item px-2 pt-2 ">
                                    <a href="/user_card">
                                        <img className="position-relative" src="assets/images/icon-credit.png" alt=""/> <br/>
                                        <span>CARD</span>
                                    </a>
                                    
                                </div>
                                <div className="menu-item px-2 pt-2">
                
                                    <a href="/feed">
                                        <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                        <span>FEED</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="">
                                        <img className="" src="assets/images/icon-home.png" alt=""/><br/>
                                        <span>HOME</span>
                                    </a>
                                </div>
                                <div className="menu-item px-2 ">
                                    <div className="menu-item-active bg-white rounded-circle shadow">
                                        <div className="menu-item-active-ab">
                                            <a href="/promotion_category">
                                                <img className="" src="assets/images/icon-gift.png" alt=""/> <br/>
                                                <span>COUPON</span>
                                            </a>
                                        </div>
                                    </div>
                
                                </div>
                                <div className="menu-item px-2 pt-2">
                                    <a href="/account">
                                        <img className="" src="assets/images/icon-user.png" alt=""/> <br/>
                                        <span>ACCOUNT</span>
                                    </a>
                                </div>
                            </div>

                    </div>
                </div>
                 
            </div>
        )
    }
}
export default Promotion_category;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;