import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';

import {config} from '../config'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class User_interest extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
    }

    state = {
        data : [],
        cateid : [],
        cat_id:this.props.match.params.cat_id,
        userid:sessionStorage.getItem('user_id')
    };


    async componentDidMount(){
        console.log(this.state.userid)
        const result = await axios.get(config.api.base_url+'/users/api/interest/')
        if(result.status===200){
            // this.setState({data: result.data});
        }

        const userinterestresult = await axios.get(config.api.base_url+'/users/api/interest/'+this.state.userid)
        if(userinterestresult.status===200){
            
            userinterestresult.data.forEach(element => {
                if (element.userid !== null) {
                    this.state.cateid.push(element.cateid)
                }
            });
            this.setState({cateid:this.state.cateid})

            this.setState({data: userinterestresult.data});
        }

    }

 
    handleChange = (event,cateid) => {
        if(event.target.checked){
            this.handleChecked(cateid)
        }else{
            this.handleUnChecked(cateid)
        }
      
        
    }

    handleChecked(cateid){
        let objCateid = this.state.cateid;
        objCateid.push(cateid)
        this.setState({cateid:objCateid})
        
        
    }
    handleUnChecked(cateid){
        let objCateid = this.state.cateid;
        for(let i=0; i<objCateid.length;i++){
            if(objCateid[i]===cateid){
                objCateid.splice(i, 1);
            }
        }
        this.setState({cateid:objCateid})

        let userCateDate = this.state.data;
        for(let i=0; i<userCateDate.length;i++){
            if(userCateDate[i].cateid===cateid){
                userCateDate[i].userid = null;
                
            }
        }
        console.log(userCateDate)
    }
    
    handleSubmit = event => {
        event.preventDefault();
        const bodyFormData = new FormData(event.target);
        bodyFormData.set('userid',this.state.userid)
        bodyFormData.set('cateid',this.state.cateid)
        axios({
            method: 'post',
            url: config.api.base_url+'/users/api/userinterest',
            data: bodyFormData,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                window.location='/account'
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
        });
        
    }
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/user_interest.css"/>
                <div className="container-mobile m-auto">
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="/account">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h3 className="align-title m-0">
                                ความสนใจของฉัน
                            </h3>     
                        </div>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="interest p-8">
                            <div className="row m-0">
                                {
                                    this.state.data.map((row,key) => (
                                        <div className="col-6 p-2" key={key}>
                                            <div className="interest-box shadow b-1">
                                                <div className={'interest-item'}>
                                                    <div className="interest-checkbox">
                                                        {row.userid !== null ?
                                                            <div className="custom-control form-control-lg custom-checkbox">  
                                                                <input type="checkbox" className="custom-control-input" id={`${key}`} checked="checked" value={row.cateid} name="cateid" onChange={(e) => (this.handleChange(e,row.cateid))}/>  
                                                                <label className="custom-control-label" htmlFor={`${key}`}/>
                                                            </div>
                                                        :
                                                            <div className="custom-control form-control-lg custom-checkbox">  
                                                                <input type="checkbox" className="custom-control-input" id={`${key}`} value={row.cateid} name="cateid" onChange={(e) => (this.handleChange(e,row.cateid))}/>  
                                                                <label className="custom-control-label" htmlFor={`${key}`}/>
                                                            </div>
                                                        }
                                                    </div>
                                                        <img className="" src={row.cateimg} alt={row.category}/> 
                                                    <p>{row.category}</p>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div> 
                            {/* <!-- row --> */}
                            <button className="btn ">
                                บันทึก
                            </button>
                        </div>
                    </form>
                    {/* <!-- Category --> */}
                </div>
            </div>
        )
    }
}
export default User_interest;
