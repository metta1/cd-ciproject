import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import {getParam,hexToRgb} from '../lib/Helper';
import $ from 'jquery'
import Moment from 'moment';
import NumberFormat from 'react-number-format';
import Store_color from '../template/Store_color'
import {Notification_num_inside} from '../template'
import Swal from 'sweetalert2'

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();


class Notifications extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.goBack = this.goBack.bind(this);
        this.copyToClipboard = this.copyToClipboard.bind(this);
    }

    state = {
        data : [],
        orderData: [],
        colorData : [],
        user_id : sessionStorage.getItem('user_id'),
        store_id : localStorage.getItem('storageStoreID'),
        colortabbar: '',
        coloricon: '',
        colorfont: '',
        colorUnreadRGBA : undefined,
        rewardData: [],
        copySuccess: ''
    };

    copyToClipboard = (e, trackingNo) => {
        e.preventDefault();
        navigator.clipboard.writeText(trackingNo)
        Swal.fire({
            position: 'middle',
            icon: 'success',
            title: 'Copied',
            showConfirmButton: false,
            timer: 1500
        })
    };

    goBack(){
        if (getParam('activetab') !== undefined) {
            if (this.state.store_id === undefined || this.state.store_id === '') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+this.state.store_id;
            }
        // } else if (localStorage.getItem('isFromLogin') === 'Y') {
        //     if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0') {
        //         window.location = "/home";
        //     } else {
        //         window.location = "/shop/"+localStorage.getItem('storageStoreID');
        //     }
        } else {
            // this.props.history.goBack();
            if (localStorage.getItem('storageStoreID') === null || localStorage.getItem('storageStoreID') === '0' || localStorage.getItem('storageStoreID') === '') {
                window.location = "/home";
            } else {
                window.location = "/shop/"+localStorage.getItem('storageStoreID');
            }
        }
    }

    async componentDidMount(){
        const colorresult = await axios.get(config.api.base_url+'/store/api/storeColor/'+localStorage.getItem('storageStoreID'))
        if(colorresult.status===200){
            this.setState({colorData: colorresult.data});
        }

        console.log(this.state.colorData);
        if(this.state.colorData !== undefined){
            let colortabbar = this.state.colorData['colortabbar'];
            let coloricon   = this.state.colorData['coloricon'];
            let colorfont   = this.state.colorData['colorfont'];
            this.setState({colortabbar: colortabbar});
            this.setState({coloricon: coloricon});
            this.setState({colorfont: colorfont});
            console.log(colortabbar)
            let tabbartorgb     = hexToRgb(this.state.colorData['colortabbar'])
            if(tabbartorgb){
                let colorUnreadRGBA = tabbartorgb.join(", ")
                colorUnreadRGBA = colorUnreadRGBA+', .2';
                console.log(colorUnreadRGBA)
                this.setState({
                    colorUnreadRGBA : colorUnreadRGBA
                })
            }
            
            
           
        }



        if(getParam('activetab') !== undefined){
            let activetab = parseInt(getParam('activetab'));
            this.activeStatus(this,activetab,this.state.store_id);

            let tabnum = activetab - 1 <= 0 ? 0 : activetab - 1;
            $('.nav-order-historty .nav-link').removeClass('active');
            $(".nav-link" ).css( "background-color", "#ddd");
            $('.nav-order-historty .nav-link').eq(tabnum).addClass('active');

            if (this.state.colortabbar != undefined && this.state.colortabbar != undefined && this.state.colorfont != undefined) {
                $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#"+this.state.colortabbar);
                $('.btn.btn-primary').attr('style', 'background-color:#'+this.state.colortabbar+'!important;'+
                                                    'border-color:#'+this.state.colortabbar+'!important;'+
                                                    'color:#'+this.state.colorfont+'!important;')    
                if(this.state.colorUnreadRGBA !== undefined){
                    $('.unread').attr('style', 'background-color:rgba('+this.state.colorUnreadRGBA+') !important;')
                }                  
                
            } else {
                $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#F9CE43");
                $('.btn.btn-primary').attr('style', 'background-color:#F9CE43!important;'+
                                                    'border-color:#F9CE43!important;')
            }
        } else {
            this.activeStatus(this,1,this.state.store_id);
        }
        const result = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.store_id)
        if(result.status===200){
            this.setState({data: result.data});
        }
        // const result = await axios.get(config.api.base_url+'/store_notification/api/notification_store/'+this.state.user_id).catch(function (error) {
        //     if (error.response) {
        //       console.log(error.response.status);
        //     }
        // });
        // if(result.status===200){
        //     this.setState({data: result.data});
        //     // console.log(result.data)
        // }
    }

    async activeStatus(e,statusID,storeId) {
        statusID = statusID===2 ? '2-3' : statusID;
        console.log("this.state.storeid:",storeId);
        console.log("statusID:",statusID);

        $('.nav-order-historty .nav-link').click(function(){
            $('.nav-order-historty .nav-link').removeClass('active');
            $(".nav-link" ).css( "background-color", "#ddd");
            $(this).addClass('active')
            // $('.nav-link.active').css("background-color", "#"+colortabbar);

        })

        let orderResult;
        if (storeId === undefined || storeId === '') {
            orderResult =  await axios.get(config.api.base_url+'/store_order/api/order/null/'+sessionStorage.getItem('user_id')+'/'+statusID);
        } else {
            orderResult =  await axios.get(config.api.base_url+'/store_order/api/order/'+storeId+'/'+sessionStorage.getItem('user_id')+'/'+statusID);
        }
        if(orderResult.status===200){
            this.setState({orderData: orderResult.data});
        }else{
            this.setState({orderData: []});
        }

        // Reward
        let rewardResult = null;
        let rewardStatus = null;
        if (statusID === 2 || statusID === '2' || statusID === '2-3') {
            // แลกของรางวัล
            rewardStatus = 0;
        } else if (statusID === 6 || statusID === '6') {
            // จัดส่งสำเร็จ
            rewardStatus = 1;
        } else if (statusID === 4 || statusID === '4') {
            // จัดส่ง
            rewardStatus = 2;
        } else if (statusID === 5 || statusID === '5') {
            // ยกเลิก
            rewardStatus = 3;
        }
        console.log(rewardStatus)
        if (rewardStatus === 0 || rewardStatus === 1 || rewardStatus === 2 || rewardStatus === 3){
            if (storeId === undefined || storeId === '') {
                rewardResult =  await axios.get(config.api.base_url+'/store_reward/api/getuserreward/'+sessionStorage.getItem('user_id')+'?reward_status='+rewardStatus);
            } else {
                rewardResult =  await axios.get(config.api.base_url+'/store_reward/api/getuserreward/'+sessionStorage.getItem('user_id')+'?store_id='+storeId+'&reward_status='+rewardStatus);
            }
            if(rewardResult.status===200){
                this.setState({rewardData: rewardResult.data});
            }else{
                this.setState({rewardData: []});
            }
        } else {
            this.setState({rewardData: []});
        }

        console.log(this.state.rewardData)

        this.setState({statusActive: statusID});
        this.setState({rewardStatusActive: rewardStatus});
        //console.log(this.state.data)
        //console.log(this.state.statusActive)
        if (this.state.colortabbar != undefined && this.state.colortabbar != undefined && this.state.colorfont != undefined) {
            $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#"+this.state.colortabbar);
            $('.btn.btn-primary').attr('style', 'background-color:#'+this.state.colortabbar+'!important;'+
                                                'border-color:#'+this.state.colortabbar+'!important;'+
                                                'color:#'+this.state.colorfont+'!important;')
            if(this.state.colorUnreadRGBA !== undefined){
             
                $('.unread').attr('style', 'background-color:rgba('+this.state.colorUnreadRGBA+') !important;')
            }              
        } else {
            $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#F9CE43");
            $('.btn.btn-primary').attr('style', 'background-color:#F9CE43!important;'+
                                                'border-color:#F9CE43!important;')
        }
        
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    handlePayment = (e,orderid) => {
        window.location="/order_detail/"+orderid;
    }

    render() {
        return (
            <div>
                
                <link rel="stylesheet" type="text/css" href="/assets/css/shop.css?v=3"/>
                <link rel="stylesheet" type="text/css" href="/assets/css/notifications.css?v=3"/>
                <Store_color/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-4 pl-4 notifications">
                        <div className="row">
                            <div className="col-12 head-text">
                                <a onClick={this.goBack}>
                                    <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </a>
                                <h3 className="m-0 pl-2">แจ้งเตือนทั้งหมดของคุณ</h3> 
                            </div>
                        </div>
                    </div>
                    <div className="notification mt-1">
                        <div className="row m-0 mt-2 background-white">
                            <div className="col-12 p-0 row border-bottom m-0 mt-1" >
                                <a className="w-100 row m-0" href="/ho_notifications">
                                    <div className="col-2 my-auto">
                                        <img className="w-100" src="/assets/images/icon-ho-new-v2.png" alt="" /> <br/>
                                    </div>
                                    <div className="col-7 detail mb-1">
                                        <div>
                                            แจ้งเตือนจาก HO
                                        </div>
                                        <div>
                                            <span className="text-grey">การแจ้งเตือนโปรโมชั่นและข่าวสารจาก HO</span>
                                        </div>
                                    </div>
                                    <div className="col-1 p-0 my-auto">
                                        <Notification_num_inside storeid={this.state.store_id} notitype={'HO'}/>

                                    </div>
                                    <div className="col-2 my-auto">
                                        <img className="w-75 p-1" src="/assets/images/icon-go.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                            <div className="col-12 py-2 px-0 row border-bottom m-0" >
                                <a className="w-100 row m-0" href={'/store_notifications/'+this.state.store_id}>
                                    <div className="col-2 my-auto">
                                        <img className="w-100" src="/assets/images/icon-mail.svg" alt="" /> <br/>
                                    </div>
                                    <div className="col-7 detail mb-1">
                                        <div>
                                            แจ้งเตือนจากร้านค้าของคุณ
                                        </div>
                                        <div>
                                            <span className="text-grey" style={{whiteSpace:'nowrap'}}>การแจ้งเตือนโปรโมชั่นและข่าวสารจากร้านค้า</span>
                                        </div>
                                    </div>
                                    <div className="col-1 p-0 my-auto">
                                        <Notification_num_inside storeid={this.state.store_id} notitype={'store'}/>

                                    </div>
                                    <div className="col-2 my-auto">
                                        <img className="w-75 p-1" src="/assets/images/icon-go.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="text-head px-3 mt-4">
                        <h4 className="m-0 d-inline">แจ้งเตือนคำสั่งซื้อของคุณ</h4> 
                        <p>อัพเดทแจ้งเตือนคำสั่งซื้อของคุณ</p>
                    </div>
                    
                    <div className="search-bar">
                        <div className="col-12 p-0">
                            <div className="table-responsive">
                                <ul class="nav nav-pills nav-fill nav-order-historty orderSelectedButton">
                                    <li class="nav-item" onClick={(e) => this.activeStatus(e,1,this.state.store_id)}>
                                        <a class="nav-link active" >แจ้งชำระสินค้า</a>
                                    </li>
                                    <li class="nav-item" onClick={(e) => this.activeStatus(e,2,this.state.store_id)}>
                                        <a class="nav-link">สินค้าที่ต้องจัดส่ง</a>
                                    </li>
                                    <li class="nav-item" onClick={(e) => this.activeStatus(e,4,this.state.store_id)}>
                                        <a class="nav-link" >สินค้าที่ต้องได้รับ</a>
                                    </li>
                                    <li class="nav-item" onClick={(e) => this.activeStatus(e,6,this.state.store_id)}>
                                        <a class="nav-link">สินค้าที่จำหน่ายสำเร็จ</a>
                                    </li>
                                    <li class="nav-item" onClick={(e) => this.activeStatus(e,5,this.state.store_id)}>
                                        <a class="nav-link">สินค้าที่ถูกยกเลิก</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="cart-detail mb-5 ">
                        <div className="row pt-2 mt-1 m-0 w-100 h-100 ">
                            <div className="col-12 p-0">
                                <div className="">
                                {
                                    this.state.orderData === undefined ? (
                                        <div/>
                                    ) : (
                                        <ul className="cart-product">
                                        {
                                        this.state.orderData.length === 0 ? <></> : 
                                            <li class="pl-3 pt-2 h3 text-bold border-bottom">
                                                <label>สินค้า</label>
                                            </li>
                                        }
                                        {
                                        this.state.orderData.map((row,key) => (
                                            <li className={row.isRead==="0" ? "product-item-detail order-list unread" : "product-item-detail order-list"} key={key}>
                                                <div className="container">
                                                    <a href={'/shop/'+row.storeid}>
                                                        <div className="row">
                                                            <div className="col-3 my-auto">
                                                                <div class="w-100 text-center   pt-2 pb-2">
                                                                    <div class="coupon-detail-img rounded-circle mx-auto  border">
                                                                        <div className="">
                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                </div>
                                                            </div>
                                                            <div className="col-9 mx-auto my-auto p-0">
                                                                <h3 className="mb-0">{row.storename}</h3>
                                                            </div>
                                                            
                                                        </div>  
                                                    </a>
                                                    <hr className="mb-0 mt-0"/>
                                                    <a href={'/order_detail/'+row.orderid}>
                                                        <div className="row">
                                                            <div className="col-3 ">
                                                                <div class="w-100 text-center   pb-2 pt-2">
                                                                    <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                                        <img class="img-fluid img-product" src={row.productimg==='' ? '/assets/images/slide.png' : row.productimg}/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div className="col-9 mx-auto pt-2 p-0">
                                                                <h3 className="mb-1 text-gray text-overflow" style={{textOverflow: 'ellipsis'}}>{row.productname} X{row.amount}</h3>
                                                                <h4 className="mb-1">ORDER ID : {row.orderno}</h4>
                                                                <h4 className="mb-1 text-gray">{Moment(row.orderdate).format('D MMM YYYY H:mm')}</h4>
                                                                <h3 className="display-price"><NumberFormat value={parseFloat(row.totalprice)+parseFloat(row.deliverryprice != null ? row.deliverryprice : 0)} displayType={'text'} thousandSeparator={true}/> THB</h3>
                                                            </div>
                                                            
                                                        </div>
                                                    </a> 
                                                    <hr className="mb-0 mt-0"/>
                                                    <div className="row mb-2 mt-2">
                                                    {
                                                    (() => {
                                                        switch (this.state.statusActive) {
                                                            case 1:   
                                                                return (
                                                                <>
                                                                <div className="col-8">
                                                                    <h4 className="text-gray m-0">{row.paymenttypename}</h4>
                                                                    {
                                                                        row.paymenttypeid==2 || row.paymenttypeid ==3 || row.paymenttypeid==0 ? (
                                                                            <h4 className="text-gray">จะได้รับการยืนยันจากทางร้าน</h4>
                                                                        ) : (
                                                                            <h4 className="text-gray">ชำระเงินภายในวันที่ {Moment(row.orderdate).add(1, 'days').format('d MMM YYYY H:mm')}</h4>
                                                                        )
                                                                    }
                                                                </div>
                                                                <div className="col-4 pl-0 text-right mb-auto mt-1">
                                                                    {
                                                                        row.paymenttypeid==2 || row.paymenttypeid ==3 || row.paymenttypeid==0 ? (
                                                                            <button type="button" className="btn btn-primary pl-2 pr-2 p-1 text-bold active" onClick={(e) => this.handlePayment(e,row.orderid)}>สินค้ารอการยืนยัน</button>
                                                                        ) : (
                                                                            <button type="button" className="btn btn-primary pl-2 pr-2 p-1 text-bold active" onClick={(e) => this.handlePayment(e,row.orderid)}>สินค้ารอชำระ</button>
                                                                        )
                                                                    }
                                                                
                                                                    
                                                                </div>
                                                                </>);
                                                            case 4:
                                                                return (
                                                                <>
                                                                    <div className="col-8">
                                                                        <h4 className="text-gray m-0">การจัดส่ง</h4>
                                                                        <div>
                                                                            <h4 className="mb-1" id="delivery_name">{row.deliverryname}</h4>
                                                                        </div>
                                                                    </div>
                                                                    {row.link === "" ? <></> :
                                                                    <div className="col-4">
                                                                        <h4 className="text-gray m-0">ลิงค์ติดตาม</h4>
                                                                        <div>
                                                                            <a href={row.link} target="_blank"><h4 className="mb-1 text-primary" id="link">Link</h4></a>
                                                                        </div>
                                                                    </div>
                                                                    }
                                                                    <div className="col-8">
                                                                        <h4 className="text-gray m-0">หมายเลขคำสั่งซื้อ</h4>
                                                                        <div>
                                                                            {(() => {
                                                                                if ( row.status == 4) {
                                                                                    return (
                                                                                        <h4 className="mb-1" id="trackingNo">{row.trackingNo}</h4>
                                                                                    )
                                                                                }else if(row.status == 5){
                                                                                    return (
                                                                                        <h4 className="mb-1">หมายเหตุ : ยกเลิกสินค้า</h4>
                                                                                    )
                                                                                }
                                                                            }
                                                                            )()}
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-4 my-2">
                                                                        <button type="button" className="btn btn-primary pl-2 pr-2 p-1 active"  onClick={(e) => this.copyToClipboard(e,row.trackingNo)}>คัดลอกหมายเลข</button>
                                                                    </div>


                                                                </>
                                                                )
                                                            default:      
                                                                return "";
                                                        }
                                                    })()
                                                    }
                                                    </div>
                                                </div>
                                            </li>
                                        ))
                                        }
                                        </ul>
                                    )
                                }
                                </div>
                                <div className="">
                                {
                                    this.state.rewardData === undefined ? (
                                        <div/>
                                    ) : (
                                        <ul className="cart-product">
                                        {
                                        this.state.rewardData.length === 0 ? <></> : 
                                            <li class="pl-3 pt-2 h3 text-bold border-bottom">
                                                <label>ของรางวัล</label>
                                            </li>
                                        }
                                        {
                                        this.state.rewardData.map((row,key) => (
                                            <li className={row.isRead==="0" ? "product-item-detail order-list unread" : "product-item-detail order-list"} key={key}>
                                                <div className="container">
                                                    <a href={'/shop/'+row.store_id}>
                                                        <div className="row">
                                                            <div className="col-3 my-auto">
                                                                <div class="w-100 text-center   pt-2 pb-2">
                                                                    <div class="coupon-detail-img rounded-circle mx-auto  border">
                                                                        <div className="">
                                                                        <img className="" src={row.store_logo} alt=""/>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                </div>
                                                            </div>
                                                            <div className="col-9 mx-auto my-auto p-0">
                                                                <h3 className="mb-0">{row.store_name}</h3>
                                                            </div>
                                                            
                                                        </div>  
                                                    </a>
                                                    <hr className="mb-0 mt-0"/>
                                                    <a href={'/online_reward_detail/'+row.point_id}>
                                                        <div className="row">
                                                            <div className="col-3 ">
                                                                <div class="w-100 text-center   pb-2 pt-2">
                                                                    <a href={'/product/'+this.state.store_id+'/'+row.productid}>
                                                                        <img class="img-fluid img-product" src={row.reward_img==='' ? '/assets/images/slide.png' : row.reward_img}/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div className="col-9 mx-auto pt-2 p-0">
                                                                <h3 className="mb-1 text-gray text-overflow" style={{textOverflow: 'ellipsis'}}>{row.rewardname}</h3>
                                                                <h4 className="mb-1">รหัส Reward : {row.rewardcode}</h4>
                                                                <h4 className="mb-1 text-gray">{Moment(row.point_update).format('D MMM YYYY H:mm')}</h4>
                                                                <h3 className="display-price"><NumberFormat value={parseFloat(row.rewardpoint)} displayType={'text'} thousandSeparator={true}/> คะแนน</h3>
                                                            </div>
                                                            
                                                        </div>
                                                    </a> 
                                                    <hr className="mb-0 mt-0"/>
                                                    <div className="row mb-2 mt-2">
                                                    {
                                                    (() => {
                                                        switch (this.state.rewardStatusActive) {
                                                            case 2:
                                                                return (
                                                                <>
                                                                    <div className="col-8">
                                                                        <h4 className="text-gray m-0">หมายเลขพัสดุ</h4>
                                                                        <div>
                                                                            <h4 className="mb-1" id="trackingNo">{row.trackingNo}</h4>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-4 my-2">
                                                                        {/* <button type="button" className="btn btn-primary pl-2 pr-2 p-1 active" onClick={() => navigator.clipboard.writeText(row.trackingNo)}>คัดลอกหมายเลข</button> */}
                                                                        <button type="button" className="btn btn-primary pl-2 pr-2 p-1 active" onClick={(e) => this.copyToClipboard(e,row.trackingNo)}>คัดลอกหมายเลข</button>
                                                                        <div className="text-center">
                                                                            <span className="text-danger">{this.state.copySuccess}</span>
                                                                        </div>
                                                                    </div>
                                                                </>
                                                                )
                                                            case 3:
                                                                return (
                                                                <>
                                                                    <div className="col-12">
                                                                        <h4 className="text-gray m-0">เหตุผลที่ยกเลิก</h4>
                                                                        <div>
                                                                            <h4 className="mb-1">{row.reasonCancel}</h4>
                                                                        </div>
                                                                    </div>
                                                                </>
                                                                )
                                                            default:
                                                                return "";
                                                        }
                                                    })()
                                                    }
                                                    </div>
                                                </div>
                                            </li>
                                        ))
                                        }
                                        </ul>
                                    )
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Notifications;
