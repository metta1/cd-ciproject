import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import Store_color from '../template/Store_color'

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();

class Store_notifications extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.goBack = this.goBack.bind(this);
    }

    state = {
        data : [],
        colorData: [],
        user_id : sessionStorage.getItem('user_id'),
        store_id : localStorage.getItem('storageStoreID')
    };

    goBack(){
        this.props.history.goBack();
    }

    async componentDidMount(){
        // console.log(this.state.storeid)
        const result = await axios.get(config.api.base_url+'/store_notification/api/notification_store/'+this.state.user_id+'/'+this.state.store_id).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/notifications.css?v=3"/>
                <Store_color/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-4 pl-4 notifications">
                        <div className="row">
                            <div className="col-12 head-text">
                                <a onClick={this.goBack}>
                                    <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </a>
                                <h3 className="m-0 pl-2">แจ้งเตือนร้านค้าของคุณ</h3> 
                            </div>
                        </div>
                    </div>
                    <div className="notification mt-1">
                        <div className="row m-0 mt-2">
                            {
                                this.state.data.length === 0 ? (
                                    <div className="w-100 text-center">
                                        <h4 className="m-0">ไม่มีการแจ้งเตือน</h4>
                                    </div>
                                ) : (
                                this.state.data.map((row,key) => (
                                    <div className={row.isRead==="0" ? "col-12 p-2 row border-bottom m-0 mt-1 background-white unread" : "col-12 p-2 row border-bottom m-0 mt-1 background-white"} key={key}>
                                        <a className="w-100 row m-0" href={'/shop/'+row.storeid}>
                                            <div className="col-2 my-auto">
                                                <img className="w-100 mb-1 mt-1" src={row.store_img} alt="" /> <br/>
                                            </div>
                                            <div className="col-8 detail my-auto">
                                                <div>
                                                    {row.storename}
                                                </div>
                                                <div>
                                                    {row.detail}
                                                </div>
                                            </div>
                                            <div className="col-2 my-auto">
                                                <img className="w-75 p-1 mt-1" src="/assets/images/icon-go.svg" alt=""/>
                                            </div>
                                        </a>
                                    </div>
                                )))
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Store_notifications;
