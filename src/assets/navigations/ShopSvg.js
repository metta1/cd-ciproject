import React from 'react'

const shopSvg = (props) => {

  let style = {
    stroke: "#" + props.activeColor
  }

  return (
    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"  style={style}>
      <g fill="none" style={style}>
        <path style={{stroke: "#" + props.activeColor, strokeMiterLimit: "10" }} d="m21.16 13.35-12.87 1.1a.85.85 0 0 1 -.9-.67l-1.59-7.53a.84.84 0 0 1 .83-1h15.83a.84.84 0 0 1 .83 1l-1.37 6.43a.87.87 0 0 1 -.76.67z"/>
        <g style={{stroke: "#" + props.activeColor, strokeLinecap:'round', strokeLinejoin:'round'}}>
          <path style={style} d="m.61 1.41a3.62 3.62 0 0 1 4 1.33c.83 1.06.92 2.65 1.21 3.93.54 2.41 1 4.77 1.55 7.18"/>
          <path style={style} d="m8 14.46a5.13 5.13 0 0 0 -2.58.59 2.43 2.43 0 0 0 -1.21 1.46 2.29 2.29 0 0 0 .79 2.23 3.58 3.58 0 0 0 2.33.77"/>
          <circle style={style} cx="10.11" cy="19.48" r="2.45"/>
          <circle style={style} cx="19.99" cy="19.48" r="2.45"/>
          <path style={style} d="m12.56 19.48h4.98"/>
        </g>
        </g>
    </svg>
  )
}

export default shopSvg
