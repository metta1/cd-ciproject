import React from 'react'

const MemberSvg = (props) => {
  let style = {
    stroke: "#" + props.activeColor
  }

  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style={style}>
      <defs>
        <style dangerouslySetInnerHTML={{__html: `
            .cls-1,.cls-2{fill:none;stroke:#231f20;}
            .cls-1{stroke-linecap:round;stroke-linejoin:round;}
            .cls-2{stroke-miterlimit:10;}
          `}}>
        </style>
      </defs>
      <g id="Member" style={style}>
        <line style={style} className="cls-1" x1="13.03" y1="14.51" x2="3.37" y2="14.51"/>
        <line style={style} className="cls-1" x1="10.67" y1="16.81" x2="3.37" y2="16.81"/>
        <rect style={style} className="cls-2" x="0.5" y="3.78" width="23.01" height="16.38" rx="1.57"/>
        <rect style={style} className="cls-1" x="0.5" y="6.41" width="23.01" height="4.42"/>
        <rect style={style} className="cls-1" x="15.91" y="14.17" width="4.96" height="2.97"/>
      </g>
    </svg>
  )
}

export default MemberSvg