import React from 'react'

const PromotionSvg = (props) => {
  let style = {
    stroke: "#" + props.activeColor
  }

  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style={style}>
      <defs>
      <style dangerouslySetInnerHTML={{__html: `
          .cls-1{fill:none;stroke:#231f20;stroke-linecap:round;stroke-linejoin:round;}
        `}}>
      </style>

      </defs>
      <g id="Promotion" style={style}>
        <path style={style} className="cls-1" d="M11.78,4.19a1.31,1.31,0,0,0-.95.4L1.07,14.85A1.62,1.62,0,0,0,.9,17.12l6.26,6a1.63,1.63,0,0,0,2.27-.28l9.75-10.25a1.27,1.27,0,0,0,.35-1l.61-7.44A1,1,0,0,0,19,3.1Z"/>
        <path style={style} className="cls-1" d="M18.52,1.74A3.85,3.85,0,0,1,20.73.54a2.51,2.51,0,0,1,2.15.73,2.5,2.5,0,0,1,.59,2,3.56,3.56,0,0,1-.88,1.89,3.48,3.48,0,0,1-1.68,1.11,2.78,2.78,0,0,1-2.54-.64"/>
      </g>
    </svg>
  )
}

export default PromotionSvg
