import React from 'react'

const NewsSvg = (props) => {
  let style = {
    stroke: "#" + props.activeColor
  }

  return (
    <svg id="News" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style={style}>
      <defs style={style}>
        <style dangerouslySetInnerHTML={{__html: `
            .cls-1{fill:none;stroke:#231f20;stroke-linecap:round;stroke-linejoin:round;}
          `}}>
        </style>
      </defs>
    <path style={style} className="cls-1" d="M19.55,3.14V21.83c0,1,.58,1.72,1.3,1.72h.28c.72,0,1.3-.77,1.3-1.72V3.14Z"/>
    <path style={style} className="cls-1" d="M19.55,22V.53H1.71V21.84a1.72,1.72,0,0,0,1.72,1.72H20.91v0A1.57,1.57,0,0,1,19.55,22Z"/>
    <rect style={style} className="cls-1" x="4.05" y="8.94" width="13.1" height="5.95"/>
    <rect style={style} className="cls-1" x="4.05" y="3.04" width="3.83" height="3.83"/>
    <line style={style} className="cls-1" x1="10.1" y1="3.66" x2="17.15" y2="3.66"/>
    <line style={style} className="cls-1" x1="10.1" y1="6.26" x2="17.15" y2="6.26"/>
    <line style={style} className="cls-1" x1="4.05" y1="17.68" x2="9.58" y2="17.68"/>
    <line style={style} className="cls-1" x1="11.62" y1="17.68" x2="17.15" y2="17.68"/>
    <line style={style} className="cls-1" x1="4.05" y1="20.15" x2="9.58" y2="20.15"/>
    <line style={style} className="cls-1" x1="11.62" y1="20.15" x2="17.15" y2="20.15"/>
    </svg>
  )
}

export default NewsSvg