import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import {config} from '../config';




class Help_rules_of_use extends Component {
    render() {
        return (
 
            <div className="container-mobile m-auto">
                
                <link rel="stylesheet" type="text/css" href="assets/css/help.css?v=2"/>
                <div className="head bg-yellow shadow position-relative">
                    <div className="icon-back">
                        <a href="/help">
                            <div className="icon rounded-circle shadow">
                                <img src="/assets/images/back.svg" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div className=" h-100 p-3 pl-5">
                        <h3 className="h-0">
                            กฎระเบียบการใช้งาน
                        </h3> 
                    </div>
                </div>

                <div className="feed p-3" style={{marginBottom: '4em'}}>

                    <p className="">
                            Lorem ipsum dolor sit anet, consectetuer adipiscing elit,
                        sed diam nonummy nibh euismod tincidunt ut laoreet
                        aolore magna aliquam erat volutpat. Ut wisi enim adminim
                        veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                        nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure
                        dolor in henderit in vulputate velit esse molestie consequat,
                        Vel illum dolore eu feugiat nulla facilisis at vero eros et iusto
                        odio dignissim qui blandit praesent luptatum zzril delenit augue
                        duis dolore te feugiat nulla facilisi.

                    </p>
                   
                </div>
            </div>
        )
    }
}
export default Help_rules_of_use;
