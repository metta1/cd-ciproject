import React, { Component } from "react";
import axios from "axios";

import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Header";
import Footer from "./Footer";
import { config } from "../config";
import parse from 'html-react-parser';
import { Store_color } from "../template";

const defaultPDPA = `<div className="text-left p-2">
<h4 className="m-0">
    นโยบายความเป็นส่วนตัว
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    HO ได้จัดทำนโยบายคุ้มครองความเป็นส่วนตัวฉบับนี้ขึ้น เพื่อคุ้มครองข้อมูลส่วนบุคคลของผู้ใช้บริการทุกท่าน (Personal Information) ที่เข้ามายังระบบของ HO ดังนี้
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การเก็บรวบรวมข้อมูลส่วนบุคคล
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    1.	เพื่อความสะดวกในการให้บริการแก่ผู้ใช้บริการทุกท่านที่แวะมายังระบบ (ของหน่วยงาน) ทางระบบจึงได้จัดเก็บรวบรวมข้อมูลส่วนบุคคลของท่านไว้ เช่น อีเมล์แอดเดรส (Email Address) ชื่อ (Name) ที่อยู่หรือที่ทำงาน (Home or Work Address) รหัสไปรษณีย์ (ZIP Code) หรือหมายเลขโทรศัพท์ (Telephone Number) เป็นต้น
</p>
<p className="mb-1">
    2.	ในกรณีที่ท่านสมัคร (Sign up/Register) เพื่อสมัครสมาชิกหรือเพื่อใช้บริการอย่างใดอย่างหนึ่ง HO จะ เก็บรวบรวมข้อมูลส่วนบุคคลของท่านเพิ่มเติม เช่น เพศ (Sex) อายุ (Gender) สิ่งที่โปรดปราน/ความชอบ (Preferences/Favorites) ความสนใจ (Interests) และที่อยู่ในการแจ้งค่าใช้จ่าย (Billing Address) เป็นต้น
</p>
<p>
    3.	นอกจากนั้น เพื่อสำรวจความนิยมในการใช้บริการ อันจะเป็นประโยชน์ในการนำสถิติไปใช้ในการปรับปรุงคุณภาพในการให้บริการ HO จึงจำเป็นต้องจัดเก็บรวบรวมข้อมูลของท่านบางอย่างเพิ่มเติมด้วย ได้แก่ หมายเลขไอพี (IP Address) ชนิดของโปรแกรมค้นผ่าน (Browser Type) ชื่อโดเมน (Domain Name) บันทึกหน้าเว็บ (web page) ของระบบที่ผู้ใช้เยี่ยมชม เวลาที่เยี่ยมชมระบบ (Access Times) และระบบที่ผู้ใช้บริการเข้าถึงก่อนหน้านั้น (Referring Website Addresses)
</p>
<p>
    4.	HO ขอแนะนำให้ท่านตรวจสอบนโยบายความเป็นส่วนตัวของระบบอื่นที่เชื่อมโยงจาก ระบบนี้ เพื่อจะได้ทราบและเข้าใจว่าระบบดังกล่าวเก็บรวบรวม ใช้ หรือดำเนินการเกี่ยวกับข้อมูลส่วยบุคคลของท่านอย่างไร ทั้งนี้ HO ไม่ สามารถรองรับข้อความ หรือรับรองการดำเนินการตามที่ได้มีการประกาศไว้ในระบบดังกล่าวได้ และไม่ขอรับผิดชอบใดๆ หากระบบเหล่านั้นไม่ได้ปฏิบัติการหรือดำเนินการใดๆ ตามนโยบายความเป็นส่วนตัวที่ระบบดังกล่าวได้ประกาศไว้
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    นโยบายความเป็นส่วนตัว
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
    <p className="mb-1">
    HO ได้จัดทำนโยบายคุ้มครองความเป็นส่วนตัวฉบับนี้ขึ้น เพื่อคุ้มครองข้อมูลส่วนบุคคลของผู้ใช้บริการทุกท่าน (Personal Information) ที่เข้ามายังระบบของ HO ดังนี้
    </p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การใช้ข้อมูลส่วนตัว
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    1.	HO จะใช้ข้อมูลส่วนบุคคลของท่านเพียงเท่าที่จำเป็น เช่น ชื่อ และที่อยู่ เพื่อใช้ในการติดต่อ ให้บริการ ประชาสัมพันธ์ หรือให้ข้อมูลข่าวสารต่างๆ รวมทั้งสำรวจความคิดเห็นของท่านในกิจการหรือกิจกรรมของ HO เท่านั้น
</p>
<p>
    2.	HO ขอรับรองว่าจะไม่นำข้อมูลส่วนบุคคลของท่านที่ HO ได้เก็บรวบรวมไว้ ไปขายหรือเผยแพร่ให้กับบุคคลภายนอกโดยเด็ดขาด เว้นแต่จะได้รับอนุญาตจากท่านเท่านั้น
</p>
<p>
    3.	ในกรณีที่ HO ได้ ว่าจ้างหน่วยงานอื่นเพื่อให้ดำเนินการเกี่ยวกับข้อมูลส่วนบุคคลของท่าน เช่น การจัดส่งพัสดุไปรษณีย์ การวิเคราะห์เชิงสถิติในกิจการหรือกิจกรรมของ HO เป็นต้น HO จะกำหนดให้หน่วยงานที่ได้ว่าจ้างให้ดำเนินการดังกล่าว เก็บรักษาความลับและความปลอดภัยของข้อมูลส่วนบุคคลของท่าน และกำหนดข้อห้ามมิให้มีการนำข้อมูลส่วนบุคคลดังกล่าวไปใช้นอกเหนือจาก กิจกรรมหรือกิจการของ HO
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    สิทธิในการควบคุมข้อมูลส่วนบุคคลของท่าน
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    เพื่อประโยชน์ในการรักษาความเป็นส่วนตัวของท่านๆ มีสิทธิที่จะให้มีการใช้หรือแชร์ข้อมูลส่วนบุคคลของท่าน หรืออาจเลือกที่จะไม่รับข้อมูลหรือสื่อทางการตลาดใดๆจาก HO ก็ได้ โดยเพียงแต่ท่านกรอกความจำนงดังกล่าวเพื่อแจ้งให้ HO เป็นลายลักษณ์อักษร หรือ ทาง อีเมล์แอดเดรส (Email Address)    
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การรักษาความปลอดภัยสำหรับข้อมูลส่วนบุคคล
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    เพื่อประโยชน์ในการรักษาความลับและความปลอดภัยสำหรับข้อมูลส่วนบุคคลของท่าน HO จึง ได้กำหนดระเบียบภายในหน่วยงานเพื่อกำหนดสิทธิในการเข้าถึงหรือใช้ข้อมูลส่วน บุคคลของท่าน และเพื่อรักษาความลับและความปลอดภัยของข้อมูลบางอย่างที่มีความสำคัญอย่าง ยิ่ง เช่น หมายเลขบัตรเครดิต เป้นต้น HO จึงได้จัดให้มีช่องทางการสื่อสารแบบปลอดภัยสำหรับข้อมูลดังกล่าวด้วยการเข้า รหัสลับข้อมูลดังกล่าว เช่น จัดให้มีการใช้ Secure Socket Layer (SSL) protocol เป็นต้น
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การใช้คุกกี้ (Cookies)
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    “คุกกี้” คือ ข้อมูลที่ HO ส่ง ไปยังโปรแกรมค้นผ่านระบบ (Web browser) ของผู้ใช้บริการ และเมื่อมีการติดตั้งข้อมูลดังกล่าวไว้ในระบบของท่านแล้ว หากมีการใช้ “คุกกี้” ก็จะทำให้ระบบ (ของหน่วยงาน) สามารถบันทึกหรือจดจำข้อมูลของผู้ใช้บริการไว้ จนกว่าผู้ใช้บริการจะออกจากโปรแกรมค้นผ่านระบบ หรือจนกว่าผู้ใช้บริการจะทำการลบ “คุกกี้” นั้นเสีย หรือไม่อนุญาตให้ “คุกกี้” นั้นทำงานต่อไป
    หากท่านเลือกใช้ “คุกกี้” แล้ว ท่านจะได้รับความสะดวกสบายในการท่องระบบมากขึ้น เพราะ “คุกกี้” จะช่วยจดจำระบบที่ท่านแวะ, เยี่ยมชม หรือท่อง
    ทั้งนี้ HO จะนำข้อมูลที่ “คุกกี้” ได้บันทึกหรือเก็บรวบรวมไว้ไปใช้ในการวิเคราะห์เชิงสถิติ หรือในกิจกรรมอื่นของ HO เพื่อปรับปรุงคุณภาพการให้บริการของ HO ต่อไป
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การปรับปรุงนโยบายความเป็นส่วนตัว
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    HO อาจ ทำการปรับปรุงหรือแก้ไขนโยบายความเป็นส่วนตัวโดยไม่ได้แจ้งให้ท่านทราบล่วง หน้า ทั้งนี้ เพื่อความเหมาะสมและมีประสิทธิภาพในการให้บริการ ดังนี้ HO จึงขอแนะนำให้ผู้ใช้บริการอ่านนโยบายความเป็นส่วนตัวทุกครั้งที่แวะชม หรือเยี่ยมชม หรือมีการใช้บริการจากระบบ (ของหน่วยงาน)      
</p>
</div>
<div className="text-left p-2">
<h4 className="m-0">
    การปฏิบัติตามนโยบายความเป็นส่วนตัวและการติดต่อกับ HO
</h4> 
</div>
<div className="col p-3-3 w-100 mb-5">
<p className="mb-1">
    ในกรณีที่ท่านมีข้อสงสัย ข้อเสนอแนะ หรือข้อติชมใดๆ เกี่ยวกับนโยบายความเป็นส่วนตัว หรือการปฏิบัติตามนโยบายความเป็นส่วนตัวฉบับนี้ HO ยินดีที่จะตอบข้อสงสัย รับฟังข้อเสนอแนะ และคำติชมทั้งหลาย อันจะเป็นประโยชน์ต่อการปรับปรุงการให้บริการของ HO ต่อไป โดยท่านสามารถติดต่อกับ HO ตามที่ปรากฏข้างล่างนี้
</p>
<p>E-mail : hello@ho.app</p>
</div>`;

class Help_privacy_policy extends Component {
  state = {
    store_id: localStorage.getItem('storageStoreID'),
    pdpa_condition: "",
  };
  async componentDidMount() {
    const obj = this;
    if (this.state.store_id) {
      await axios({
        method: "get",
        url:
          config.api.base_url +
          "/store_form_pdpa/api/pdpa/" +
          this.state.store_id,
      })
        .then(function (response) {
            if (response.status == 200) {
                console.log(response)
                obj.setState({
                    pdpa_condition: response.data.pdpa_condition,
                });
            }else{
                obj.setState({
                    pdpa_condition: defaultPDPA,
                });  
            }
        })
        .catch(function (response) {
            obj.setState({
                pdpa_condition: defaultPDPA,
            }); 
        });
    }else{
        obj.setState({
            pdpa_condition: defaultPDPA,
        }); 
    }
  }

  handleBackPage = () => {
    const { prevPath } = this.props.location.state;
    window.location = prevPath;
  };

  render() {
    return (
      <div className="container-mobile m-auto">
        <link rel="stylesheet" type="text/css" href="assets/css/help.css?v=2" />
        {
                this.state.store_id &&(
                    <Store_color/>
                )
        }
        <div className="head bg-yellow shadow position-relative">
          <div className="icon-back">
            <a href="#" onClick={this.handleBackPage}>
              <div className="icon rounded-circle shadow">
                <img src="/assets/images/back.svg" alt="" />
              </div>
            </a>
          </div>
          <div className=" h-100 p-3 pl-5">
            <h3 className="align-title m-0">นโยบายความเป็นส่วนตัว</h3>
          </div>
        </div>
        <div className="feed p-3" style={{ marginBottom: "4em" }}>
          {parse(this.state.pdpa_condition)}
        </div>
      </div>
    );
  }
}
export default Help_privacy_policy;
