import React, { Component } from 'react'
import axios from 'axios'
import {Route, Link, Redirect} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import {config} from '../config';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Help extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        eventid:this.props.match.params.eventid
    };


    async componentDidMount(){
        
        const result = await axios.get(config.api.base_url+'/admin_event/api/event/'+this.state.eventid).catch(function (error) {

           
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
            console.log(result.data)
        }
        
      
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            
                
            <div className="container-mobile m-auto">
                
                <Header/>

                <div className="feed p-3" style={{marginBottom: '4em'}}>
                    <a href="/faq">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">คำถามที่พบบ่อย</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href="/privacy_condition">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">เงื่อนไขความเป็นส่วนตัว</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <Link className="link" to={{
                            pathname: "/privacy_policy",
                            state: { prevPath: this.props.location.pathname }
                    }}
                    >
                            <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">นโยบายความเป็นส่วนตัว</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </Link>
                    {/* <a href="/rules_of_use">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-100 ">
                                <span className="pl-auto">กฎระเบียบการใช้งาน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a> */}
                   
                </div>
                    

                {/* <Footer/> */}
                
                
            </div>
        )
    }
}
export default Help;
