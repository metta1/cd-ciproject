import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
        <>
        <link rel="stylesheet" type="text/css" href="assets/css/help.css?v=2"/>
        
            <div className="head bg-yellow shadow position-relative">
                <div className="icon-back">
                    <a href="/account">
                        <div className="icon rounded-circle shadow">
                            <img src="/assets/images/back.svg" alt=""/>
                        </div>
                    </a>
                </div>
                <div className=" h-100 p-3 pl-5">
                    <h3 className="align-title m-0">
                        ช่วยเหลือ
                    </h3> 
                </div>
            </div>
        </>)
    
        

    }
}
export default Header;
