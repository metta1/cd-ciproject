import React, { Component } from 'react'
import $ from 'jquery'
import './navigation.css'
import axios from 'axios'
import {config} from '../config';
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();

const menuHomeArray = [
    'home'
]
const menuAccountArray = [
    'account'
]

const menuDealArray = [
    'promotion_category'
]

const menuFeedArray = [
    'feed',
    'feed_detail'
]

const menuCardArray = [
    'user_card',
]

const menuShopArray = [
    'shop',
]

const hiddenMenuShopArray = [
    83,
    139,
    140,
    141,
    162
]

class Navigation_shop extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleChangeActive = this.handleChangeActive.bind(this);        
    }

    state = {
        data : [],
        storeid : localStorage.getItem('storageStoreID') !== undefined ? localStorage.getItem('storageStoreID') : this.props.storeid,
        menuActive : undefined,
        //showHome : sessionStorage.getItem('showHome') !== '0' ? 1 : 0
        showHome : 0//ปิดเมนู Home
    };


    async componentDidMount(){

        if(this.state.showHome===0){//ซ่อนเมู home ให้ขยายเมนูใหญ่ขึ้น
            $('.menu-item').css("width", "25%");
        }

        if(inArray(this.state.storeid,hiddenMenuShopArray)){
            $('.menu-item').css("width", "30%");
        }
        
        this.handleChangeActive()
     
        const result = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.storeid)
        if(result.status===200){
            this.setState({data: result.data});
        }

       
        if(this.state.data !== undefined){
            let colortabbar = this.state.data['colortabbar'];
            let coloricon   = this.state.data['coloricon'];
            let colorfont   = this.state.data['colorfont'];
            if(colortabbar && coloricon && colorfont){
                console.log(1)
               
                $(".footer" ).css( "background-color", "#"+colortabbar );
                $(".footer-bg" ).css( "color", "#"+colortabbar );
                $('.img-border-color').css( "fill", "#"+colorfont );
                $('.img-border-color-stroke').css( "stroke", "#"+colorfont );
                $('.footer .menu-item span').css("color", "#"+colorfont);//, .noti-num-border
                $('.head.bg-black, .bg-black, .mestyle__wrapper').css("background-color", "#"+colortabbar);
                $('.link__box .tab, .status_and_qrcode__box, .detail__box .tab').css("background-color", "#"+coloricon);
                $('.account-img-bg-color' ).css( "fill", "#"+coloricon );
                $('.account-img-stroke').css( "stroke", "#"+coloricon );
                $('.btn-add').css('background-color','#'+colortabbar)
                /* $('.btn-add, .noti-num-border').css('background-color','#'+colortabbar)
                $('.noti-num-border').css('border-color','#'+coloricon) */

                $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg, .footer.menu-shop .menu-item.active .img-bg-color" ).css( "fill", "#"+coloricon );
                //$('.footer.menu-shop .menu-item.active .img-border-color-stroke').css( "stroke", "#fff" );
                //$('.footer.menu-shop .menu-item.active .img-border-color').css( "fill", "#FFF" );
                
                
                $('.stamp .stamp-bg, #stamp-bg').css("background-color", "#"+colortabbar);
                
                
                /* ส่วนนี้ไว้กก่อน*/
                /* $('.head h1.text-white').css("color", "#"+colorfont);//ติด
                $('.mestyle__wrapper').css( "background-color", "#"+colortabbar );
                $('.mestyle__wrapper .status_and_qrcode__box, .mestyle__wrapper .link__box .tab, .shop_detail__wrapper .detail__box .tab, .shop_detail__wrapper .link__box .tab').css( "background-color", "#"+coloricon );
                $('.product-add').css( "background-color", "#"+coloricon ); */
            }else{
                console.log(2)
                $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg, .footer.menu-shop .menu-item.active .img-bg-color" ).css( "fill", "#DA1111" );
                $(".footer" ).css( "background-color", "#000000" );
                $('.img-border-color').css( "fill", "#FFFFFF" );
                $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
            }
            
        }else{
            console.log(3)
            $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg" ).css( "fill", "#DA1111" );
            $(".footer" ).css( "background-color", "#000000" );
            $('.img-border-color').css( "fill", "#FFFFFF" );
            $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
         
        }

        
    }


    handleChangeActive = e => {
        if(this.props.active !== undefined){
            let active        = (this.props.active).toLowerCase();
            
            this.setState({menuActive : active})
        }else{
            if(inArray(currentComponent,menuAccountArray)){
                this.setState({menuActive : 'account'})
            }
            if(inArray(currentComponent,menuDealArray)){
                this.setState({menuActive : 'deal'})
            }
            if(inArray(currentComponent,menuFeedArray)){
                this.setState({menuActive : 'feed'})
            }
            if(inArray(currentComponent,menuHomeArray)){
                this.setState({menuActive : 'home'})
            }
            if(inArray(currentComponent,menuCardArray)){
                this.setState({menuActive : 'card'})
            }
            if(inArray(currentComponent,menuShopArray)){
                this.setState({menuActive : 'shop'})
            }

            
        }
        
    }


    render() {
        return (
            <>
                    <div className="container-mobile m-auto mt-footer pb-footer">
                        <div className="footer menu-shop mx-auto">
                                <div className="menu w-100">

                        {
                            this.state.showHome!==0 &&(
                        
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='home' ? 'active' : '')}>
                                        <a href="/home">
                                            <div className="position-relative">
                                                <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3911" data-name="Group 3911" transform="translate(-115.5 -341.201)">
                                                    <path className="img-border-color-stroke" id="Path_48" data-name="Path 48" d="M229.354,1584.949l-32.206-45.176c-6.483-6.691-12.548,0-12.548,0l-33.46,45.176v49.275h78.214Z" transform="translate(0 -1180.684)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                    <path className="img-border-color-stroke" id="Path_49" data-name="Path 49" d="M179.737,1634.224v-33.879s10.51-11.763,22.274,0v33.879" transform="translate(-1 -1180.607)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                </svg>
                                                <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3918" data-name="Group 3918" transform="translate(-115.5 -539.201)">
                                                    <g id="Group_3903" data-name="Group 3903" transform="translate(7 -19)">
                                                        <path className="img-bg-color" id="Path_46" data-name="Path 46" d="M237.733,1588.37l-32.206-45.176c-6.489-6.692-12.549,0-12.549,0l-33.456,45.176v49.275h78.215Z" transform="translate(-7 -963.684)" fill="#ffc22b"/>
                                                        <path className="img-bg-color" id="Path_47" data-name="Path 47" d="M187.488,1637.645v-33.88s10.51-11.763,22.274,0v33.88" transform="translate(-7 -959.684)" fill="#fff"/>
                                                    </g>
                                                    </g>
                                                </svg>                            
                                            </div>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                            )
                        }
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='feed' ? 'active' : '')}>
                                        <a href={'/store_feed/'+localStorage.getItem('storageStoreID')}>
                                            <div className="position-relative">
                                            <svg class="img-border" xmlns="http://www.w3.org/2000/svg"  
                                                    x="0px" y="0px" width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" >
                                                    <g id="News_Feed" transform="translate(3430 4289)">
                                                        <g id="iconfinder_Artboard_3_5740093" transform="translate(-3409 -4269)">
                                                            <path className="img-border-color-stroke img-border-color"  id="Path_18711" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="
                                                                M-1.269-18.1v9.08H-19.43v77.184c0,7.522,6.098,13.619,13.62,13.619h69.193c9.418-0.025,17.048-7.651,17.07-17.069V-18.1H-1.269z
                                                                M-10.35,68.164V0.061h9.08v68.103c-0.048,2.508-2.12,4.501-4.627,4.452C-8.337,72.569-10.304,70.604-10.35,68.164z
                                                                M71.374,64.714c0,4.413-3.578,7.99-7.991,7.99l0,0H6.995c0.528-1.455,0.804-2.992,0.816-4.541V-9.02h63.563V64.714z"/>
                                                            <rect className="img-border-color-stroke img-border-color"  id="Rectangle_1877" x="26.236" y="0.576" fill="none" width="35.072" height="8.35"/>
                                                            <rect className="img-border-color-stroke img-border-color"  id="Rectangle_1878" x="16.216" y="18.946" fill="none" width="45.093" height="10.02"/>
                                                            <rect className="img-border-color-stroke img-border-color"  id="Rectangle_1879" x="16.216" y="35.646" fill="none" width="45.093" height="10.02"/>
                                                            <rect className="img-border-color-stroke img-border-color"  id="Rectangle_1880" x="16.216" y="54.018" fill="none" width="45.093" height="10.02"/>
                                                        </g>
                                                        <rect id="Rectangle_2023" x="-3463.5" y="-4320.5" fill="#FFFFFF" fill-opacity="0" width="167" height="167"/>
                                                    </g>
                                                </svg>
                                                <svg class="img-bg" xmlns="http://www.w3.org/2000/svg"
                                                    x="0px" y="0px" width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" >
                                                    <g id="News_Feed" transform="translate(3367 4287)">
                                                        <g id="iconfinder_Artboard_3_5740093" transform="translate(-3347 -4269)">
                                                            <path className="img-bg-color"  id="Path_18711" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M-1.939-19.439v9.08
                                                                H-20.1v77.184c0,7.521,6.098,13.619,13.62,13.619h69.194c9.418-0.024,17.047-7.651,17.069-17.07v-82.813H-1.939z M-11.02,66.824
                                                                V-1.278h9.08v68.103c-0.048,2.507-2.12,4.5-4.626,4.452C-9.007,71.229-10.974,69.263-11.02,66.824z M70.705,63.373
                                                                c0,4.413-3.578,7.991-7.991,7.991l0,0H6.325c0.528-1.456,0.804-2.992,0.816-4.542V-10.36h63.564V63.373z"/>
                                                            <rect className="img-bg-color"  id="Rectangle_1877" x="25.566" y="-0.764" width="35.071" height="8.35"/>
                                                            <rect className="img-bg-color"  id="Rectangle_1878" x="15.546" y="17.606" width="45.091" height="10.02"/>
                                                            <rect className="img-bg-color"  id="Rectangle_1879" x="15.546" y="34.306" width="45.091" height="10.021"/>
                                                            <rect className="img-bg-color"  id="Rectangle_1880" x="15.546" y="52.677" width="45.091" height="10.02"/>
                                                        </g>
                                                        <rect id="Rectangle_2023" x="-3400.5" y="-4318.5" fill="none" fill-opacity="0" width="167" height="167"/>
                                                        <rect className="img-bg-color" id="Rectangle_2029" x="-3358.75" y="-4273.41" width="11.69" height="76.82"/>
                                                        <rect className="img-bg-color" id="Rectangle_2030" x="-3340.38" y="-4278.42" width="68.47" height="81.83"/>
                                                    </g>
                                                </svg>
                                                                   
                                            </div>
                                            <span>News Feed</span>
                                        </a>
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='deal' ? 'active' : '')}>
                                        <a href={'/deal/'+localStorage.getItem('storageStoreID')}>
                                            <div className="position-relative">
                                            <svg  class="img-border" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                    width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" enable-background="new -27.5 -13.43 155 126.859"
                                                    >
                                                <g id="Promotion" transform="translate(3367 3840)">
                                                    <rect  id="Rectangle_2027" x="-3391.5" y="-3862.5" fill="#FFFFFF" fill-opacity="0" width="156" height="156"/>
                                                    <g id="Group_5582" transform="translate(-3339.984 -3833) rotate(17)">
                                                        <path className="img-border-color-stroke img-border-color" id="Path_18717" fill="none" stroke="#000000" stroke-width="2" d="M33.483,33.375c-3.813-0.885-7.622,1.489-8.507,5.302
                                                            c-0.426,1.836-0.103,3.765,0.896,5.362c0.992,1.599,2.586,2.734,4.423,3.147c3.814,0.884,7.622-1.491,8.506-5.306
                                                            c0.427-1.835,0.104-3.763-0.895-5.36C36.903,34.93,35.315,33.799,33.483,33.375z M33.66,43.15
                                                            c-1.57,0.973-3.632,0.491-4.606-1.076c-0.975-1.569-0.493-3.631,1.076-4.607c1.563-0.97,3.617-0.496,4.596,1.062
                                                            c0.984,1.562,0.515,3.624-1.047,4.608C33.673,43.14,33.666,43.144,33.66,43.15z"/>
                                                        <path className="img-border-color-stroke img-border-color" id="Path_18718" fill="none" stroke="#000000" stroke-width="2" d="M62.689,40.118c-3.813-0.885-7.621,1.489-8.507,5.304
                                                            c-0.426,1.834-0.103,3.764,0.897,5.361c0.993,1.6,2.585,2.733,4.422,3.148c3.813,0.884,7.623-1.491,8.506-5.306
                                                            c0.425-1.834,0.102-3.764-0.897-5.361C66.111,41.675,64.52,40.542,62.689,40.118z M62.867,49.892
                                                            c-1.573,0.97-3.633,0.494-4.621-1.066c-0.985-1.564-0.513-3.629,1.052-4.613c0.758-0.475,1.673-0.627,2.545-0.424
                                                            c0.866,0.196,1.618,0.73,2.091,1.481C64.902,46.846,64.428,48.906,62.867,49.892z"/>
                                                        <path  className="img-border-color-stroke img-border-color"id="Path_18719" fill="none" stroke="#000000" stroke-width="4" d="M51.511,21.963c-1.005-0.225-2.006,0.4-2.245,1.402
                                                            l-9.174,39.736c-0.224,1.006,0.4,2.006,1.403,2.245c1.005,0.225,2.005-0.4,2.245-1.403l9.173-39.736
                                                            C53.139,23.202,52.515,22.202,51.511,21.963z"/>
                                                        <path className="img-border-color-stroke img-border-color" id="Path_18720" fill="none" stroke="#000000" stroke-width="4" d="M39.365-9.669c-0.369-0.373-0.853-0.614-1.373-0.687
                                                            L7.795-14.995c-0.768-0.134-1.552,0.123-2.092,0.687L-13.674,5.07c-0.555,0.544-0.81,1.325-0.687,2.091l4.641,30.197
                                                            c0.079,0.519,0.319,0.999,0.686,1.373l50.489,50.49c1.636,1.641,3.86,2.558,6.177,2.549c2.313-0.004,4.534-0.918,6.176-2.548
                                                            l36.081-36.077c1.64-1.636,2.558-3.86,2.549-6.177c-0.004-2.313-0.921-4.532-2.55-6.176L39.365-9.669z M86.394,49.677
                                                            l-36.082,36.08c-0.717,0.72-1.696,1.121-2.713,1.111c-1.015,0.003-1.991-0.396-2.712-1.111L-5.015,35.854l-4.313-28.17
                                                            L8.318-9.963l28.171,4.313l49.902,49.902c0.721,0.718,1.122,1.694,1.111,2.713c-0.002,1.016-0.402,1.989-1.111,2.713
                                                            L86.394,49.677z"/>
                                                        <path className="img-border-color-stroke img-border-color" id="Path_18721" fill="none" stroke="#000000" stroke-width="2" d="M18.83,3.628C15.256,0.053,9.462,0.051,5.886,3.623
                                                            C5.885,3.625,5.883,3.627,5.881,3.628c-3.567,3.579-3.567,9.369,0,12.949c3.573,3.575,9.368,3.577,12.944,0.004
                                                            c0.001-0.002,0.002-0.003,0.004-0.004C22.396,12.997,22.396,7.207,18.83,3.628z M15.365,13.105
                                                            c-0.792,0.806-1.877,1.255-3.006,1.248c-1.126-0.007-2.205-0.455-3.006-1.248c-1.66-1.652-1.667-4.338-0.015-5.998
                                                            c0.005-0.006,0.01-0.01,0.015-0.016c0.792-0.805,1.877-1.255,3.007-1.248c1.126,0.008,2.205,0.455,3.006,1.248
                                                            c1.661,1.651,1.67,4.336,0.019,5.997c-0.006,0.005-0.011,0.011-0.018,0.017L15.365,13.105z"/>
                                                    </g>
                                                </g>
                                            </svg>
                                            
                                            <svg class="img-bg"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                    width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" enable-background="new -27.5 -13.43 155 126.859"
                                                 >
                                                <g id="Promotion" transform="translate(3367 3840)">
                                                    <rect id="Rectangle_2027" x="-3391.5" y="-3862.5" fill="#FFFFFF" fill-opacity="0" width="156" height="156"/>
                                                    <g id="Group_5582" transform="translate(-3339.984 -3833) rotate(17)">
                                                        <path className="img-bg-color" id="Path_18717" stroke="#000000" stroke-width="2" d="M33.483,33.374c-3.813-0.886-7.622,1.489-8.507,5.302
                                                            c-0.426,1.836-0.103,3.765,0.896,5.363c0.992,1.599,2.586,2.733,4.422,3.146c3.814,0.884,7.622-1.491,8.506-5.306
                                                            c0.426-1.836,0.104-3.763-0.896-5.361C36.903,34.93,35.315,33.799,33.483,33.374z M33.66,43.15
                                                            c-1.57,0.973-3.631,0.492-4.606-1.076c-0.975-1.57-0.493-3.631,1.076-4.607c1.564-0.971,3.618-0.496,4.597,1.062
                                                            c0.984,1.562,0.515,3.624-1.046,4.609C33.673,43.14,33.666,43.144,33.66,43.15z"/>
                                                        <path className="img-bg-color" id="Path_18718" stroke="#000000" stroke-width="2" d="M62.689,40.118c-3.814-0.885-7.621,1.49-8.507,5.304
                                                            c-0.426,1.834-0.102,3.764,0.897,5.361c0.994,1.6,2.586,2.733,4.423,3.148c3.814,0.885,7.622-1.491,8.507-5.306
                                                            c0.424-1.834,0.1-3.764-0.897-5.36C66.11,41.675,64.52,40.542,62.689,40.118z M62.866,49.891c-1.573,0.971-3.633,0.495-4.62-1.065
                                                            c-0.986-1.563-0.514-3.628,1.051-4.612c0.758-0.475,1.673-0.628,2.545-0.424c0.865,0.195,1.618,0.73,2.09,1.481
                                                            C64.901,46.845,64.427,48.905,62.866,49.891z"/>
                                                        <path className="img-bg-color" id="Path_18719" stroke="#000000" stroke-width="4" d="M51.511,21.963c-1.006-0.224-2.006,0.4-2.244,1.402l-9.175,39.736
                                                            c-0.224,1.008,0.4,2.006,1.403,2.245c1.005,0.226,2.005-0.399,2.246-1.401l9.174-39.737
                                                            C53.139,23.202,52.514,22.201,51.511,21.963z"/>
                                                        <path className="img-bg-color" id="Path_18720" stroke="#000000" stroke-width="4" d="M39.365-9.67c-0.369-0.373-0.852-0.614-1.372-0.687L7.796-14.996
                                                            c-0.768-0.134-1.553,0.124-2.092,0.687L-13.675,5.069c-0.554,0.545-0.81,1.325-0.687,2.091l4.641,30.198
                                                            c0.079,0.518,0.319,0.998,0.686,1.373l50.489,50.49c1.636,1.641,3.86,2.559,6.177,2.549c2.313-0.004,4.533-0.918,6.175-2.548
                                                            l36.081-36.076c1.642-1.637,2.559-3.861,2.551-6.177c-0.004-2.315-0.921-4.533-2.55-6.176L39.365-9.67z M86.393,49.677
                                                            l-36.081,36.08c-0.718,0.719-1.695,1.121-2.713,1.111c-1.015,0.004-1.991-0.396-2.713-1.112L-5.016,35.854l-4.313-28.17
                                                            L8.318-9.963l28.17,4.313l49.903,49.903c0.721,0.717,1.121,1.695,1.111,2.713c-0.002,1.015-0.403,1.989-1.112,2.713L86.393,49.677
                                                            z"/>
                                                        <path className="img-bg-color" id="Path_18721" stroke="#000000" stroke-width="2" d="M18.829,3.627C15.257,0.053,9.462,0.05,5.885,3.623
                                                            C5.884,3.624,5.883,3.626,5.881,3.628c-3.567,3.579-3.567,9.368-0.001,12.948c3.574,3.576,9.368,3.578,12.944,0.005
                                                            c0.001-0.002,0.002-0.003,0.004-0.004C22.396,12.996,22.396,7.206,18.829,3.627z M15.364,13.105
                                                            c-0.791,0.806-1.877,1.256-3.006,1.248c-1.127-0.007-2.205-0.455-3.006-1.248c-1.66-1.652-1.667-4.338-0.016-5.998
                                                            c0.005-0.006,0.011-0.011,0.015-0.016c0.793-0.805,1.877-1.255,3.007-1.248c1.127,0.007,2.205,0.455,3.007,1.248
                                                            c1.661,1.651,1.669,4.336,0.019,5.997c-0.007,0.005-0.012,0.012-0.018,0.017L15.364,13.105z"/>
                                                    </g>
                                                    <path  className="img-bg-color" id="Path_18843" d="M-3350.948-3829.555c-1.091-0.1-6.165,28.839-6.165,28.839l35.998,66.231l52.906-27.846l-35.901-67.224
                                                        l-25.357-10.542L-3350.948-3829.555z"/>
                                                </g>
                                            </svg>
                                            
                                            </div>
                                            <span>Promotion</span>
                                        </a>
                    
                                    </div>
                        {
                            !inArray(this.state.storeid,hiddenMenuShopArray) &&(
                            
                        
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='shop' ? 'active' : '')}>
                                        <a href={'/shop/'+localStorage.getItem('storageStoreID')}>
                                            <div className="position-relative">
                                                <svg className="img-border"  version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                    x="0px" y="0px" width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859">
                                                    <g id="E-Commerce" transform="translate(3684 4419)">
                                                        <rect className="img-border-color " id="Rectangle_2022" x="-3717.5" y="-4450.5" fill="#FFFFFF" fill-opacity="0" width="167" height="167"/>
                                                        <g className="img-border-color" id="Fill" transform="translate(-3668 -4404)">
                                                            <path className="img-border-color img-border-color-stroke " id="Path_18708" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" d="M11.042,71.562
                                                                c-7.004,0-12.683,5.679-12.683,12.685c0,7.005,5.679,12.683,12.683,12.683c7.005,0,12.684-5.678,12.684-12.683
                                                                C23.727,77.24,18.048,71.562,11.042,71.562L11.042,71.562z M11.042,88.474c-2.335,0-4.228-1.894-4.228-4.229
                                                                c0-2.335,1.893-4.229,4.228-4.229s4.229,1.894,4.229,4.229C15.271,86.58,13.378,88.474,11.042,88.474L11.042,88.474z"/>
                                                            <path className="img-border-color img-border-color-stroke " id="Path_18709" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" d="M61.775,71.562
                                                                c-7.005,0-12.684,5.679-12.684,12.685c0,7.005,5.679,12.683,12.684,12.683c7.004,0,12.683-5.678,12.683-12.683l0,0
                                                                C74.458,77.24,68.779,71.562,61.775,71.562z M61.775,88.474c-2.336,0-4.229-1.894-4.229-4.229c0-2.335,1.894-4.229,4.229-4.229
                                                                c2.335,0,4.229,1.894,4.229,4.229l0,0C66.004,86.58,64.11,88.474,61.775,88.474z"/>
                                                            <path className="img-border-color img-border-color-stroke " id="Path_18710" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" d="M-2.573-4.539l-1.818-7.314
                                                                c-1.419-5.638-6.486-9.592-12.3-9.596h-6.088v8.456h6.088c1.99-0.057,3.75,1.281,4.227,3.212L2.587,51.437l0,0l1.564,6.3
                                                                c1.43,5.687,6.567,9.653,12.43,9.598h57.876v-8.458H16.58c-1.99,0.06-3.751-1.278-4.228-3.213l-0.501-2.113l62.614-14.754
                                                                c4.629-1.078,8.266-4.651,9.428-9.258l8.456-34.068L-2.573-4.539z M75.852,27.508c-0.392,1.544-1.618,2.737-3.173,3.086L9.9,45.35
                                                                L-0.454,3.917h82.186L75.852,27.508z"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg class="img-bg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"x="0px" y="0px"
                                                        width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" enable-background="new -27.5 -13.43 155 126.859"
                                                       >
                                                    <g id="E-Commerce" transform="translate(3606 4412)">
                                                        <rect  className="img-bg-color" id="Rectangle_2022" x="-3639.5" y="-4445.5" fill="none" fill-opacity="0" width="167" height="167"/>
                                                        <g id="Fill" transform="translate(-3591 -4397)">
                                                            <path className="img-bg-color"  id="Path_18708" stroke="#000000" stroke-width="2" stroke-linecap="round" d="M10.373,69.562
                                                                c-7.005,0-12.684,5.679-12.684,12.685c0,7.005,5.679,12.683,12.684,12.683c7.005,0,12.684-5.678,12.684-12.683
                                                                C23.057,75.24,17.378,69.562,10.373,69.562L10.373,69.562z M10.373,86.474c-2.335,0-4.228-1.894-4.228-4.229
                                                                c0-2.335,1.893-4.229,4.228-4.229s4.229,1.894,4.229,4.229C14.601,84.58,12.708,86.474,10.373,86.474L10.373,86.474z"/>
                                                            <path className="img-bg-color"  id="Path_18709" stroke="#000000" stroke-width="2" stroke-linecap="round" d="M61.105,69.562
                                                                c-7.005,0-12.684,5.679-12.684,12.685c0,7.005,5.679,12.683,12.684,12.683c7.004,0,12.683-5.678,12.683-12.683l0,0
                                                                C73.788,75.24,68.109,69.562,61.105,69.562z M61.105,86.474c-2.336,0-4.229-1.894-4.229-4.229c0-2.335,1.894-4.229,4.229-4.229
                                                                c2.335,0,4.229,1.894,4.229,4.229l0,0C65.334,84.58,63.44,86.474,61.105,86.474z"/>
                                                            <path className="img-bg-color"  id="Path_18710" stroke="#000000" stroke-width="2" stroke-linecap="round" d="M-3.243-6.54l-1.818-7.314
                                                                c-1.419-5.638-6.486-9.592-12.3-9.596h-6.089v8.456h6.089c1.99-0.057,3.75,1.281,4.227,3.212L1.917,49.437l0,0l1.565,6.3
                                                                c1.43,5.687,6.567,9.653,12.43,9.598h57.876v-8.458H15.91c-1.99,0.06-3.751-1.278-4.228-3.213l-0.501-2.113l62.614-14.754
                                                                c4.628-1.078,8.266-4.651,9.428-9.259l8.456-34.068L-3.243-6.54z M75.181,25.508c-0.391,1.544-1.617,2.737-3.172,3.086L9.23,43.35
                                                                L-1.124,1.917h82.185L75.181,25.508z"/>
                                                        </g>
                                                        <path id="Path_18842" className="img-bg-color" d="M-3598.786-4399.758l14.116,51.343l69.751-17.298l8.719-34.045H-3598.786z" fill="none"/>
                                                        <circle id="Ellipse_679" cx="-3581.05" cy="-4315.24" r="8.35"/>
                                                        <circle id="Ellipse_680" cx="-3530.95" cy="-4315.24" r="8.35"/>
                                                    </g>
                                                </svg>
                                                
                                            </div>
                                            <span>Shop</span>
                                        </a>
                                    </div>
                                                                                                            
                        
                            )
                        }         
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='account' ? 'active' : '')}>
                                        <a href={'/account_shop/'+localStorage.getItem('storageStoreID')}>
                                            <div className="position-relative">
                                                <svg class="img-border" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
                                                        width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" enable-background="new -27.5 -13.43 155 126.859"
                                                      >
                                                    <g id="Member" transform="translate(3367 3727)">
                                                        <rect  id="Rectangle_2028" x="-3418.5" y="-3778.5" fill="#FFFFFF" fill-opacity="0" width="203" height="203"/>
                                                        <g id="id_card" transform="translate(-3350 -3704.15)">
                                                            <g id="Rectangle_1884" transform="translate(0 3.15)">
                                                                <path  className="img-border-color-stroke" fill="none" d="M-15.72-24.72h97.44c10.091,0,18.271,8.18,18.271,18.27v60.9c0,10.091-8.18,18.27-18.271,18.27h-97.44
                                                                    c-10.09,0-18.27-8.179-18.27-18.27v-60.9C-33.99-16.541-25.811-24.72-15.72-24.72z"/>
                                                                <path  className="img-border-color-stroke" fill="none" stroke="#FFFFFF" stroke-width="7" d="M-15.72-17.615h97.44c6.166,0,11.165,4.999,11.165,11.165v60.9
                                                                    c0,6.165-4.999,11.165-11.165,11.165h-97.44c-6.166,0-11.165-5-11.165-11.165v-60.9C-26.885-12.616-21.886-17.615-15.72-17.615z"
                                                                    />
                                                            </g>
                                                            <line className="img-border-color-stroke" id="Line_315" fill="none" stroke="" stroke-width="7" x1="-28.915" y1="7.865" x2="96.945" y2="7.865"/>
                                                            <g id="Rectangle_1895" transform="translate(40 30.15)">
                                                                <path  className="img-border-color-stroke" fill="none" d="M11.27,3.09h36.54c2.242,0,4.061,1.818,4.061,4.061v14.21c0,2.242-1.818,4.06-4.061,4.06H11.27
                                                                    c-2.241,0-4.06-1.817-4.06-4.06V7.15C7.21,4.908,9.028,3.09,11.27,3.09z"/>
                                                                <path  className="img-border-color-stroke" fill="none" stroke="#FFFFFF" stroke-width="6" d="M15.33,9.18h28.42c1.122,0,2.03,0.908,2.03,2.03v6.09
                                                                    c0,1.122-0.908,2.03-2.03,2.03H15.33c-1.122,0-2.03-0.908-2.03-2.03v-6.09C13.3,10.088,14.208,9.18,15.33,9.18z"/>
                                                            </g>
                                                            <g id="Polygon_16" transform="translate(10 34.15) rotate(-90)">
                                                                <path  className="img-border-color-stroke img-border-color" fill="none" d="M2.223-20.848c0.585-0.957,1.835-1.259,2.791-0.675c0.275,0.169,0.508,0.4,0.674,0.675l7.544,12.34
                                                                    c0.583,0.957,0.282,2.207-0.675,2.791C12.239-5.523,11.873-5.42,11.5-5.42H-3.59c-1.122-0.001-2.03-0.91-2.03-2.031
                                                                    c0-0.373,0.104-0.738,0.298-1.057L2.223-20.848z"/>
                                                                <path  className="img-border-color-stroke" fill="none" d="M3.955-21.827c0.668,0,1.336,0.323,1.732,0.972l7.545,12.346C14.06-7.156,13.085-5.42,11.5-5.42H-3.59
                                                                    c-1.586,0-2.56-1.736-1.732-3.089l7.545-12.346C2.619-21.503,3.287-21.827,3.955-21.827z"/>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                             
                                                <svg class="img-bg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
                                                        width="155px" height="126.859px" viewBox="-27.5 -13.43 155 126.859" enable-background="new -27.5 -13.43 155 126.859"
                                                        >
                                                    <g id="Member" transform="translate(3367 3727)">
                                                        <rect id="Rectangle_2028" x="-3418.5" y="-3778.5" fill="#FFFFFF" fill-opacity="0" width="203" height="203"/>
                                                        <g id="id_card" transform="translate(-3350 -3704.15)">
                                                            <g id="Rectangle_1884" transform="translate(0 3.15)">
                                                                <path className="img-bg-color" d="M-15.72-24.72H81.72c10.091,0,18.271,8.18,18.271,18.27v60.899c0,10.09-8.18,18.271-18.271,18.271H-15.72
                                                                    c-10.09,0-18.271-8.181-18.271-18.271V-6.45C-33.99-16.54-25.81-24.72-15.72-24.72z"/>
                                                                <path className="img-bg-color" fill="none" stroke="#000000" stroke-width="7" d="M-15.72-17.615H81.72c6.166,0,11.165,4.999,11.165,11.165v60.899
                                                                    c0,6.166-4.999,11.166-11.165,11.166H-15.72c-6.166,0-11.166-5-11.166-11.166V-6.45C-26.885-12.616-21.886-17.615-15.72-17.615z"
                                                                    />
                                                            </g>
                                                            <line id="Line_315" fill="none" stroke="#000000" stroke-width="7" x1="-28.915" y1="7.866" x2="96.945" y2="7.866"/>
                                                            <g id="Rectangle_1895" transform="translate(40 30.15)">
                                                                <path className="img-bg-color" fill="none" d="M11.271,3.091h36.539c2.24,0,4.06,1.817,4.06,4.06v14.21c0,2.242-1.819,4.06-4.06,4.06H11.271
                                                                    c-2.244,0-4.062-1.817-4.062-4.06V7.15C7.21,4.908,9.027,3.091,11.271,3.091z"/>
                                                                <path className="img-bg-color" fill="none" stroke="#000000" stroke-width="6" d="M15.33,9.181h28.42c1.122,0,2.03,0.906,2.03,2.03v6.09
                                                                    c0,1.121-0.908,2.029-2.03,2.029H15.33c-1.121,0-2.029-0.908-2.029-2.029v-6.09C13.301,10.087,14.209,9.181,15.33,9.181z"/>
                                                            </g>
                                                            <g id="Polygon_16" transform="translate(10 34.15) rotate(-90)">
                                                                <path className="img-bg-color" fill="none" d="M2.223-20.848c0.584-0.957,1.836-1.259,2.791-0.675c0.275,0.169,0.507,0.399,0.674,0.675l7.544,12.34
                                                                    c0.584,0.957,0.283,2.207-0.675,2.79c-0.318,0.195-0.684,0.298-1.056,0.298H-3.591C-4.712-5.421-5.62-6.33-5.62-7.451
                                                                    c0-0.372,0.104-0.738,0.298-1.057L2.223-20.848z"/>
                                                                <path className="img-bg-color" fill="none" d="M3.955-21.827c0.668,0,1.336,0.323,1.732,0.971l7.545,12.347c0.827,1.354-0.147,3.089-1.732,3.089H-3.591
                                                                    c-1.586,0-2.559-1.736-1.731-3.089l7.545-12.347C2.619-21.504,3.287-21.827,3.955-21.827z"/>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            
                                            </div>
                                            <span>Member</span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                    </div> 
                
              
        
            </>
        )
    
        

    }
}
export default Navigation_shop;
