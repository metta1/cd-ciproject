import React, { Component } from 'react'
import axios from 'axios'
import {config} from '../config';

export default class Notification_num_inside extends Component {
    constructor(props){
        super(props)
        this.state = {
            userid:sessionStorage.getItem('user_id'),
            numNoti:0,
            store_id:this.props.storeid,
            notitype: this.props.notitype
        };
    }
    async componentDidMount(){
        console.log(this.state.notitype)
        console.log(this.state.store_id)
        /* NOTI */
        if(this.state.notitype=="HO"){
          

            const hoNotiResult = await axios.get(config.api.base_url+'/admin_notification/api/countNotiUnread/'+this.state.userid).catch(function (error) {
                if (error.response) {
                console.log(error.response.status);
                }
            });
            if(hoNotiResult.status===200){
                if(hoNotiResult.data !== undefined){
                    const hoNotiLength = hoNotiResult.data.countunread !== null ? parseInt(hoNotiResult.data.countunread) : 0;
                    const numNoti = hoNotiLength+parseInt(this.state.numNoti)
                    this.setState({
                        numNoti:numNoti
                    })
                }
            }
           
        }

        if(this.state.notitype=="store"){
            const storeNotiResult = await axios.get(config.api.base_url+'/store_notification/api/countNotiUnread/'+this.state.userid+'/'+this.state.store_id).catch(function (error) {
                if (error.response) {
                console.log(error.response.status);
                }
            });
            if(storeNotiResult.status===200){
                if(storeNotiResult.data !== undefined){
                    const storeNotiLength = storeNotiResult.data.countunread !== null ? parseInt(storeNotiResult.data.countunread) : 0;
                    const numNoti = storeNotiLength+parseInt(this.state.numNoti)
                    this.setState({
                        numNoti:numNoti
                    })
                }
            } 


           

           

        }

        /* const countOrderUnreadResult =  await axios.get(config.api.base_url+'/store_order/api/countOrderUnread/'+sessionStorage.getItem('user_id')+'/'+this.state.store_id);
        if(countOrderUnreadResult.status===200){
            const numNoti = parseInt(countOrderUnreadResult.data.countorder)+parseInt(this.state.numNoti)
            this.setState({
                    numNoti:numNoti
            })
           
        } */
    }
    render() {
        return (
            
            this.state.numNoti>0 && (
                <div className="alert alert-primary noti-num-border big-alert " role="alert">
                        {this.state.numNoti}
                </div>
            )
            
        )
    }
}
