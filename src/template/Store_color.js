import React, { Component } from 'react'
import $ from 'jquery'
import './navigation.css'
import axios from 'axios'
import {config} from '../config';

class Store_color extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);


    }

    state = {
        data : [],
        storeid : localStorage.getItem('storageStoreID')
    };

    async componentDidMount(){

        const result = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.storeid)
        if(result.status===200){
            this.setState({data: result.data});
        }

        console.log(this.state.data);
        if(this.state.data !== undefined){

            let colortabbar = this.state.data['colortabbar'];
            let coloricon   = this.state.data['coloricon'];
            let colorfont   = this.state.data['colorfont'];
            if(colortabbar && coloricon && colorfont){

                $(".icon-calendar").css( "fill", "#"+coloricon );
                $(".footer" ).css( "background-color", "#"+colortabbar );
                $('.img-border-color').css( "fill", "#"+colorfont );
                $('.img-border-color-stroke').css( "stroke", "#"+colorfont );
                $('.footer, .notifications h3, .nav-link, .head.bg-yellow h3, .monthtab-header p, .head.bg-black h4, .head.bg-black h3, .btn.btn-primary').css("color", "#"+colorfont);
                $('.head.bg-black, .head.bg-yellow, .bg-black, .btn-buy, .header, .active, .monthtab-header, .nav-link.active, .btn.btn-primary').css("background-color", "#"+colortabbar);
                $('.tab, .button-color').css("background-color", "#"+coloricon);
                $('.shop-list-title').css("background-color", "#"+colortabbar);
                /* $('.head h1.text-white').css("color", "#"+colorfont);//ติด
                $('.mestyle__wrapper').css( "background-color", "#"+colortabbar );
                $('.mestyle__wrapper .status_and_qrcode__box, .mestyle__wrapper .link__box .tab, .shop_detail__wrapper .detail__box .tab, .shop_detail__wrapper .link__box .tab').css( "background-color", "#"+coloricon );
                $('.product-add').css( "background-color", "#"+coloricon ); */
                $('.btn.btn-primary').css("border-color", "#"+colortabbar);

                $('.font-color, .new-point p.fontcolor, .button-color, .reward-point h4, .reward-point p').css( "color", "#"+colorfont );
                $('.mestyle__wrapper .new-point, .bar-status, .tabbar-color').css("background-color", "#"+colortabbar);
                $('.new-point .account-img-stroke').css("stroke", "#"+coloricon);
                $('#stamp-bg, .stamp-qr-code').css("background", "#"+colortabbar);
                $('.reward-flag-color').css("fill", "#"+colortabbar);
                $('.stamp-item').css("border-color", "#"+colorfont);
                $('.qrColor').css("fill", "#"+colorfont);
                $('.arrowTopColor').css('stroke', "#"+colorfont);
                // $('.stamp-item').css("background", "#"+colortabbar+"a1");

                /****login */
                //$(".account.p-4" ).css( "background-color", "#"+colortabbar );
                $('.account.p-4, .container-mobile.m-auto.bg-yellow').css('background-color', "#FFF");
                $('.btnlog').css('background-color', "#"+colortabbar);
                $('.btnlog').css('color', "#FFF");
                $('input[type=submit]').css('background-color', "#"+colortabbar);
                $('input[type=submit]').css('color', "#"+colorfont);
                //$('label.custom-control-label').css('color', "#"+colorfont);
                $('.custom-control-label::before').css('background-color', "#"+colortabbar);





            }else{
                $(".img-bg-color" ).css( "fill", "#DA1111" );
                $(".footer" ).css( "background-color", "#000000" );
                $('.img-border-color').css( "fill", "#FFFFFF" );
                $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );

            }

        }else{

            $(".img-bg-color" ).css( "fill", "#DA1111" );
            $(".footer" ).css( "background-color", "#000000" );
            $('.img-border-color').css( "fill", "#FFFFFF" );
            $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
        }


    }


    render() {
        return (
            <>




            </>
        )



    }
}
export default Store_color;
