import React, { Component } from 'react'
import $ from 'jquery'
import './navigation.css'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';


const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();

const menuHomeArray = [
    'home'
]
const menuAccountArray = [
    'account'
]

const menuDealArray = [
    'promotion_category'
]

const menuFeedArray = [
    'feed',
    'feed_detail'
]

const menuCardArray = [
    'user_card',
]


class Navigation extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleChangeActive = this.handleChangeActive.bind(this);
        
        

    }
    state = {
        menuActive : undefined,
    };

    async componentDidMount(){
        this.handleChangeActive()
        

        //$(".img-bg-color" ).css( "fill", "#F9CE43" );
        $(".footer" ).css( "background-color", "#fff" );
        
        
    }

    handleChangeActive = e => {
        if(inArray(currentComponent,menuHomeArray)){
            this.setState({menuActive : 'home'})
        }else if(inArray(currentComponent,menuFeedArray)){
            this.setState({menuActive : 'feed'})
        }else if(inArray(currentComponent,menuCardArray)){
            this.setState({menuActive : 'card'})
        }else if(inArray(currentComponent,menuDealArray)){
            this.setState({menuActive : 'deal'})
        }else if(inArray(currentComponent,menuAccountArray)){
            this.setState({menuActive : 'account'})
        }else if(this.props.active !== undefined){
            let active        = (this.props.active).toLowerCase();
            this.setState({menuActive : active})
        }
    }


    render() {
        return (
            <>
                    <div className="container-mobile m-auto mt-footer pb-footer">
                        <div className="footer menu-main mx-auto">
                                <div className="menu w-100 shadow">
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='home' ? 'active' : '')}>
                                        <a href="/home">
                                            <div className="position-relative">
                                            <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3911" data-name="Group 3911" transform="translate(-115.5 -341.201)">
                                                <path id="Path_48" data-name="Path 48" d="M229.354,1584.949l-32.206-45.176c-6.483-6.691-12.548,0-12.548,0l-33.46,45.176v49.275h78.214Z" transform="translate(0 -1180.684)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                <path id="Path_49" data-name="Path 49" d="M179.737,1634.224v-33.879s10.51-11.763,22.274,0v33.879" transform="translate(-1 -1180.607)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                </g>
                                            </svg>
                                            <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3918" data-name="Group 3918" transform="translate(-115.5 -539.201)">
                                                <g id="Group_3903" data-name="Group 3903" transform="translate(7 -19)">
                                                    <path className="img-bg-color" id="Path_46" data-name="Path 46" d="M237.733,1588.37l-32.206-45.176c-6.489-6.692-12.549,0-12.549,0l-33.456,45.176v49.275h78.215Z" transform="translate(-7 -963.684)" fill="#ffc22b"/>
                                                    <path className="img-bg-color" id="Path_47" data-name="Path 47" d="M187.488,1637.645v-33.88s10.51-11.763,22.274,0v33.88" transform="translate(-7 -959.684)" fill="#fff"/>
                                                </g>
                                                </g>
                                            </svg>                            
                                            </div>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='feed' ? 'active' : '')}>
                                            <a href="/feed">
                                                <div className="position-relative">
                                                <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3912" data-name="Group 3912" transform="translate(-372.5 -341.201)">
                                                        <g id="Rectangle_721" data-name="Rectangle 721" transform="translate(404.265 368.148)">
                                                        <rect id="Rectangle_1055" data-name="Rectangle 1055" width="85.039" height="85.039" rx="13.287" fill="none"/>
                                                        <rect id="Rectangle_1056" data-name="Rectangle 1056" width="81.053" height="81.053" rx="11.294" transform="translate(1.993 1.993)" fill="none" stroke="#303030" stroke-width="3"/>
                                                        </g>
                                                        <path id="Rectangle_723" data-name="Rectangle 723" d="M422.307,1568.806h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1568.806Z" transform="translate(0 -1172.256)" fill="#fff"/>
                                                        <path id="Rectangle_724" data-name="Rectangle 724" d="M422.307,1587.874h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1587.874Z" transform="translate(0 -1176.506)" fill="#fff"/>
                                                        <path id="Rectangle_725" data-name="Rectangle 725" d="M422.307,1606.943h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1606.943Z" transform="translate(0 -1180.755)" fill="#fff"/>
                                                        <path id="Rectangle_726" data-name="Rectangle 726" d="M418.786,1565.31h56.233a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H418.786a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,418.786,1565.31Z" transform="translate(0 -1171.477)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                        <path id="Rectangle_727" data-name="Rectangle 727" d="M418.786,1584.379h56.233a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H418.786a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,418.786,1584.379Z" transform="translate(0 -1175.727)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                        <path id="Rectangle_728" data-name="Rectangle 728" d="M418.786,1603.447h56.233a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H418.786a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,418.786,1603.447Z" transform="translate(0 -1179.976)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                </svg>
                                                <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3917" data-name="Group 3917" transform="translate(-372.5 -539.201)">
                                                        <g id="Group_3904" data-name="Group 3904" transform="translate(1 -44)">
                                                        <rect className="img-bg-color" id="Rectangle_722" data-name="Rectangle 722" width="86.368" height="85.039" rx="13.287" transform="translate(409.579 614.871)" fill="#ffc22b"/>
                                                        <path className="img-bg-color" id="Rectangle_723" data-name="Rectangle 723" d="M422.307,1568.806h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1568.806Z" transform="translate(0 -936.684)" fill="#fff"/>
                                                        <path className="img-bg-color" id="Rectangle_724" data-name="Rectangle 724" d="M422.307,1587.874h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1587.874Z" transform="translate(0 -936.684)" fill="#fff"/>
                                                        <path className="img-bg-color" id="Rectangle_725" data-name="Rectangle 725" d="M422.307,1606.943h56.232a4.9,4.9,0,0,1,4.9,4.9h0a4.9,4.9,0,0,1-4.9,4.9H422.307a4.9,4.9,0,0,1-4.9-4.9h0A4.9,4.9,0,0,1,422.307,1606.943Z" transform="translate(0 -936.684)" fill="#fff"/>
                                                        </g>
                                                    </g>
                                                </svg>                            
                                                </div>
                                                <span>FEED</span>
                                            </a>
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='card' ? 'active' : '')}>
                                        <a href="/user_card">
                                            <div className="position-relative">
                                            <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3913" data-name="Group 3913" transform="translate(-622.5 -341.201)">
                                                    <g id="Group_9" data-name="Group 9" transform="translate(636.691 368.116)">
                                                    <rect id="Rectangle_11-2" data-name="Rectangle 11-2" width="119.548" height="82.812" rx="16.704" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <circle id="Ellipse_4-2" data-name="Ellipse 4-2" cx="7.628" cy="7.628" r="7.628" transform="translate(13.282 58.529)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <circle id="Ellipse_5-2" data-name="Ellipse 5-2" cx="7.628" cy="7.628" r="7.628" transform="translate(23.557 58.529)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <path id="Rectangle_12-2" data-name="Rectangle 12-2" d="M653.919,1565.376H741.5a5.812,5.812,0,0,1,5.811,5.812h0A5.811,5.811,0,0,1,741.5,1577H653.919a5.812,5.812,0,0,1-5.812-5.811h0A5.812,5.812,0,0,1,653.919,1565.376Z" transform="translate(-636.691 -1550.226)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <line id="Line_1-2" data-name="Line 1-2" x2="13.699" transform="translate(51.057 65.17)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <line id="Line_2-2" data-name="Line 2-2" x2="13.699" transform="translate(70.67 65.17)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    <line id="Line_3-2" data-name="Line 3-2" x2="13.699" transform="translate(90.699 65.17)" fill="none" stroke="#303030" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                </g>
                                            </svg>
                                            <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3919" data-name="Group 3919" transform="translate(-622.5 -539.201)">
                                                    <g id="Group_3905" data-name="Group 3905" transform="translate(-2 -47)">
                                                    <g id="Group_10" data-name="Group 10" transform="translate(0 -936.684)">
                                                        <rect className="img-bg-color" id="Rectangle_11" data-name="Rectangle 11" width="119.548" height="82.812" rx="16.704" transform="translate(643.651 1554.402)" fill="#ffc22b"/>
                                                        <circle className="img-bg-color" id="Ellipse_4" data-name="Ellipse 4" cx="7.628" cy="7.628" r="7.628" transform="translate(656.935 1612.931)" fill="#fff"/>
                                                        <circle className="img-bg-color" id="Ellipse_5" data-name="Ellipse 5" cx="7.628" cy="7.628" r="7.628" transform="translate(667.208 1612.931)" fill="#fff"/>
                                                        <path className="img-bg-color" id="Rectangle_12" data-name="Rectangle 12" d="M660.879,1569.554h87.585a5.811,5.811,0,0,1,5.811,5.811h0a5.812,5.812,0,0,1-5.811,5.812H660.879a5.812,5.812,0,0,1-5.812-5.812h0A5.812,5.812,0,0,1,660.879,1569.554Z" fill="#fff"/>
                                                        <line className="img-bg-color" id="Line_1" data-name="Line 1" x2="13.699" transform="translate(694.708 1619.572)" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                        <line className="img-bg-color" id="Line_2" data-name="Line 2" x2="13.699" transform="translate(714.323 1619.572)" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                        <line className="img-bg-color" id="Line_3" data-name="Line 3" x2="13.699" transform="translate(734.35 1619.572)" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                    </g>
                                                </g>
                                            </svg>                            
                                            </div>
                                            
                                            <span>CARD</span>
                                        </a>
                                        
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='deal' ? 'active' : '')}>
                                        <a href="/promotion_category">
                                            <div className="position-relative">
                                            <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="85.43" height="110.548" viewBox="0 -10 85.43 110.548">
                                                <g id="Group_3921" data-name="Group 3921" transform="translate(1.5 -0.835)">
                                                    <path id="Path_5326" data-name="Path 5326" d="M431.342,640.44v41.594a12.238,12.238,0,0,1-12.237,12.237H363.762a12.242,12.242,0,0,1-12.237-12.237V640.44Z" transform="translate(-350.219 -597.389)" fill="none" stroke="#303030" stroke-width="3"/>
                                                    <path id="Path_5325" data-name="Path 5325" d="M431.82,611.687V624.53H349.39V611.687a12.234,12.234,0,0,1,12.237-12.237h57.956A12.234,12.234,0,0,1,431.82,611.687Z" transform="translate(-349.39 -581.478)" fill="none" stroke="#303030" stroke-width="3"/>
                                                    <rect id="Rectangle_459" data-name="Rectangle 459" width="16.932" height="78.912" transform="translate(32.922 17.972)" fill="#fff" stroke="#303030" stroke-width="3"/>
                                                    <path id="Path_5327" data-name="Path 5327" d="M420.212,594.775H402.818a8.817,8.817,0,0,1,17.394,0Z" transform="translate(-370.127 -576.803)" fill="#fff" stroke="#303030" stroke-width="3"/>
                                                    <path id="Path_5328" data-name="Path 5328" d="M389.895,589.585H377.324l-7.285-5.874s-2.833-4.886-.347-7.3c4.505-4.368,10.24-2.724,15.783,5.163A50.1,50.1,0,0,1,389.895,589.585Z" transform="translate(-356.703 -571.613)" fill="none" stroke="#303030" stroke-width="3"/>
                                                    <path id="Path_5329" data-name="Path 5329" d="M429.628,589.585h12.134s12.986-7.526,7.367-13.17S435.911,575.163,429.628,589.585Z" transform="translate(-380.533 -571.613)" fill="none" stroke="#303030" stroke-width="3"/>
                                                    <path id="Path_5330" data-name="Path 5330" d="M7.6,0" transform="translate(36.912 25.44)" fill="none" stroke="#303030" stroke-linecap="round" stroke-width="1"/>
                                                </g>
                                            </svg>
                                            <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="70" height="110.548" viewBox="0 -9 70 110.548">
                                                <g id="Group_3939" data-name="Group 3939" transform="translate(-917.925 -943.402)">
                                                    <path className="img-bg-color" id="Path_5320" data-name="Path 5320" d="M428.409,588.089H411.014a8.817,8.817,0,0,1,17.395,0Z" transform="translate(539.428 369.193)"/>
                                                    <path className="img-bg-color" id="Path_5321" data-name="Path 5321" d="M399.106,585.63H386.535s-13.453-7.526-7.632-13.17S392.6,571.207,399.106,585.63Z" transform="translate(552.458 373.323)"/>
                                                    <path className="img-bg-color" id="Path_5322" data-name="Path 5322" d="M438.839,585.63h12.134s12.986-7.526,7.367-13.17S445.122,571.207,438.839,585.63Z" transform="translate(528.628 373.323)"/>
                                                    <path className="img-bg-color" id="Path_5323" data-name="Path 5323" d="M440.3,604.665v12.843h-82.43V604.665a12.234,12.234,0,0,1,12.237-12.237h57.956A12.234,12.234,0,0,1,440.3,604.665Z" transform="translate(560.056 364.649)"/>
                                                    <path className="img-bg-color" id="Path_5324" data-name="Path 5324" d="M439.821,633.418v41.594a12.238,12.238,0,0,1-12.237,12.237H372.241A12.242,12.242,0,0,1,360,675.012V633.418Z" transform="translate(559.227 348.739)"/>
                                                </g>
                                            </svg>                           
                                            </div>  
                                            <span>REWARD</span>
                                        </a>
                    
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='account' ? 'active' : '')}>
                                        <a href="/account">
                                            <div className="position-relative">
                                            <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3915" data-name="Group 3915" transform="translate(-1170.5 -341.201)">
                                                    <path id="Path_140" data-name="Path 140" d="M1213.155,1632.56l16.289-45H1261l15.272,45" transform="translate(0 -1184.56)" fill="none" stroke="#303030" stroke-linecap="round" stroke-width="3"/>
                                                    <g id="Ellipse_55" data-name="Ellipse 55" transform="translate(1224.647 361.974)">
                                                    <circle id="Ellipse_560" data-name="Ellipse 560" cx="19.6" cy="19.6" r="19.6" fill="none"/>
                                                    <circle id="Ellipse_561" data-name="Ellipse 561" cx="17.5" cy="17.5" r="17.5" transform="translate(2.1 2.1)" fill="none" stroke="#303030" stroke-width="3"/>
                                                    </g>
                                                </g>
                                            </svg>
                                            <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                <g id="Group_3921" data-name="Group 3921" transform="translate(-1170.5 -539.201)">
                                                    <g id="Group_3907" data-name="Group 3907" transform="translate(0 -50)">
                                                    <path className="img-bg-color" id="Path_139" data-name="Path 139" d="M1221.555,1636.76l16.289-45H1269.4l15.272,45" transform="translate(0 -936.684)" fill="#ffc22b"/>
                                                    <circle className="img-bg-color" id="Ellipse_54" data-name="Ellipse 54" cx="19.6" cy="19.6" r="19.6" transform="translate(1233.048 610.364)" fill="#ffc22b"/>
                                                    </g>
                                                </g>
                                            </svg>                            
                                            </div>
                                            <span>ACCOUNT</span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                    </div> 
                
              
        
            </>
        )
    
        

    }
}
export default Navigation;
