import React, { Component } from 'react'
import $ from 'jquery'
import './navigation.css'
import axios from 'axios'
import {config} from '../config';
import { getParam, inArray, readURL, getSizeFromImgDataURL } from '../lib/Helper';

import ShopSvg from '../assets/navigations/ShopSvg.js'
import NewsSvg from '../assets/navigations/NewsSvg.js'
import PromotionSvg from '../assets/navigations/PromotionSvg.js'
import MemberSvg from '../assets/navigations/MemberSvg.js'


const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();

const menuHomeArray = [
    'home'
]
const menuAccountArray = [
    'account'
]

const menuDealArray = [
    'promotion_category'
]

const menuFeedArray = [
    'feed',
    'feed_detail'
]

const menuCardArray = [
    'user_card',
]

const menuShopArray = [
    'shop',
]

const hiddenMenuShopArray = [
    83,
    139,
    140,
    141,
    162
]

class Navigation_shop extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleChangeActive = this.handleChangeActive.bind(this);
        console.log('props',props)
    }

    state = {
        data : [],
        storeid : localStorage.getItem('storageStoreID') !== undefined ? localStorage.getItem('storageStoreID') : this.props.storeid,
        menuActive : undefined,
        //showHome : sessionStorage.getItem('showHome') !== '0' ? 1 : 0
        showHome : 0//ปิดเมนู Home
    };



    async componentDidMount(){

        if(this.state.showHome===0){//ซ่อนเมู home ให้ขยายเมนูใหญ่ขึ้น
            $('.menu-item').css("width", "25%");
        }

        if(inArray(this.state.storeid,hiddenMenuShopArray)){
            $('.menu-item').css("width", "30%");
        }

        this.handleChangeActive()

        const result = await axios.get(config.api.base_url+'/store/api/storeColor/'+this.state.storeid)
        if(result.status===200){
            this.setState({data: result.data});
        }


        if(this.state.data !== undefined){
            let colortabbar = this.state.data['colortabbar'];
            let coloricon   = this.state.data['coloricon'];
            let colorfont   = this.state.data['colorfont'];
            if(colortabbar && coloricon && colorfont){
                console.log(1)

                $(".footer" ).css( "background-color", "#"+colortabbar );
                $(".footer-bg" ).css( "color", "#"+colortabbar );
                $('.img-border-color').css( "fill", "#"+colorfont );
                $('.img-border-color-stroke').css( "stroke", "#"+colorfont );

                $('.head.bg-black, .bg-black, .mestyle__wrapper').css("background-color", "#"+colortabbar);
                $('.link__box .tab, .status_and_qrcode__box, .detail__box .tab').css("background-color", "#"+coloricon);
                $('.account-img-bg-color' ).css( "fill", "#"+coloricon );
                $('.account-img-stroke').css( "stroke", "#"+coloricon );
                $('.btn-add').css('background-color','#'+colortabbar)
                /* $('.btn-add, .noti-num-border').css('background-color','#'+colortabbar)
                $('.noti-num-border').css('border-color','#'+coloricon) */

                $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg, .footer.menu-shop .menu-item.active .img-bg-color" ).css( "fill", "#"+coloricon );
                //$('.footer.menu-shop .menu-item.active .img-border-color-stroke').css( "stroke", "#fff" );
                //$('.footer.menu-shop .menu-item.active .img-border-color').css( "fill", "#FFF" );


                $('.stamp .stamp-bg, #stamp-bg').css("background-color", "#"+colortabbar);


                /* ส่วนนี้ไว้กก่อน*/
                /* $('.head h1.text-white').css("color", "#"+colorfont);//ติด
                $('.mestyle__wrapper').css( "background-color", "#"+colortabbar );
                $('.mestyle__wrapper .status_and_qrcode__box, .mestyle__wrapper .link__box .tab, .shop_detail__wrapper .detail__box .tab, .shop_detail__wrapper .link__box .tab').css( "background-color", "#"+coloricon );
                $('.product-add').css( "background-color", "#"+coloricon ); */
            }else{
                console.log(2)
                $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg, .footer.menu-shop .menu-item.active .img-bg-color" ).css( "fill", "#DA1111" );
                $(".footer" ).css( "background-color", "#000000" );
                $('.img-border-color').css( "fill", "#FFFFFF" );
                $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );
            }

        }else{
            console.log(3)
            $(".footer.menu-shop .menu-item.active, .footer.menu-shop .menu-item.active .img-bg" ).css( "fill", "#DA1111" );
            $(".footer" ).css( "background-color", "#000000" );
            $('.img-border-color').css( "fill", "#FFFFFF" );
            $('.img-border-color-stroke').css( "stroke", "#FFFFFF" );

        }


    }


    handleChangeActive = e => {
        if(this.props.active !== undefined){
            let active        = (this.props.active).toLowerCase();

            this.setState({ menuActive: active })

        }else{
            if(inArray(currentComponent,menuAccountArray)){
                this.setState({menuActive : 'account'})
            }
            if(inArray(currentComponent,menuDealArray)){
                this.setState({menuActive : 'deal'})
            }
            if(inArray(currentComponent,menuFeedArray)){
                this.setState({menuActive : 'feed'})
            }
            if(inArray(currentComponent,menuHomeArray)){
                this.setState({menuActive : 'home'})
            }
            if(inArray(currentComponent,menuCardArray)){
                this.setState({menuActive : 'card'})
            }
            if(inArray(currentComponent,menuShopArray)){
                this.setState({menuActive : 'shop'})
            }


        }

    }


    render() {
        const activeColor = this.state.data && this.state.data.coloricon

        return (
            <>
                    <div className="container-mobile m-auto mt-footer pb-footer">
                        <div className="footer menu-shop mx-auto" style={{width:'100%'}}>
                                <div className="menu w-100 py-1">

                        {
                            this.state.showHome!==0 &&(

                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='home' ? 'active' : '')}>
                                        <a href="/home">
                                            <div className="position-relative">
                                                <svg class="img-border" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3911" data-name="Group 3911" transform="translate(-115.5 -341.201)">
                                                    <path className="img-border-color-stroke" id="Path_48" data-name="Path 48" d="M229.354,1584.949l-32.206-45.176c-6.483-6.691-12.548,0-12.548,0l-33.46,45.176v49.275h78.214Z" transform="translate(0 -1180.684)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                    <path className="img-border-color-stroke" id="Path_49" data-name="Path 49" d="M179.737,1634.224v-33.879s10.51-11.763,22.274,0v33.879" transform="translate(-1 -1180.607)" fill="none" stroke="#303030" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                </svg>
                                                <svg class="img-bg" xmlns="http://www.w3.org/2000/svg" width="155" height="126.859" viewBox="0 0 155 126.859">
                                                    <g id="Group_3918" data-name="Group 3918" transform="translate(-115.5 -539.201)">
                                                    <g id="Group_3903" data-name="Group 3903" transform="translate(7 -19)">
                                                        <path className="img-bg-color" id="Path_46" data-name="Path 46" d="M237.733,1588.37l-32.206-45.176c-6.489-6.692-12.549,0-12.549,0l-33.456,45.176v49.275h78.215Z" transform="translate(-7 -963.684)" fill="#ffc22b"/>
                                                        <path className="img-bg-color" id="Path_47" data-name="Path 47" d="M187.488,1637.645v-33.88s10.51-11.763,22.274,0v33.88" transform="translate(-7 -959.684)" fill="#fff"/>
                                                    </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                            )
                        }
                                    <div className="menu-item px-2 pt-2">
                                        <a className="menu-block" href={'/store_feed/'+localStorage.getItem('storageStoreID')}>
                                            <div className={`position-relative nav-icon ${this.state.menuActive==='feed' ? 'active' : ''}`}>

                                            <NewsSvg activeColor={this.state.menuActive==='feed' ? activeColor : ''} />
                                            </div>
                                            <span style={this.state.menuActive==='feed' ? {color: '#' +  activeColor } : {color: '#656565' }} >
                                                News
                                            </span>
                                        </a>
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a className="menu-block" href={'/deal/'+localStorage.getItem('storageStoreID')}>
                                            <div className={`position-relative nav-icon ${this.state.menuActive ==='deal' ? 'active' : ''}`}>
                                                <PromotionSvg activeColor={this.state.menuActive==='deal' ? activeColor : ''} />

                                            </div>
                                            <span style={this.state.menuActive==='deal' ? {color: '#' +  activeColor } : {color: '#656565' }}>
                                                Promotion
                                            </span>
                                        </a>
                                    </div>
                        {
                            !inArray(this.state.storeid,hiddenMenuShopArray) &&(


                                    <div className="menu-item px-2 pt-2">
                                        <a className="menu-block" href={'/shop/'+localStorage.getItem('storageStoreID')}>
                                            <div className={`test position-relative nav-icon ${this.state.menuActive==='shop' ? 'active' : ''}`}>
                                                <ShopSvg activeColor={this.state.menuActive==='shop' ? activeColor : ''} />
                                            </div>
                                            <span style={this.state.menuActive==='shop' ? {color: '#' +  activeColor } : {color: '#656565' }}>
                                                Shop
                                            </span>
                                        </a>
                                    </div>


                            )
                        }
                                    <div className="menu-item px-2 pt-2">
                                        <a className="menu-block" href={'/account_shop/'+localStorage.getItem('storageStoreID')}>
                                            <div className={`position-relative nav-icon ${this.state.menuActive==='account' ? 'active' : ''}`}>
                                                <MemberSvg activeColor={this.state.menuActive==='account' ? activeColor : ''}/>
                                            </div>
                                            <span style={this.state.menuActive==='account' ? {color: '#' +  activeColor } : {color: '#656565' }} >
                                                Member
                                            </span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                    </div>



            </>
        )



    }
}
export default Navigation_shop;
