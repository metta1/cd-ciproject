import React, { Component } from 'react'
import $ from 'jquery'
import './navigation.css'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';


const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();

const menuHomeArray = [
    'home'
]
const menuAccountArray = [
    'account'
]

const menuDealArray = [
    'promotion_category'
]

const menuFeedArray = [
    'feed',
    'feed_detail'
]

const menuCardArray = [
    'user_card',
]


class Navigation extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.handleChangeActive = this.handleChangeActive.bind(this);
        
        

    }
    state = {
        menuActive : undefined,
    };

    async componentDidMount(){
        this.handleChangeActive()
        

        //$(".img-bg-color" ).css( "fill", "#F9CE43" );
        $(".footer" ).css( "background-color", "#fff" );
        
        
    }

    handleChangeActive = e => {
        if(this.props.active !== undefined){
            let active        = (this.props.active).toLowerCase();
            this.setState({menuActive : active})
        }else{
            if(inArray(currentComponent,menuAccountArray)){
                this.setState({menuActive : 'account'})
            }
            if(inArray(currentComponent,menuDealArray)){
                this.setState({menuActive : 'deal'})
            }
            if(inArray(currentComponent,menuFeedArray)){
                this.setState({menuActive : 'feed'})
            }
            if(inArray(currentComponent,menuHomeArray)){
                this.setState({menuActive : 'home'})
            }
            if(inArray(currentComponent,menuCardArray)){
                this.setState({menuActive : 'card'})
            }

            
        }
        
    }


    render() {
        return (
            <>
                    <div className="container-mobile m-auto mt-footer pb-footer">
                        <div className="footer menu-main mx-auto">
                                <div className="menu w-100">
                                    
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='home' ? 'active' : '')}>
                                        <a href="/home">
                                            <div className="position-relative">
                                                <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="122" height="99" viewBox="0 0 122 99"><g transform="translate(10615 -1778)"><rect width="122" height="99" transform="translate(-10615 1778)" fill="none"/><g transform="translate(-10716.153 1626.357)"><path d="M10780.04-1546.782h-22.848v-27.674c-2.833-2.833-5.857-4.27-8.987-4.27a12.307,12.307,0,0,0-6.623,2.135,14.466,14.466,0,0,0-2.581,2.135v27.674h-22.848v-40.249l27.331-36.9a7.966,7.966,0,0,1,5.238-2.429,6.978,6.978,0,0,1,5.012,2.429l26.306,36.9v40.249Z" transform="translate(-10582.157 1789.794)" className="img-bg-color" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/></g></g></svg>
                                                <img className="img-border" src="/assets/images/icon-ft-home.svg" alt=""/>
                                            </div>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='feed' ? 'active' : '')}>
                                            <a href="/feed">
                                                <div className="position-relative">
                                                    <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="122" height="99" viewBox="0 0 122 99"><g transform="translate(10299 -1778)"><rect width="122" height="99" transform="translate(-10299 1778)" fill="none"/><g transform="translate(-10361 109)"><g transform="translate(12641 -7205.578)"><path d="M-2225,7160.579h-45a10.012,10.012,0,0,1-10-10v-44a10.012,10.012,0,0,1,10-10h45a10.011,10.011,0,0,1,10,10v44A10.011,10.011,0,0,1-2225,7160.579Zm-45.423-22.316a3.691,3.691,0,0,0-3.686,3.688,3.69,3.69,0,0,0,3.686,3.686h42.321a3.691,3.691,0,0,0,3.688-3.686,3.692,3.692,0,0,0-3.687-3.687Zm0-14.352a3.691,3.691,0,0,0-3.686,3.688,3.691,3.691,0,0,0,3.686,3.688h42.321a3.692,3.692,0,0,0,3.688-3.688,3.692,3.692,0,0,0-3.687-3.687Zm0-14.35a3.691,3.691,0,0,0-3.686,3.688,3.69,3.69,0,0,0,3.686,3.686h42.321a3.691,3.691,0,0,0,3.688-3.686,3.692,3.692,0,0,0-3.687-3.687Z" transform="translate(-10268 1797)" className="img-bg-color"/></g></g></g></svg>
                                                    <img className="img-border" src="/assets/images/icon-ft-feed.svg" alt=""/>
                                                </div>
                                                <span>FEED</span>
                                            </a>
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='card' ? 'active' : '')}>
                                        <a href="/user_card">
                                            <div className="position-relative">
                                                <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="122" height="99" viewBox="0 0 122 99">
                                                    <g transform="translate(10771 -1778)">
                                                        <rect width="122" height="99" transform="translate(-10771 1778)" fill="none"/>
                                                    <g transform="translate(-13525 -2298)"><g transform="translate(2249 2070)"><path d="M11349.883,287.493H11288a12,12,0,0,1-12-12V240a12,12,0,0,1,12-12h61.883a12.005,12.005,0,0,1,12,12v35.492a12,12,0,0,1-12,12Zm-55.173-7.916h0a5.478,5.478,0,1,0-3.69,1.429,5.471,5.471,0,0,0,3.69-1.429Zm46.448-4.759h0Zm-14.389,0h0Zm-14.089,0h0Zm-24.3-35.934a4.176,4.176,0,0,0,0,8.352h62.923a4.176,4.176,0,0,0,0-8.352Z" transform="translate(-10750 1800)" className="img-bg-color"/></g></g></g></svg>
                                                <img className="img-border" src="/assets/images/icon-ft-card.svg" alt=""/>
                                            </div>
                                            
                                            <span>CARD</span>
                                        </a>
                                        
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='deal' ? 'active' : '')}>
                                        <a href="/promotion_category">
                                            <div className="position-relative">
                                                <svg className="img-bg"  xmlns="http://www.w3.org/2000/svg" width="122" height="99" viewBox="0 0 122 99">  
                                                    <g id="Group_3888" data-name="Group 3888" transform="translate(-922.232 -1557.513)" className="img-bg-color">
                                                    <g id="Group_3886" data-name="Group 3886" className="img-bg-color">
                                                    <path className="img-bg-color" id="Path_6944" data-name="Path 6944" d="M1044.258,1557.513v72.8H965.973c0-6.308-4.394-11.416-9.821-11.416s-9.822,5.108-9.822,11.416h-24.1v-72.8h24.1c0,6.308,4.395,11.416,9.822,11.416s9.821-5.108,9.821-11.416Zm-20.552,50.95c0-4.027-2.811-7.294-6.275-7.294s-6.275,3.267-6.275,7.294,2.81,7.293,6.275,7.293S1023.706,1612.49,1023.706,1608.463Zm-26.623-29.1c0-4.027-2.811-7.294-6.275-7.294s-6.275,3.267-6.275,7.294,2.81,7.293,6.275,7.293S997.083,1583.386,997.083,1579.359Z" fill="none"/>
                                                    <ellipse className="img-bg-color" id="Ellipse_562" data-name="Ellipse 562" cx="6.275" cy="7.294" rx="6.275" ry="7.294" transform="translate(1011.156 1601.169)" fill="none"/>
                                                    <ellipse className="img-bg-color" id="Ellipse_563" data-name="Ellipse 563" cx="6.275" cy="7.294" rx="6.275" ry="7.294" transform="translate(984.533 1572.065)" fill="none"/>
                                                    </g>
                                                    <g id="Group_3887" data-name="Group 3887" className="img-bg-color">
                                                    <line className="img-bg-color" id="Line_83" data-name="Line 83" y2="7.163" transform="translate(955.815 1608.154)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                                    <line className="img-bg-color" id="Line_84" data-name="Line 84" y2="7.163" transform="translate(955.815 1596.286)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                                    <line className="img-bg-color" id="Line_85" data-name="Line 85" y2="7.163" transform="translate(955.815 1584.419)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                                    <line className="img-bg-color" id="Line_86" data-name="Line 86" y2="7.163" transform="translate(955.815 1572.504)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                                    <line className="img-bg-color" id="Line_87" data-name="Line 87" x1="26.623" y2="43.691" transform="translate(990.808 1572.065)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                                    <ellipse className="img-bg-color" id="Ellipse_564" data-name="Ellipse 564" cx="6.275" cy="7.294" rx="6.275" ry="7.294" transform="translate(1011.156 1601.169)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3"/>
                                                    <ellipse className="img-bg-color" id="Ellipse_565" data-name="Ellipse 565" cx="6.275" cy="7.294" rx="6.275" ry="7.294" transform="translate(984.533 1572.065)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3"/>
                                                    </g>
                                                </g>                                          </svg>
                                                <img className="img-border" src="/assets/images/icon-ft-coupon.svg" alt=""/>
                                            </div>
                                            <span>COUPON</span>
                                        </a>
                    
                                    </div>
                                    <div className={"menu-item px-2 pt-2 "+(this.state.menuActive==='account' ? 'active' : '')}>
                                        <a href="/account">
                                            <div className="position-relative">
                                                <svg className="img-bg" xmlns="http://www.w3.org/2000/svg" width="50.01" height="71.08" viewBox="0 0 50.01 71.08">
                                                    <g transform="translate(-858.446 -1683)">
                                                        <g transform="translate(851.791 1683)"><path d="M878.791,1853.591l12.906-35.654H916.7l12.1,35.654" transform="translate(-872.136 -1782.511)" className="img-bg-color"/>
                                                        <circle cx="15.5" cy="15.5" r="15.5" transform="translate(16.209)" className="img-bg-color"/></g></g>
                                                    </svg>
                                                <img className="img-border" src="/assets/images/icon-ft-account.svg" alt=""/>
                                            </div>
                                            <span>ACCOUNT</span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                    </div> 
                
              
        
            </>
        )
    
        

    }
}
export default Navigation;
