import React, { Component } from 'react'
import queryString from 'query-string'
import $ from 'jquery'
export function getParam(paramName) {
    let locationSearch = window.location.search
    let getParam       = queryString.parse(locationSearch);
    return getParam[paramName]=='' ? undefined : getParam[paramName];
}
  
export function inArray(needle, haystack){
    let length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) 
        return true;
    }
    return false;
}

export function time(){
    let d = new Date();
    let n = d.getTime();
    return n;
}

export function readURL(input,displayID) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
  
      reader.onload = function(e) {
        $('#'+displayID).attr('src', e.target.result);
      }
      return reader.readAsDataURL(input.files[0]);
    }
}

export function getSizeFromImgDataURL(imageDataURL,unit='byte'){
    //console.log(imageDataURL);
  
    let base64String = imageDataURL.split(",")[1];
    let ch, st, re = [], j=0, str=base64String;
    for (let i = 0; i < str.length; i++ ) { 
        ch = str.charCodeAt(i);
        if(ch < 127){
            re[j++] = ch & 0xFF;
        }else{
            st = [];   
            do {
                st.push( ch & 0xFF );  // push byte to stack
                ch = ch >> 8;          // shift value down by 1 byte
            }
            while ( ch );
            st = st.reverse();
            for(let k=0;k<st.length; ++k)
                re[j++] = st[k];
        }
    }
    if(unit.toUpperCase()=='MB'){
        return (re.length)/1024000;
    }else if(unit.toLocaleUpperCase()=='KB'){
        return (re.length)/1024;
    }else{
        return re.length;
    }
    
}


export function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}


export function hexToRgb(hex=1){
    if(hex.length>5){
        var aRgbHex = hex.match(/.{1,2}/g);
        var aRgb = [
            parseInt(aRgbHex[0], 16),
            parseInt(aRgbHex[1], 16),
            parseInt(aRgbHex[2], 16)
        ];
        return aRgb;
    }else{
        return undefined
    }
    
}

export function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


