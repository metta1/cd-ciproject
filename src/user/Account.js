import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import {config} from '../config';
import queryString from 'query-string'
import Swal from 'sweetalert2'
import InputMask from 'react-input-mask';
import {getParam, time} from '../lib/Helper';
import {Navigation} from '../template'
import NumberFormat from 'react-number-format';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Account extends Component {
    constructor(){//เริ่มต้น run component
        super();
        this.initialize = this.initialize.bind(this);


        this.state = {
            displayName : '',
            userId : '',
            pictureUrl : 'assets/images/profile.svg',
            imageUrl:'',
            statusMessage : '',
            user_id : sessionStorage.getItem('user_id'),
            store_id : 4,
            cardData: [],
            point:0
            
              
        };
       

    }


    sendMessage() {
        liff.sendMessages([{
          type: 'text',
          text: "Hi LIFF"
        }]).then(() => {
          liff.closeWindow();
        });
      }
    
    closeLIFF() {
        liff.closeWindow();
    }
    


    async componentDidMount() { //เมื่อรันเสร็จแล้ว
        //window.addEventListener('load', this.initialize);
        const userData =  await axios.get(config.api.base_url+'/api/userData/userid/'+this.state.user_id)
        if(userData.status===200){
                let rowData = userData.data;
                this.setState({
                    userid : rowData.userid,
                    displayName  : rowData.fname+' '+rowData.lname,
                    usertel: rowData.usertel,
                    email  : rowData.email,
                    imageUrl : rowData.imageurl
                
                });
                console.log(this.state)    
        }
        const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountcard/'+this.state.store_id+'/'+this.state.user_id)
        if(cardRs.status===200){
            this.setState({
                cardData: cardRs.data,
                point: parseFloat(cardRs.data[0]['point'])
            });
        }

        this.initialize()
    }

    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1', '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm';
        }
    }

    

    initialize() {
        liff.init(async (data) => {
          let profile = await liff.getProfile();
          this.setState({
            //displayName : profile.displayName,
            userId : profile.userId,
            pictureUrl : profile.pictureUrl,
            statusMessage : profile.statusMessage
          });

           
        }); 
    }
    
    handleChange = event => {
        console.log(event.target.name)
        //this.setState({ name: event.target.value });
        this.setState({[event.target.name]: event.target.value});
    }

    handleLogOut = event => {
        if(this.state.user_id !==''){
            Swal.fire({
                title: 'ยืนยันออกจากระบบ',
                html: '',
                icon: 'question',
                showCancelButton: true,
                reverseButtons :true,
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    let bodyFormData = new FormData();
                    bodyFormData.set('userid',this.state.user_id)
                    axios({
                        method: 'post',
                        url: config.api.base_url+'/users/api/logout',
                        data: bodyFormData,
                        headers: {'Content-Type': 'multipart/form-data' }
                        })
                        .then(function (response) {
                            if(response.status===200){
                                sessionStorage.removeItem('user_id');
                                // Remove all saved data from sessionStorage
                                sessionStorage.clear();
                                window.location='/login'
                            }else{
                                Swal.fire({
                                    title : 'ขออภัย',
                                    html: 'เกิดข้อผิดพลาด กรุณาทำรายการใหม่อีกครั้ง',
                                    icon:'error',
                                    confirmButtonText:'ตกลง',
                                    customClass: {
                                        confirmButton: 'btn-darkblue',
                                    },
                                })
                            }
                            //console.log(response);
                        })
                        .catch(function (response) {
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'ไม่สามารถเชื่อมต่อได้ กรุณาทำรายการใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                    });
                }
            })
            
            
        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
        }
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="assets/css/account.css?v=2"/>
                <div className="container-mobile m-auto ">
        <div className="h-100 w-100">
            <div className="bg-head">

            </div>
            <div className="account p-4">
                <div className="shadow rounded-lg bg-white ">
                    <div className="px-4 py-2 ">
                        <div className="row">
                            <div className="col-3">
                                <div className="profile">
                                    
                                    <div className="profile-img bg-white rounded-circle shadow">
                                        <img className="" src={this.state.imageUrl ? (this.state.imageUrl) : (this.state.pictureUrl)} alt="..." />
                                    </div>
                                    <div className="profile-upload">
                                        <a target="_BLANK" href={config.base_url+"/TakePhotoProfile?openExternalBrowser=1&token="+btoa(this.state.user_id+','+time())}>
                                        <div className="icon bg-white rounded-circle shadow">
                                          
                                                <img className="p-1" src="assets/images/icon-upload.svg" alt="..." />
                                            
                                        </div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-6">
                                <div className="text-center head">
                                    <h3 className="m-0 h">Account</h3>
                                    <h5 className="name-overflow">{this.state.displayName==='' ? 'Name Surname' : this.state.displayName}</h5>
                                </div>
                            </div>
                            <div className="col-3 text-right">
                                <div className="icon bg-white rounded-circle shadow">
                                    <a href="/edit_account">
                                        <img className="icon-edit" src="assets/images/icon-create.svg" alt="..." />
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div className="text-left detail pt-3">
                            <h5>บัญชีสมาชิก </h5>
                            <div className="mb-2">
                                <img className="" src="assets/images/icon-mail.svg" alt=""/>
                                <span className="pl-3">{this.state.email}</span>
                            </div>
                            <div className="mb-2">
                                <img className="p-1" src="assets/images/icon-tel.svg" alt=""/>
                                <span className="pl-3">{this.state.usertel}</span>
                            </div>
                            {/*<div className="mb-2">
                                <span className="">รหัสสมาชิก 3105-ตามด้วยหมายเลขโทรศัพท์</span>
                            </div>
                            */}
    
                        </div>
                    </div>
                    <div className="row w-100 mt-3 ml-0 rounded-bottom coin" >
                        <div className="col-4 bg-point rounded-bottom-left">
                            <p className="text-point mb-0 pt-1 h">HO Point</p>
                        </div>
                      
                        <div className="col-6 bg-point text-right">
                            <p className="mb-0 text-value-point pt-1 h"><NumberFormat value={this.state.point} displayType={'text'} thousandSeparator={true}/> คะแนน</p>
                        
                        </div>
                        
                        <div className="col-2 bg-yellow text-center rounded-bottom-right">
                        <a onClick={this.handleClickScanQRCode}>
                            <p className="mb-0 text-black text-scan h">SCAN</p>
                            <img className=" mb-1" src="/assets/images/scanwhite.png" width="100%" alt="" style={{marginTop: '-10px',padding:'10%'}}/>
                        </a>
                        </div>
                    </div>

                    
                </div>
                    <a href="/ho_membership_card">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/iconcardho.png" alt=""/>
                                <span className="pl-3">บัตรสมาชิก</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href={'/coupon_list/'+this.state.userId+'?v='+ new Date().getTime()}>
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/coupon.png" alt=""/>
                                <span className="pl-3">คูปองของฉัน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href={'/user_interest/'+this.state.userId+'?v='+ new Date().getTime()}>
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/fav.png" alt=""/>
                                <span className="pl-3">ความสนใจของฉัน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    {/* <a href="">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/icon-qr.svg" alt=""/>
                                <span className="pl-3">QR Code ของฉัน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a> */}
                    <a href="/usage_history">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/icon-history.svg" alt=""/>
                                <span className="pl-3">ประวัติการใช้งาน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href={config.base_url+"/share?openExternalBrowser=1&re="+this.state.usertel} target="_blank">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/icon-user-group.svg" alt=""/>
                                <span className="pl-3">แนะนำเพื่อน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    {/* <a href="">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/icon-lang.svg" alt=""/>
                                <span className="pl-3">เปลี่ยนภาษา</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a> */}
                    <a href="/setting">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon img-1" src="assets/images/account.png" alt=""/>
                                <span className="pl-3">ตั้งค่าบัญชี</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href="/help">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" style={{objectFit:'contain'}} src="assets/images/question.png" alt=""/>
                                <span className="pl-3">ช่วยเหลือ</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <a href="/maps?mode=account">
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon" src="assets/images/hox.png" alt="" style={{width: '10%',paddingLeft:'5px',marginLeft: '2px',marginRight: '8px'}} />
                                <span className="pl-3">ค้นหา HOX ใกล้ฉัน</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a>
                    <div className="shop_detail__wrapper">
                        <div className="detail__box">
                            <div className="tab"></div>
                            <div className="content__wrapper">
                                    <a className="link__box" href={'tel:0966695959'}>
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-phone.svg" alt="icon" className="icon"/>
                                                    <p>096-669-5959</p>
                                        </div>
                                    </a>
                                    <a className="link__box" href={'mailto:hello@ho.app'} target="_BLANK">
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-email.svg" alt="icon" className="icon"/>
                                                    <p>hello@ho.app</p>
                                        </div>
                                    </a>
                                    <a href="https://www.ho.app?openExternalBrowser=1" target="_BLANK" className="link__box">
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-website.svg" alt="icon" className="icon"/>
                                                    <p>https://www.ho.app</p>
                                        </div>
                                    </a>
                                    <a href="https://lin.ee/zv9Ewk4" className="link__box">
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-line.svg" alt="icon" className="icon"/>
                                                    <p>@hoooo</p>
                                        </div>
                                    </a>
                                    <a className="link__box" href="https://www.facebook.com/Hoapplication?openExternalBrowser=1" target="_BLANK">
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-facebook.svg" alt="icon" className="icon"/>
                                                    <p>HoApplication</p>
                                        </div>
                                    </a>
                                    {/*
                                    <a className="link__box" target="_BLANK" href={"https://www.instagram.com/Ho.app?openExternalBrowser=1"}>
                                        <div className="box">
                                                    <img src="/assets/images/icon-shop-instagram.svg" alt="icon" className="icon"/>
                                                    <p>Ho.app</p>
                                        </div>
                                    </a>
                                    */}
                            </div>
                        </div>
                    </div>
                    {/* <a >
                        <div className="account-menu w-100 shadow  mt-3 rounded-lg d-flex flex-row">
                            <div className="box rounded-left "></div>
                            <div className="account-menu-detail pl-3 h-100 w-75 ">
                                <img className="menu-icon img-2" src="assets/images/aboutho.png" alt="" />
                                <span className="pl-3">เกี่ยวกับ Ho</span>
                            </div>
                            <div className="account-menu-detail ml-auto">
                                <img className="h-100 p-1" src="assets/images/icon-go.svg" alt=""/>
                            </div>
                        </div>
                    </a> */}

                    
                    {/* <div className="w-100 text-center mt-4">
                        <button type="button" className="w-50 btn bg-yellow text-white" onClick={(e) => this.handleLogOut(e)}>
                                ออกจากระบบ
                        </button>
                    </div> */}
 
            </div>
        </div>

        <Navigation active={'account'}/>
        
    </div>   
    {/* container */} 
                 
            </div>
        )
    }
}
export default Account;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;