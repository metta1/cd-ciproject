import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import Swal from 'sweetalert2'
import Modal from 'react-bootstrap4-modal';
import {Navigation} from '../template'
import {config} from "../config";
import {getParam, time} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class User_card extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        let user_id = sessionStorage.getItem('user_id');
        if(!user_id){
            sessionStorage.setItem('referer',currentComponent);
            //window.location='/register'
        }
        
        this.addActiveClass = this.addActiveClass.bind(this);
        this.handleFilterCard = this.handleFilterCard.bind(this);
        

        
      
        
    }

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        activeClass:'',
        activeSearchBox:false,
        user_id : sessionStorage.getItem('user_id'),
    };


    async componentDidMount(){
        
        let user_id = sessionStorage.getItem('user_id');
        if(user_id!==null){
            const result = await axios.get(config.api.base_url+'/admin_othercard/api/offline/'+user_id)
            if(result.status===200){
                this.setState({data: result.data});
            }
        }
        this.addActiveClass('');
        //this.handleFilterCard(this);
        
        
        console.log(this.state.data)

    }

   

    addActiveClass(e){
        if(e){
            e.preventDefault();
        }
       
        //console.log(e.target)
        //let elememtCard =  e.target;
        //console.log(e.target.parentElement.attributes.href)
        $('.card-selected').click(function() {
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            //window.location='/shop/'+storeID
            
            
        })
        $('.member-card:not(.card-selected)').click(function() {
            
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            $(this).addClass('card-selected');
            
      
            
        })
        $('.container-member-card.container-selected').click(function(){
            $(this).removeClass('container-selected');
        })
        $('.container-member-card:not(.container-selected)').click(function(){
            $('.container-member-card').removeClass('container-selected');
            $(this).addClass('container-selected');
        })
        
    }

    async handleFilterCard(e){
        
        let user_id = sessionStorage.getItem('user_id');
        if(user_id!==null){
            let keyWord = e.target.value;
            const result = await axios.get(config.api.base_url+'/admin_othercard/api/offline/'+user_id+'?keywords='+keyWord)
            console.log(result)
            if(result.status===200){
                this.setState({data: result.data});
            } else {
                this.setState({data: []});
            }
        }
        /*$("#searchInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".user-card-container .container-member-card a").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              console.log($(this).toggle($(this).text().toLowerCase().indexOf(value)))
            });
        });*/
    }

    handleClick = event => {
        event.preventDefault();
        //console.log(event.target.parentNode.attributes.href.nodeValue)
        //console.log(event.target.parentNode.outerHTML)
        let cardID = event.target.parentNode.attributes[1].nodeValue
        console.log(event.target)
      
        this.setState({
            activeClass:cardID
        })
        
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-member-card').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-member-card').css({"padding-top": 'unset'})

        this.componentDidMount();
    }
    

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    handleAddCard = event => {
       
       
    }
    
    handleGotoShop(e,storeid){
        window.location='/shop/'+storeid
    }

    handleGotoOfflineCard(e,otherid){
        e.preventDefault()
        console.log(otherid)
        window.location='/other_card_detail/'+otherid
    }


    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-yellow').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    handleDeleteCard(e,otherid){
        e.preventDefault();
        Swal.fire({
            title: 'ต้องการลบบัตรสมาชิก',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
                'ตกลง',
            focusConfirm: false,
            cancelButtonText:
                'ยกเลิก',
            }).then((result) => {
            if (result.value) {
                let user_id = sessionStorage.getItem('user_id');
                let other_id = otherid;
                let bodyFormData = new FormData();
                bodyFormData.set('user_id',user_id)
                bodyFormData.set('other_id',other_id)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/admin_othercard/api/removeothercard',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        window.location = '/user_card_offline';
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถลบข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                });
            }
        })
    }

    

   
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>

                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                    
                    <div className="modal-body">
                        <div className="form-group">
                            <label>ค้นหาบัตรสมาชิก</label>
                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCard}/>
                        </div>
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                    
                </Modal>
                
                    <div className="container-mobile m-auto ">
                        <div className="offline head bg-yellow shadow">
            {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back">
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCard}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                                <div className="col-8 h-100">
                                    <div className=" h-100 p-3">
                                        <h1 className="m-0 h"> <span className="text-blue">CA</span>RD</h1> 
                                        <img className="head-logo pr-0" src="/assets/images/icon-ho-new.png" alt=""/>
                                        <p >บัตรสมาชิกต่างๆของคุณ</p>
                                    </div>
                                </div>
                                <div className="col-4 h-100">
                                    <div className="row float-right h-100 w-100">
                                        <div className="col p-2 my-auto">
                                            <div className="icon icon-top-right rounded-circle" onClick={this.openSearchBox}>
                                                <img src="/assets/images/icon-search@2x.png" alt=""/>
                                            </div>
                                        </div>
                                        <div className="col p-2 my-auto">
                                            <div className="icon icon-top-right rounded-circle">
                                                <a href="/notifications">
                                                    <img src="/assets/images/icon-noti@2x.png" alt=""/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                    </div>
                )
            }

                    </div>

                    <div className=" my-card mt-3 div-mycard">
                            <div className="row w-100 m-0">
                                <div className="col-6 px-2">
                                    <a href="/user_card">
                                    <div id="app-card"  className="btn-card text-center shadow not-active">
                                        <h3>ONLINE CARD</h3>
                                        <h5>ร้านประจำของคุณ</h5>
                                    </div>
                                    </a>    

                                </div> 
                                <div className="col-6 px-2">
                                    <div id="other-card" className="btn-card text-center shadow">
                                        <h3>OFFLINE CARD</h3>
                                        <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                    </div>
                                </div>
                            </div>
                    

                {this.state.data.length==0 ? (
                    <div className="offline phone text-center w-100 position-relative user-card-container user-card-new">
                            <div className="w-25 mx-auto mt-3">
                                <div className="phone-relative position-relative">
                                    <img className="w-100 phone1" src="/assets/images/phone1.svg" alt=""/>
                                    <img className=" phone2" src="/assets/images/phone2.svg" alt=""/>
                                    <img className="w-100 phone3" src="/assets/images/phone3.svg" alt=""/>
                                </div>

                            </div>
                    {
                        this.state.activeSearchBox ? (
                            <h2 className="">
                                ไม่พบข้อมูลบัตรที่คุณค้นหา <br/>
                                หรือเพิ่มบัตรอื่นๆกันเลย
                            </h2>
                        ) : (
                            <h2 className="">
                                เพิ่ม แอปร้านประจำ <br/>
                                หรือบัตรอื่นๆกันเลย
                            </h2>
                        )
                    }
                            

                    </div>
                ) : (
            
                        <div className="text-center p-4 w-100 pb-200 position-relative user-card-container user-card-new">
                            {/* <div><img src="/assets/images/blank.gif" width="1" height="80"/></div> */}
                        {
                           
                  
                            this.state.data.map((row,key) => {
                                let loopClass='';
                                if(key==0){
                                    loopClass = 'overlap-first';
                                }else if((this.state.data.length-1)==key){
                                    loopClass= 'overlap overlap-last'
                                }else{
                                    loopClass= 'overlap'
                                }
                                
                                let offline_img = '/assets/images/card_default.png';
                                if (row.imgcard != null && row.imgcard != '') {
                                    offline_img = row.imgcard
                                } else if (row.imgfront != null && row.imgfront != '') {
                                    offline_img = row.imgfront
                                }
                                    return (
                                     
                                    
                                       
                                            <>
                                            {/* <div className="container-member-card">
                                                <a href="#" data-primary={key} onClick={this.handleClick}>
                                                    <img  className={loopClass+' w-100 shadow member-card bor'}  src={offline_img} data-primary={key} data-store_id={row.storeid} alt={row.storename} onClick={this.addActiveClass} />
                                                    <div class="bottom-right"><img src="/assets/images/btn_enterdetailcard.png" onClick={(e) => this.handleGotoOfflineCard(e,row.otherid)}/></div>
                                                </a>
                                            </div> */}
                                            <div className="container-member-card mt-2">
                                                <a href="#" data-primary={key} data-store_id={row.storeid} alt={row.othername} onClick={this.addActiveClass}>
                                                    <img  className={loopClass+' w-100 shadow member-card bor h-card'}  src={offline_img==='' ? '/assets/images/card_default.png' : offline_img} data-primary={key} data-store_id={row.storeid} alt={row.storename} onClick={this.addActiveClass} />
                                                    <div className="row w-100 mg-275">
                                                        <div className="col-3 ml-1">
                                                            {/* <img className="top-left" src={row.storelogo != "" ? row.storelogo : "/assets/images/icon-ho-new.png"} width="50%" alt="Logo shop"/> */}
                                                            <div className="top-left-nameshop">
                                                                <p className="name-shop shadow">{row.othername}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-9 pl-0">
                                                            
                                                        </div> 
                                                    </div>
                                                    {/* <div className="w-75 text-center mx-auto top-bar">
                                                        <h2 className="m-0 point-container fw-600 h">50</h2>
                                                        <h2 className="mt-point point-container fw-600 h">คะแนน</h2>

                                                        <div className="row w-100 mt-48">
                                                            <div className="col-6 text-left pl-0">
                                                                <p className="ml-3 fw-600 h">DEFAULT</p>
                                                            </div>
                                                            
                                                            <div className="col-6 text-right pr-0"> 
                                                                <p className="ml-3 fw-600 h">SILVER</p>
                                                            </div>
                                                        </div>
                                                        <div className="w-100">
                                                            <div className="position-relative shadow-sm">
                                                                <div className="bar shadow">
                                                                </div>
                                                                <div className="bar-status shadow">
                                    
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div className="row w-100 m-0 mt-1">
                                                            <div className="col-6 text-left">
                                                                <p className="fw-600">0</p>
                                                            </div>
                                                            
                                                            <div className="col-6 text-right"> 
                                                                <p className="fw-600">500</p>
                                                            </div>
                                                        </div>
                                                    </div> */}
                                                    <div style={{height: '9.5rem'}}></div>
                                                    <div className="d-flex justify-content-between card-bottom-content">
                                                    <div>
                                                        <p className="card-id mb-2 bg-no-name shadow h">{'Name : '+row.name}</p>
                                                        <p className="card-id bg-no-name shadow text-start h">{'No : '+row.barcode}</p>
                                                    </div>
                                                    {/* <div className="align-self-end expire-date">
                                                        <small>{row.dateexpire==null ? ('EXP 2020/07') : ('EXP '+row.dateexpire)}</small>
                                                    </div> */}
                                                    </div>
                                                    
                                                    <div className="row w-100">
                                                        <div className="col-3">
                                                            <div className="bottom-left"><img src="/assets/images/delete-card.png" width="40%" onClick={(e) => this.handleDeleteCard(e,row.otherid)}/></div>
                                                        </div>
                                                        <div className="col-9">
                                                            <div className="bottom-right"><img src="/assets/images/btn_enterdetailcard.png" onClick={(e) => this.handleGotoOfflineCard(e,row.otherid)}/></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </>
                                       
                                    



                                       
                                    )
                                
                                
                               
                            })


                        }
                    
                        
                        </div>
                )}
                </div>
                        
                        <div className="add addFront">
                            <a  href={config.base_url+'/other_card?openExternalBrowser=1&token='+btoa(this.state.user_id+','+time())} target="_BLANK" className="btn-add rounded-circle text-center shadow">
                                <img className="w-75 mt-2" src="/assets/images/add.svg" alt=""/>
                            </a>
                        
                        </div>


                        <Navigation active={'card'}/>
                    </div>  
                    {/* <!-- container --> */}  

    

                  
            </div>
        )
    }
}
export default User_card;
