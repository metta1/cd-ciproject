import React, { Component } from 'react'
import axios from 'axios'
import $ from 'jquery'
import {config} from '../config';
import NumberFormat from 'react-number-format';
import 'bootstrap/dist/css/bootstrap.min.css';


var QRCode = require('qrcode.react');
var ReactDOM = require('react-dom');
var Barcode = require('react-barcode');


class Ho_membership_card extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
      
    }
    state = {
        cardData: [],
        user_id : sessionStorage.getItem('user_id'),
        store_id : 4
    };

    async componentDidMount(){
        

        const cardRs = await axios.get(config.api.base_url+'/store_card/api/myaccountcard/'+this.state.store_id+'/'+this.state.user_id)
        if(cardRs.status===200){
            this.setState({
                cardData: cardRs.data,
                point: parseFloat(cardRs.data[0]['point'])
            });
        }

    
        console.log(this.state.cardData)


        this.addActiveClass()   
    }
    addActiveClass = e => {
        //e.preventDefault();
        /************ JQUERY SELECTOR ***************/
        $('#cardQR:not(#cardQR.active)').click(function(){
            $(this).addClass('active');
            $(this).attr("src", "/assets/images/icon-arrow.svg");
            $('#card-click').removeClass('d-none');
        })

        $('#cardQR.active').click(function(){
            $(this).removeClass('active');
            $(this).attr("src", "/assets/images/icon-qr-code.svg");
            $('#card-click').addClass('d-none')
        })

        $('.stampClick:not(.stampClick.active)').click(function(){
            let key = $(this).data('key');
            $(this).addClass('active');
            $(this).attr("src", "/assets/images/icon-arrow.svg");
            $('.stamp-click').removeClass('d-none');
        })

        $('.stampClick.active').click(function(){
            let key = $(this).data('key');
            $(this).removeClass('active');
            $(this).attr("src", "/assets/images/icon-qr-code.svg");
            $('.stamp-click').addClass('d-none')
        })
        

        
    }
    toggleSelected = (e,selected,hidden) => {
        e.preventDefault();
        $('#'+selected).removeClass('d-none')
        $('#'+selected+'-text').addClass('toggle-active')
        $('#'+hidden).addClass('d-none')
        $('#'+hidden+'-text').removeClass('toggle-active')
    }


    render() {
        return (
            <div>
                
                <link rel="stylesheet" type="text/css" href="/assets/css/ho_member_card.css?v=3"/>
            
                <div className="container-mobile m-auto ">
                
                    <div className="head bg-yellow shadow position-relative">
                        <div className="icon-back">
                            <a href="/account">
                                <div className="icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h3 className="align-title m-0">
                                บัตรสมาชิก HO
                            </h3>     
                        </div>
                    </div>
                    {/* CARD */}
                    {this.state.cardData.length==0 ? (
                        <div/>
                    ) : (
                        this.state.cardData.map((row,key) => {
                            let point = row.point;
                            let progresspoint = parseFloat(point)*100/500;

                            return (
                            <>
                            <div className="card shadow p-4 ">
                                <div className="w-100 position-relative">
                                    <img className="w-100" src={row.cardimg==''? '/assets/images/ho_card.png': row.cardimg} alt=""/>
                                    <div className="card-detail">
                                        <div className="w-75 text-center mx-auto">
                                           
                                            <h2 className="m-0 point-container">{row.point}</h2>
                                            
                                                
                                            <div className="row w-100 m-0">
                                                <div className="col-6 text-left pl-0">
                                                    <p>DEFAULT</p>
                                                </div>
                                               
                                                <div className="col-6 text-right pr-0"> 
                                                    <p>SILVER</p>
                                                </div>
                                            </div>
                                            <div className="w-100">
                                                <div className="position-relative shadow-sm">
                                                    <div className="bar">
                                                    </div>
                                                    <div className="bar-status shadow" style={{width:progresspoint+"%", maxWidth:'100%'}} >
                        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div className="row w-100 m-0 mt-1">
                                                <div className="col-6 text-left">
                                                    <p>0</p>
                                                </div>
                                              
                                                <div className="col-6 text-right"> 
                                                    <p>500</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-between card-bottom-content">
                                            <div>
                                                <p className="card-id mb-2">{'Name : '+row.username}</p>
                                                <p className="card-id">{'No : '+row.cardcode}</p>
                                            </div>
                                            <div className="align-self-end expire-date">
                                                <small>{row.dateexpire==null ? ('EXP 2020/07') : ('EXP '+row.dateexpire)}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="card-click" className="card-qr text-center mt-3 d-none">
                                    <div id="cardQRselected" className="mx-auto w-50" style={{height:'220px'}}>
                                        <h5 id="card-text">QR CODE</h5>
                                        <QRCode value={row.cardcode} size="500" className="w-100 "/>
                                    </div>
                                    <div id="cardBCselected" className="mx-auto w-100 d-none" style={{height:'220px'}}>
                                        <h5 id="card-text">BARCODE</h5>
                                        <Barcode value={row.cardcode} height={'100%'} margin={0} text=''/>
                                        {/* <h5>{row.cardcode}</h5> */}
                                    </div>
                                    {/* <Switch onChange={this.handleChange} checked={this.state.checked} className="toggle-barcode" width={200} /> */}
                                    <div className="toggle mx-auto shadow w-75 mt-3" style={{borderRadius:'1rem'}}>
                                        <div className="row w-100 m-0">
                                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardQRselected','cardBCselected')}>
                                                <h5 id="cardQRselected-text" className="m-0 toggle-active">QR CODE</h5>
                                            </div>
                                            <div className="col text-center p-0" onClick={(e) => this.toggleSelected(e,'cardBCselected','cardQRselected')}>
                                                <h5 id="cardBCselected-text" className="m-0">BARCODE</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        


                            <div className="card-qr-code bg-black text-center mx-auto p-2">
                                <img id="cardQR"  className="" src="/assets/images/icon-qr-code.svg" alt="" onClick={this.addActiveClass}/>
                            </div>
                            </>
                            )
                        })

                    )
                    }
                    {/* END CARD */}


                    <div className="text-left p-2">
                        <h4 className="m-0">
                            สิทธิพิเศษของบัตรสมาชิกโหการ์ด
                        </h4> 
                    </div>
                    <div className="col p-3-3 w-100">
                            <ul>
                                <li>รับส่วนลดสมาชิกเพิ่มเมื่อซื้อสินค้าที่ร่วมรายการกับ โหหรือสินค้าชั้นนำที่โหคัดสรรมาให้  (ยกเว้นสินค้าลดล้างสต๊อค สินค้าฝากขาย สินค้าในโปรโมชั่น ซื้อ1แถม1 โปรโมชั่นลดราคาตั้งแต่ 50% ขึ้นไป และโปรโมชั่นสินค้าราคาพิเศษและคะแนนเพิ่มพิเศษเฉพาะสมาชิก)</li>
                                <li>ทุกๆยอดซื้อสินค้า 100 บาท รับคะแนนสะสม 1 คะแนนในบัตรสมาชิกโหการ์ดซิลเวอร์ </li>
                                <li>รับสิทธิซื้อสินค้าราคาสุดพิเศษเฉพาะสมาชิกและสินค้าโปรโมชั่นรับคะแนนเพิ่มพิเศษเฉพาะสมาชิก</li>
                                <li>รับสิทธิแลกรีวอร์ดตามเงื่อนไข</li>
                                <li>รับสิทธิพิเศษในเดือนเกิดและส่วนลดโปรโมชั่นส่งตรงถึงสมาชิกโหการ์ดทาง SMS line Broadcast หรือทาง Ho application</li>
                            </ul>

                            
                    </div>
                    <div className="text-left p-2">
                        <h4 className="m-0">
                            เงื่อนไขของบัตรสมาชิกโหการ์ด
                        </h4> 
                    </div>
                    <div className="col p-3-3 w-100 mb-5">
                            <p className="mb-1">
                            1. สิทธิพิเศษของบัตรสมาชิกโหการ์ดนี้ใช้ได้เฉพาะโหในประเทศไทย เท่านั้น
                            </p>
                            <p className="mb-0">
                            2. สิทธิพิเศษของบัตรสมาชิกโหการ์ดเป็นไปตามเงื่อนไขและรายละเอียดของแต่ละสิทธิพิเศษ
                            </p>
                            <ul>
                                <li>การสะสมคะแนนจากการซื้อสินค้าจากร้านค้าที่ร่วมรายการ</li>
                                <li>การสะสมคะแนนจากเงื่อนไขพิเศษอื่นๆของโห</li>
                                <li>การแลกคะแนนสะสมเป็นส่วนลดหรือรางวัล</li>
                
                            </ul>
                            <p className="mb-1">
                            3. บริษัท อิสระเสรี จำกัด ขอสงวนสิทธิโดยไม่ต้องแจ้งล่วงหน้าในการเปลี่ยนสินค้าโปรโมชั่น หรือเปลี่ยนแปลงที่เข้าร่วมการโปรโมชั่นต่างๆ ของบัตรสมาชิกโหการ์ด
                            </p>
                            <p className="mb-1">
                            4. บริษัท ฯ ขอสงวนสิทธิในการยกเลิกเปลี่ยนแปลงเงื่อนไขและสิทธิประโยชน์ของบัตรสมาชิกโหการ์ดโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
                            </p>
                    </div>
                </div>  
                {/* <!-- container --> */}
        </div>
        )
    }
}
export default Ho_membership_card;
