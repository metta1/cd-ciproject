import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest'

class User extends Component {
    constructor(){
        super();
        console.log('construct')
    }

    state = {
        data : []
    };

/*
    async componentDidMount(){
        //console.log('componentDidMount')
        const result = await axios.get('https://jsonplaceholder.typicode.com/users');
        
        this.setState({data: result.data});
        
    }
*/
    onChange = event => {
        //console.log(event.target.value)
        this.props.onNameChange(event.target.value)//ส่งค่าไปเปลี่ยน state
    }



    render() {
        console.log('render');
        const name = this.props.name;//รับ props ที่ส่งมาจาก app
        return (
            <div>
                <div> User {name}</div>
                <input name="input_user" type="text" onChange={this.onChange}/>
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>email</th>
                        </tr>
                    </thead>
                    <tbody>
                {this.props.data === undefined ? (
                    <div/>
                ) : (
                this.props.data.map(user => (//state ถ้าใช้จาก  component เอง ใช้ props เพราะรับค่าจาก higher component
                    <tr key={user.id}>
                        <td>{user.id}</td>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                    </tr>
                )
                ))}
                    </tbody>
                </table>
                
            </div>
        )
    }
}
export default User;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;