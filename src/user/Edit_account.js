import React, { Component } from 'react'
import UserForm from './UserForm'
import axios from 'axios'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import {config} from '../config';
import queryString from 'query-string'
import Swal from 'sweetalert2'
import InputMask from 'react-input-mask';
import moment from 'moment';
import DatePicker, { registerLocale } from 'react-datepicker';
import Store_color from '../template/Store_color'
import 'react-datepicker/dist/react-datepicker.css';
import th from "date-fns/locale/th"; // the locale you want
import { ResponsiveEmbed } from 'react-bootstrap';
registerLocale("th", th); // register it with the name you want



class Edit_account extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        
        let locationSearch = this.props.location.search
        let getParam       = queryString.parse(locationSearch);
        let refer_url      = '/home';
        if(getParam.referer !== undefined && getParam.referer !== ''){
            refer_url = '/'+getParam.referer;
        }
       
        this.handleChangeDate = this.handleChangeDate.bind(this);

        this.state = {
            displayName : '',
            lineid : '',
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
            refer_url : refer_url,
            fname:'',
            lname:'',
            email:'',
            usertel:'',
            user_id : sessionStorage.getItem('user_id'),
            birthday : '',
            gender :  '',
            store_id : localStorage.getItem('storageStoreID'),
            birthDate : undefined
        };
        
    
        
    
        
       
    }


    handleBackPage = e => {
        if (this.state.store_id != '') {
            window.location='/account_shop/'+this.state.store_id;
        } else {
            window.location='/account';
        }
    }
    
    
    async componentDidMount(){

        console.log("storageStoreID: ",this.state.store_id)
       
        const userData =  await axios.get(config.api.base_url+'/api/userData/userid/'+this.state.user_id)
        //console.log(userData)
        if(userData.status===200){
            let rowData = userData.data;
            this.setState({
                userid : rowData.userid,
                fname  : rowData.fname,
                lname  : rowData.lname,
                usertel: rowData.usertel,
                email  : rowData.email,
                birthday  : (rowData.birthday !== "" && rowData.birthday !=='0000-00-00') ? moment(rowData.birthday).toDate() : '',
                birthDate  : (rowData.birthday !== "" && rowData.birthday !=='0000-00-00') ? rowData.birthday : undefined,
                gender  : rowData.gender
            });

         
            
        }
        
        
         
    }

    handleChange = event => {
      console.log(event.target.name);
        if(event.target.name=='usertel'){
             let tel = (event.target.value).replace(/[^\d]/g, '');
             this.setState({[event.target.name]: tel});
        }else{
             this.setState({[event.target.name]: event.target.value});
        }
         
         //console.log(this.state)
     }

    async handleChangeDate(date) {
        if (date !== null) {
            this.setState({ birthday: moment(date).toDate() });
        } else {
            this.setState({ birthday: '' });
        }
    };

    handleSubmit = event => {
        //console.log(this.state)
        const obj    = this;
        if(this.state.user_id !==''){
            if(this.state.fname && this.state.email && this.state.usertel && this.state.birthday && this.state.gender){
                let bodyFormData = new FormData();
                bodyFormData.set('fname',this.state.fname)
                bodyFormData.set('lname',this.state.lname)
                if(this.state.birthDate===undefined){//ยังไม่มีข้อมูลในเบส
                    bodyFormData.set('birthday', moment(this.state.birthday).format('YYYY-MM-DD'))
                }
                bodyFormData.set('gender',this.state.gender)
                bodyFormData.set('email',this.state.email)
                bodyFormData.set('usertel',this.state.usertel)
                bodyFormData.set('userid',this.state.user_id)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/users/api/update',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        if(response.status===200 || response.status===201){
                            if (obj.state.store_id != '') {
                                window.location='/account_shop/'+obj.state.store_id;
                            } else {
                                window.location='/account';
                            }
                        }else if(response.status===203){
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'บันทึกข้อมูลไม่สำเร็จ เนื่องจาก '+response.data.message,
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })

                        }else{
                            Swal.fire({
                                title : 'ขออภัย',
                                html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                                icon:'error',
                                confirmButtonText:'ตกลง',
                                customClass: {
                                    confirmButton: 'btn-darkblue',
                                },
                            })
                        }
                        //console.log(response);
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'การเชื่อมต่อผิดพลาด กรุณาทำรายการใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            customClass: {
                                confirmButton: 'btn-darkblue',
                            },
                        })
                });
            }else{
                Swal.fire({
                    title : 'ขออภัย',
                    html: 'กรุณากรอกข้อมูลให้ครบถ้วน',
                    icon:'info',
                    confirmButtonText:'ตกลง',
                    customClass: {
                        confirmButton: 'btn-darkblue',
                    },
                })
            }
            
        }else{
            Swal.fire({
                title : 'ขออภัย',
                html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
                icon:'error',
                confirmButtonText:'ตกลง',
                customClass: {
                    confirmButton: 'btn-darkblue',
                },
            })
        }
        
      
        
    }
    
   
    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/account.css?v=3"/>
                <Store_color/>
                <div className="container-mobile m-auto ">
                    <div className="head bg-yellow shadow position-relative tabbar-color">
                        <div className="edit-icon-back">
                            <a onClick={this.handleBackPage}>
                                <div className="edit-icon rounded-circle shadow">
                                    <img src="/assets/images/back.svg" alt=""/>
                                </div>
                            </a>
                        </div>
                        <div className=" h-100 p-3 pl-5">
                            <h3 className="align-title m-0" >
                                แก้ไขบัญชีผู้ใช้
                            </h3> 
                        </div>
                    </div>
                   
                    
                    <div className=" p-4 w-100 frame shadow bg-white">
                        <div className="row">             
                            <div class="col-md-12">
                    
                                <form action="#">
                                    <div className="form-group">
                                        <label>ชื่อ</label><span class="star">*</span>
                                        <input className="form-control" type="text" id="fname" name="fname" placeholder="ชื่อจริง" value={this.state.fname} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>นามสกุล</label><span class="star">*</span>
                                        <input className="form-control" type="text" id="lname" name="lname" placeholder="นามสกุล" value={this.state.lname} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>วันเกิด</label><span class="star">{this.state.birthDate === undefined &&('*')}</span>
                                        {
                                            this.state.birthDate === undefined ? (
                                                <DatePicker
                                                    selected={this.state.birthday}
                                                    onChange={this.handleChangeDate}
                                                    locale={th}
                                                    dateFormat="dd/MM/yyyy"
                                                    peekNextMonth
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    dropdownMode="select"
                                                    className="form-control"
                                                    name="birthday"
                                                    id="birthday"
                                                    autoComplete="off"

                                                />
                                            ) : (
                                                <InputMask mask="" maskChar={null}  
                                                name=""
                                                className="form-control"
                                                placeholder=""
                                                
                                                value={moment(this.state.birthday).format('DD/MM/YYYY')}
                                                readOnly/>
                                            )
                                        }
                                        
                                    </div>
                                    <div className="form-group">
                                        <div>
                                            <label>เพศ</label><span className="star">*</span>
                                        </div>
                                        <div className="row col-12 pl-0">
                                            <div className="col-3 pr-0">
                                                <input type="radio" id="genderMen" name="gender" value="1" onChange={this.handleChange} checked={this.state.gender === '1'}/> ชาย
                                            </div>
                                            <div className="col-3 pr-0 pl-1">
                                                <input type="radio" id="genderWomen" name="gender" value="2" onChange={this.handleChange} checked={this.state.gender === '2'}/> หญิง
                                            </div>
                                            <div className="col-3 pl-0">
                                                <input type="radio" id="genderOther" name="gender" value="3" onChange={this.handleChange} checked={this.state.gender === '3'}/> อื่นๆ
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>อีเมลของคุณ</label><span class="star">*</span>
                                        <input className="form-control" type="text" id="email" name="email" placeholder="อีเมล" value={this.state.email} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        
                                        <label style={{whiteSpace:'nowrap'}}>เบอร์โทรศัพท์มือถือ</label><span class="star">*</span>
                                        <InputMask mask="999-999-9999" maskChar={null}  
                                        name="usertelNONONONO"
                                        className="form-control"
                                        placeholder="เบอร์โทรศัพท์มือถือ"
                                        
                                        value={this.state.usertel}
                                        readOnly/>
                                       
                                    </div>
                                    <div className="btn-group w-100 text-center d-block">
                                        <input class="w-100 position-relative" type="button" value="บันทึก" onClick={this.handleSubmit} className="btn-blue-rouded pl-4 pr-4 button-color"/>
                                    </div>
                                
                            
                                
                                
                                    
                                </form>

                                
                            
                            </div>
                        
                        </div>      
                    </div>
                   
                    
                </div>  
              
        </div>
        )
    }
}
export default Edit_account;
