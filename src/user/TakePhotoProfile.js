import React, { Component, useState, useEffect} from 'react'
import $ from 'jquery'
import Camera, { FACING_MODES, IMAGE_TYPES }  from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import ImagePreview from './ImagePreview';
import Swal from 'sweetalert2'
import {config} from '../config';
import {getParam} from '../lib/Helper';
import axios from 'axios'


export default class TakePhotoProfile extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.state = {
          dataUri:'',
          token:getParam('token')
        };
        
    }
   
    handleTakePhoto (dataUri) {
        // Do stuff with the photo...
        $('.white-flash').addClass('do-transition');
        console.log('takePhoto');
    }

    handleSaveImage = event => {
      if(this.state.dataUri !=='' && this.state.token !== undefined){
              console.log(this.state.dataUri)
              let bodyFormData = new FormData();
              bodyFormData.set('dataUri',this.state.dataUri)
              bodyFormData.set('token',this.state.token)
              
              axios({
                  method: 'post',
                  url: config.api.base_url+'/users/api/saveimageprofile',
                  data: bodyFormData,
                  headers: {'Content-Type': 'multipart/form-data' }
                  })
                  .then(function (response) {
                      if(response.status===201){
                         window.location='line://app/1654185466-nKkG6Xok?referer=account'
                      }else{
                          Swal.fire({
                              title : 'ขออภัย',
                              html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                              icon:'error',
                              confirmButtonText:'ตกลง',
                              customClass: {
                                  confirmButton: 'btn-darkblue',
                              },
                          })
                      }
                      console.log(response);
                  })
                  .catch(function (response) {
                    Swal.fire({
                      title : 'ขออภัย',
                      html: 'ไม่สามารถเชื่อมต่ได้ กรุณาทำรายการใหม่อีกครั้ง',
                      icon:'error',
                      confirmButtonText:'ตกลง',
                      customClass: {
                          confirmButton: 'btn-darkblue',
                      },
                  })
              });
         
          
      }else{
          Swal.fire({
              title : 'ขออภัย',
              html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
              icon:'error',
              confirmButtonText:'ตกลง',
              customClass: {
                  confirmButton: 'btn-darkblue',
              },
          })
      }
    }



     
    async handleTakePhotoAnimationDone (dataUri) {
        const result = await this.resizeImage(dataUri, 500, 500);
        //console.log(result)
        //const result = dataUri
        this.setState({dataUri:result})
        // Do stuff with the photo...
        
        setTimeout(function(){ 
            /* $('.white-flash').addClass('do-transition');
            $('img').css({'display': 'inline-block'}); */
            console.log('takePhoto2');
        }, 3000);
       
    }

    handleRetake = event => {
        this.setState({dataUri:''})
    }

    resizeImage = (base64Str, maxWidth = 400, maxHeight = 350) => {
        return new Promise((resolve) => {
          let img = new Image()
          img.src = base64Str
          img.onload = () => {
            let canvas = document.createElement('canvas')
            console.log(canvas.height)
            const MAX_WIDTH = maxWidth
            const MAX_HEIGHT = maxHeight
            let width = 500
            let height = 500
      
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width
                width = MAX_WIDTH
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height
                height = MAX_HEIGHT
              }
            }
            canvas.width = width
            canvas.height = height
            let ctx = canvas.getContext('2d')
            ctx.drawImage(img, 0, 0, width, height)
            resolve(canvas.toDataURL())
          }
        })
    }
     
    handleCameraError (error) {
        console.log('handleCameraError', error);
    }
     
    handleCameraStart (stream) {
        console.log('handleCameraStart');
    }
     
    handleCameraStop () {
        console.log('handleCameraStop');
    }
    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="assets/css/account.css?v=2"/>
                {
                    (this.state.dataUri)
                    ? (<>
                    <ImagePreview dataUri={this.state.dataUri}
                        isFullscreen={false}
                    />
                    <div>
                    <div className="btn-group group-panel w-100 bg-dark pt-2 pb-2">
                      <div className="col-6">
                        <button className="btn btn-light w-100" onClick={this.handleSaveImage}>บันทึก</button>
                      </div>
                      <div className="col-6">
                        <button className="btn btn-light w-100" onClick={this.handleRetake}>ยกเลิก</button>
                      </div>
                      
                    </div>
                  </div>
                    </>)
                    : <Camera
                            onTakePhoto = { (dataUri) => { this.handleTakePhoto(dataUri); } }
                            onTakePhotoAnimationDone = { (dataUri) => { this.handleTakePhotoAnimationDone(dataUri); } }
                            onCameraError = { (error) => { this.handleCameraError(error); } }
                            idealFacingMode = {FACING_MODES.USER}
                            idealResolution = {{width: 500, height: 500}}
                            imageType = {IMAGE_TYPES.JPG}
                            imageCompression = {0.97}
                            isMaxResolution = {false}
                            isImageMirror = {true}
                            isSilentMode = {false}
                            isDisplayStartCameraError = {true}
                            isFullscreen = {false}
                            //sizeFactor = {1}
                            onCameraStart = { (stream) => { this.handleCameraStart(stream); } }
                            onCameraStop = { () => { this.handleCameraStop(); } }
                            />
                }
            </div>
                
        )
    }
}
