import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import Swal from 'sweetalert2'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class User_card extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        let user_id = sessionStorage.getItem('user_id');
        if(!user_id){
            sessionStorage.setItem('referer',currentComponent);
            //window.location='/register'
        }
        
        this.addActiveClass = this.addActiveClass.bind(this);
        

        
      
        
    }

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        activeClass:''
    };


    async componentDidMount(){
        
        let user_id = sessionStorage.getItem('user_id');
        if(user_id!==null){
            const result = await axios.get('https://backend-ho.dreamnolimit.com/api/card/'+user_id)
            if(result.status===200){
                this.setState({data: result.data});
            }
        }
        this.addActiveClass('');
        
        
        //console.log(this.state.data)

        
     
        
    }

    addActiveClass(e){
        if(e){
            e.preventDefault();
        }
       
        //console.log(e.target)
        //let elememtCard =  e.target;
        //console.log(e.target.parentElement.attributes.href)
        $('.card-selected').click(function() {
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            window.location='/shop/'+storeID
            
            
        })
        $('.member-card:not(.card-selected)').click(function() {
            
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            $(this).addClass('card-selected');
            
            //$("a[data-primary="+cardID+"]").prepend('fffffff');
            //console.log(elememtCard)
            //$('a[data-primary="'+cardID+'"]').html('<img class="" src="/assets/images/icon-home.svg" alt="'+cardID+'">')
            // $( 'a[data-primary="'+cardID+'"]').html('');
            // console.log($(this).find('a'));
            // $(this).remove();
            
            //console.log($(this).before())
        
            
        })
        
    }

    handleClick = event => {
        event.preventDefault();
        //console.log(event.target.parentNode.attributes.href.nodeValue)
        //console.log(event.target.parentNode.outerHTML)
        let cardID = event.target.parentNode.attributes[1].nodeValue
        console.log(event.target)
      
        this.setState({
            activeClass:cardID
        })
        
    }
    

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    handleAddCard = event => {
       
       
    }
    

    

   
    
    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>
                
                    <div className="container-mobile m-auto ">
                        <div className="head bg-yellow shadow">
                            <div className="row w-100 h-100">
                                <div className="col-8 h-100">
                                    <div className=" h-100 p-3 pl-5">
                                        <h1 className="m-0 "> <span className="text-blue">CA</span>RD</h1> 
                                        <p >บัตรสมาชิกต่างๆของคุณ</p>
                                    </div>
                                </div>
                                <div className="col-4 h-100">
                                    <div className="row float-right h-100 w-100">
                                        <div className="col p-1 my-auto">
                                            <div className="icon rounded-circle">
                                                <img className="w-100" src="/assets/images/icon-search.svg" alt=""/>
                                            </div>
                                        </div>
                                        <div className="col p-1 my-auto">
                                            <div className="icon rounded-circle">
                                                <img className="" src="/assets/images/icon-scan.svg" alt=""/>
                                            </div>
                                        </div>
                                        <div className="col p-1 my-auto">
                                            <div className="icon rounded-circle">
                                                <img className="" src="/assets/images/icon-noti.svg" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>

                {this.state.data.length==0 ? (
                    <div className="phone text-center">
                            <div className="w-25 mx-auto ">
                                <div className="phone-relative position-relative">
                                    <img className="w-100 phone1" src="/assets/images/phone1.svg" alt=""/>
                                    <img className=" phone2" src="/assets/images/phone2.svg" alt=""/>
                                    <img className="w-100 phone3" src="/assets/images/phone3.svg" alt=""/>
                                </div>

                            </div>
                            <h2 className="">
                                เพิ่ม แอปร้านประจำ <br/>
                                หรือบัตรอื่นๆกันเลย
                            </h2>

                    </div>
                ) : (
            
                        <div className="text-center p-4 w-100">
                            
                        {
                           
                  
                            this.state.data.map((row,key) => {
                                let loopClass='';
                                if(key==0){
                                    loopClass = 'overlap-first';
                                }else if((this.state.data.length-1)==key){
                                    loopClass= 'overlap overlap-last'
                                }else{
                                    loopClass= 'overlap'
                                }
                                
                                    return (
                                     
                                    
                                       this.state.activeClass==key ? (
                                            <>
                                                <a href={'/shop/'+row.storeid} data-active={key} onClick={this.handleClick}>
                                                    <img  className={loopClass+' w-100 shadow member-card'}  src={row.card_img==='' ? '/assets/images/card_default.png' : row.card_img} data-primary={key} data-store_id={row.storeid} alt={row.storename} onClick={this.addActiveClass} />
                                                    
                                                </a>
                                                {/* <a href={'/shop/'+row.storeid} data-active={key} onClick={this.handleClick}>
                                                    <img  className={loopClass+' w-100 shadow member-card'}  src={row.card_img==='' ? '/assets/images/card_default.png' : row.card_img} data-primary={key} data-store_id={row.storeid} alt={row.storename} onClick={this.addActiveClass} />
                                                </a> */}
                                            </>
                                       ):(
                                            <>
                                            <a href="#" data-primary={key} onClick={this.handleClick}>
                                                <img  className={loopClass+' w-100 shadow member-card'}  src={row.card_img==='' ? '/assets/images/card_default.png' : row.card_img} data-primary={key} data-store_id={row.storeid} alt={row.storename} onClick={this.addActiveClass} />
                                            </a>
                                        </>
                                       )
                                    



                                       
                                    )
                                
                                
                               
                            })


                        }
                    
                        
                        </div>
                )}
                        
                        <div className="add">
                            <Link  to={'/app_card?v='+ new Date().getTime()} className="btn btn-add rounded-circle text-center shadow">
                                <img className="w-100" src="/assets/images/add.svg" alt=""/>
                            </Link>
                        
                        </div>


                        <div className="footer mx-auto">
                                <img className="w-100 position-relative" src="/assets/images/footer_card.svg" alt=""/>
                                <div className="menu position-absolute w-100">
                                    <div className="menu-item px-2 pt-2 ">
                                        <div className="ml-2 menu-item-active bg-white rounded-circle shadow">
                                            <div className="menu-item-active-ab">
                                                <a href="/user_card">
                                                    <img className="" src="/assets/images/icon-credit.svg" alt=""/> <br/>
                                                    <span>CARD</span>
                                                </a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                
                                        <a href="/feed">
                                            <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                            <span>Feed</span>
                                        </a>
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a href="/home">
                                            <img className="" src="/assets/images/icon-home.svg" alt=""/><br/>
                                            <span>HOME</span>
                                        </a>
                                    </div>
                                    <div className="menu-item px-2 ">
                                        <a href="/promotion_category">
                                            <img className="" src="/assets/images/icon-gift.svg" alt=""/> <br/>
                                            <span>PROMOTION</span>
                                        </a>
                    
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a href="/account">
                                            <img className="position-relative" src="/assets/images/icon-user.svg" alt=""/> <br/>
                                            <span>ACCOUNT</span>
                                        </a>

                                    </div>
                                </div>

                        </div>
                    </div>  
                    {/* <!-- container --> */}

    

                 
            </div>
        )
    }
}
export default User_card;
