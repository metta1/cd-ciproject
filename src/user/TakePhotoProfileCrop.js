import React, { Component, useState, useEffect} from 'react'
import $ from 'jquery'
import Camera, { FACING_MODES, IMAGE_TYPES }  from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import ImagePreview from './ImagePreview';
import Swal from 'sweetalert2'
import {config} from '../config';
import axios from 'axios'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Modal from 'react-bootstrap4-modal';
import './css/takephoto.css';



export default class TakePhotoProfile extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        this.state = {
          dataUri:'',
          token:getParam('token'),
          camera1:0,
          sourceCamera1:undefined,
          crop1: {
              unit: '%',
              width: 100,
              aspect: 10 / 10,
          },
          isOpen1: false,
          src1:null,
        };
        
    }

    async componentDidMount(){
    
      $('#inputSource1').click();
      
       
     
      $("body" ).css( "background-color","#f9ce43" );
      
    }
   
    handleTakePhoto (dataUri) {
        // Do stuff with the photo...
        $('.white-flash').addClass('do-transition');
        console.log('takePhoto');
    }

    handleOpencam1 = (event) => {
        this.setState({
        camera1: 1
        })

        $('#inputSource1').click();
    }

    handleinputSource1 = e => { 
      e.preventDefault();
      let file = e.target.files[0];
      let reader = new FileReader();
      if(file){
          reader.readAsDataURL(file);
          reader.onloadend = () => {
              let trueSize = getSizeFromImgDataURL(reader.result,'MB');
              if(trueSize>=100){
                
                  Swal.fire({
                      title : 'ขออภัย',
                      html: 'ไฟล์ภาพต้องมีขนาดไม่เกิน 1 MB',
                      icon:'info',
                      confirmButtonText:'ตกลง',
                      customClass: {
                          confirmButton: 'btn-black',
                      },
                  })
                  
                  
                  this.setState({
                      sourceCamera1: undefined,
                    
                  });
              }else{
                  this.setState({
                      isOpen1:true,
                      sourceCamera1: reader.result,
                      src1: reader.result
                  });
                  $('.custom-modal').css("z-index",9999999);
                  $('.modal-backdrop').css("z-index",99999);
                  $('.takephoto-container #img').hide()

                  console.log(reader.result)
              }

              
          };
      }else{
          this.setState({
              sourceCamera1: undefined,
          });
      }
  }

  /************ CROP **************/

  onImageLoaded1 = image => {
      this.imageRef = image;
    };
  
    onCropComplete1 = crop => {
      this.makeClientCrop1(crop);
    };
  
    onCropChange1 = (crop1, percentCrop) => {
      this.setState({ crop1 });
    };
  
    async makeClientCrop1(crop) {
      if (this.imageRef && crop.width && crop.height) {
        const sourceCamera1 = await this.getCroppedImg(
          this.imageRef,
          crop,
          'newFile.png'
        );
      
        this.setState({ sourceCamera1 });
      }
    }
  
    getCroppedImg(image, crop, fileName) {
      const canvas = document.createElement('canvas');
      const scaleX = image.naturalWidth / image.width;
      const scaleY = image.naturalHeight / image.height;
      canvas.width = crop.width;
      canvas.height = crop.height;
      const ctx = canvas.getContext('2d');
  
      ctx.drawImage(
        image,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height
      );

      const base64Image = canvas.toDataURL('image/jpeg');
      return base64Image;
  
     
    }

    handleSaveImage = event => {
      if(this.state.sourceCamera1 !=='' && this.state.token !== undefined){
              //console.log(this.state.dataUri)
              let bodyFormData = new FormData();
              bodyFormData.set('dataUri',this.state.sourceCamera1)
              bodyFormData.set('token',this.state.token)
              
              axios({
                  method: 'post',
                  url: config.api.base_url+'/users/api/saveimageprofile',
                  data: bodyFormData,
                  headers: {'Content-Type': 'multipart/form-data' }
                  })
                  .then(function (response) {
                      if(response.status===201){
                         window.location='line://app/1654185466-nKkG6Xok?referer=account'
                      }else{
                          Swal.fire({
                              title : 'ขออภัย',
                              html: 'บันทึกข้อมูลไม่สำเร็จ กรุณาทำรายการใหม่อีกครั้ง',
                              icon:'error',
                              confirmButtonText:'ตกลง',
                              customClass: {
                                  confirmButton: 'btn-darkblue',
                              },
                          })
                      }
                      console.log(response);
                  })
                  .catch(function (response) {
                    Swal.fire({
                      title : 'ขออภัย',
                      html: 'ไม่สามารถเชื่อมต่ได้ กรุณาทำรายการใหม่อีกครั้ง',
                      icon:'error',
                      confirmButtonText:'ตกลง',
                      customClass: {
                          confirmButton: 'btn-darkblue',
                      },
                  })
              });
         
          
      }else{
          Swal.fire({
              title : 'ขออภัย',
              html: 'เกิดข้อผิดลาด กรุณาทำรายการใหม่อีกครั้ง',
              icon:'error',
              confirmButtonText:'ตกลง',
              customClass: {
                  confirmButton: 'btn-darkblue',
              },
          })
      }
    }



     
    async handleTakePhotoAnimationDone (dataUri) {
        const result = await this.resizeImage(dataUri, 500, 500);
        //console.log(result)
        //const result = dataUri
        this.setState({dataUri:result})
        // Do stuff with the photo...
        
        setTimeout(function(){ 
            /* $('.white-flash').addClass('do-transition');
            $('img').css({'display': 'inline-block'}); */
            console.log('takePhoto2');
        }, 3000);
       
    }

    handleRetake = event => {
        this.setState({
          src1:null,
          sourceCamera1:undefined
        })
        //this.setState({dataUri:''})
    }

    handleCropDone = e => {
      this.setState({
        src1:null,
      })
    }

    resizeImage = (base64Str, maxWidth = 400, maxHeight = 350) => {
        return new Promise((resolve) => {
          let img = new Image()
          img.src = base64Str
          img.onload = () => {
            let canvas = document.createElement('canvas')
            console.log(canvas.height)
            const MAX_WIDTH = maxWidth
            const MAX_HEIGHT = maxHeight
            let width = 500
            let height = 500
      
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width
                width = MAX_WIDTH
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height
                height = MAX_HEIGHT
              }
            }
            canvas.width = width
            canvas.height = height
            let ctx = canvas.getContext('2d')
            ctx.drawImage(img, 0, 0, width, height)
            resolve(canvas.toDataURL())
          }
        })
    }
     
    handleCameraError (error) {
        console.log('handleCameraError', error);
    }
     
    handleCameraStart (stream) {
        console.log('handleCameraStart');
    }
     
    handleCameraStop () {
        console.log('handleCameraStop');
    }
    render() {
        return (
          <>
            <div className="text-center mt-2 mb-2">
                <img className="takephoto head-logo" src="/assets/images/icon-ho-new.png" alt="" />
                <div className="takephoto-container">
                {this.state.src1 ? (
                  <>
                                <ReactCrop
                                    src={this.state.src1}
                                    crop={this.state.crop1}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded1}
                                    onComplete={this.onCropComplete1}
                                    onChange={this.onCropChange1}
                                />
                                <div className="btn-group group-panel w-100 pt-2 pb-0">
                                  <div className="col-6">
                                    <button className="btn takephoto btn-blue  w-100" onClick={this.handleCropDone}>ตกลง</button>
                                  </div>
                                  <div className="col-6">
                                    <button className="btn takephoto btn-blue  w-100" onClick={this.handleRetake}>ยกเลิก</button>
                                  </div>
                                  
                                </div>
                    </>
                    ) : (
                      <>
                      <img className="img-thumbnail mb-2" src={this.state.sourceCamera1!==undefined ? this.state.sourceCamera1 : '/assets/images/icon-upload.svg'} onClick={this.handleOpencam1} id="img1"/>
                      <input type="file" id="inputSource1" name="inputSource1" accept="image/*" capture onChange={this.handleinputSource1} style={{display:'none'}}/>
                      {
                        this.state.sourceCamera1 !== undefined ? (
                          <button className="btn takephoto btn-blue  w-100" onClick={this.handleSaveImage}>บันทึก</button>
                        ) : (
                          <button className="btn takephoto btn-blue  w-100" onClick={this.handleOpencam1}>ถ่ายรูป</button>
                        )
                      }
                     
                      </>
                    ) 
                    
                  }
                    
                </div>
               

              
            </div>
        
          

          </>
                
        )
    }
}
