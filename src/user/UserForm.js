import React, { Component } from 'react'
import axios from 'axios'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import SweetAlert from "react-bootstrap-sweetalert";
import {config} from '../config';
import queryString from 'query-string'
import Swal from 'sweetalert2'



export default class UserForm extends Component {
    constructor(props){
        super(props);
      
      
        this.state = {
            displayName : '',
            lineid : '',
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
            fname:'',
            email:'',
            usertel:'',
            user_id : sessionStorage.getItem('user_id'),
        };
        
     
        
    
        
       
    }
    
    
    async componentDidMount(){
        alert()
        const userData =  await axios.get(config.api.base_url+'/api/userData/userid/'+this.state.user_id)
        console.log(userData)
        if(userData.status===200){
            let rowData = userData.data;
            console.log(rowData)
            this.setState({
                userid : rowData.userid,
                fname  : rowData.fname,
                usertel: rowData.usertel,
                email  : rowData.email,
               
            });

           
            
            
        }
        
        
        
         
    }
    
  
    componentDidMount() { 
       
    }
    render() {
        return (
            <div>
                <div className="row">             
                    <div class="col-md-12">
            
                        <form action="#">
                            <div className="form-group">
                                <label>ชื่อ-นามสกุล</label><span class="star">*</span>
                                <input className="form-control" type="text" id="fname" name="fname" placeholder="ชื่อจริง" value={this.state.fname} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <label>อีเมลของคุณ</label><span class="star">*</span>
                                <input className="form-control" type="text" id="lname" name="email" placeholder="อีเมล" value={this.state.email} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                 
                                <label >เบอร์โทรศัพท์มือถือ</label><span class="star">*</span>
                                <input className="form-control" type="text" id="lname" name="usertel" placeholder="เบอร์โทรศัพท์มือถือ" value={this.state.usertel} onChange={this.handleChange}/>
                            </div>
                            <div className="btn-group">
                                <input class="w-100 position-relative" type="submit" value="ยืนยันการลงทะเบียน" onClick={this.handleSubmit} className="btn-blue-rouded"/>
                            </div>
                         
                    
                        
                           
                            
                        </form>

                        
                    
                    </div>
                   
                </div>      
            </div>
        )
    }
}
