import React, { Component } from 'react'
import axios from 'axios'
//import withRequest from '../lib/withRequest';
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './card-touch.css';
import {Route, Link, useParams} from 'react-router-dom'
import {config} from '../config';
import $ from 'jquery'
import Swal from 'sweetalert2'
import Modal from 'react-bootstrap4-modal';
import {Navigation,Notification_num} from '../template'
import {getParam, time} from '../lib/Helper';

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class User_card_offline extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        let user_id = sessionStorage.getItem('user_id');
        if(!user_id){
            sessionStorage.setItem('referer',currentComponent);
            //window.location='/register'
        }
        
        this.addActiveClass = this.addActiveClass.bind(this);
        this.handleFilterCard = this.handleFilterCard.bind(this);
        

        this.handleGenerateCardsJson = this.handleGenerateCardsJson.bind(this)
        this.nextCard = this.nextCard.bind(this)
        this.prevCard = this.prevCard.bind(this)
        this.handleTouchStart = this.handleTouchStart.bind(this)
        this.handleTouchMove = this.handleTouchMove.bind(this)
        

        
      
        
    }

    state = {
        data : [],
        cat_id:this.props.match.params.cat_id,
        activeClass:'',
        activeSearchBox:false,
        activeCardTouch:0,
        xDown : null,
        yDown : null,
        user_id :sessionStorage.getItem('user_id')
    };


    async componentDidMount(){
        
        let user_id = sessionStorage.getItem('user_id');
        if(user_id!==null){
            const result = await axios.get(config.api.base_url+'/admin_othercard/api/offline/'+user_id)
            if(result.status===200){
                this.setState({data: result.data});
            }
            console.log(result)
        }
        this.addActiveClass('');
        //this.handleFilterCard(this);
        this.handleGenerateCardsJson(this)
        
        
        console.log(this.state.data)
        let cardTouch = document.getElementById("cardTouch");
        if(cardTouch){
            cardTouch.addEventListener('touchstart', this.handleTouchStart, false);        
            cardTouch.addEventListener('touchmove', this.handleTouchMove, false);
        }

        let nextCard = document.getElementById("next-card");
        let prevCard = document.getElementById("prev-card");
        if(nextCard){
            nextCard.addEventListener('click', this.nextCard, false);
        }
        if(prevCard){
            prevCard.addEventListener('click', this.prevCard, false);
        }

        let objReact = this
        $('.btn-deletecard').click(function(){
            let otherid = $(this).data('id');
            objReact.handleDeleteCard(undefined,otherid)
            
        })
        
        if(this.state.data.length != 0){
            switch(this.state.data.length){
                case 1:
                    $('#cardTouch').css({"height": "280px"})
                    break;
                case 2:
                    $('#cardTouch').css({"height": "300px"})
                    break;
                case 3:
                    $('#cardTouch').css({"height": "370px"})
                    break;
                default:
                    $('#cardTouch').css({"height": "390px"})
            }
        }

        
    }

   

    addActiveClass(e){
        if(e){
            e.preventDefault();
        }
       
        //console.log(e.target)
        //let elememtCard =  e.target;
        //console.log(e.target.parentElement.attributes.href)
        $('.card-selected').click(function() {
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            //window.location='/shop/'+storeID
            
            
        })
        $('.member-card:not(.card-selected)').click(function() {
            
            let storeID = $(this).data('store_id');
            let cardID = $(this).data('primary');
            $('.member-card').removeClass('card-selected');
            $('.member-card').removeClass('card-selected-last');
            $(this).addClass('card-selected');
            
      
            
        })
        $('.container-member-card.container-selected').click(function(){
            $(this).removeClass('container-selected');
        })
        $('.container-member-card:not(.container-selected)').click(function(){
            $('.container-member-card').removeClass('container-selected');
            $(this).addClass('container-selected');
        })
        
    }

    async handleFilterCard(e){
        
        let user_id = sessionStorage.getItem('user_id');
        if(user_id!==null){
            let keyWord = e.target.value;
            const result = await axios.get(config.api.base_url+'/api/card/'+user_id+'?keywords='+keyWord)
            console.log(result)
            if(result.status===200){
                this.setState({data: result.data});
                $('#cardTouch').html('');
                this.handleGenerateCardsJson(this)
            }
        }
        /*$("#searchInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".user-card-container .container-member-card a").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              console.log($(this).toggle($(this).text().toLowerCase().indexOf(value)))
            });
        });*/
    }

    handleClick = event => {
        event.preventDefault();
        //console.log(event.target.parentNode.attributes.href.nodeValue)
        //console.log(event.target.parentNode.outerHTML)
        // let cardID = event.target.parentNode.attributes[1].nodeValue
        // console.log(event.target)
      
        // this.setState({
        //     activeClass:cardID
        // })
        
    }

    openModal = () => {
        this.setState({
          isOpen: true
        });
        setTimeout(function(){
            $('.modal-backdrop').removeClass("modal-backdrop");
            $('.modal').css({"height": 'unset'})
            $('.container-member-card').css({"padding-top": '50px'})
         }, 100);
       
    }
       
    hideModal = () => {
        this.setState({
          isOpen: false
        });
        $('.container-member-card').css({"padding-top": 'unset'})

        this.componentDidMount();
    }
    

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    handleAddCard = event => {
       
       
    }

    handleGenerateCardsJson(e){
   
      //var items = $(data).find('item');
        let item = this.state.data
        item.map((row,key) => {
        
            console.log(row)
        //if(!isCDATA)
          //description = $(items[i]).find('description').html();
        var card_img = row.imgfront
        var othername = row.othername;
        // var logoshop = row.storelogo;
        // var minLevelText = row.oldCardName != null ? row.oldCardName : "";
        // var maxLevelText = row.cardName != null ? row.cardName : "";
        // var point        = row.point;
        // var userpoint = row.point;
        // let rangePoint = parseInt(row.cardPoint)-parseInt(row.oldCardPoint)
        // if (userpoint => row.oldCardPoint) {
        //     userpoint = userpoint-row.oldCardPoint;
        // }
        // let progresspoint = parseFloat(userpoint)*100/parseFloat(rangePoint);
        // if (progresspoint > 100) {
        //     progresspoint = 100;
        // }
        // let cardPoint = row.cardPoint;
        // let oldCardPoint = row.oldCardPoint;
        let cardUsername = row.name;
        let cardCode = row.barcode
        let otherid  =row.otherid
        let btndelete='<img src="/assets/images/delete-card.png"  class="btn-deletecard" data-id="'+otherid+'">';
        // if(storeid != 4){
        //     btndelete = '<img src="/assets/images/delete-card.png"  class="btn-deletecard" data-id="'+storeid+'">';
        // }

        // if(logoshop===''){
        //     logoshop =  '/assets/images/icon-ho-new.png';
        // }

        if(card_img==='' || card_img===null){
            card_img = '/assets/images/card_default.png';
        }
        
        


        //console.log(1)
        var html = this.generateHtml([othername,cardUsername,cardCode,otherid,btndelete,(key * -2)]);
    
        $("#cardTouch").append(html);
        this.fetchImage(card_img,key);
      })
      /*for(var i = 0; i < data.length; i++){
        
      }*/
      /* $("#cardTouch").css("max-height","600px");
      $("#cardTouch").css("margin-top","20px"); */
      
      this.arrangeCards();
    
      $("#cardTouch li").click(function(){
        $(this).find(".card-content").toggleClass("open");
        if($(this).find(".card-content").hasClass("open")){
          $("#cardTouch li").eq(this.state.activeCardTouch).find("a").removeAttr("tabindex");
        }
      })
    
      $('#cardTouch li').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
          $(this).find(".card-content").toggleClass("open");
          if($(this).find(".card-content").hasClass("open")){
            $("#cardTouch li").eq(this.state.activeCardTouch).find("a").removeAttr("tabindex");
          }
          else{
            $("#cardTouch li").eq(this.state.activeCardTouch).find("a").attr("tabindex",-1);
          }
          return false;  
        }
      });  
    
      $(".card-content a").click(function(e){
        e.stopPropagation();
      })
    }


    generateHtml(data){
        var html = `<li class="{{category}}" style="z-index:{{z-index}};" tabindex="0">
                        <div class="card-image">
                            <div class="row m-0 row-header">
                                <div class="col-3 pt-2 pr-2 pl-1">
                                    <div className="top-left-nameshop">
                                    <p class="name-shop shadow">{{othername}}</p>
                                    </div>
                                </div>
                                <div class="col-9 pt-2 pl-1">
                                    
                                </div>
                            </div>
                            <div style="height: 5rem"></div>
                            <div class="row m-0 row-bottom">
                                <div class="col-12 p-0">
                                    <div class="d-flex justify-content-between card-bottom-content">
                                        <div>
                                            <p class="card-id mb-1 bg-no-name shadow h text-left">Name : {{cardUsername}}</p>
                                            <p class="card-id bg-no-name shadow text-start h">No : {{cardCode}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row w-100 row-footer">
                            <div class="col-3">
                                <div class="bottom-left">
                                    {{btndelete}}
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="bottom-right">
                                    <img src="/assets/images/btn_enterdetailcard.png" onclick="window.location='/other_card_detail/{{otherid}}'">
                                </div>
                            </div>
                        </div>
                    </li>`;
        var before = ["{{othername}}","{{cardUsername}}","{{cardCode}}","{{otherid}}","{{btndelete}}","{{z-index}}"];
        return this.replaceText(html, before, data);
    }

    fetchImage(url, i){
        console.log(url)
        $("#cardTouch li").eq(i).css("background-image","url("+url+")");
        $("#cardTouch li").eq(i).find(".card-image").css("background-color","unset");
        /* $("#cardTouch li").eq(i).find(".card-image").css("background-image","url("+url+")");  */
        //$("#cardTouch li").eq(i).find(".card-image").css("background-color","unset"); 
        /*let img = "<img class='w-100' src='"+url+"'>";
        $("#cardTouch li").eq(i).find(".card-image").prepend(img)*/
        
    }

    checkHeight(height, i, order){
        if(height <= 740){
            $("#cardTouch li").eq(i).css("transform", "translate3d(0px,"+(order*-35) +"px,"+order*-50+"px) rotateX(0deg)");
        }
    }

    arrangeCards(){
        var order = 0;
        for (var i = this.state.activeCardTouch; i < this.state.data.length; i++){
            $("#cardTouch li").removeAttr("tabindex");
            if(this.state.data.length == 1){
              $("#cardTouch li").eq(0).css("transform", "translate3d(0px, -30px, 0px) rotateX(0deg)");
              this.checkHeight(window.innerHeight, i, order);
            }
            if(this.state.data.length == 2){
              $("#cardTouch li").eq(i).css("transform", "translate3d(0px,"+(order*-35) +"px,"+order*-50+"px) rotateX(0deg)");
              this.checkHeight(window.innerHeight, i, order);
            }
            if(this.state.data.length == 3){
              $("#cardTouch li").eq(i).css("transform", "translate3d(0px,"+(order*-30) +"px,"+order*-50+"px) rotateX(0deg)");
              this.checkHeight(window.innerHeight, i, order);
            }
            if(this.state.data.length >= 4){
               /*  $("#cardTouch li").eq(i).css("transform", "translate3d(0px, 0px, "+order*-50+"px) rotateX(0deg)"); */
              $("#cardTouch li").eq(i).css("transform", "translate3d(0px, "+(20+(order*-25))+"px, "+order*-50+"px) rotateX(0deg)");
              this.checkHeight(window.innerHeight, i, order);
            }
          order++;
          
          console.log(order)
        }
        $("#cardTouch .card-content").removeClass("open");
        $("#cardTouch li").eq(this.state.activeCardTouch).attr("tabindex",0);
        $("#cardTouch li").eq(this.state.activeCardTouch).find("a").attr("tabindex",-1);
        $("#cardTouch li").eq(this.state.activeCardTouch).find(".open a").removeAttr("tabindex");
    }


    nextCard(){
        //console.log(this.state.activeCardTouch)
        if(this.state.activeCardTouch < this.state.data.length - 1){
          $("nav button").removeAttr("disabled");
          $("#cardTouch li").eq(this.state.activeCardTouch).addClass("go-away");
          let activeCardChange = (this.state.activeCardTouch)+1;
          console.log(activeCardChange)
          this.setState({
            activeCardTouch:activeCardChange
          })
          this.arrangeCards();
      
          /* if(this.state.activeCardTouch == this.state.data.length - 1)
            $(".next-card").attr("disabled",""); */
        }
    }
      
    prevCard(){
        console.log(this.state.activeCardTouch)
        if(this.state.activeCardTouch > 0){
          $("nav button").removeAttr("disabled");
          let activeCardChange = (this.state.activeCardTouch)-1;
          console.log(activeCardChange)
          this.setState({
            activeCardTouch:activeCardChange
          })
          $("#cardTouch li").eq(activeCardChange).removeClass("go-away");
          this.arrangeCards();
      
          //if(this.state.activeCardTouch == 0)
            //$(".prev-card").attr("disabled","");
        }
    }

    handleTouchStart(evt) {
        this.setState({
            xDown : evt.touches[0].clientX,
            yDown : evt.touches[0].clientY
        })
      };                                                
      
    handleTouchMove(evt) {
        if ( ! this.state.xDown || ! this.state.yDown ) {
          return;
        }
      
        var xUp = evt.touches[0].clientX;
        var yUp = evt.touches[0].clientY;
      
        var xDiff = this.state.xDown - xUp;
        var yDiff = this.state.yDown - yUp;
      
        // เลื่อน ซ้าย-ขวา
        // if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
        //   if ( xDiff > 0 ) {
        //     this.nextCard();
        //   } else {
        //     this.prevCard();
        //   }                       
        // }

        // เลื่อน ขึ้น-ลง
        if ( Math.abs( xDiff ) < Math.abs( yDiff ) ) {
            if ( yDiff > 0 ) {
                this.prevCard();
            } else {
                this.nextCard();
            }
        }
        this.setState({
            xDown : null,
            yDown : null
        })
    };

    replaceText(original, before, after){
        if(before.constructor !== Array){
          before = [before];
          after = [after];
        }
        var result = original;
        for(var i = 0; i < before.length; i++){
            /* console.log(before[i])
            console.log(result) */
            result = result.split(before[i]).join(after[i]);
        }
        return result;
    }



    
    handleGotoShop(e,storeid){
        window.location='/shop/'+storeid
    }

    handleDeleteCard(e,otherid){
        if(e){
            e.preventDefault();
        }
        Swal.fire({
            title: 'ต้องการลบบัตรสมาชิก',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
                'ตกลง',
            focusConfirm: false,
            cancelButtonText:
                'ยกเลิก',
            }).then((result) => {
            if (result.value) {
                let user_id = sessionStorage.getItem('user_id');
                let other_id = otherid;
                let bodyFormData = new FormData();
                bodyFormData.set('user_id',user_id)
                bodyFormData.set('other_id',other_id)
                axios({
                    method: 'post',
                    url: config.api.base_url+'/admin_othercard/api/removeothercard',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        window.location = '/user_card_offline';
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถลบข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                });
            }
        })
    }


    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-yellow').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    displayCardAmount(){
        return this.state.data.length;
    }

    render() {

        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/card.css?v=3"/>

                <Modal visible={this.state.isOpen} onClickBackdrop={this.hideModal} className="custom-modal">
                    
                    <div className="modal-body">
                        <div className="form-group">
                            <label>ค้นหาบัตรสมาชิก</label>
                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCard}/>
                        </div>
                        <div className="w-100 text-right">
                            <button className="btn btn-primary" onClick={this.hideModal}>ปิด</button>
                        </div>
                    </div>
                    
                </Modal>
                
                    <div className="container-mobile m-auto ">
                        <div className="head bg-yellow shadow user-head">
            {
                this.state.activeSearchBox ? (
                    <div className="row w-100 h-100">
                             
                        <div className=" h-100 p-3 pt-4 pl-5 w-100">
                            <div className="icon-back">
                                <a onClick={this.handleBackPage}>
                                        <div className="icon rounded-circle shadow">
                                            <img src="/assets/images/back.svg" alt=""/>
                                        </div>
                                </a>
                            </div>
                            <div className=" h-100 pl-3">
                            <div className="input-group search-group mb-3">
                                <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterCard}/>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                </div>
                            </div>
                                
                            </div>
                                
                        </div>
                    </div>
                ) : (
                    <div className="row w-100 h-100">
                                <div className="col-8 h-100">
                                    <div className=" h-100 p-3 ml-20">
                                        <h1 className="m-0 h"> <span className="text-blue">CA</span>RD</h1> 
                                        <img className="head-logo pr-0" src="/assets/images/icon-ho-new.png" alt=""/>
                                        <p style={{whiteSpace: 'nowrap'}} >บัตรสมาชิกต่างๆของคุณ</p>
                                    </div>
                                </div>
                                <div className="col-4 h-100">
                                    <div className="row float-right h-100 w-100">
                                        <div className="col p-2 my-auto">
                                            <div className="icon icon-top-right rounded-circle" onClick={this.openSearchBox}>
                                                <img src="/assets/images/icon-search@2x.png" alt=""/>
                                            </div>
                                        </div>
                                        <div className="col p-2 my-auto">
                                            <div className="icon icon-top-right rounded-circle">
                                                <a href="/notifications">
                                                    <img src="/assets/images/icon-noti@2x.png" alt=""/>
                                                    <Notification_num/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                    </div>
                )
            }

                    </div>

                    <div className="my-card mt-3 div-mycard" >
                            <div className="row w-100 m-0">
                                <div className="col-6 px-2">
                                    <a href="/user_card">
                                    <div id="app-card"  className="btn-card text-center shadow not-active">
                                        <h3>ONLINE CARD</h3>
                                        <h5>ร้านประจำของคุณ</h5>
                                    </div>
                                    </a>
                                </div> 
                                <div className="col-6 px-2">
                                    <div id="other-card" className="btn-card text-center shadow">
                                        <h3>OFFLINE CARD</h3>
                                        <h5>เลือกเก็บบัตรอื่นของคุณ</h5>
                                    </div>
                                </div>
                            </div>
                    
                    

                {this.state.data.length==0 ? (
                    <div className="phone text-center position-relative user-card-container bg-white user-card-background">
                            <div className="w-25 mx-auto mt-3">
                                <div className="phone-relative position-relative">
                                    <img className="w-100 phone1" src="/assets/images/phone1.svg" alt=""/>
                                    <img className=" phone2" src="/assets/images/phone2.svg" alt=""/>
                                    <img className="w-100 phone3" src="/assets/images/phone3.svg" alt=""/>
                                </div>

                            </div>
                    {
                        this.state.activeSearchBox ? (
                            <h2 className="">
                                ไม่พบข้อมูลบัตรที่คุณค้นหา <br/>
                                หรือเพิ่มบัตรอื่นๆกันเลย
                            </h2>
                        ) : (
                            <h2 className="">
                                เพิ่ม แอปร้านประจำ <br/>
                                หรือบัตรอื่นๆกันเลย
                            </h2>
                        )
                    }
                            

                    </div>
                ) : (
            
                        <div className="text-center px-4 pb-4 pt-1 w-100 pb-200 position-relative user-card-container user-card-new">
                            <h3 className="text-left">บัตรของคุณ ({this.displayCardAmount()})</h3>
                            <ul id="cardTouch">
                            


                           
                            {/* {
                                this.state.data.map((row,key) => (
                                    //let zNumber = key * -2;
                                    <>
                                    <li className={'fff'} key={key} style={{zIndex:(key * -2)}}>
                                        <div className="card-image">
                                        </div>
                                    
                                    </li>
                                    </>
                                ))
                            } */}

                            </ul>
                            {/* <p>{this.state.activeCardTouch}</p> */}
                            <nav className="cardtouch">
                                {/* <button class="prev-card"  onClick={this.prevCard}><i class="fas fa-arrow-left"></i></button> */}
                                {/* <button class="next-card" onClick={this.nextCard}><i class="fas fa-arrow-right"></i></button> */}
                                {/* ซ่อนปุ่มซ้ายขวา */}
                                {/* <button class="prev-card" id="prev-card"><i class="fas fa-arrow-left"></i></button>
                                <button class="next-card" id="next-card"><i class="fas fa-arrow-right"></i></button> */}
                            </nav>
                               
                          
                    
                        
                        </div>
                )}
                </div>
                        
                        <div className="add addFront">
                            {/* <Link  to={'/app_card?v='+ new Date().getTime()} className="btn-add rounded-circle text-center shadow">
                                <img className="w-75 mt-2" src="/assets/images/add.svg" alt=""/>
                            </Link> */}
                            <a  href={config.base_url+'/other_card?openExternalBrowser=1&token='+btoa(this.state.user_id+','+time())} target="_BLANK" className="btn-add rounded-circle text-center shadow">
                                <img className="w-75 mt-2" src="/assets/images/add.svg" alt=""/>
                            </a>
                        
                        </div>


                        <Navigation active={'card'}/>
                    </div>  
                    {/* <!-- container --> */}  

    

                  
            </div>
        )
    }
}
export default User_card_offline;