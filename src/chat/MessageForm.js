import React, { Component } from 'react'

export default class MessageForm extends Component {
    state = {
        text : ''
    }

    onSubmit = e => {
        e.preventDefault();
    }

    onChange = e => {
        this.setState({ text : e.target.value})
    }
    render() {
        return (
            <div>
                <input type="text" value={this.state.text} onChange={this.onChange}/>
                <button type="button">SEND</button>
            </div>
        )
    }
}

//export default MessageForm;
