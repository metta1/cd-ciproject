import React, { Component } from 'react'
import socketIOClient from 'socket.io-client'

export default class Chatroom extends Component {
    state = {
        socket:null,
        count:0
    }
    componentDidMount(){
        const socket = socketIOClient('localhost:8080')
        this.setState({ socket:socket})
        //socket.emit("emit", {data:"test"});
        /* socket.on("count", function(data){
            this.setState({count:data.count})
            //console.log(data)
        }) */
        socket.on("count", data => this.handleCount(data));
    }

    handleCount = data => {
        this.setState({count:data.count})
    }
    onClick = () =>{
        let {socket,count} = this.state;
        count = count + 1;
        this.setState({count : count})
        socket.emit("emit", {count :count});
    }
    render() {
        return (
            <div>
                chatroom
                <div>{this.state.count}</div>
                <button onClick={this.onClick}>count</button>
            </div>
        )
    }
}
