import React, { Component } from 'react'
import Message from './Message'

export default class MessageList extends Component {
    render() {
        const {messages} = this.props;
        console.log(messages)
        return messages.map(message => (
                <ul>
                <Message message={message}/>
               
                </ul>
        ));
    }
}
//export default MessageList;