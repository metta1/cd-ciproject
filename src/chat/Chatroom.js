import React, { Component } from 'react'
import socketIOClient from 'socket.io-client'
import MessageList from './MessageList'
import MessageForm from './MessageForm'

export default class Chatroom extends Component {
    state = {
        messages:[
            {text: 'Hi', member:"ron"},
            {text: 'Hi', member:"HO.APP"}
        ]
     
    }

    onMessageSend = (message) =>{
        this.setState({messages:[...this.state.messages,{...message}]})
    }
    
    render() {
        return (
            <div>
               <MessageList messages={this.state.messages}/>
               <MessageForm onMessageSend={this.onMessageSend}/>
            </div>
        )
    }
}

//export default Chatroom;
