import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <>
                
                <div className="footer mx-auto">
                    <img className="w-100 position-relative" src="/assets/images/footer-home.svg" alt=""/>
                                <div className="menu position-absolute w-100">
                                    <div className="menu-item px-2 pt-2 ">
                                       
                                                <a href={'/user_card?v='+ new Date().getTime()}>
                                                    <img className="" src="/assets/images/icon-credit.svg" alt=""/> <br/>
                                                    <span>CARD</span>
                                                </a>
                                           
                                        
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                      
                                        <a href={'/feed?v='+ new Date().getTime()}>
                                            <img className="position-relative" src="/assets/images/icon-feed.svg" alt=""/><br/>
                                            <span>Feed</span>
                                        </a>
                                         
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <div className="ml-2 menu-item-active bg-white rounded-circle shadow">
                                            <div className="menu-item-active-ab">
                                                <a href={'/home?v='+ new Date().getTime()}>
                                                    <img className="" src="/assets/images/icon-home.svg" alt=""/><br/>
                                                    <span>HOME</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="menu-item px-2 ">
                                        <a href={'/promotion_category?v='+ new Date().getTime()}>
                                            <img className="" src="/assets/images/icon-gift.svg" alt=""/> <br/>
                                            <span>DEAL</span>
                                        </a>
                    
                                    </div>
                                    <div className="menu-item px-2 pt-2">
                                        <a href={'/account?v='+ new Date().getTime()}>
                                            <img className="position-relative" src="/assets/images/icon-user.svg" alt=""/> <br/>
                                            <span>ACCOUNT</span>
                                        </a>

                                    </div>
                                </div>

                </div>
        
            </>
        )
    
        

    }
}
export default Header;
