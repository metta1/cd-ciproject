import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <>
                <link rel="stylesheet" type="text/css" href="/assets/css/new-home.css?v=3" />
                <div className="text-center head2 bg-yellow">
                    <img className="head2-logo" src="/assets/images/icon-ho.png" alt="" />
                </div>
                <div className="row p-2 mt-1 w-100 h-100 ">
                    <div className="col-9 ml-1">
                        <div className="position-relative">
                            <input type="text" className="search w-100 h-50 rounded form-control shadow" placeholder="Search" />
                            <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" />
                        </div>
                    </div>
                    <div className="col-3 row head2-icon">
                        <div className="head2-menu">
                            <div className="icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block mr-2">
                                <a href="/maps">
                                    <img className="" src="/assets/images/icon-map.png" alt="..." />
                                </a>
                            </div>
                            <div className="icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block mr-2">
                                <a href="">
                                    <img className="" src="/assets/images/icon-scan.svg" alt="..." />
                                </a>
                            </div>
                            <div className="icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block">
                                <a href="">
                                    <img class="" src="/assets/images/icon-noti.svg" alt="..." />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </>)



    }
}
export default Header;
