import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'
import {config} from '../config';
import NumberFormat from 'react-number-format';
import $ from 'jquery'
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import moment from 'moment';
import 'moment/locale/th';
import DateCountdown from 'react-date-countdown-timer';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import Swal from 'sweetalert2'
import {Navigation,Notification_num} from '../template'
import {getParam} from '../lib/Helper';
import ContentLoader, { Facebook as FacebookLoader } from 'react-content-loader'
import { LazyLoadImage } from 'react-lazy-load-image-component';
 

const MyFacebookLoader = () => <FacebookLoader />
const MyLoader = () => (
    <ContentLoader viewBox="0 0 380 70">
      {/* Only SVG shapes */}    
      <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
      <rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
      <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
    </ContentLoader>
  )

const cardLoader = (
    <>
    <div className="home-card">
        <div className="row w-100 mt-2 ml-0">
            <div className="col-12 text-head px-3 mt-2">
                <h4 className="m-0">เพิ่มบัตรสมาชิก</h4>
                <p>บัตรสมาชิกที่ HO แนะนำมาให้คุณ</p>
                </div>
            </div>
            <div className="col-7 p-0 pl-3 d-flex mb-3 mr-3" >
                <div className="position-relative" style={{width:'100%',height:'150px'}}>
                <ContentLoader
                    
                    width={'100%'}
                    speed={1}
                    viewBox="0 0 380 70"
                >
                    {/* Only SVG shapes */}
                    <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
                    <rect x="80" y="0" rx="4" ry="4" width="300" height="13" />
                    <rect x="80" y="20" rx="3" ry="3" width="90%" height="8" />
                    <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
                    <rect x="80" y="60" rx="3" ry="3" width="100%" height="12" />
                    <rect x="80" y="80" rx="3" ry="3" width="250" height="10" />
                    <rect x="0" y="120" rx="5" ry="5" width="70" height="70" />
                    <rect x="80" y="100" rx="4" ry="4" width="300" height="13" />
                    <rect x="80" y="120" rx="3" ry="3" width="90%" height="8" />
                    <rect x="80" y="140" rx="3" ry="3" width="250" height="10" />
                    <rect x="80" y="160" rx="3" ry="3" width="100%" height="12" />
                    <rect x="80" y="180" rx="3" ry="3" width="250" height="10" />
                    
                </ContentLoader>
            
                                            
                </div>
                                        
        </div>
    </div>
    </>
)

const specialpointLoader = (
    <>
    <div className="score shadow pl-3 py-2 mt-3">
        <div className="d-flex bd-highlight mb-6">
                            <div className="px-0 py-0  bd-highlight">
                                <h4 className="m-0">รับคะแนนพิเศษ</h4>
                                <span className="text-grey">เพิ่มร้านค้าเพื่อรับคะแนนพิเศษเลย</span>
                            </div>
                            <div className="ml-auto px-2 pr-3 bd-highlight ">
                                <a className="text-blue-more" href="/point">
                                    <u> เพิ่มเติม </u>
                                </a>
                            </div>
        </div>
        <div className="row pl-3 pb-2  w-100">
                <ContentLoader
                    
                    width={'100%'}
                    speed={1}
                    viewBox="0 0 380 70"
                >
                    {/* Only SVG shapes */}
                    <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
                    <rect x="80" y="0" rx="4" ry="4" width="300" height="13" />
                    <rect x="80" y="20" rx="3" ry="3" width="90%" height="8" />
                    <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
                    <rect x="80" y="60" rx="3" ry="3" width="100%" height="12" />
                    <rect x="80" y="80" rx="3" ry="3" width="250" height="10" />
                    <rect x="0" y="120" rx="5" ry="5" width="70" height="70" />
                    <rect x="80" y="100" rx="4" ry="4" width="300" height="13" />
                    <rect x="80" y="120" rx="3" ry="3" width="90%" height="8" />
                    <rect x="80" y="140" rx="3" ry="3" width="250" height="10" />
                    <rect x="80" y="160" rx="3" ry="3" width="100%" height="12" />
                    <rect x="80" y="180" rx="3" ry="3" width="250" height="10" />
                    
                </ContentLoader>
        </div>
    </div>
   
    </>
)

const productPromotionLoader = (
    <div class="product product-promotion">
                        <div className="text-head px-3 mt-4">
                            <h4 className="m-0 d-inline">สินค้าโปรโมชั่น</h4> 
                            <div className="ml-auto d-inline float-right">
                                <a className="text-blue-more" href="/productpromotion">
                                    <u> เพิ่มเติม </u>
                                </a>
                            </div>
                            <p>สินค้าโปรโมชั่นพิเศษ HO จัดมาให้คุณ</p>
                        </div>
                        <div className="row m-0 mt-2 w-100">
                        <ContentLoader
                    
                            width={'100%'}
                            speed={1}
                            viewBox="0 0 380 70"
                        >
                            {/* Only SVG shapes */}
                            <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
                            <rect x="80" y="0" rx="4" ry="4" width="300" height="13" />
                            <rect x="80" y="20" rx="3" ry="3" width="90%" height="8" />
                            <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
                            <rect x="80" y="60" rx="3" ry="3" width="100%" height="12" />
                            <rect x="80" y="80" rx="3" ry="3" width="250" height="10" />
                            <rect x="0" y="120" rx="5" ry="5" width="70" height="70" />
                            <rect x="80" y="100" rx="4" ry="4" width="300" height="13" />
                            <rect x="80" y="120" rx="3" ry="3" width="90%" height="8" />
                            <rect x="80" y="140" rx="3" ry="3" width="250" height="10" />
                            <rect x="80" y="160" rx="3" ry="3" width="100%" height="12" />
                            <rect x="80" y="180" rx="3" ry="3" width="250" height="10" />
                            
                        </ContentLoader>
                        </div>
                    </div>
)

const storeLoader = (
    <div className="score shadow pl-3 py-2 mt-3 mb-20">
                    <div className="d-flex bd-highlight ">
                        <div className="px-0 py-0  bd-highlight">
                            <h4 className="m-0">ร้านค้าแนะนำ</h4>
                            <span className="text-grey">ร้านค้าที่ HO ให้คุณ</span>
                        </div>
                        <div className="ml-auto px-2 pr-3 bd-highlight ">
                            <a className="text-blue-more" href="/recommendstore">
                                <u> เพิ่มเติม </u>
                            </a>
                        </div>
                    </div>
                    <div className="row ml-0 pb-2  w-100">
                        <ContentLoader
                        
                        width={'100%'}
                        speed={1}
                        viewBox="0 0 380 70"
                    >
                        {/* Only SVG shapes */}
                        <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
                        <rect x="80" y="0" rx="4" ry="4" width="300" height="13" />
                        <rect x="80" y="20" rx="3" ry="3" width="90%" height="8" />
                        <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
                        <rect x="80" y="60" rx="3" ry="3" width="100%" height="12" />
                        <rect x="80" y="80" rx="3" ry="3" width="250" height="10" />
                        <rect x="0" y="120" rx="5" ry="5" width="70" height="70" />
                        <rect x="80" y="100" rx="4" ry="4" width="300" height="13" />
                        <rect x="80" y="120" rx="3" ry="3" width="90%" height="8" />
                        <rect x="80" y="140" rx="3" ry="3" width="250" height="10" />
                        <rect x="80" y="160" rx="3" ry="3" width="100%" height="12" />
                        <rect x="80" y="180" rx="3" ry="3" width="250" height="10" />
                        
                    </ContentLoader>
                    </div>
    </div>
)


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();
const nowDate = new Date().getTime();




class Home extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        let site = getParam('site')
        if(site==='demo'){
            sessionStorage.setItem('user_id',146) 
        } 


        let user_id = sessionStorage.getItem('user_id');
        if(user_id==null){
            window.location='/auth';
        }
        localStorage.setItem('storageStoreID','');
        localStorage.setItem('isFromLogin','N');

       
        this.handleClick = this.handleClick.bind(this);
        this.addCard = this.addCard.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    state = {
        userid:sessionStorage.getItem('user_id'),
        couponData : [],
        specialpointData : [],
        feedData : [],
        promotionData : [],
        eventDataData : [],
        newsData: [],
        cardData: [],
        cardCount: 0,
        cardDataLoaded: false,
        productPromotionData : [],
        storeData : [],
        rewardData : [],
        inputsearch : '',
        store_id:'',
        defaultBanner: ''
    };
    
    async componentDidMount(){
        //console.log(sessionStorage.getItem('user_id'))
        /* window.addEventListener('load', this.handleCountDate(1519026163000)); */

       

        const couponRs = await axios.get(config.api.base_url+'/store_coupon/api/allusercoupon/'+sessionStorage.getItem('user_id')+'?limit=6')
        if(couponRs.status===200){
                this.setState({couponData: couponRs.data});
        }
        console.log(this.state.couponData)

        // const specialpointRs = await axios.get(config.api.base_url+'/store/api/store')
        // if(specialpointRs.status===200){
        //         this.setState({specialpointData: specialpointRs.data});
        // }
        //console.log(this.state.specialpointData)
        const specialpointRs = await axios.get(config.api.base_url+'/store_card/api/card/'+sessionStorage.getItem('user_id')+'/add')
        if(specialpointRs.status===200){
            this.setState({
                specialpointData: specialpointRs.data,
                specialpointDataLoaded : true
            });
        }else{
            this.setState({
                specialpointDataLoaded : true
            })
        }

        this.setState({
            specialpointDataLoaded : true
        })

        const feedRs = await axios.get(config.api.base_url+'/store_feed/api/feed')
        if(feedRs.status===200){
                this.setState({feedData: feedRs.data});
        }
        // console.log(this.state.feedData)

        const promotionRs = await axios.get(config.api.base_url+'/admin_promotion/api/promotion')
        if(promotionRs.status===200){
                this.setState({
                    promotionData: promotionRs.data,
                    promotionDataLoaded : true
                });
        }else{
            this.setState({
                promotionDataLoaded : true
            })
        }

        // // Product promotion
        const resultProductPromo = await axios.get(config.api.base_url+'/store_product/api/product_promotion')
        if(resultProductPromo.status===200){
            this.setState({productPromotionData: resultProductPromo.data});
        }

        const eventRs = await axios.get(config.api.base_url+'/admin_event/api/event')
        if(eventRs.status===200){
                this.setState({eventData: eventRs.data});
        }
        
        const newsRs = await axios.get(config.api.base_url+'/admin_news/api/news')
        if(newsRs.status===200){
                this.setState({newsData: newsRs.data});
        }

        let user_id = sessionStorage.getItem('user_id');
        
        const cardRs = await axios.get(config.api.base_url+'/api/card/'+user_id+'/add')
        if(cardRs.status===200){
            this.setState({cardData: cardRs.data});
            this.setState({cardCount: cardRs.data.length});
            this.setState({defaultBanner: '/assets/images/Hoapp-banner.png'});
            this.setState({
                cardDataLoaded : true
            })
        }else{
            this.setState({
                cardDataLoaded : true
            })
        }
        console.log("cardData:",this.state.cardData)

        const storeRs = await axios.get(config.api.base_url+'/store/api/allstore')
        if(storeRs.status===200){
                this.setState({
                    storeData: storeRs.data,
                    storeDataLoaded : true
                });
        }else{
            this.setState({
                storeDataLoaded : true
            })
        }

        const rewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id')+'&limit=6')
        if(rewardRs.status===200){
            this.setState({rewardData: rewardRs.data});
        }
        console.log(this.state.rewardData)
            
        $('.btn-getcoupon, .btn-usecoupon').click(function(){
            let key = $(this).data('primary');
            let storeid = $(this).data('storeid');
            let couponid = $(this).data('couponid');
            let coupontype = $(this).data('coupontype');

            if (coupontype === 1) {
                $(this).css({'backgroundColor':'#10A504'}).html('<h5 class="m-0">ใช้คูปอง</h5>').prop('disabled',true).clone();
                $('body').addClass('modal-open')
                $('<div class="modal-backdrop fade show"></div>').appendTo('body');
                $('#couponModal-'+key).css({'display':'block'})
            } else {
                localStorage.setItem('storageStoreID',storeid);
                window.location = "/coupon_detail/"+couponid;
            }
        })

        $('button[data-dismiss="modal"').click(function(){
            let key = $(this).data('key');
            $('#couponModal-'+key).css({'display':'none'})
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
            window.location.reload();
        })
        


       
      
        
    }

    handleClickScanQRCode = e => {
        if(window.navigator.userAgent.indexOf("Mac")!= -1){//MAC
            window.open(config.base_url+'/qrcodescanner?openExternalBrowser=1', '_blank');
        }else{
            window.location='line://app/1654185466-GlmD0jdm';
        }
    }
   


    handleCountDate = date => event => {
        
        setInterval(function(){ 
            

        
        this.state = {startDate:1519026163000, timeEnd:1519126755000} // example

        const startDate = moment(this.state.startDate);
        const timeEnd = moment(this.state.timeEnd);
        const diff = timeEnd.diff(startDate);
        const diffDuration = moment.duration(diff);
        
        console.log("Total Duration in millis:", diffDuration.asMilliseconds());
        console.log("Days:", diffDuration.days());
        console.log("Hours:", diffDuration.hours());
        console.log("Minutes:", diffDuration.minutes());
        console.log("Seconds:", diffDuration.seconds());
        },1000)
                
    }
  

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }

    getCoupon(event,e) {
        const couponid = e.couponid;
        const storeid = e.storeid;
        const userid = sessionStorage.getItem('user_id');

        if (e.coupontype==="1") {
            const data = new FormData();
            data.set('couponid', couponid);
            data.set('storeid', storeid);
            data.set('userid', userid);
            axios({
                method: 'post',
                url: config.api.base_url+'/store_coupon/api/coupon',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                console.log(response.data)
            })
            .catch(function (response) {
                    //handle error
                    //console.log(response);
            });
        }
    }

    useCoupon(e,data) {
        e.preventDefault();
        localStorage.setItem('storageStoreID',data.storeid);
        if (data.coupontype==="0") {
            window.location = "/coupon_detail/"+data.couponid;
        } else {
            window.location = "/shop/"+data.storeid;
        }
        /*Swal.fire({
            title: 'ต้องการไปที่ร้านค้าหรือไม่',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ตกลง',
            focusConfirm: false,
            cancelButtonText:
              'ยกเลิก',
          }).then((result) => {
            if (result.value) {
                localStorage.setItem('storageStoreID',data.storeid);
                if (data.coupontype==="0") {
                    window.location = "/coupon_detail/"+data.couponid;
                } else {
                    window.location = "/shop/"+data.storeid;
                }
            }
        })*/
    }

    async handleClick(e,storeid,coin) {
        e.preventDefault();
        const checkUserMember = await axios.get(config.api.base_url+'/store_card/api/checkcardmember/'+storeid+'/'+sessionStorage.getItem('user_id'))
        if(checkUserMember.status===200){
            if (checkUserMember.data === 1){
                window.location = '/shop/'+storeid;
            } else {
                this.addCard(storeid,coin);
            }
        }
    }

    async toProductDetail(e,storeid,productid) {
        const checkUserMember = await axios.get(config.api.base_url+'/store_card/api/checkcardmember/'+storeid+'/'+sessionStorage.getItem('user_id'))
        if(checkUserMember.status===200){
            console.log(checkUserMember)
            if (checkUserMember.data === 1){
                window.location = '/product/'+storeid+'/'+productid;
            } else {
                Swal.fire({
                    title: 'ขออภัย',
                    type:'info',
                    html : 'คุณยังไม่ได้เป็นสมาชิกของร้าน<br>ไปเพิ่มบัตรสมาชิก ?',
                    showCloseButton: true,
                    showCancelButton: true,
                    reverseButtons :true,
                    confirmButtonText:
                      'ตกลง',
                    focusConfirm: false,
                    cancelButtonText:
                      'ยกเลิก',
                  }).then((result) => {
                    if (result.value) {
                        window.location = '/app_card';
                    }
                })
            }
        }
        
    }

    addCard(storeid,coin) {
        Swal.fire({
            title: 'ต้องการเพิ่มบัตรใช่หรือไม่',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ตกลง',
            focusConfirm: false,
            cancelButtonText:
              'ยกเลิก',
            onOpen: () => document.activeElement.blur()
          }).then((result) => {
            if (result.value) {
                let user_id = sessionStorage.getItem('user_id');
                let store_id = storeid;
                let user = {
                    userid : user_id,
                    storeid : store_id
                }
                let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('storeid',store_id)
                if (coin != undefined) {
                    bodyFormData.set('coin',coin)
                }
                axios({
                    method: 'post',
                    url: config.api.base_url+'/api/card',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        $('.member-card[data-store_id="'+store_id+'"').hide();
                        window.location = '/shop/'+store_id;
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง',
                            onOpen: () => Swal.getConfirmButton().focus()
                        })
                        
                        //handle error
                        //console.log(response);
                    });
            }
        })
    }

    async handleSearch(e){
        e.preventDefault();

        let user_id = sessionStorage.getItem('user_id');
        // let keyWord = this.state.inputsearch;
        let keyWord = e.target.value

        const searchCardRs = await axios.get(config.api.base_url+'/api/card/'+user_id+'/add?keywords='+keyWord)
        if(searchCardRs.status===200){
            this.setState({cardData: searchCardRs.data});
        } else {
            this.setState({cardData: []});
        }

        // const searchSpecialpointRs = await axios.get(config.api.base_url+'/store/api/store?keywords='+keyWord)
        const searchSpecialpointRs = await axios.get(config.api.base_url+'/store_card/api/card/'+sessionStorage.getItem('user_id')+'/add?keywords='+keyWord)
        if(searchSpecialpointRs.status===200){
                this.setState({specialpointData: searchSpecialpointRs.data});
        } else {
            this.setState({specialpointData: []});
        }

        const searchResultProductPromo = await axios.get(config.api.base_url+'/store_product/api/product_promotion?keywords='+keyWord)
        if(searchResultProductPromo.status===200){
            this.setState({productPromotionData: searchResultProductPromo.data});
        } else {
            this.setState({productPromotionData: []});
        }

        const searchCouponRs = await axios.get(config.api.base_url+'/store_coupon/api/allusercoupon/'+sessionStorage.getItem('user_id')+"?keyword="+keyWord+"&limit=20")
        console.log(searchCouponRs)
        if(searchCouponRs.status===200){
                this.setState({couponData: searchCouponRs.data});
        } else {
            this.setState({couponData: []});
        }

        const searchStoreRs = await axios.get(config.api.base_url+'/store/api/store?keywords='+keyWord)
        if(searchStoreRs.status===200){
                this.setState({storeData: searchStoreRs.data});
        } else {
            this.setState({storeData: []});
        }

        const searchRewardRs = await axios.get(config.api.base_url+'/store_reward/api/reward?user_id='+sessionStorage.getItem('user_id')+'&keywords='+keyWord)
        if(searchRewardRs.status===200){
            this.setState({rewardData: searchRewardRs.data});
        } else {
            this.setState({rewardData: []});
        }
    }

    handleChangeReward = (e,itemid,storeid) => {
        e.preventDefault();
        localStorage.setItem('storageStoreID',storeid);
        window.location='/reward_detail/'+itemid;
    }

    async couponDetail(e,data) {
        e.preventDefault();
        
        if (data.coupontype==="0" && data.couponused==="1") {
            console.log(data);
            localStorage.setItem('storageStoreID',data.storeid);
            window.location = "/coupon_detail/"+data.couponid;
        }
    }

    render(props) {
       
        const useCouponButton = {
            backgroundColor: '#12a508'
        };
        const usedCouponButton = {
            backgroundColor: '#d0d0d0'
        };
        const noFocusBorder = {
            boxShadow:'none',
            border:0
        };
    
        return (
            <div className="container-mobile m-auto">
                {/* <Header/> */}
                <link rel="stylesheet" type="text/css" href="/assets/css/new-home.css?v=3" />
                <div class="head new-head" >
                <div className="text-center head2 bg-yellow">
                    <img className="head2-logo" src="/assets/images/icon-ho-new.png" alt="" />
                </div>
                <div className="row p-2 mt-1 w-100 h-100 ">
                    <div className="col-9 ml-1">
                        <div className="position-relative">
                            <input type="text" style={noFocusBorder} className="search w-100 h-50 rounded form-control shadow" name="inputsearch" placeholder="Search" onKeyUp={this.handleSearch}/>
                            <img className="icon-search" src="/assets/images/icon-search-gray.svg" alt="" /*onClick={this.handleSearch}*//>
                        </div>
                    </div>
                    <div className="col-3 row head2-icon">
                        <div className="head2-menu">
                            <div className="icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block mr-2">
                                <a href="/maps">
                                    <img className="" src="/assets/images/icon-map.png" alt="..." />
                                </a>
                            </div>
                            <div className="icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block mr-2">
                                <a onClick={this.handleClickScanQRCode}>
                                    <img className="" src="/assets/images/icon-scan.svg" alt="..." />
                                </a>
                            </div>
                            <div className="position-relative icon-head bg-white rounded-circle shadow-yellow text-center d-inline-block">
                                <a href="/notifications">
                                    <img class="" src="/assets/images/icon-noti.svg" alt="..." />
                                    <Notification_num/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                { (this.state.cardData === undefined || this.state.cardCount === 0) ? (
                    (!this.state.cardDataLoaded) &&(
                        cardLoader
                    )
                   
                ) : (
                    <div className="home-card">
                        <div className="row w-100 mt-2 ml-0">
                            <div className="col-12 text-head px-3 mt-2">
                                <h4 className="m-0">เพิ่มบัตรสมาชิก</h4>
                                <p>บัตรสมาชิกที่ HO แนะนำมาให้คุณ</p>
                            </div>
                        </div>
                        <div className="card-section mt-0">
                            <div className="row w-100 m-0">
                                {this.state.cardData.map((row,key) => (
                                    <div className="col-7 p-0 pl-3 d-flex mb-3 mr-3" key={key}>
                                        <div className="position-relative">
                                            {
                                                <LazyLoadImage
                                                    effect="blur"
                                                    src={row.card_img==='' ? '/assets/images/card_default.png' : row.card_img} 
                                                    alt={row.storename}
                                                    class={'mr-3 bor ml-8 shadow '}
                                                />
                                            }
                                           
                                            <div className="row w-100 card-header-home">
                                                <div className="col-3">
                                                    {
                                                        <LazyLoadImage
                                                        alt={"Logo shop"}
                                                        effect="blur"
                                                        src={row.storelogo != "" ? row.storelogo : "/assets/images/icon-ho-new.png"} 
                                                        class={'top-left ml-2 mt-2 p-1 shadow '}
                                                        />
                                                    }
                                                    
                                                </div>
                                                <div className="col-9 pl-0">
                                                    <div className="top-left-nameshop mt-2 pt-1">
                                                        <p className="name-shop shadow">{row.storename}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div  data-store_id={row.storeid} className="card-add shadow btnAddCard">
                                            <a href="#" onClick={(e) => {this.handleClick(e, row.storeid, row.storecoin)}}>
                                                <img  src="/assets/images/add.svg" alt="" className=""/>
                                            </a>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                )}
                {this.state.specialpointData === undefined || this.state.specialpointData.length === 0 ? (
                    (!this.state.specialpointDataLoaded) &&(
                        specialpointLoader
                    )
                ) : (
                    <div className="score shadow pl-3 py-2 mt-3">
                        <div className="d-flex bd-highlight mb-6">
                            <div className="px-0 py-0  bd-highlight">
                                <h4 className="m-0">รับคะแนนพิเศษ</h4>
                                <span className="text-grey">เพิ่มร้านค้าเพื่อรับคะแนนพิเศษเลย</span>
                            </div>
                            <div className="ml-auto px-2 pr-3 bd-highlight ">
                                <a className="text-blue-more" href="/point">
                                    <u> เพิ่มเติม </u>
                                </a>
                            </div>
                        </div>
                        <div className="row pl-3 pb-2  w-100">
                            {this.state.specialpointData.map((row, key) => (
                                <div className="col-2 p-1 mr-10" key={key}>
                                    <a href="#" onClick={(e) => {this.handleClick(e, row.storeid,row.storecoin)}}>
                                        <div className="score-item bg-white rounded-circle shadow">
                                            {(row.store_img) ? 
                                                <img className="position-absolute w-100 my-auto store-logo-icon" src={row.store_img} alt=""/>
                                            :
                                                <img className="position-absolute w-100 my-auto store-logo-icon" src="/assets/images/icon-ho-new.png" alt=""/>
                                            }
                                        </div>
                                        <span className="position-absolute score-item-num rounded-circle" style={{paddingTop: '0.4rem'}}>
                                            +{row.storecoin}
                                        </span>
                                    </a>
                                </div>
                            ))}
                        </div>
                    </div>
                )}
                {/* Promotion */}
                { this.state.productPromotionData === undefined || this.state.productPromotionData.length === 0 ? (
                    (!this.state.productPromotionDataLoaded) &&(
                        productPromotionLoader
                    )
                ):(
                    <div class="product product-promotion">
                        <div className="text-head px-3 mt-4">
                            <h4 className="m-0 d-inline">สินค้าโปรโมชั่น</h4> 
                            <div className="ml-auto d-inline float-right">
                                <a className="text-blue-more" href="/productpromotion">
                                    <u> เพิ่มเติม </u>
                                </a>
                            </div>
                            <p>สินค้าโปรโมชั่นพิเศษ HO จัดมาให้คุณ</p>
                        </div>
                        <div className="row m-0 mt-2 w-100">
                        { this.state.productPromotionData.map((row,key) => (
                            <div className="col-5 pl-0 mb-3 ml-2" key={key}>
                                <div className="product-item shadow h-100 d-flex align-items-end flex-column">
                                    {
                                        row.point=== undefined || row.point=== '' || row.point=== null ? (
                                            <></>
                                        ):(
                                            <div className="flag">
                                                <img src="/assets/images/icon-flag.svg" alt=""/>
                                                <div className="flag-detail text-center p-1">
                                                    <h5 className="m-0">+<NumberFormat value={row.point} displayType={'text'} thousandSeparator={true}/></h5>
                                                    <p>POINTS</p>
                                                </div>
                                            </div>
                                        )
                                    }
                                    <div className="text-center w-100 product-img ">
                                        {
                                            <LazyLoadImage
                                                effect="blur"
                                                src={row.product_img===''? '' : row.product_img} 
                                                class={'img_thumb'}
                                            />
                                        }
                                        
                                    </div>
                                    <div className="product-item-detail px-3 w-100 text-left">
                                        <h4 className="m-0">{row.productname}</h4>
                                        <p className="m-0">{row.catname}</p>
                                    </div>
                                    <div className="d-flex justify-content-between pb-3 w-100 px-3 mt-auto">
                                        <div className="mt-auto">
                                            {
                                                row.pricebeforediscount==='' || row.pricebeforediscount === undefined || row.pricebeforediscount=== null || row.pricebeforediscount=== '0' ? (
                                                    <small className="d-block"></small>
                                                ):(
                                                    <small className="d-block"><del><NumberFormat value={row.pricebeforediscount} displayType={'text'} thousandSeparator={true}/> THB</del></small>
                                                )
                                            }
                                            <span className="fw-600"><NumberFormat value={row.price} displayType={'text'} thousandSeparator={true}/> THB</span>
                                        </div>
                                        <div>
                                            <a href="#" onClick={(e) => this.toProductDetail(e,row.storeid,row.productid)}>
                                                <div className="product-add rounded-circle text-center">
                                                    <img className="w-50" src="/assets/images/icon-add.svg" alt="" />
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                        </div>
                    </div>
                )}
                {(this.state.couponData === undefined || this.state.couponData ==='' || this.state.couponData.length === 0) 
                    && (this.state.rewardData === undefined || this.state.rewardData ==='' || this.state.rewardData.length === 0) ? <></> :
                <div>
                <div className="text-head px-3 ">
                    <h4 className="m-0 d-inline">ดีลใหม่</h4> 
                    <div className="ml-auto d-inline float-right">
                        <a className="text-blue-more" href="/promotion_category">
                            <u> เพิ่มเติม </u>
                        </a>
                    </div>
                    <p>ดีลใหม่ที่ HO จัดมาให้คุณ</p>
                </div>
                <div className="coupon">
                    <div className="row w-100 m-0">
                {this.state.couponData === undefined || this.state.couponData ==='' || this.state.couponData.length === 0 ? (
                    <div/>
                ) : (
                this.state.couponData.map((row,key) => (
                    ((new Date(row.enddate).getTime() > nowDate) && (new Date(row.startdate).getTime() < nowDate) && row.couponstatus == "1") ?
                        <div className="col-10 p-0 pl-2">
                            <div className="position-relative">
                                <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                <div className="coupon-detail row ml-1 h-100">
                                    <div className="col-4">
                                        <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                        <div className="w-100 text-center mt-2">
                                            <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                <img className="" src={row.storelogo} alt=""/>
                                            </div>
                                            <p className="mt-2">{row.storename}</p>
                                        </div>
                                    </div>
                                    <div className="col-4 px-0 text-center mt-2 h-100">
                                        <p className="mt-2 mb-0">{row.couponname}</p>
                                        <h2>ลดราคา</h2>
                                        {row.storebranch!== null ? (
                                             <small className="d-block">สาขา : {row.storebranch}</small>
                                        ):(
                                            <></>
                                        )}
                                       
                                        <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                        {row.statususer === "1" && row.couponused === "1" ? ( 
                                            <button className="btn btn-orange p-0 mt-1" style={usedCouponButton} onClick={(e) => this.couponDetail(e,row)} data-primary={key}> <h5 className="m-0">ใช้แล้ว</h5></button>
                                        ) : row.statususer === "1" ? (
                                            <button className="btn btn-orange p-0 mt-1 btn-usecoupon" style={useCouponButton} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}/*onClick={(e) => this.useCoupon(e,row)}*/>
                                                <h5 className="m-0">ใช้คูปอง</h5>
                                            </button>
                                        ) : (
                                            <div key={key}>
                                                <button className="btn btn-orange p-0 mt-1 btn-getcoupon" onClick={(e) => this.getCoupon(e,row)} data-primary={key} data-couponid={row.couponid} data-storeid={row.storeid} data-coupontype={row.coupontype}> <h5 className="m-0">รับคูปอง</h5></button>
                                                {/* MODAL COUPON */}
                                                {/* <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                                    <div className="modal-dialog modal-dialog-centered" role="document">
                                                    <div className="modal-content w-75 mx-auto border-0">
                                                        <div className="modal-header bg-yellow">
                                                        <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                            <div className="position-relative">
                                                                <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                                <div className="coupon-detail row ml-1 h-100">
                                                                    <div className="col-4">
                                                                        <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                        <div className="w-100 text-center mt-2">
                                                                            <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                                <img className="" src={row.storelogo} alt=""/>
                                                                            </div>
                                                                            <p className="mt-2">{row.storename}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-4 px-0 text-center mt-2 h-100">
                                                                        <p className="mt-2 mb-0">{row.couponname}</p>
                                                                        <h2>ลดราคา</h2>
                                                                        {row.storebranch!== null ? (
                                                                            <small className="d-block">สาขา : {row.storebranch}</small>
                                                                        ):(
                                                                            <></>
                                                                        )}
                                                                        <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                        <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                                    </div>
                                                                    <div className="col-4 p-0 text-center my-auto">
                                                                        <div className="coupon-detail-side ">
                                                                            <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                            <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div></h5>
                                                        <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                            <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                        </button>
                                                        </div>
                                                        <div className="modal-body text-center">
                                                            <h4>คุณได้รับคูปองเรียบร้อย!</h4>
                                                            <button data-dismiss="modal" className="btn bg-yellow w-75">
                                                                <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div> */}
                                            </div>
                                        )}
                                        <div className="modal fade show" id={'couponModal-'+key} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
                                            <div className="modal-dialog modal-dialog-centered" role="document">
                                            <div className="modal-content w-75 mx-auto border-0">
                                                <div className="modal-header bg-yellow">
                                                <h5 className="modal-title" id="exampleModalLongTitle"><div id="cp-1" className="col-10 p-0 pl-2">
                                                    <div className="position-relative">
                                                        <img className="w-100" src="/assets/images/coupon-black.svg" alt=""/>
                                                        <div className="coupon-detail row ml-1 h-100">
                                                            <div className="col-4 mt-3">
                                                                <p className="mt-3 ml-2 mb-0">{row.catename}</p>
                                                                <div className="w-100 text-center mt-2">
                                                                    <div className="coupon-detail-img rounded-circle mx-auto shadow">
                                                                        <img className="" src={row.storelogo} alt=""/>
                                                                    </div>
                                                                    <p className="mt-2">{row.storename}</p>
                                                                </div>
                                                            </div>
                                                            <div className="col-4 px-0 text-center mt-3 h-100">
                                                                <p className="mt-3 mb-0">{row.couponname}</p>
                                                                <h2>ลดราคา</h2>
                                                                {row.storebranch!== null ? (
                                                                    <small className="d-block">สาขา : {row.storebranch}</small>
                                                                ):(
                                                                    <></>
                                                                )}
                                                                <small className=""><DateCountdown dateTo={new Date(row.enddate).toString()}  locales_plural={['ปี','เดือน','วัน',':',':','นาที']} locales={['ปี','เดือน','วัน',':',':','นาที']}  /></small>
                                                                <button className="btn btn-orange p-0 mt-2"  disabled="" style={{backgroundColor: 'rgb(16, 165, 4)'}}><h5 className="m-0">ใช้คูปอง</h5></button>
                                                            </div>
                                                            <div className="col-4 p-0 text-center my-auto">
                                                                <div className="coupon-detail-side ">
                                                                    <h1 className="m-0 mt-3">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                                                    <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></h5>
                                                <button type="button" className="close bg-grey rounded-circle px-2 p-0" data-dismiss="modal" data-key={key} aria-label="Close">
                                                    <img aria-hidden="true" className=" w-100" src="/assets/images/x-home.png" alt=""/>
                                                </button>
                                                </div>
                                                <div className="modal-body text-center">
                                                    <h4>คุณได้รับคูปองเรียบร้อย!11</h4>
                                                    <button data-dismiss="modal" className="btn bg-yellow w-75">
                                                        <a onClick={(e) => this.useCoupon(e,row)}>เริ่มช็อปกันเลย!</a>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-4 p-0 text-center my-auto">
                                        <div className="coupon-detail-side mt-2" style={{whiteSpace: "nowrap"}}>
                                        <h1 className="m-0">{row.coupondiscount}{row.discountstatus==='0' ? (' บาท') :(' %')}</h1>
                                            <h5>ขั้นต่ำ <NumberFormat value={row.minpurchase} displayType={'text'} thousandSeparator={true} /> บาท</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    : 
                    <></>
                )
                ))}
                            {this.state.rewardData === undefined || this.state.rewardData ==='' || this.state.rewardData.length === 0 ? (
                                    <div/>
                                ) : (
                                this.state.rewardData.map(row => (
                                    <div className="col-10 p-0 pl-2">
                                        <div className="position-relative">
                                            <img className="w-100" src="/assets/images/rewards-bg.svg" alt=""/>
                                            <div className="coupon-detail row ml-1 h-100">
                                                <div className="col-5">
                                                    <div className="w-100 text-center mt-2">
                                                        <div className="row w-100">
                                                            <div className="coupon-detail-img rounded-circle shadow" style={{padding: '20%', marginLeft: '0.5rem'}}>
                                                                <img className="" src={row.storelogo} alt=""/>
                                                            </div>
                                                            <p className="mt-3 ml-2 mb-0">{row.storename}</p>
                                                        </div>
                                                        <p className="mt-0 mb-0 fs-40"><h5 className="mb-1">{row.itempoint}<br/>คะแนน</h5></p>
                                                    {
                                                        // (parseFloat(row.itempoint) <= row.user_point) ? (
                                                        //     <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                        //         { row.redeemstatus === "1" ? 
                                                        //         <p className="m-0 font-tf">แลกแล้ว</p>
                                                        //         :
                                                        //         <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                        //         }
                                                        //     </button>
                                                        // ) : (
                                                        //     <button className="btn btn-orange p-10 mt-0"> <p className="m-0 font-tf">สะสมคะแนนเพิ่มอีกนิด</p></button>
                                                        // )
                                                        (parseFloat(row.itempoint) <= row.user_point) ? (
                                                            (row.redeemstatus==='1' || row.redeemstatus==='0') && row.redeemtime_status ==='1' ? (
                                                                <button className="btn btn-green p-10 mt-0" style={row.redeemstatus === "1" ? usedCouponButton : useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}>
                                                                { row.redeemtime_status === "1" ? 
                                                                <p className="m-0 font-tf">แลกแล้ว</p>
                                                                :
                                                                <p className="m-0 font-tf">แลกรางวัลนี้</p>
                                                                }
                                                                </button>
                                                            ):(
                                                                <button className="btn btn-green p-10 mt-0" style={useCouponButton} onClick={(e) => this.handleChangeReward(e,row.itemid,row.storeid)}> <p className="m-0 font-tf" style={{lineHeight:'20px'}}>แลกรางวัลนี้</p></button>
                                                            )
                                                        ) : (
                                                            <button className="btn coupon-btn btn-orange p-10" style = {{position:'relative', bottom: 10}}> <p className="m-0 font-tf">สะสมคะแนนเพิ่ม</p></button>
                                                        )
                                                    }
                                                    </div>
                                                </div>
                                                <div className="col-7 p-0 text-center mt-2">
                                                    <p className="m-0 mr-3 text-right pr-4 pt-2">{row.catename}</p>
                                                    <img className="coupon-pic" src={row.itemimg} width="43%" alt=""/>
                                                    <p className="m-0 pl-3 pr-3 text-center">{row.itemname}</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    ))
                                )
                            }
                    </div>
                </div>
                </div>
                }
                {this.state.storeData === undefined || this.state.storeData.length === 0 ? (
                    (!this.state.storeLoaded) &&(
                        storeLoader
                    )
                ) : (
                <div className="score shadow pl-3 py-2 mt-3 mb-20">
                    <div className="d-flex bd-highlight ">
                        <div className="px-0 py-0  bd-highlight">
                            <h4 className="m-0">ร้านค้าแนะนำ</h4>
                            <span className="text-grey">ร้านค้าที่ HO ให้คุณ</span>
                        </div>
                        <div className="ml-auto px-2 pr-3 bd-highlight ">
                            <a className="text-blue-more" href="/recommendstore">
                                <u> เพิ่มเติม </u>
                            </a>
                        </div>
                    </div>
                    <div className="row ml-0 pb-2  w-100">
                        {/* {this.state.storeData === undefined || this.state.storeData.length === 0 ? (
                            <></>
                        ) : ( */}
                        {this.state.storeData.map((row, key) => (
                            <div className="col-2 p-1 mr-10" key={key}>
                                <a href="#" onClick={(e) => {this.handleClick(e, row.storeid, row.storecoin)}}>
                                    <div className="score-item bg-white rounded-circle shadow">
                                        <img className="position-absolute w-100 my-auto store-logo-icon" src={row.store_img} alt=""/>
                                    </div>
                                </a>
                            </div>
                        ))}
                    </div>
                </div>
                )}
              
                <br/><br/><br/>
                
                    
                <Navigation active={'home'}/>

            </div>
        )
    }
}
export default Home;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;