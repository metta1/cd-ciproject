import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'


const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Home extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
        sessionStorage.removeItem('user_id');
        this.initialize = this.initialize.bind(this);


        this.state = {
            displayName : '',
            userId : '',
            pictureUrl : 'assets/images/profile.svg',
            statusMessage : '',
        };
       
    }

    state = {
        data : []
    };


    async componentDidMount(){
        window.addEventListener('load', this.initialize);
        
    }

    initialize() {
        liff.init(async (data) => {
          let profile = await liff.getProfile();
          this.setState({
            displayName : profile.displayName,
            userId : profile.userId,
            pictureUrl : profile.pictureUrl,
            statusMessage : profile.statusMessage
          });
            const result = await axios.get('https://backend-ho.dreamnolimit.com/api/userData/lineid/'+this.state.userId)
            
            if(result.status===200){
                let user_id = result.data.userid;
                sessionStorage.setItem('user_id',user_id)
                //alert(JSON.stringify(result))
                
            }else{

                let bodyFormData = new FormData();
                bodyFormData.set('name',this.state.displayName)
                bodyFormData.set('lastName','')
                bodyFormData.set('lineID',this.state.userId)
                axios({
                    method: 'post',
                    url: 'https://backend-ho.dreamnolimit.com/api/register',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        let user_id = response.data.userid;
                        sessionStorage.setItem('user_id',user_id)
                    })
                    .catch(function (response) {
                        //handle error
                        //console.log(response);
                });
            }
        }); 
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            <div className="container-mobile m-auto">
               
               <Header/>
                
               <div className="text-head px-3 mt-2">
                    <h4 className="m-0">404</h4>
                    <p>ไม่พบหน้าที่คุณร้องของ</p>
                </div>
                
                    
                <Footer/>

                
               
                 
            </div>
        )
    }
}
export default Home;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;