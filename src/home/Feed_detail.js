import React, { Component } from 'react'
import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header'
import Footer from './Footer'

const required = value => value ? undefined : 'Required'
const liff = window.liff;  
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Feed_list extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);
       
    }

    state = {
        data : [],
        feed_id:this.props.match.params.feed_id
    };


    async componentDidMount(){
        
        const result = await axios.get('https://backend-ho.dreamnolimit.com/store_feed/api/feed/'+this.state.feed_id).catch(function (error) {

           
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
            console.log(result.data)
        }
        
      
     
        
    }

 
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit = event => {
    
        
    }
    
    render() {

        return (
            
                
            <div className="container-mobile m-auto">
                
                <Header/>

                <div className="feed p-3">
            {
            
          
                this.state.data.map((row,key) => (
                    <div className="card w-100 shadow border-0 mb-3" key={key}>
                        <div className="feed-head px-3 py-2 d-flex w-100">
                            <div className="profile">
                                <div className="profile-ratio w-100 rounded-circle">
                                    <img src={row.storelogo==='' ? '/assets/images/profile.svg' : row.storelogo} className="w-100" />
                                </div>
                            </div>
                            <div className="pl-2 w-100">
                                <h4 className="m-0">{row.storename==='' ? 'HO' : row.storename}</h4>
                                <small><ins>{row.publicdate}</ins></small>
                            </div>
                        </div>
          
                        <p className="px-3 mb-1">{row.feedname}</p>
                        <img className="w-100" src={row.feed_img}  alt=""/>
                        <p className="px-3 mt-3">{row.feeddetail}</p>
                        <div className="feed-footer p-3  w-100">
                            <div className="d-flex">
                                <div className="icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-comment.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>

                                <p className="mt-1 ml-3 mb-0">77 Comment</p>
                                <div className="ml-auto icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-heart.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                                <div className="ml-2 icon-comment">
                                    <a href="">
                                        <div className="icon rounded-circle">
                                            <img className="w-100" src="/assets/images/icon-share.svg" alt=""/>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                ))
            }
                    
                </div>
                    

                <Footer/>
                
                
            </div>
        )
    }
}
export default Feed_list;
