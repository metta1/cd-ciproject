let currentURL = window.location.href;
let domain = (new URL(currentURL));
let hostName = domain.hostname
let fileConfig;
export let config;

if(hostName.indexOf("demo-ho.dreamnolimit.com") >= 0){
    fileConfig = require('./testing_config');
    console.log('env:testing')

}else if(hostName.indexOf("localhost") >= 0){
    fileConfig = require('./production_config');//เชื่อม production ก่อน
    console.log('env:prod')

}else{
    fileConfig =  require('./production_config');
    console.log('env:prod')

}
config = fileConfig.config