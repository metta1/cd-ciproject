export const config = {
        base_url :'https://demo-ho.dreamnolimit.com/',
        liff_auth_url : 'line://app/1655674672-gMKA8Ddl',
        api:{
            base_url : 'https://admin.hoplatform.co'
        },
        liff:{
            auth : {
                id: '1655674672-gMKA8Ddl'
            },
            chat : {
                id: '1655674672-n6yJLD5N'
            }
        },
        recommend : {
            url : 'line://app/1655674672-gMKA8Ddl',
            link : 'https://liff.line.me/1655674672-gMKA8Ddl'
        },
        fbLogin : {
            appId : '575332273905229'
        },
        googleLogin : {
            clientId : '219608642552-8b7kq6afim5jh1a7jugg8fsfspoaot8m.apps.googleusercontent.com'
        }
}
