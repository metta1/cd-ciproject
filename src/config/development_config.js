export const config = {
        base_url :'https://react-ho.dreamnolimit.com',
        liff_auth_url : 'line://app/1654185466-nKkG6Xok',
        api:{
            base_url : 'http://dev.backend.ho.com'
        },
        liff:{
            auth : {
                id: '1654185466-nKkG6Xok'
            },
            chat : {
                id: '1654185466-BjYpVw9Y'
            }
        },
        recommend : {
            url : 'line://app/1654185466-nKkG6Xok',
            link : 'https://liff.line.me/1654185466-nKkG6Xok'
        },
        fbLogin : {
            appId : '575332273905229'
        },
        googleLogin : {
            clientId : '219608642552-8b7kq6afim5jh1a7jugg8fsfspoaot8m.apps.googleusercontent.com'
        }
}
