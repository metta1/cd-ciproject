import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Link} from 'react-router-dom'
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import $, { get } from 'jquery'
import Swal from 'sweetalert2'
import {getParam,inArray, readURL, getSizeFromImgDataURL} from '../lib/Helper';
import Modal from 'react-bootstrap4-modal';

class Maps extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleBranchMarkerClick = this.handleBranchMarkerClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleFilterStore = this.handleFilterStore.bind(this);
    }

    state = {
        data : [],
        branchData: [],
        apiKey: 'AIzaSyAEvK0X5Lq9hCcVJjf9pLOKFL8ZqGHbe3s',
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {},
        selectedBranchPlace: {},
        centerPoint: {lat:13.736717, lng:100.523186},// BKK
        listItemActive : null,
        mapZoom : 11,
        mode: getParam('mode')
        
    };

    

    async componentDidMount(){
        this.goToShopUrl();


        const result = await axios.get(config.api.base_url+'/store/api/allstore').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
             console.log(result.data)
            this.setState({data: result.data});
        }

        const branchresult = await axios.get(config.api.base_url+'/store/api/storebranch').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(branchresult.status===200){
             console.log(branchresult.data)
            this.setState({branchData: branchresult.data});
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    handleMarkerClick = (props, marker, e) => {
        console.log(marker)
        this.setState({
            selectedPlace: this.state.data[props.placeIndex],
            selectedBranchPlace: {},
            activeMarker: marker,
            showingInfoWindow: true
        });

        let user_id = sessionStorage.getItem("user_id");
        let bodyFormData = new FormData();
        bodyFormData.set("user_id", user_id);
        bodyFormData.set("store_id", this.state.data[props.placeIndex].storeid);
        axios({
            method: "post",
            url: config.api.base_url + "/users/api/viewmap", //config.api.base_url+
            data: bodyFormData,
            headers: { "Content-Type": "multipart/form-data" },
        })
        .then(function (response) {

        })
        // console.log(Object.keys(this.state.selectedPlace).length)
        // console.log(Object.keys(this.state.selectedBranchPlace).length)
    };

    handleBranchMarkerClick = (props, marker, e) => {
        // e.preventDefault();
        console.log(props)
        this.setState({
            selectedPlace: {},
            selectedBranchPlace: this.state.branchData[props.placeIndex],
            activeMarker: marker,
            showingInfoWindow: true
        });

        let user_id = sessionStorage.getItem("user_id");
        let bodyFormData = new FormData();
        bodyFormData.set("user_id", user_id);
        bodyFormData.set("store_id", this.state.branchData[props.placeIndex].storeid);
        bodyFormData.set("branch_id", this.state.branchData[props.placeIndex].branch_id);
        axios({
            method: "post",
            url: config.api.base_url + "/users/api/viewmap", //config.api.base_url+
            data: bodyFormData,
            headers: { "Content-Type": "multipart/form-data" },
        })
        .then(function (response) {

        })
        // console.log(Object.keys(this.state.selectedPlace).length)
        // console.log(Object.keys(this.state.selectedBranchPlace).length)
    };
    
      handleClose = () => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null,
            });
        }
    };

    handleClickList (e,storeid,lat,lng){
        e.preventDefault();
        this.setState({
            listItemActive : storeid
        })
        if(lat && lng){
           
            this.setState({
                centerPoint : {
                    lat : parseFloat(lat),
                    lng : parseFloat(lng)
                },
                mapZoom : 12,
            })
            
        }
    }

    
    async goToShopUrl(){
        if(getParam('gotoshop') !== undefined){
            let storeid = getParam('gotoshop')
            const checkUserMember = await axios.get(config.api.base_url+'/store_card/api/checkcardmember/'+storeid+'/'+sessionStorage.getItem('user_id'))
            if(checkUserMember.status===200){
                //console.log(checkUserMember)
                if (checkUserMember.data === 1){
                    window.location = '/shop/'+storeid;
                } else {
                    Swal.fire({
                        title: 'ขออภัย',
                        type:'info',
                        html : 'คุณยังไม่ได้เป็นสมาชิกของร้าน<br>ไปเพิ่มบัตรสมาชิก ?',
                        showCloseButton: true,
                        showCancelButton: true,
                        reverseButtons :true,
                        confirmButtonText:
                        'ตกลง',
                        focusConfirm: false,
                        cancelButtonText:
                        'ยกเลิก',
                    }).then((result) => {
                        if (result.value) {
                            window.location = '/app_card';
                        }
                    })
                }
            }
        }
    }

    goToShop (e,storeid,storename) {
       
     
       /*  const checkUserMember = await axios.get(config.api.base_url+'/store_card/api/checkcardmember/'+storeid+'/'+sessionStorage.getItem('user_id'))
        if(checkUserMember.status===200){
            console.log(checkUserMember)
            if (checkUserMember.data === 1){
                window.location = '/shop/'+storeid;
            } else {
                Swal.fire({
                    title: 'ขออภัย',
                    type:'info',
                    html : 'คุณยังไม่ได้เป็นสมาชิกของร้าน '+storename+'<br>ไปเพิ่มบัตรสมาชิก ?',
                    showCloseButton: true,
                    showCancelButton: true,
                    reverseButtons :true,
                    confirmButtonText:
                      'ตกลง',
                    focusConfirm: false,
                    cancelButtonText:
                      'ยกเลิก',
                  }).then((result) => {
                    if (result.value) {
                        window.location = '/app_card';
                    }
                })
            }
        } */


    }

    handleBackPage = event => {
        if(getParam('mode') == 'account'){
            window.location='/account';
        }else{
            window.location='/home';
        }
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-black').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    async handleFilterStore(e){
        let keyWord = e.target.value;
        const storesearch = await axios.get(config.api.base_url+'/store/api/allstore?keywords='+keyWord).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(storesearch.status===200){
            this.setState({data: storesearch.data});
        } else {
            this.setState({data: []});
        }

        const branchsearch = await axios.get(config.api.base_url+'/store/api/storebranch?keywords='+keyWord).catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(branchsearch.status===200){
            this.setState({branchData: branchsearch.data});
        } else {
            this.setState({branchData: []});
        }
    }



    render() {

        return (
            <>
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/map.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-4 pl-4">
                    {
                        this.state.activeSearchBox ? (
                            <div className=" w-100 h-100">
                                <div className="row col-12">
                                    <div className="icon-back" style={{left: '1rem'}}>
                                        <a href="/maps">
                                            <div className="">
                                                <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                            </div>
                                        </a>
                                    </div>
                                    <div className=" h-100 pl-3 w-85">
                                        <div className="input-group search-group">
                                            <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterStore}/>
                                            <div className="input-group-append">
                                                <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : (
                        <div className="row w-100 h-100">
                            <div className="col-10 head-text">
                                <Link onClick={this.handleBackPage}>
                                    <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </Link>
                                <h3 className="m-0 pl-2">ตำแหน่งร้านค้า</h3>
                            </div>
                            <div className="col-2 h-100">
                                <div className="row float-right h-100 w-100">
                                    <div className="col  my-auto">
                                        <div className="icon-search-map icon-top-right rounded-circle" style={{width:'35px'}}>
                                            <a href="#"><img className="w-100" src="/assets/images/icon-search@2x.png" alt="" onClick={this.openSearchBox}/></a>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        )
                    }
                    </div>
                    {/* <div className="shadow header p-4 pl-4">
                        <div className="row">
                            <div className="col-12 head-text">
                                <Link onClick={this.handleBackPage}>
                                    <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </Link>
                                <h3 className="m-0 pl-2">ตำแหน่งร้านค้า</h3> 
                            </div>
                        </div>
                    </div> */}
                    <div className="map mt-1" >
                        <Map
                            google={this.props.google}
                            className={"map-google"}
                            style={{ height: '80vh', width: '100%' }}
                            zoom={this.state.mapZoom}
                            initialCenter={this.state.centerPoint}
                            center={this.state.centerPoint} 
                        >
                            {this.state.data.map((location, i) => {
                                return (
                                    <Marker
                                        key={i}
                                        name={location.storename}
                                        title={location.storename}
                                        position={{ lat: location.latitude, lng: location.longitude }}
                                        icon={{url: '/assets/images/Ho-Icon-pin.png', scaledSize:  new this.props.google.maps.Size(40,40)}}
                                        onClick={this.handleMarkerClick}
                                        placeIndex={i}
                                    />
                                );
                            })}
                            {this.state.branchData.map((location, i) => {
                                return (
                                    <Marker
                                        key={i}
                                        name={location.branch_name}
                                        title={location.branch_name}
                                        position={{ lat: location.latitude, lng: location.longitude }}
                                        icon={{url: '/assets/images/Ho-Icon-pin.png', scaledSize:  new this.props.google.maps.Size(40,40)}}
                                        onClick={this.handleBranchMarkerClick}
                                        placeIndex={i}
                                    />
                                );
                            })}
                            <InfoWindow
                                marker={this.state.activeMarker}
                                visible={this.state.showingInfoWindow}
                                onClose={this.handleClose}
                            >
                                <div className="row">
                                    <div className="col-12">
                                        {Object.keys(this.state.selectedPlace).length === 0 ? <></> :
                                            <h4 className="text-center">{this.state.selectedPlace.storename}</h4>
                                        }
                                        {Object.keys(this.state.selectedBranchPlace).length === 0 ? <></> :
                                            <h4 className="text-center">{this.state.selectedBranchPlace.storename} {this.state.selectedBranchPlace.branch_name}</h4>
                                        }
                                    </div>
                                    <div className="col-12">
                                        {Object.keys(this.state.selectedPlace).length === 0 ? <></> :
                                            <p className="text-center">
                                            
                                            {/* <a href="#" onClick={(e) => this.goToShop(e,this.state.selectedPlace.storeid,this.state.selectedPlace.storename)} className=" map-link pr-1">ซื้อสินค้า </a> */}
                                            
                                                <a href={'/maps?gotoshop='+this.state.selectedPlace.storeid} className=" map-link pr-1">ซื้อสินค้า </a>
                                                
                                                <a target="_BLANK" href={'https://www.google.com/maps/search/?api=1&query='+this.state.selectedPlace.latitude+','+this.state.selectedPlace.longitude+'&openExternalBrowser=1'} className="map-link pl-1"> เส้นทาง</a>
                                                
                                            </p>
                                        }
                                        {Object.keys(this.state.selectedBranchPlace).length === 0 ? <></> :
                                            <p className="text-center">
                                            
                                            {/* <a href="#" onClick={(e) => this.goToShop(e,this.state.selectedPlace.storeid,this.state.selectedPlace.storename)} className=" map-link pr-1">ซื้อสินค้า </a> */}
                                            
                                                <a href={'/maps?gotoshop='+this.state.selectedBranchPlace.storeid} className=" map-link pr-1">ซื้อสินค้า </a>
                                                
                                                <a target="_BLANK" href={'https://www.google.com/maps/search/?api=1&query='+this.state.selectedBranchPlace.latitude+','+this.state.selectedBranchPlace.longitude+'&openExternalBrowser=1'} className="map-link pl-1"> เส้นทาง</a>
                                                
                                            </p>
                                        }
                                    </div>
                                </div>
                            </InfoWindow>
                        </Map>

                    </div>
                    <div className="shop-detail mb-3 mt-2">
                        {
                            this.state.data=== undefined && this.state.branchData=== undefined ? (
                                <div/>
                            ) : (
                                <ul className="list-group shop-list">
                                    <li className="list-group-item shop-list-title"><h4 className="mb-0">HO Shop</h4></li>
                                {
                                this.state.data.map((row, key) => {
                                    return (
                                    <li className={`list-group-item shop-list-item  ${this.state.listItemActive === row.storeid ? 'active': ''}`}  key={key}  onClick={(e) => this.handleClickList(e,row.storeid,row.latitude,row.longitude)}>
                                        <div className="row">
                                            <div className="col-2 pr-0 flex-center">
                                                <img src={row.store_img ==='' ? '/assets/images/Ho-Icon-pin.png' : row.store_img} className="img-fluid"/>
                                            </div>
                                            <div className="col-10 pl-1 flex-center">
                                                {row.storename}
                                            </div>
                                        </div>
                                    </li>
                                    )
                                })
                                }
                                {
                                this.state.branchData.map((row, key) => {
                                    return (
                                    <li className={`list-group-item shop-list-item  ${this.state.listItemActive === row.storeid ? 'active': ''}`}  key={key}  onClick={(e) => this.handleClickList(e,row.storeid,row.latitude,row.longitude)}>
                                        <div className="row">
                                            <div className="col-2 pr-0 flex-center">
                                                <img src={row.store_img ==='' ? '/assets/images/Ho-Icon-pin.png' : row.store_img} className="img-fluid"/>
                                            </div>
                                            <div className="col-10 pl-1 flex-center">
                                                {row.storename} {row.branch_name}
                                            </div>
                                        </div>
                                    </li>
                                    )
                                })
                                }
                                </ul>
                            )
                        }
                       
                    </div>
                </div>
            </div>
            </>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAEvK0X5Lq9hCcVJjf9pLOKFL8ZqGHbe3s',
    language: 'th',
})(Maps)
