import React, { Component } from 'react'
import axios from 'axios'
import {Navigation,Notification_num} from '../template'

import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from '../config';
import {Route, Link, useParams} from 'react-router-dom'
import $ from 'jquery'
import Swal from 'sweetalert2'

const required = value => value ? undefined : 'Required'
const liff = window.liff;
const currentURL = window.location.href;
const currentURLArray = currentURL.split("/");
const currentComponent = currentURLArray[currentURLArray.length - 1].toLowerCase();


class Recommend_store extends Component {
    constructor(props){//เริ่มต้น run component
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.addCard = this.addCard.bind(this);
        this.handleFilterStore = this.handleFilterStore.bind(this);
    }

    state = {
        data : [],
        activeSearchBox:false
    };

    async componentDidMount(){
        const result = await axios.get(config.api.base_url+'/store/api/allstore').catch(function (error) {
            if (error.response) {
              console.log(error.response.status);
            }
        });
        if(result.status===200){
            this.setState({data: result.data});
            console.log(result.data)
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = event => {

    }

    async handleClick(e,storeid,coin) {
        e.preventDefault();
        const checkUserMember = await axios.get(config.api.base_url+'/store_card/api/checkcardmember/'+storeid+'/'+sessionStorage.getItem('user_id'))
        if(checkUserMember.status===200){
            if (checkUserMember.data === 1){
                window.location = '/shop/'+storeid;
            } else {
                const checkPacketMember = await axios.get(config.api.base_url+'/store/api/checkstorepacket/'+storeid);
                if(checkPacketMember.status===200){
                    let limituserCount = checkPacketMember.data.limituserCount;
                    let storeMember = checkPacketMember.data.storeMember;
                    if (parseInt(limituserCount) !== 0 && parseInt(storeMember) >= parseInt(limituserCount)) {
                        Swal.fire({
                            icon: 'error',
                            title: 'ขออภัย',
                            html: 'ไม่สามารถเพิ่มบัตรสมาชิกได้',
                            showCloseButton: true,
                            confirmButtonText: 'ตกลง'
                        })
                    } else {
                        this.addCard(storeid,coin);
                    }
                }
            }
        }
    }

    addCard(storeid,coin) {
        Swal.fire({
            title: 'ต้องการเพิ่มบัตรใช่หรือไม่',
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons :true,
            confirmButtonText:
              'ตกลง',
            focusConfirm: false,
            cancelButtonText:
              'ยกเลิก',
          }).then((result) => {
            if (result.value) {
                let user_id = sessionStorage.getItem('user_id');
                let store_id = storeid;
                let user = {
                    userid : user_id,
                    storeid : store_id
                }
                let bodyFormData = new FormData();
                bodyFormData.set('userid',user_id)
                bodyFormData.set('storeid',store_id)
                if (coin != undefined) {
                    bodyFormData.set('coin',coin)
                }
                axios({
                    method: 'post',
                    url: config.api.base_url+'/api/card',
                    data: bodyFormData,
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (response) {
                        $('.member-card[data-store_id="'+store_id+'"').hide();
                        window.location = '/shop/'+store_id;
                    })
                    .catch(function (response) {
                        Swal.fire({
                            title : 'ขออภัย',
                            html: 'ไม่สามารถบันทึกข้อมูลได้<BR/>กรุณาตรวจสอบใหม่อีกครั้ง',
                            icon:'error',
                            confirmButtonText:'ตกลง'
                        })
                        //handle error
                        //console.log(response);
                    });
            }
        })
    }

    async handleFilterStore(e){
        let keyWord = e.target.value;
        const result = await axios.get(config.api.base_url+'/store/api/allstore?keywords='+keyWord)
        if(result.status===200){
            this.setState({data: result.data});
        } else {
            this.setState({data: []});
        }
    }

    handleBackPage = e => {
        window.location.reload();
        
    }

    openSearchBox = e => {
        $('.btn-add').css({"padding": '0'})
        $('.head.bg-yellow').css({"height": 'unset'})
        $('.btn-add img').css({"padding": '8px',"display": 'block'})
      
        this.setState({
            activeSearchBox:true
        })
    }

    render() {
        return (
            <div>
                <link rel="stylesheet" type="text/css" href="/assets/css/point.css?v=3"/>
                <div className="container-mobile m-auto">
                    <div className="shadow header p-4 pl-4">
                    {
                        this.state.activeSearchBox ? (
                            <div className="row w-100">
                                    
                                <div className="pl-5 w-100">
                                    <div className="icon-back">
                                        <a onClick={this.handleBackPage}>
                                            <img  src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle shadow ml-3 mt-1"/>
                                        </a>
                                    </div>
                                    <div className=" h-100 pl-3">
                                    <div className="input-group search-group mb-3">
                                        <input type="text" className="form-control" id="searchInput" onKeyUp={this.handleFilterStore}/>
                                        <div className="input-group-append">
                                            <span className="input-group-text" id="basic-addon2"><i className="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                        
                                    </div>
                                        
                                </div>
                            </div>
                        ) : (
                        <div className="row">
                            <div className="col-8 head-text">
                                <Link to="/home">
                                    <img style={{marginTop:'0.1rem'}} src="/assets/images/icon-back.png" alt="" className="bg-white rounded-circle"/>
                                </Link>
                                <h3 className="m-0 pl-2" style={{fontWeight:600}}>ร้านค้าแนะนำ</h3> 
                            </div>
                            <div className="col-4">
                                <div className="float-right">
                                    <div className="col p-1">
                                        <div className="icon-head bg-white rounded-circle text-center d-inline-block mr-1">
                                            <a href="#">
                                                <img className="h-50 w-100" src="/assets/images/icon-search.svg" alt="..." onClick={this.openSearchBox}/>
                                            </a>
                                        </div>
                                        <div className="icon-head bg-white rounded-circle text-center d-inline-block">
                                            <a href="/notifications">
                                                <img className="h-50 w-100" src="/assets/images/icon-noti.svg" alt="..." />
                                                <Notification_num/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        )
                    }
                    </div>
                    <div className="promotion p-2 mt-3">
                        <div className="row m-0">
                            {
                                this.state.data === undefined ? (
                                    <div/>
                                ) : (
                                this.state.data.map((row,key) => (
                                    <div className="col-3 p-2" key={key}>
                                        <a href="#" onClick={(e) => {this.handleClick(e, row.storeid,row.storecoin)}}>
                                            <div className="promotion-box ">
                                                <div className="promotion-item shadow">
                                                        <img className="mt1" src={row.store_img} alt=""/> <br/>
                                                </div>
                                            </div>
                                        </a>
                                        <h4 className="storename">{row.storename}</h4>
                                    </div>
                                )))
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Recommend_store;
