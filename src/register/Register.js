import React, { Component } from 'react'
import axios from 'axios'
import withRequest from '../lib/withRequest';
import { Form, Well, Button, FormGroup, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Field, reduxForm } from 'redux-form'
import $ from 'jquery'
import SweetAlert from "react-bootstrap-sweetalert";

const required = value => value ? undefined : 'Required'
const liff = window.liff;  


class Register extends Component {
    constructor(){
        super();
        this.initialize = this.initialize.bind(this);
        

    }

    componentDidMount() { //เมื่อรันเสร็จแล้ว
        window.addEventListener('load', this.initialize);
    }

    initialize() {
        liff.init(async (data) => {
          let profile = await liff.getProfile();
          this.setState({
            displayName : profile.displayName,
            line_id : profile.userId,
            pictureUrl : profile.pictureUrl,
            statusMessage : profile.statusMessage
          });
            const result = await axios.get('https://backend-ho.dreamnolimit.com/api/userData/lineid/'+this.state.line_id)
            //alert(JSON.stringify(result))
            if(result.status===200){
                let user_id = result.data.userid;
                sessionStorage.setItem('user_id',user_id)
                const referer = sessionStorage.getItem('referer');
                if(referer){
                        sessionStorage.removeItem('referer')
                        window.location='/'+referer;
                }else{
                        window.location='/user_card';
                }
            }else{
                $('#titleRegister').html('ลงทะเบียนผู้ใช้งาน')
                $('#formRegister').show();
            }
         
         

        }); 
    }

    state = {
        name: '',
        password:'',
        password2:'',
        tel:''
    }
    
    handleChange = event => {
        console.log(event.target.name)
        //this.setState({ name: event.target.value });
        this.setState({[event.target.name]: event.target.value});
        //alert(this.state.line_id)
    }
    
    handleSubmit = event => {
        
        //console.log($('input[name="name"').val()+'fff')
        $('#submit').prop('disabled',true);
        
        event.preventDefault();
       
        if(this.state.password !== this.state.password2){
            alert('ขออภัยค่ะ กรุณากรอกรหัสผ่านให้เหมือนกัน')
            $('#submit').prop('disabled',false);
        }else{
            const user = {
                name: this.state.name,
                lastName: this.state.lastName,
                username: this.state.email,
                password: this.state.password,
                tel:'',
                line_id:this.state.line_id
              };

              let bodyFormData = new FormData();
              bodyFormData.set('name',this.state.name)
              bodyFormData.set('lastName',this.state.lastName)
              bodyFormData.set('username',this.state.email)
              bodyFormData.set('password',this.state.password)
              bodyFormData.set('tel','')
              bodyFormData.set('lineID',this.state.line_id)
      
              //console.log(this.state.password+' '+this.state.password2)
              axios({
                method: 'post',
                url: 'https://backend-ho.dreamnolimit.com/api/register',
                data: bodyFormData,
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (response) {
                    //handle success
                    //console.log(response);
                    let user_id = response.data.userid;
                    sessionStorage.setItem('user_id',user_id)
                    const referer = sessionStorage.getItem('referer');
                    if(referer){
                        sessionStorage.removeItem('referer')
                        window.location='/'+referer;
                    }else{
                        window.location='/user_card';
                    }
                    $('#submit').prop('disabled',false);
                    //console.log(response.data)
                })
                .catch(function (response) {
                    //handle error
                    //console.log(response);
                });
          
            
        }
        
    }
    

    render() {

        return (
            <div>
                <script src="https://unpkg.com/react/umd/react.production.min.js" ></script>
                <script
                src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
                ></script>

                <script
                src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
                ></script>
                <div class="row">
                    <div class="container text-left">
                        <div class="col-12">
               
                        <h4 id="titleRegister">กรุณารอสักครู่</h4>
                        <Form onSubmit={this.handleSubmit} id="formRegister" style={{display:'none'}}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>ชื่อจริง</Form.Label>
                                <Form.Control type="text" placeholder="ชื่อจริง" name="name" onChange={this.handleChange} required/>
                               
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>นามสกุลจริง</Form.Label>
                                <Form.Control type="text" placeholder="นามสกุลจริง" name="lastName" required onChange={this.handleChange} />
                                
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>อีเมลของคุณ</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" required name="email" onChange={this.handleChange}/>
                                
                            </Form.Group>

                            {/* <Form.Group controlId="formBasicPassword">
                                <Form.Label>รหัสผ่าน</Form.Label>
                                <Form.Control type="password" placeholder="Password" required name="password" onChange={this.handleChange} />
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>ยืนยันรหัสผ่าน</Form.Label>
                                <Form.Control type="password" placeholder="Password" required name="password2" onChange={this.handleChange} />
                            </Form.Group> */}
                           
                            <Button variant="primary" type="submit" id="submit">
                                ยืนยันการลงทะเบียน
                            </Button>
                        </Form>
                        </div>
                    </div>
                </div>
                 
            </div>
        )
    }
}
export default Register;
/* export default withRequest('https://jsonplaceholder.typicode.com/users')(User); higher*/

//export default User;